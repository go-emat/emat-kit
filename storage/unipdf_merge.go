package storage

import (
	"bytes"
	"errors"
	"fmt"
	"image"
	"io"
	"log"
	"os"
	"strings"
	"time"

	"github.com/unidoc/unipdf/v3/common/license"
	"github.com/unidoc/unipdf/v3/core"
	"github.com/unidoc/unipdf/v3/creator"
	"github.com/unidoc/unipdf/v3/model"
	"gitlab.com/go-emat/emat-kit/storage/pdf_merge"
	"gitlab.com/go-emat/emat-kit/storage/unipdf_handler"
	imgtiff "golang.org/x/image/tiff"
)

type UnipdfMergeService struct {
	StorageFactory *DocumentStorageFactory
}

const UnipdfCustomerName = "Mattex Asia Development Limited"

func NewUnipdfMergeService(storageFactory *DocumentStorageFactory) *UnipdfMergeService {
	if license.GetLicenseKey() == nil || (license.GetLicenseKey() != nil && !license.GetLicenseKey().IsLicensed()) {
		err := license.SetLicenseKey(os.Getenv("UNIPDF_LICENSE"), UnipdfCustomerName)
		log.Println("license.GetLicenseKey()", license.GetLicenseKey())
		if err != nil {
			log.Println("UniPDF license error: ", err)
		}
	}
	return &UnipdfMergeService{
		StorageFactory: storageFactory,
	}
}

func (s *UnipdfMergeService) MergeFiles(req *PdfMergeRequest) (resp *PdfMergeResponse, errs *pdf_merge.Errors) {
	startTime := time.Now()
	resp = &PdfMergeResponse{}
	errs = pdf_merge.NewErrors()
	pdfStateBefore, _ := license.GetMeteredState()
	log.Println("UniPdf Credit (Beginning): ", pdfStateBefore.Used, "/", pdfStateBefore.Credits)
	pdfCreator := creator.New()

	defer func() {
		resp.Duration = time.Since(startTime).Seconds()
		if len(errs.Errs) == 0 && len(errs.FatalErrs) == 0 {
			errs = nil
		}
	}()
	if len(req.InputFiles) == 0 {
		errs.AddFatal(errors.New("pdfMergeService.MergeFiles: should provide at least one input file"))
		return
	}
	files, totalUnsupportedFiles, es := s.getFilesAndUnsupportedFiles(req.InputFiles)
	log.Println("totalUnsupportedFiles", totalUnsupportedFiles)
	if es != nil {
		errs.Append(es)
	}
	for _, f := range files {
		switch f.FileType {
		case PdfMergeFileTypeUnsupported, PdfMergeFileTypeNotExist:
			resp.UnsupportedFileNames = append(resp.UnsupportedFileNames, f.FileName)
		default:
			resp.SupportedFileNames = append(resp.SupportedFileNames, f.FileName)
		}
	}
	log.Println("validate end", len(files))
	outputSource, err := s.StorageFactory.GetWriteCloser(req.OutputFilePath, "application/pdf")
	if err != nil {
		errs.AddFatal(err)
		return
	}
	defer func() {
		if err := outputSource.Close(); err != nil {
			errs.AddFatal(err)
		}
	}()

	var watermarkTexts []string
	createAndMergePageWithWatermarks := func(pdfCreator *creator.Creator) {
		if req.DocumentNo != "" {
			watermarkTexts = append(watermarkTexts, fmt.Sprintf("Please find unsupported file(s) in document %v on the website", req.DocumentNo))
		} else {
			watermarkTexts = append(watermarkTexts, "Please find unsupported file(s) in attachments")
		}

		pdfCreator, err = s.createBlankPdfWithWatermarkText(strings.Join(watermarkTexts, "\n"), pdfCreator)
		if err != nil {
			errs.AddFatal(err)
		}

		watermarkTexts = []string{}
	}
	createAppendixLeadPage := func(file *PdfMergeFile, pdfCreator *creator.Creator) {
		pdfCreator, err = s.createAppendixLeadPage(file, pdfCreator)
		if err != nil {
			errs.AddFatal(err)
		}
	}

	for _, f := range files {
		if f.FileType == PdfMergeFileTypeUnsupported {
			watermarkTexts = append(watermarkTexts, fmt.Sprintf("Cannot merge file %s", f.FileName))
		} else if f.FileType == PdfMergeFileTypeNotExist {
			watermarkTexts = append(watermarkTexts, fmt.Sprintf("Cannot merge nonexistent file %s", f.FileName))
		} else if len(watermarkTexts) > 0 {
			createAndMergePageWithWatermarks(pdfCreator)
		}

		fileInputSource, err := s.StorageFactory.GetFileReadSeekCloser(f.FilePath)
		if err != nil {
			errs.AddFatal(err)
		}

		if f.FileType == PdfMergeFileTypePDF {
			if f.Appendix != "" {
				createAppendixLeadPage(f, pdfCreator)
			}
			pdfCreator, errs = s.mergePdfFiles(fileInputSource, errs, pdfCreator)
		}

		if f.FileType == PdfMergeFileTypeImage {
			if f.Appendix != "" {
				createAppendixLeadPage(f, pdfCreator)
			}

			pdfCreator, errs = s.mergeImageFiles(f, errs, pdfCreator)
		}
	}

	if len(watermarkTexts) > 0 {
		createAndMergePageWithWatermarks(pdfCreator)
	}

	err = pdfCreator.Write(outputSource)
	if err != nil {
		errs.AddFatal(err)
	}

	return
}

func (s *UnipdfMergeService) mergeImageFiles(f *PdfMergeFile, errs *pdf_merge.Errors, pdfCreator *creator.Creator) (*creator.Creator, *pdf_merge.Errors) {
	encoder := core.NewDCTEncoder()
	fileBytes, err := s.StorageFactory.DownloadStorageFile(f.FilePath)
	if err != nil {
		errs.AddFatal(err)
		return nil, errs
	}
	var img *creator.Image = nil
	if unipdf_handler.IsTiffImage(f.FileExtension) {
		var imgDecode image.Image
		imgDecode, err = imgtiff.Decode(bytes.NewReader(fileBytes))
		if err != nil {
			errs.AddFatal(err)
			return pdfCreator, errs
		}
		img, err = pdfCreator.NewImageFromGoImage(imgDecode)
		if err != nil {
			errs.AddFatal(err)
			return pdfCreator, errs
		}
	} else {
		img, err = pdfCreator.NewImageFromData(fileBytes)
		if err != nil {
			errs.AddFatal(err)
			return pdfCreator, errs
		}
	}

	img.ScaleToWidth(612.0)
	img.SetPos(0, 0)
	img.SetEncoder(encoder)

	if pdfCreator != nil {
		pdfCreator.SetPageSize(creator.PageSizeA4)
		pdfCreator.NewPage()
		err = pdfCreator.Draw(img)

		if err != nil {
			errs.AddFatal(err)
			return pdfCreator, errs
		}
	}
	return pdfCreator, errs
}

func (s *UnipdfMergeService) mergePdfFiles(fileInputSource io.ReadSeekCloser, errs *pdf_merge.Errors, pdfCreator *creator.Creator) (*creator.Creator, *pdf_merge.Errors) {
	pdfReader, err := model.NewPdfReaderWithOpts(fileInputSource, &model.ReaderOpts{
		LazyLoad:       false,
		ComplianceMode: false,
	})
	if err != nil {
		errs.AddFatal(err)
		return pdfCreator, errs
	}
	numPages, err := pdfReader.GetNumPages()
	if err != nil {
		errs.AddFatal(err)
		return pdfCreator, errs
	}
	for i := 0; i < numPages; i++ {
		pageNum := i + 1

		page, err := pdfReader.GetPage(pageNum)
		if err != nil {
			errs.AddFatal(err)
		}

		err = pdfCreator.AddPage(page)
		if err != nil {
			errs.AddFatal(err)
		}
	}

	return pdfCreator, errs
}

func (s *UnipdfMergeService) GetPdfPageCount(filepath string) (pageCount int, err error) {
	fileInputSource, err := s.StorageFactory.GetFileReadSeekCloser(filepath)
	if err != nil {
		return 0, err
	}
	defer func() {
		if err := fileInputSource.Close(); err != nil {
			return
		}
	}()
	pdfReader, err := model.NewPdfReaderWithOpts(fileInputSource, &model.ReaderOpts{
		LazyLoad:       false,
		ComplianceMode: false,
	})
	if err != nil {
		return 0, err
	}
	numPages, err := pdfReader.GetNumPages()
	if err != nil {
		return 0, err
	}
	return numPages, nil
}

func (s *UnipdfMergeService) ValidateMergeFileFormat(inputFile *PdfMergeFileInfo, outputDir string) (f *PdfMergeFile, err error) {
	f = &PdfMergeFile{
		PdfMergeFileInfo: inputFile,
	}

	fileInputSource, err := s.StorageFactory.GetFileReadSeekCloser(inputFile.FilePath)
	if err != nil {
		f.FileType = PdfMergeFileTypeNotExist
		err = pdf_merge.NewFileOpenError(inputFile.FileName, err)
		return f, err
	}
	defer func() {
		if err := fileInputSource.Close(); err != nil {
			return
		}
	}()
	f.InputSource = fileInputSource
	f.FileType = getTypeByExt(strings.ToLower(inputFile.FileExtension))

	_, err = s.StorageFactory.DownloadStorageFile(inputFile.FilePath)
	if err != nil {
		return f, err
	}
	return f, nil
}

func (s *UnipdfMergeService) AddWatermarksToPdf(req *PdfWatermarksFileInfo) (err error) {
	if req.Watermarks == nil {
		return fmt.Errorf("Watermarks is required")
	}
	fileInputSource, err := s.StorageFactory.GetFileReadSeekCloser(req.FilePath)
	if err != nil {
		err = pdf_merge.NewFileOpenError(req.FileName, err)
		return err
	}
	defer func() {
		if err := fileInputSource.Close(); err != nil {
			return
		}
	}()
	pdfCreator := creator.New()

	for _, watermark := range req.Watermarks {
		// add watermark text to new pdf page
		pdfCreator, err = s.createBlankPdfWithWatermarkText(watermark.Text, pdfCreator)
		if err != nil {
			return err
		}
	}

	outputSource, err := s.StorageFactory.GetWriteCloser(req.FilePath, "application/pdf")
	if err != nil {
		return err
	}
	defer func() {
		if err := outputSource.Close(); err != nil {
			return
		}
	}()
	err = pdfCreator.Write(outputSource)
	if err != nil {
		return err
	}
	return
}

func (s *UnipdfMergeService) getFilesAndUnsupportedFiles(inputFiles []*PdfMergeFileInfo) (files, unsupportedFiles []*PdfMergeFile, errs *pdf_merge.Errors) {
	files, unsupportedFiles = []*PdfMergeFile{}, []*PdfMergeFile{}
	errs = pdf_merge.NewErrors()
	defer func() {
		if len(errs.Errs) == 0 && len(errs.FatalErrs) == 0 {
			errs = nil
		}
	}()

	for _, inputFile := range inputFiles {
		f, err := s.ValidateMergeFileFormat(inputFile, "")
		log.Println("ValidateMergeFileFormat err", err, f.FileType)
		if err != nil {
			errs.Add(err)
		}
		files = append(files, f)
		if f.FileType == PdfMergeFileTypeUnsupported {
			unsupportedFiles = append(unsupportedFiles, f)
		}
	}

	return
}

func (s *UnipdfMergeService) createBlankPdfWithWatermarkText(text string, pdfCreator *creator.Creator) (*creator.Creator, error) {
	// add a blank page to the pdf
	pdfCreator.SetPageSize(creator.PageSizeA4)
	pdfCreator.NewPage()
	// add watermark text to the created page
	p := pdfCreator.NewParagraph(text)
	timesBold, err := model.NewStandard14Font("Times-Bold")
	if err != nil {
		return pdfCreator, err
	}
	p.SetFont(timesBold)
	p.SetFontSize(14)
	p.SetPos(10, 300)
	p.SetColor(creator.ColorRGBFrom8bit(104, 112, 112))
	p.SetMargins(0, 0, 0, 0)
	err = pdfCreator.Draw(p)
	if err != nil {
		return pdfCreator, err
	}

	return pdfCreator, nil
}

func (s *UnipdfMergeService) createAppendixLeadPage(file *PdfMergeFile, pdfCreator *creator.Creator) (*creator.Creator, error) {
	pdfCreator.NewPage()
	p := pdfCreator.NewParagraph(file.FileName)
	timesBold, err := model.NewStandard14Font("Times-Bold")
	if err != nil {
		return pdfCreator, err
	}
	p.SetFont(timesBold)
	p.SetPos(100, 100)

	err = pdfCreator.Draw(p)
	if err != nil {
		return pdfCreator, err
	}

	return pdfCreator, nil
}
