// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package edmspb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// EdmsServiceClient is the client API for EdmsService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type EdmsServiceClient interface {
	// INNOSHARE
	TestRun(ctx context.Context, in *TestRunRequest, opts ...grpc.CallOption) (*TestRunResponse, error)
	RunPendingCronJob(ctx context.Context, in *RunPendingRequest, opts ...grpc.CallOption) (*RunPendingResponse, error)
	HandleCallbackFrmEdms(ctx context.Context, in *CallBackRequest, opts ...grpc.CallOption) (*CallBackResponse, error)
	GetOutgoingCategories(ctx context.Context, in *GetOutgoingCategoriesRequest, opts ...grpc.CallOption) (*GetOutgoingCategoriesResponse, error)
	CallInnoshareAlertCheck(ctx context.Context, in *RunPendingRequest, opts ...grpc.CallOption) (*CallBackResponse, error)
	RunPatchBy(ctx context.Context, in *RunPatchRequest, opts ...grpc.CallOption) (*CallBackResponse, error)
	// DBC
	DbcGetDocumentList(ctx context.Context, in *DbcGetDocumentListRequest, opts ...grpc.CallOption) (*DbcGetDocumentListResponse, error)
	DbcSubReconcileReport(ctx context.Context, in *DbcSubReconcileReportRequest, opts ...grpc.CallOption) (*DbcSubReconcileReportResponse, error)
}

type edmsServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewEdmsServiceClient(cc grpc.ClientConnInterface) EdmsServiceClient {
	return &edmsServiceClient{cc}
}

func (c *edmsServiceClient) TestRun(ctx context.Context, in *TestRunRequest, opts ...grpc.CallOption) (*TestRunResponse, error) {
	out := new(TestRunResponse)
	err := c.cc.Invoke(ctx, "/reviewer.EdmsService/TestRun", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *edmsServiceClient) RunPendingCronJob(ctx context.Context, in *RunPendingRequest, opts ...grpc.CallOption) (*RunPendingResponse, error) {
	out := new(RunPendingResponse)
	err := c.cc.Invoke(ctx, "/reviewer.EdmsService/RunPendingCronJob", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *edmsServiceClient) HandleCallbackFrmEdms(ctx context.Context, in *CallBackRequest, opts ...grpc.CallOption) (*CallBackResponse, error) {
	out := new(CallBackResponse)
	err := c.cc.Invoke(ctx, "/reviewer.EdmsService/HandleCallbackFrmEdms", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *edmsServiceClient) GetOutgoingCategories(ctx context.Context, in *GetOutgoingCategoriesRequest, opts ...grpc.CallOption) (*GetOutgoingCategoriesResponse, error) {
	out := new(GetOutgoingCategoriesResponse)
	err := c.cc.Invoke(ctx, "/reviewer.EdmsService/GetOutgoingCategories", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *edmsServiceClient) CallInnoshareAlertCheck(ctx context.Context, in *RunPendingRequest, opts ...grpc.CallOption) (*CallBackResponse, error) {
	out := new(CallBackResponse)
	err := c.cc.Invoke(ctx, "/reviewer.EdmsService/CallInnoshareAlertCheck", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *edmsServiceClient) RunPatchBy(ctx context.Context, in *RunPatchRequest, opts ...grpc.CallOption) (*CallBackResponse, error) {
	out := new(CallBackResponse)
	err := c.cc.Invoke(ctx, "/reviewer.EdmsService/RunPatchBy", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *edmsServiceClient) DbcGetDocumentList(ctx context.Context, in *DbcGetDocumentListRequest, opts ...grpc.CallOption) (*DbcGetDocumentListResponse, error) {
	out := new(DbcGetDocumentListResponse)
	err := c.cc.Invoke(ctx, "/reviewer.EdmsService/DbcGetDocumentList", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *edmsServiceClient) DbcSubReconcileReport(ctx context.Context, in *DbcSubReconcileReportRequest, opts ...grpc.CallOption) (*DbcSubReconcileReportResponse, error) {
	out := new(DbcSubReconcileReportResponse)
	err := c.cc.Invoke(ctx, "/reviewer.EdmsService/DbcSubReconcileReport", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// EdmsServiceServer is the server API for EdmsService service.
// All implementations must embed UnimplementedEdmsServiceServer
// for forward compatibility
type EdmsServiceServer interface {
	// INNOSHARE
	TestRun(context.Context, *TestRunRequest) (*TestRunResponse, error)
	RunPendingCronJob(context.Context, *RunPendingRequest) (*RunPendingResponse, error)
	HandleCallbackFrmEdms(context.Context, *CallBackRequest) (*CallBackResponse, error)
	GetOutgoingCategories(context.Context, *GetOutgoingCategoriesRequest) (*GetOutgoingCategoriesResponse, error)
	CallInnoshareAlertCheck(context.Context, *RunPendingRequest) (*CallBackResponse, error)
	RunPatchBy(context.Context, *RunPatchRequest) (*CallBackResponse, error)
	// DBC
	DbcGetDocumentList(context.Context, *DbcGetDocumentListRequest) (*DbcGetDocumentListResponse, error)
	DbcSubReconcileReport(context.Context, *DbcSubReconcileReportRequest) (*DbcSubReconcileReportResponse, error)
	mustEmbedUnimplementedEdmsServiceServer()
}

// UnimplementedEdmsServiceServer must be embedded to have forward compatible implementations.
type UnimplementedEdmsServiceServer struct {
}

func (UnimplementedEdmsServiceServer) TestRun(context.Context, *TestRunRequest) (*TestRunResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method TestRun not implemented")
}
func (UnimplementedEdmsServiceServer) RunPendingCronJob(context.Context, *RunPendingRequest) (*RunPendingResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method RunPendingCronJob not implemented")
}
func (UnimplementedEdmsServiceServer) HandleCallbackFrmEdms(context.Context, *CallBackRequest) (*CallBackResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method HandleCallbackFrmEdms not implemented")
}
func (UnimplementedEdmsServiceServer) GetOutgoingCategories(context.Context, *GetOutgoingCategoriesRequest) (*GetOutgoingCategoriesResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetOutgoingCategories not implemented")
}
func (UnimplementedEdmsServiceServer) CallInnoshareAlertCheck(context.Context, *RunPendingRequest) (*CallBackResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CallInnoshareAlertCheck not implemented")
}
func (UnimplementedEdmsServiceServer) RunPatchBy(context.Context, *RunPatchRequest) (*CallBackResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method RunPatchBy not implemented")
}
func (UnimplementedEdmsServiceServer) DbcGetDocumentList(context.Context, *DbcGetDocumentListRequest) (*DbcGetDocumentListResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DbcGetDocumentList not implemented")
}
func (UnimplementedEdmsServiceServer) DbcSubReconcileReport(context.Context, *DbcSubReconcileReportRequest) (*DbcSubReconcileReportResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DbcSubReconcileReport not implemented")
}
func (UnimplementedEdmsServiceServer) mustEmbedUnimplementedEdmsServiceServer() {}

// UnsafeEdmsServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to EdmsServiceServer will
// result in compilation errors.
type UnsafeEdmsServiceServer interface {
	mustEmbedUnimplementedEdmsServiceServer()
}

func RegisterEdmsServiceServer(s grpc.ServiceRegistrar, srv EdmsServiceServer) {
	s.RegisterService(&_EdmsService_serviceDesc, srv)
}

func _EdmsService_TestRun_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(TestRunRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(EdmsServiceServer).TestRun(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.EdmsService/TestRun",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(EdmsServiceServer).TestRun(ctx, req.(*TestRunRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _EdmsService_RunPendingCronJob_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RunPendingRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(EdmsServiceServer).RunPendingCronJob(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.EdmsService/RunPendingCronJob",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(EdmsServiceServer).RunPendingCronJob(ctx, req.(*RunPendingRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _EdmsService_HandleCallbackFrmEdms_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CallBackRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(EdmsServiceServer).HandleCallbackFrmEdms(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.EdmsService/HandleCallbackFrmEdms",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(EdmsServiceServer).HandleCallbackFrmEdms(ctx, req.(*CallBackRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _EdmsService_GetOutgoingCategories_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetOutgoingCategoriesRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(EdmsServiceServer).GetOutgoingCategories(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.EdmsService/GetOutgoingCategories",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(EdmsServiceServer).GetOutgoingCategories(ctx, req.(*GetOutgoingCategoriesRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _EdmsService_CallInnoshareAlertCheck_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RunPendingRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(EdmsServiceServer).CallInnoshareAlertCheck(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.EdmsService/CallInnoshareAlertCheck",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(EdmsServiceServer).CallInnoshareAlertCheck(ctx, req.(*RunPendingRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _EdmsService_RunPatchBy_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RunPatchRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(EdmsServiceServer).RunPatchBy(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.EdmsService/RunPatchBy",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(EdmsServiceServer).RunPatchBy(ctx, req.(*RunPatchRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _EdmsService_DbcGetDocumentList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DbcGetDocumentListRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(EdmsServiceServer).DbcGetDocumentList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.EdmsService/DbcGetDocumentList",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(EdmsServiceServer).DbcGetDocumentList(ctx, req.(*DbcGetDocumentListRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _EdmsService_DbcSubReconcileReport_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DbcSubReconcileReportRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(EdmsServiceServer).DbcSubReconcileReport(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.EdmsService/DbcSubReconcileReport",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(EdmsServiceServer).DbcSubReconcileReport(ctx, req.(*DbcSubReconcileReportRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _EdmsService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "reviewer.EdmsService",
	HandlerType: (*EdmsServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "TestRun",
			Handler:    _EdmsService_TestRun_Handler,
		},
		{
			MethodName: "RunPendingCronJob",
			Handler:    _EdmsService_RunPendingCronJob_Handler,
		},
		{
			MethodName: "HandleCallbackFrmEdms",
			Handler:    _EdmsService_HandleCallbackFrmEdms_Handler,
		},
		{
			MethodName: "GetOutgoingCategories",
			Handler:    _EdmsService_GetOutgoingCategories_Handler,
		},
		{
			MethodName: "CallInnoshareAlertCheck",
			Handler:    _EdmsService_CallInnoshareAlertCheck_Handler,
		},
		{
			MethodName: "RunPatchBy",
			Handler:    _EdmsService_RunPatchBy_Handler,
		},
		{
			MethodName: "DbcGetDocumentList",
			Handler:    _EdmsService_DbcGetDocumentList_Handler,
		},
		{
			MethodName: "DbcSubReconcileReport",
			Handler:    _EdmsService_DbcSubReconcileReport_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "reviewer/edms/edms.proto",
}
