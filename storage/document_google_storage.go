package storage

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	"cloud.google.com/go/storage"
	"github.com/360EntSecGroup-Skylar/excelize/v2"
	"github.com/SebastiaanKlippert/go-wkhtmltopdf"
	"gitlab.com/go-emat/emat-kit/storage/cloudstorage"
	"gitlab.com/go-emat/emat-kit/storage/common"
)

var (
	//PdfGoogleStorageDocumentBasePath pdf base path on google storage
	PdfGoogleStorageDocumentBasePath = "document/"
	//PdfBackupGoogleStorageBasePath pdf backup base path on google storage
	PdfBackupGoogleStorageBasePath = "del/"

	//PdfGoogleStorageDocumentBasePath pdf base path on google storage
	PdfGoogleStorageFilesBasePath = "files/"

	ZipGoogleStorageFilesBasePath = "zip/"
)

type DocumentGoogleStorage struct {
	GoogleStorageBucketName string
}

func NewDocumentGoogleStorage(bucketName string) *DocumentGoogleStorage {
	return &DocumentGoogleStorage{
		GoogleStorageBucketName: bucketName,
	}
}

func (s *DocumentGoogleStorage) GetWriter(newFilePath string, contentType string) (writer interface{}, err error) {
	gcs, err := cloudstorage.NewGoogleCloudStorage(s.GoogleStorageBucketName)
	log.Printf("s.GoogleStorageBucketName %v", gcs)
	if err != nil {
		return nil, err
	}

	log.Printf("s.GoogleStorageBucketName filePath %v", newFilePath)
	writer = gcs.GetWriterByFilePathAndContentType(newFilePath, contentType+"/"+GetFileExtension(newFilePath))

	return
}

func (s *DocumentGoogleStorage) GetFileWriter(newFilePath string) (savedPath string, writer interface{}, err error) {
	gcs, err := cloudstorage.NewGoogleCloudStorage(s.GoogleStorageBucketName)
	log.Printf("s.GoogleStorageBucketName %v", gcs)
	if err != nil {
		return "", nil, err
	}
	savedPath = PdfGoogleStorageFilesBasePath + newFilePath
	log.Printf("s.GoogleStorageBucketName filePath %v", savedPath)
	writer = gcs.GetWriterByFilePathAndContentType(savedPath, "application/"+GetFileExtension(newFilePath))

	return
}

func (s *DocumentGoogleStorage) WriteFile(writer interface{}, buffer []byte, index int64) (err error) {
	if writer == nil {
		return errors.New("No writer")
	}
	w := writer.(*storage.Writer)
	if _, err := w.Write(buffer); err != nil {
		return err
	}
	return nil
}

func (s *DocumentGoogleStorage) CloseFileWriter(writer interface{}) error {
	if writer == nil {
		return errors.New("No writer")
	}
	w := writer.(*storage.Writer)
	if err := w.Close(); err != nil {
		log.Printf("s.CloseFileWriter err %v", err)
		return err
	}
	return nil
}

func (s *DocumentGoogleStorage) SaveExcelToStorage(filename string, xlsx *excelize.File, extension string) (url string, err error) {
	gcs, err := cloudstorage.NewGoogleCloudStorage(s.GoogleStorageBucketName)
	if err != nil {
		return "", err
	}
	filePath := PdfGoogleStorageDocumentBasePath + filename
	return gcs.SaveStorageExport(filePath, xlsx, extension)
}

// SaveFileToStorage save file to storage (local or google)
func (s *DocumentGoogleStorage) SaveFileToStorage(req *UploadFileRequest) (*UploadFileResponse, error) {
	//save google storage
	gcs, err := cloudstorage.NewGoogleCloudStorage(s.GoogleStorageBucketName)
	if err != nil {
		return nil, err
	}
	filePath := PdfGoogleStorageFilesBasePath + req.FilePath
	if len(req.PutUnderDocumentType) > 0 {
		filePath = PdfGoogleStorageDocumentBasePath + getDocumentPdfStoragePath(req.PutUnderDocumentType) + req.FileName
	}
	if len(req.FileContent) == 0 && req.FileContentReader != nil {
		writer := gcs.GetWriterByFilePathAndContentType(filePath, "application/"+req.FileExtension)
		defer writer.Close()
		if seeker, ok := req.FileContentReader.(io.Seeker); ok {
			seeker.Seek(io.SeekStart, 0)
		}
		_, err = io.Copy(writer, req.FileContentReader)
	} else {
		_, err = gcs.SaveStorageDocument(filePath, req.FileContent, req.FileExtension)
	}
	return &UploadFileResponse{
		FilePath: filePath,
		//StorageType:          StorageTypeGoogle,
	}, err
}

// SavePdfToStorage save pdf to storage (local or google) BY WKHTML object
func (s *DocumentGoogleStorage) SavePdfToStorage(pdfg *wkhtmltopdf.PDFGenerator, documentType, fileName string) (*GeneratePdfToStorageResponse, error) {
	documentStoragePath := getDocumentPdfStoragePath(documentType)
	//save google storage
	filePath := PdfGoogleStorageDocumentBasePath + documentStoragePath + fileName
	gcs, err := cloudstorage.NewGoogleCloudStorage(s.GoogleStorageBucketName)
	if err != nil {
		return nil, err
	}
	_, err = gcs.SaveStorageDocument(filePath, pdfg.Bytes(), "pdf")
	return &GeneratePdfToStorageResponse{
		FilePath:    filePath,
		StorageType: StorageTypeGoogle,
	}, err
}

// BackupStoragePdf backup pdf process
func (s *DocumentGoogleStorage) BackupStoragePdf(dbPdfPath string) (bool, error) {
	newFilePath := PdfBackupGoogleStorageBasePath + dbPdfPath
	gcs, err := cloudstorage.NewGoogleCloudStorage(s.GoogleStorageBucketName)
	if err != nil {
		return false, err
	}
	newCloudStoragePath := strings.Replace(newFilePath, "./storage/", "", -1)
	if err := gcs.MoveStorageDocument(dbPdfPath, newCloudStoragePath); err != nil {
		return false, err
	}
	return true, nil
}

// MoveStorageFile Move StorageFile
func (s *DocumentGoogleStorage) MoveStorageFile(srcPath, destPath string) error {
	ctx := context.Background()
	gcs, err := cloudstorage.NewGoogleCloudStorage(s.GoogleStorageBucketName)
	if err != nil {
		return err
	}
	src := gcs.StorageBucket.Object(srcPath)
	dst := gcs.StorageBucket.Object(destPath)
	log.Println("move file:", srcPath)
	// TODO: move functions
	if _, err := dst.CopierFrom(src).Run(ctx); err != nil {
		log.Println(err)
		return err
	}
	if err := src.Delete(ctx); err != nil {
		log.Println(err)
		return err
	}
	return nil
}

func (s *DocumentGoogleStorage) DeleteStorageFile(filePath string) error {
	ctx := context.Background()
	gcs, err := cloudstorage.NewGoogleCloudStorage(s.GoogleStorageBucketName)
	if err != nil {
		return err
	}
	src := gcs.StorageBucket.Object(filePath)
	if err := src.Delete(ctx); err != nil {
		log.Println(err)
		return err
	}
	return nil
}

// DownloadFile
func (s *DocumentGoogleStorage) DownloadFile(filePath string) ([]byte, error) {
	gcs, err := cloudstorage.NewGoogleCloudStorage(s.GoogleStorageBucketName)
	if err != nil {
		return nil, err
	}
	return gcs.ReadStorageDocument(filePath)
}

// FileExists check if file exist
func (s *DocumentGoogleStorage) FileExists(filePath string) bool {
	gcs, err := cloudstorage.NewGoogleCloudStorage(s.GoogleStorageBucketName)
	if err != nil {
		return false
	}
	ctx := context.Background()
	_, err = gcs.StorageBucket.Object(filePath).Attrs(ctx)
	return err != storage.ErrObjectNotExist
}

// ZipStorageDocument
func (s *DocumentGoogleStorage) ZipStorageDocument(zipFilePath, zipFolder string, files []string) (string, error) {
	gcs, err := cloudstorage.NewGoogleCloudStorage(s.GoogleStorageBucketName)
	if err != nil {
		return "", err
	}
	zipObjectFilePath := ZipGoogleStorageFilesBasePath + zipFilePath
	err = gcs.ZipStorageDocument(zipObjectFilePath, zipFolder, files...)
	return zipObjectFilePath, err
}

// ZipContentToStorageDocument
func (s *DocumentGoogleStorage) ZipContentToStorageDocument(zipContent *common.ZipContent) ([]byte, error) {
	gcs, err := cloudstorage.NewGoogleCloudStorage(s.GoogleStorageBucketName)
	if err != nil {
		return nil, err
	}
	buf, err := gcs.ZipContentToStorageDocument(zipContent)
	return buf, err
}

// GenerateSignedDownloadURL
func (s *DocumentGoogleStorage) GenerateSignedDownloadURL(fileName string, option ...common.SignedUrlOption) (string, error) {
	gcs, err := cloudstorage.NewGoogleCloudStorage(s.GoogleStorageBucketName)
	if err != nil {
		return "", err
	}
	var mOption *common.SignedUrlOption
	if len(option) > 0 {
		mOption = &option[0]
	}
	serviceAccount := os.Getenv("GOOGLE_APPLICATION_CREDENTIALS")
	return gcs.GenerateV4GetObjectSignedURL(fileName, serviceAccount, mOption)
}

func (s *DocumentGoogleStorage) GenerateResumableSessionURL(req *SessionURLRequest) (string, error) {
	gcs, err := cloudstorage.NewGoogleCloudStorage(s.GoogleStorageBucketName)
	if err != nil {
		return "", err
	}
	serviceAccount := os.Getenv("GOOGLE_APPLICATION_CREDENTIALS")
	uploadURL, err := gcs.GenerateResumableSessionURL(&cloudstorage.SessionURLRequest{
		FileName:             req.FileName,
		ServiceAccount:       serviceAccount,
		ContentType:          req.ContentType,
		InitialRequestLength: req.InitialRequestLength,
		Origin:               req.Origin,
		XUploadContentType:   req.XUploadContentType,
		XUploadContentLength: req.XUploadContentLength,
	})

	if err != nil {
		return "", fmt.Errorf("url.Parse: %v", err)
	}
	return uploadURL.String(), nil
}

func (s *DocumentGoogleStorage) CompleteSessionUrl(filepath, uploadID string) error {
	return nil
}

// GetFileReadSeeker
func (s *DocumentGoogleStorage) GetFileReadSeekCloser(filePath string) (io.ReadSeekCloser, error) {
	gcs, err := cloudstorage.NewGoogleCloudStorage(s.GoogleStorageBucketName)
	if err != nil {
		return nil, err
	}
	return gcs.NewGcsReadSeekCloser(filePath)
}

// GetFileWriteCloser
func (s *DocumentGoogleStorage) GetFileWriteCloser(filePath, contentType string) (io.WriteCloser, error) {
	gcs, err := cloudstorage.NewGoogleCloudStorage(s.GoogleStorageBucketName)
	if err != nil {
		return nil, err
	}
	return gcs.GetWriterByFilePathAndContentType(filePath, contentType), nil
}

func (s *DocumentGoogleStorage) GetReader(filePath string) (reader io.ReadCloser, err error) {
	gcs, err := cloudstorage.NewGoogleCloudStorage(s.GoogleStorageBucketName)
	if err != nil {
		return nil, err
	}

	ctx := context.Background()
	return gcs.StorageBucket.Object(filePath).NewReader(ctx)
}

func (s *DocumentGoogleStorage) GetFileMeta(filePath string) (*common.FileMeta, error) {
	fileMeta := common.FileMeta{}
	gcs, err := cloudstorage.NewGoogleCloudStorage(s.GoogleStorageBucketName)
	if err != nil {
		return nil, nil
	}
	ctx := context.Background()
	attrs, err := gcs.StorageBucket.Object(filePath).Attrs(ctx)
	if err != nil {
		return nil, nil
	}
	fileMeta.FileSize = attrs.Size
	return &fileMeta, nil
}