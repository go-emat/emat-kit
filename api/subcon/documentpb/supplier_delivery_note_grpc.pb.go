// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package documentpb

import (
	context "context"
	commonpb "gitlab.com/go-emat/emat-kit/api/commonpb"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// SupplierDeliveryNoteServiceClient is the client API for SupplierDeliveryNoteService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type SupplierDeliveryNoteServiceClient interface {
	ReadSupplierDeliveryNote(ctx context.Context, in *ReadSupplierDeliveryNoteRequest, opts ...grpc.CallOption) (*ReadSupplierDeliveryNoteResponse, error)
	ListSupplierDeliveryNote(ctx context.Context, in *ListSupplierDeliveryNoteRequest, opts ...grpc.CallOption) (*ListSupplierDeliveryNoteResponse, error)
	ListSupplierDeliveryNoteOptions(ctx context.Context, in *commonpb.OptionsRequest, opts ...grpc.CallOption) (*ListSupplierDeliveryNoteOptionsResponse, error)
	ConfirmSupplierDeliveryNote(ctx context.Context, in *ConfirmSupplierDeliveryNoteRequest, opts ...grpc.CallOption) (*SimpleResponse, error)
	CreateSupplierDNFromTms(ctx context.Context, in *CreateSupplierDNFromTmsRequest, opts ...grpc.CallOption) (*SimpleResponse, error)
	ConfirmSupplierDeliveryNoteByTms(ctx context.Context, in *ConfirmSupplierDeliveryNoteByTmsRequest, opts ...grpc.CallOption) (*SimpleResponse, error)
	CancelSupplierDeliveryOrderByTms(ctx context.Context, in *CancelSupplierDeliveryOrderByTmsRequest, opts ...grpc.CallOption) (*SimpleResponse, error)
}

type supplierDeliveryNoteServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewSupplierDeliveryNoteServiceClient(cc grpc.ClientConnInterface) SupplierDeliveryNoteServiceClient {
	return &supplierDeliveryNoteServiceClient{cc}
}

func (c *supplierDeliveryNoteServiceClient) ReadSupplierDeliveryNote(ctx context.Context, in *ReadSupplierDeliveryNoteRequest, opts ...grpc.CallOption) (*ReadSupplierDeliveryNoteResponse, error) {
	out := new(ReadSupplierDeliveryNoteResponse)
	err := c.cc.Invoke(ctx, "/subcon.SupplierDeliveryNoteService/ReadSupplierDeliveryNote", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *supplierDeliveryNoteServiceClient) ListSupplierDeliveryNote(ctx context.Context, in *ListSupplierDeliveryNoteRequest, opts ...grpc.CallOption) (*ListSupplierDeliveryNoteResponse, error) {
	out := new(ListSupplierDeliveryNoteResponse)
	err := c.cc.Invoke(ctx, "/subcon.SupplierDeliveryNoteService/ListSupplierDeliveryNote", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *supplierDeliveryNoteServiceClient) ListSupplierDeliveryNoteOptions(ctx context.Context, in *commonpb.OptionsRequest, opts ...grpc.CallOption) (*ListSupplierDeliveryNoteOptionsResponse, error) {
	out := new(ListSupplierDeliveryNoteOptionsResponse)
	err := c.cc.Invoke(ctx, "/subcon.SupplierDeliveryNoteService/ListSupplierDeliveryNoteOptions", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *supplierDeliveryNoteServiceClient) ConfirmSupplierDeliveryNote(ctx context.Context, in *ConfirmSupplierDeliveryNoteRequest, opts ...grpc.CallOption) (*SimpleResponse, error) {
	out := new(SimpleResponse)
	err := c.cc.Invoke(ctx, "/subcon.SupplierDeliveryNoteService/ConfirmSupplierDeliveryNote", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *supplierDeliveryNoteServiceClient) CreateSupplierDNFromTms(ctx context.Context, in *CreateSupplierDNFromTmsRequest, opts ...grpc.CallOption) (*SimpleResponse, error) {
	out := new(SimpleResponse)
	err := c.cc.Invoke(ctx, "/subcon.SupplierDeliveryNoteService/CreateSupplierDNFromTms", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *supplierDeliveryNoteServiceClient) ConfirmSupplierDeliveryNoteByTms(ctx context.Context, in *ConfirmSupplierDeliveryNoteByTmsRequest, opts ...grpc.CallOption) (*SimpleResponse, error) {
	out := new(SimpleResponse)
	err := c.cc.Invoke(ctx, "/subcon.SupplierDeliveryNoteService/ConfirmSupplierDeliveryNoteByTms", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *supplierDeliveryNoteServiceClient) CancelSupplierDeliveryOrderByTms(ctx context.Context, in *CancelSupplierDeliveryOrderByTmsRequest, opts ...grpc.CallOption) (*SimpleResponse, error) {
	out := new(SimpleResponse)
	err := c.cc.Invoke(ctx, "/subcon.SupplierDeliveryNoteService/CancelSupplierDeliveryOrderByTms", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// SupplierDeliveryNoteServiceServer is the server API for SupplierDeliveryNoteService service.
// All implementations must embed UnimplementedSupplierDeliveryNoteServiceServer
// for forward compatibility
type SupplierDeliveryNoteServiceServer interface {
	ReadSupplierDeliveryNote(context.Context, *ReadSupplierDeliveryNoteRequest) (*ReadSupplierDeliveryNoteResponse, error)
	ListSupplierDeliveryNote(context.Context, *ListSupplierDeliveryNoteRequest) (*ListSupplierDeliveryNoteResponse, error)
	ListSupplierDeliveryNoteOptions(context.Context, *commonpb.OptionsRequest) (*ListSupplierDeliveryNoteOptionsResponse, error)
	ConfirmSupplierDeliveryNote(context.Context, *ConfirmSupplierDeliveryNoteRequest) (*SimpleResponse, error)
	CreateSupplierDNFromTms(context.Context, *CreateSupplierDNFromTmsRequest) (*SimpleResponse, error)
	ConfirmSupplierDeliveryNoteByTms(context.Context, *ConfirmSupplierDeliveryNoteByTmsRequest) (*SimpleResponse, error)
	CancelSupplierDeliveryOrderByTms(context.Context, *CancelSupplierDeliveryOrderByTmsRequest) (*SimpleResponse, error)
	mustEmbedUnimplementedSupplierDeliveryNoteServiceServer()
}

// UnimplementedSupplierDeliveryNoteServiceServer must be embedded to have forward compatible implementations.
type UnimplementedSupplierDeliveryNoteServiceServer struct {
}

func (UnimplementedSupplierDeliveryNoteServiceServer) ReadSupplierDeliveryNote(context.Context, *ReadSupplierDeliveryNoteRequest) (*ReadSupplierDeliveryNoteResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ReadSupplierDeliveryNote not implemented")
}
func (UnimplementedSupplierDeliveryNoteServiceServer) ListSupplierDeliveryNote(context.Context, *ListSupplierDeliveryNoteRequest) (*ListSupplierDeliveryNoteResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListSupplierDeliveryNote not implemented")
}
func (UnimplementedSupplierDeliveryNoteServiceServer) ListSupplierDeliveryNoteOptions(context.Context, *commonpb.OptionsRequest) (*ListSupplierDeliveryNoteOptionsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListSupplierDeliveryNoteOptions not implemented")
}
func (UnimplementedSupplierDeliveryNoteServiceServer) ConfirmSupplierDeliveryNote(context.Context, *ConfirmSupplierDeliveryNoteRequest) (*SimpleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ConfirmSupplierDeliveryNote not implemented")
}
func (UnimplementedSupplierDeliveryNoteServiceServer) CreateSupplierDNFromTms(context.Context, *CreateSupplierDNFromTmsRequest) (*SimpleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateSupplierDNFromTms not implemented")
}
func (UnimplementedSupplierDeliveryNoteServiceServer) ConfirmSupplierDeliveryNoteByTms(context.Context, *ConfirmSupplierDeliveryNoteByTmsRequest) (*SimpleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ConfirmSupplierDeliveryNoteByTms not implemented")
}
func (UnimplementedSupplierDeliveryNoteServiceServer) CancelSupplierDeliveryOrderByTms(context.Context, *CancelSupplierDeliveryOrderByTmsRequest) (*SimpleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CancelSupplierDeliveryOrderByTms not implemented")
}
func (UnimplementedSupplierDeliveryNoteServiceServer) mustEmbedUnimplementedSupplierDeliveryNoteServiceServer() {
}

// UnsafeSupplierDeliveryNoteServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to SupplierDeliveryNoteServiceServer will
// result in compilation errors.
type UnsafeSupplierDeliveryNoteServiceServer interface {
	mustEmbedUnimplementedSupplierDeliveryNoteServiceServer()
}

func RegisterSupplierDeliveryNoteServiceServer(s grpc.ServiceRegistrar, srv SupplierDeliveryNoteServiceServer) {
	s.RegisterService(&_SupplierDeliveryNoteService_serviceDesc, srv)
}

func _SupplierDeliveryNoteService_ReadSupplierDeliveryNote_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ReadSupplierDeliveryNoteRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SupplierDeliveryNoteServiceServer).ReadSupplierDeliveryNote(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/subcon.SupplierDeliveryNoteService/ReadSupplierDeliveryNote",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SupplierDeliveryNoteServiceServer).ReadSupplierDeliveryNote(ctx, req.(*ReadSupplierDeliveryNoteRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SupplierDeliveryNoteService_ListSupplierDeliveryNote_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListSupplierDeliveryNoteRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SupplierDeliveryNoteServiceServer).ListSupplierDeliveryNote(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/subcon.SupplierDeliveryNoteService/ListSupplierDeliveryNote",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SupplierDeliveryNoteServiceServer).ListSupplierDeliveryNote(ctx, req.(*ListSupplierDeliveryNoteRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SupplierDeliveryNoteService_ListSupplierDeliveryNoteOptions_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(commonpb.OptionsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SupplierDeliveryNoteServiceServer).ListSupplierDeliveryNoteOptions(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/subcon.SupplierDeliveryNoteService/ListSupplierDeliveryNoteOptions",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SupplierDeliveryNoteServiceServer).ListSupplierDeliveryNoteOptions(ctx, req.(*commonpb.OptionsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SupplierDeliveryNoteService_ConfirmSupplierDeliveryNote_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ConfirmSupplierDeliveryNoteRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SupplierDeliveryNoteServiceServer).ConfirmSupplierDeliveryNote(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/subcon.SupplierDeliveryNoteService/ConfirmSupplierDeliveryNote",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SupplierDeliveryNoteServiceServer).ConfirmSupplierDeliveryNote(ctx, req.(*ConfirmSupplierDeliveryNoteRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SupplierDeliveryNoteService_CreateSupplierDNFromTms_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateSupplierDNFromTmsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SupplierDeliveryNoteServiceServer).CreateSupplierDNFromTms(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/subcon.SupplierDeliveryNoteService/CreateSupplierDNFromTms",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SupplierDeliveryNoteServiceServer).CreateSupplierDNFromTms(ctx, req.(*CreateSupplierDNFromTmsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SupplierDeliveryNoteService_ConfirmSupplierDeliveryNoteByTms_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ConfirmSupplierDeliveryNoteByTmsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SupplierDeliveryNoteServiceServer).ConfirmSupplierDeliveryNoteByTms(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/subcon.SupplierDeliveryNoteService/ConfirmSupplierDeliveryNoteByTms",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SupplierDeliveryNoteServiceServer).ConfirmSupplierDeliveryNoteByTms(ctx, req.(*ConfirmSupplierDeliveryNoteByTmsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SupplierDeliveryNoteService_CancelSupplierDeliveryOrderByTms_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CancelSupplierDeliveryOrderByTmsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SupplierDeliveryNoteServiceServer).CancelSupplierDeliveryOrderByTms(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/subcon.SupplierDeliveryNoteService/CancelSupplierDeliveryOrderByTms",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SupplierDeliveryNoteServiceServer).CancelSupplierDeliveryOrderByTms(ctx, req.(*CancelSupplierDeliveryOrderByTmsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _SupplierDeliveryNoteService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "subcon.SupplierDeliveryNoteService",
	HandlerType: (*SupplierDeliveryNoteServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "ReadSupplierDeliveryNote",
			Handler:    _SupplierDeliveryNoteService_ReadSupplierDeliveryNote_Handler,
		},
		{
			MethodName: "ListSupplierDeliveryNote",
			Handler:    _SupplierDeliveryNoteService_ListSupplierDeliveryNote_Handler,
		},
		{
			MethodName: "ListSupplierDeliveryNoteOptions",
			Handler:    _SupplierDeliveryNoteService_ListSupplierDeliveryNoteOptions_Handler,
		},
		{
			MethodName: "ConfirmSupplierDeliveryNote",
			Handler:    _SupplierDeliveryNoteService_ConfirmSupplierDeliveryNote_Handler,
		},
		{
			MethodName: "CreateSupplierDNFromTms",
			Handler:    _SupplierDeliveryNoteService_CreateSupplierDNFromTms_Handler,
		},
		{
			MethodName: "ConfirmSupplierDeliveryNoteByTms",
			Handler:    _SupplierDeliveryNoteService_ConfirmSupplierDeliveryNoteByTms_Handler,
		},
		{
			MethodName: "CancelSupplierDeliveryOrderByTms",
			Handler:    _SupplierDeliveryNoteService_CancelSupplierDeliveryOrderByTms_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "subcon/document/supplier_delivery_note.proto",
}
