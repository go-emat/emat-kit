package slack

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

func CallAPI(url string, payload interface{}) error {
	messageBytes, err := json.Marshal(payload)
	if err != nil {
		return fmt.Errorf("could not marshal message: %v", err)
	}

	resp, err := http.Post(url, "application/json", bytes.NewBuffer(messageBytes))
	if err != nil {
		return fmt.Errorf("could not send message: %v", err)
	}
	defer resp.Body.Close()

	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("could not read response body: %v", err)
	}
	bodyString := string(bodyBytes)

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("received non-ok response: %d - %s", resp.StatusCode, bodyString)
	}

	return nil
}
