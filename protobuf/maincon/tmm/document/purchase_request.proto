syntax = "proto3";

package maincon.tmm;

option go_package = "maincon/tmm/documentpb";

import "common/common.proto";
import "google/protobuf/timestamp.proto";
import "maincon/tmm/document/document_common.proto";

service PurchaseRequestService {
  rpc CreatePurchaseRequest (CreatePurchaseRequestParam) returns (CreatePurchaseRequestResponse);
  rpc GetPRMaterialProcurement (GetPRMaterialProcurementRequest) returns (stream GetPRMaterialProcurementResponse);
  rpc RevertPurchaseRequestByVsm (RevertPurchaseRequestByVsmRequest) returns (SimpleResponse);

  rpc MarkPurchaseRequestAward(MarkPurchaseRequestAwardRequest) returns (SimpleResponse);
  rpc AwardPurchaseRequest(AwardPurchaseRequestRequest) returns (AwardPurchaseRequestResponse);
}

message AwardPurchaseRequestRequest {
  common.Language lang = 1;
  int64 purchase_request_id = 2;
  int64 vsm_request_document_id = 3;
  int64 tender_analysis_id = 4;
  string tender_analysis_no = 5;
  string pdf_url = 6;
  string currency = 7;
  float rate = 8;
  string terms_condition_code = 9;
  int64 department_id = 10;
  int64 payment_term_id = 11;

  repeated TenderSupplier tender_suppliers = 12;
  repeated FileItem files =13;
}

message AwardPurchaseRequestResponse {
  bool success = 1;
  string message = 2;

  ApiSubmissionLog api_submission_log = 3;
}

message ApiSubmissionLog {
  string submission_time = 1;
  string request_data = 6;
  string response_data = 2;
  string response_status_code = 3;
  string status = 4;
  string error_message = 5;
}

message TenderSupplier {
  int64 tmm_company_profile_id = 1;
  int64 emat_company_id = 2;
  int64 tmm_company_contact_id1 = 3;
  int64 contact_id1 = 4;
  int64 tmm_company_contact_id2 = 5;
  int64 contact_id2 = 6;
  int64 tmm_company_contact_id3 = 7;
  int64 contact_id3 = 8;

  repeated TenderAnalysis tender_analyses = 9;
}

message TenderAnalysis {
  int64 tender_analysis_item_id = 1;
  int32 line_no = 3;
	string code = 4;
	string supplier_item_code = 5;
	string description = 6;
	string specifications = 7;
	int64 unit_id = 15;
	string unit = 16;
	string unit_short = 17;
	bool rate_only = 18;
	double qty = 19;
	int32 sort = 20;
	int32 unit_discount_type = 30;
	double unit_discount = 31;
	double unit_discount_value = 32;
	int32 line_discount_type = 33;
	double line_discount = 34;
	double line_discount_value = 35;
	double unit_price = 36;
	double unit_price_after_unit_discount = 37;
	double unit_price_after_line_discount = 38;
	double amount = 39;
	double amount_before_discount = 40;
	google.protobuf.Timestamp delivery_date = 45;
	string country_of_origin = 46;
	string factory_location = 47;
	string brand = 48;
	string remark = 49;
  string delivery_infos = 51;
  string company_code = 52;

  RequestDocumentMaterial request_document_material = 55;
}

message RequestDocumentMaterial {
  int64 vsm_request_document_material_id = 1;
  int64 vsm_request_document_id = 2;
  int64 source_document_material_id = 3;
  int32 line_no = 4;
  string description = 5;
  int64 category_id = 6;
  int64 unit_id = 7;
  string unit = 8;
  bool rate_only = 9;
  double qty = 10;
  double estimated_unit_price = 11;
  double estimated_wastage = 12;
  string charge_to = 13;
  string contractor = 14;
  string cost_center = 15;
  string cost_code = 16;
  string location = 17;
  bool green_item = 18;
  bool test_certificate = 19;
  string site_remark = 20;
  string required_delivery_date_type = 21;
  google.protobuf.Timestamp expected_delivery_date = 22;
  string required_delivery_date_range = 23;
  string Specification_compliance = 24;
  double budget_amount = 25;
  double tender_estimate_amount = 26;
  string budget_code = 27;
  int32 priority = 28;
  int64 created_by = 29;
  int64 updated_by = 30;
  bool guarantee = 31;
  string material_code = 32;
  string company_code = 33;
}

message FileItem {
  string file_path = 1;
    string file_name = 2;
    string file_type_name = 3;
    string encoded_file = 4;
    string extension = 5;
    string file_url = 6;
    string remark = 7;
}

message RevertPurchaseRequestByVsmRequest {
  common.Language lang = 1;
  int64 purchase_request_id = 2;
  int64 vsm_request_document_id = 3;
  string note = 4;
}

message CreatePurchaseRequestParam {
  int64 project_id = 1;
  int64 requester_id = 2;
  int64 functional_group_id = 3;
  int64 transaction_type_id = 4;
  int64 purchase_contract_id = 5;
  string remark = 6;
  bool back_date_order = 7;
  bool is_rental = 9;
  repeated int64 delivery_contact_ids = 10;

  MaterialContainer material_container = 11;
  repeated MaterialContainerComponent material_container_components = 12;
  repeated File files = 13;
  string document_type = 14;
}

message File {
  string file_name = 1;
  int64 file_type_id = 2;
  string file_path = 3;
  bool attach = 4;
}

message MaterialContainer {
  int64 id = 1;
  int64 project_id = 2;
  int64 category_id = 3;
  string responsible_party = 4;
  string sub_contract_number = 5;
  string location = 6;
  int32 version = 7;
  google.protobuf.Timestamp planned_construction_date = 8;
}

message MaterialContainerComponent {
  int64 id = 1;
  int64 project_id = 2;
  int64 requester_id = 3;
  string description = 4;
  int64 unit_id = 5;
  string unit = 6;
  string unit_short = 7;
  double quantity = 8;
}

message PurchaseRequestMaterial {
  int64 id = 1;
  int64 component_id = 2;
  int64 material_id = 3;
}
message CreatePurchaseRequestResponse {
  int64 purchase_request_id = 1;
  string document_no = 2;
  repeated PurchaseRequestMaterial purchase_request_materials = 3;
}

message PurchaseRequestProduct {
  int64 id = 1;
  int64 item_link_id = 2;
  int64 purchase_request_material_id = 3;
  string description = 4;
  double quantity = 5;
  int64 material_container_component_id = 6;
  string unit_short = 7;
  double receive_qty = 8;
  // PurchaseRequest purchase_request = 8;
  repeated PurchaseOrder purchase_orders = 9;
}

message PurchaseRequest {
  int64 id = 1;
  string document_no = 2;
  google.protobuf.Timestamp create_at = 3;
  double qty = 4;
  int32 status = 5;
  string status_label = 6;
  repeated PurchaseOrder purchase_orders = 7;
  google.protobuf.Timestamp submit_date = 8;
  google.protobuf.Timestamp approve_date = 9;
}

message PurchaseOrder {
  int64 id = 1;
  string document_no = 2;
  int64 link_id = 3;
  int64 item_link_id = 4;
  google.protobuf.Timestamp submit_to_supplier_date = 5;
  double qty = 6;
  int32 status = 7;
  string status_label = 8;
  repeated GoodsReceipt goods_receipts = 9;
  google.protobuf.Timestamp request_delivery_date = 10;
}

message GoodsReceipt {
  int64 id = 1;
  string document_no = 2;
  int64 link_id = 3;
  int64 item_link_id = 4;
  google.protobuf.Timestamp received_at = 5;
  double received_qty = 6;
  int32 status = 7;
  string status_label = 8;
}

message PRMaterialProcurement {
  int64 id = 1;
  int64 purchase_request_id = 2;
  string description = 3;
  double qty = 4;
  string unit_short = 5;
  int64 material_container_component_id = 6;
  PurchaseRequest purchase_request = 8;
  repeated PurchaseRequestProduct purchase_request_products = 9;
}

message GetPRMaterialProcurementRequest {
  repeated int64 purchase_request_material_ids = 1;
  common.Language lang = 2;
  int64 material_container_id = 3;
}

message GetPRMaterialProcurementResponse {
  PRMaterialProcurement purchase_request_material = 1;
}

message MarkPurchaseRequestAwardRequest {
  common.Language lang = 1;
  int64 purchase_request_id = 2;
  int64 vsm_request_document_id = 3;
}