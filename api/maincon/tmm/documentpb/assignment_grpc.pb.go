// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package documentpb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// AssignmentServiceClient is the client API for AssignmentService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type AssignmentServiceClient interface {
	CallSkipApproveFlowFunc(ctx context.Context, in *CallSkipApproveFlowFuncRequest, opts ...grpc.CallOption) (*SimpleResponse, error)
}

type assignmentServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewAssignmentServiceClient(cc grpc.ClientConnInterface) AssignmentServiceClient {
	return &assignmentServiceClient{cc}
}

func (c *assignmentServiceClient) CallSkipApproveFlowFunc(ctx context.Context, in *CallSkipApproveFlowFuncRequest, opts ...grpc.CallOption) (*SimpleResponse, error) {
	out := new(SimpleResponse)
	err := c.cc.Invoke(ctx, "/maincon.tmm.AssignmentService/CallSkipApproveFlowFunc", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// AssignmentServiceServer is the server API for AssignmentService service.
// All implementations must embed UnimplementedAssignmentServiceServer
// for forward compatibility
type AssignmentServiceServer interface {
	CallSkipApproveFlowFunc(context.Context, *CallSkipApproveFlowFuncRequest) (*SimpleResponse, error)
	mustEmbedUnimplementedAssignmentServiceServer()
}

// UnimplementedAssignmentServiceServer must be embedded to have forward compatible implementations.
type UnimplementedAssignmentServiceServer struct {
}

func (UnimplementedAssignmentServiceServer) CallSkipApproveFlowFunc(context.Context, *CallSkipApproveFlowFuncRequest) (*SimpleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CallSkipApproveFlowFunc not implemented")
}
func (UnimplementedAssignmentServiceServer) mustEmbedUnimplementedAssignmentServiceServer() {}

// UnsafeAssignmentServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to AssignmentServiceServer will
// result in compilation errors.
type UnsafeAssignmentServiceServer interface {
	mustEmbedUnimplementedAssignmentServiceServer()
}

func RegisterAssignmentServiceServer(s grpc.ServiceRegistrar, srv AssignmentServiceServer) {
	s.RegisterService(&_AssignmentService_serviceDesc, srv)
}

func _AssignmentService_CallSkipApproveFlowFunc_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CallSkipApproveFlowFuncRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AssignmentServiceServer).CallSkipApproveFlowFunc(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.tmm.AssignmentService/CallSkipApproveFlowFunc",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AssignmentServiceServer).CallSkipApproveFlowFunc(ctx, req.(*CallSkipApproveFlowFuncRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _AssignmentService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "maincon.tmm.AssignmentService",
	HandlerType: (*AssignmentServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CallSkipApproveFlowFunc",
			Handler:    _AssignmentService_CallSkipApproveFlowFunc_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "maincon/tmm/document/assignment.proto",
}
