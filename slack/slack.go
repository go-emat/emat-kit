package slack

import (
	"fmt"
	"os"
)

func SendToSlackWebhook(channel, text string) error {
	webhookURL := os.Getenv("SLACK_WEBHOOK_URL")
	if webhookURL == "" {
		return fmt.Errorf("SLACK_WEBHOOK_URL environment variable is not set")
	}

	payload := CreateSlackPayload(channel, text)

	return CallAPI(webhookURL, payload)
}
