// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.25.0
// 	protoc        v3.14.0
// source: maincon/mmm/submission/reviewing-process.proto

package submissionpb

import (
	proto "github.com/golang/protobuf/proto"
	commonpb "gitlab.com/go-emat/emat-kit/api/commonpb"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	timestamppb "google.golang.org/protobuf/types/known/timestamppb"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type SubmissionReviewingProcessUpdateRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id                                            int64                  `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	ReviewerPortalReviewingProcessUpdateRequestId int64                  `protobuf:"varint,2,opt,name=reviewer_portal_reviewing_process_update_request_id,json=reviewerPortalReviewingProcessUpdateRequestId,proto3" json:"reviewer_portal_reviewing_process_update_request_id,omitempty"`
	ModelId                                       int64                  `protobuf:"varint,3,opt,name=model_id,json=modelId,proto3" json:"model_id,omitempty"`
	ModelType                                     string                 `protobuf:"bytes,4,opt,name=model_type,json=modelType,proto3" json:"model_type,omitempty"`
	Revision                                      string                 `protobuf:"bytes,20,opt,name=revision,proto3" json:"revision,omitempty"`
	SubmissionRevisionId                          int64                  `protobuf:"varint,5,opt,name=submission_revision_id,json=submissionRevisionId,proto3" json:"submission_revision_id,omitempty"`
	RequesterName                                 string                 `protobuf:"bytes,6,opt,name=requester_name,json=requesterName,proto3" json:"requester_name,omitempty"`
	RequesterEmail                                string                 `protobuf:"bytes,7,opt,name=requester_email,json=requesterEmail,proto3" json:"requester_email,omitempty"`
	RequesterContact                              string                 `protobuf:"bytes,8,opt,name=requester_contact,json=requesterContact,proto3" json:"requester_contact,omitempty"`
	RequestedBy                                   int64                  `protobuf:"varint,9,opt,name=requested_by,json=requestedBy,proto3" json:"requested_by,omitempty"`
	RequestedAt                                   *timestamppb.Timestamp `protobuf:"bytes,10,opt,name=requested_at,json=requestedAt,proto3" json:"requested_at,omitempty"`
	OriginalReviewingProcessId                    int64                  `protobuf:"varint,11,opt,name=original_reviewing_process_id,json=originalReviewingProcessId,proto3" json:"original_reviewing_process_id,omitempty"`
	Remark                                        string                 `protobuf:"bytes,12,opt,name=remark,proto3" json:"remark,omitempty"`
	ResponseReviewingProcessId                    int64                  `protobuf:"varint,13,opt,name=response_reviewing_process_id,json=responseReviewingProcessId,proto3" json:"response_reviewing_process_id,omitempty"`
	ResponseRemark                                string                 `protobuf:"bytes,14,opt,name=response_remark,json=responseRemark,proto3" json:"response_remark,omitempty"`
	RespondedBy                                   int64                  `protobuf:"varint,15,opt,name=responded_by,json=respondedBy,proto3" json:"responded_by,omitempty"`
	RespondedAt                                   *timestamppb.Timestamp `protobuf:"bytes,16,opt,name=responded_at,json=respondedAt,proto3" json:"responded_at,omitempty"`
	Status                                        int32                  `protobuf:"varint,17,opt,name=status,proto3" json:"status,omitempty"`
	OriginalReviewingProcessName                  string                 `protobuf:"bytes,18,opt,name=original_reviewing_process_name,json=originalReviewingProcessName,proto3" json:"original_reviewing_process_name,omitempty"`
	ResponseReviewingProcessName                  string                 `protobuf:"bytes,19,opt,name=response_reviewing_process_name,json=responseReviewingProcessName,proto3" json:"response_reviewing_process_name,omitempty"`
}

func (x *SubmissionReviewingProcessUpdateRequest) Reset() {
	*x = SubmissionReviewingProcessUpdateRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_maincon_mmm_submission_reviewing_process_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SubmissionReviewingProcessUpdateRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SubmissionReviewingProcessUpdateRequest) ProtoMessage() {}

func (x *SubmissionReviewingProcessUpdateRequest) ProtoReflect() protoreflect.Message {
	mi := &file_maincon_mmm_submission_reviewing_process_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SubmissionReviewingProcessUpdateRequest.ProtoReflect.Descriptor instead.
func (*SubmissionReviewingProcessUpdateRequest) Descriptor() ([]byte, []int) {
	return file_maincon_mmm_submission_reviewing_process_proto_rawDescGZIP(), []int{0}
}

func (x *SubmissionReviewingProcessUpdateRequest) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *SubmissionReviewingProcessUpdateRequest) GetReviewerPortalReviewingProcessUpdateRequestId() int64 {
	if x != nil {
		return x.ReviewerPortalReviewingProcessUpdateRequestId
	}
	return 0
}

func (x *SubmissionReviewingProcessUpdateRequest) GetModelId() int64 {
	if x != nil {
		return x.ModelId
	}
	return 0
}

func (x *SubmissionReviewingProcessUpdateRequest) GetModelType() string {
	if x != nil {
		return x.ModelType
	}
	return ""
}

func (x *SubmissionReviewingProcessUpdateRequest) GetRevision() string {
	if x != nil {
		return x.Revision
	}
	return ""
}

func (x *SubmissionReviewingProcessUpdateRequest) GetSubmissionRevisionId() int64 {
	if x != nil {
		return x.SubmissionRevisionId
	}
	return 0
}

func (x *SubmissionReviewingProcessUpdateRequest) GetRequesterName() string {
	if x != nil {
		return x.RequesterName
	}
	return ""
}

func (x *SubmissionReviewingProcessUpdateRequest) GetRequesterEmail() string {
	if x != nil {
		return x.RequesterEmail
	}
	return ""
}

func (x *SubmissionReviewingProcessUpdateRequest) GetRequesterContact() string {
	if x != nil {
		return x.RequesterContact
	}
	return ""
}

func (x *SubmissionReviewingProcessUpdateRequest) GetRequestedBy() int64 {
	if x != nil {
		return x.RequestedBy
	}
	return 0
}

func (x *SubmissionReviewingProcessUpdateRequest) GetRequestedAt() *timestamppb.Timestamp {
	if x != nil {
		return x.RequestedAt
	}
	return nil
}

func (x *SubmissionReviewingProcessUpdateRequest) GetOriginalReviewingProcessId() int64 {
	if x != nil {
		return x.OriginalReviewingProcessId
	}
	return 0
}

func (x *SubmissionReviewingProcessUpdateRequest) GetRemark() string {
	if x != nil {
		return x.Remark
	}
	return ""
}

func (x *SubmissionReviewingProcessUpdateRequest) GetResponseReviewingProcessId() int64 {
	if x != nil {
		return x.ResponseReviewingProcessId
	}
	return 0
}

func (x *SubmissionReviewingProcessUpdateRequest) GetResponseRemark() string {
	if x != nil {
		return x.ResponseRemark
	}
	return ""
}

func (x *SubmissionReviewingProcessUpdateRequest) GetRespondedBy() int64 {
	if x != nil {
		return x.RespondedBy
	}
	return 0
}

func (x *SubmissionReviewingProcessUpdateRequest) GetRespondedAt() *timestamppb.Timestamp {
	if x != nil {
		return x.RespondedAt
	}
	return nil
}

func (x *SubmissionReviewingProcessUpdateRequest) GetStatus() int32 {
	if x != nil {
		return x.Status
	}
	return 0
}

func (x *SubmissionReviewingProcessUpdateRequest) GetOriginalReviewingProcessName() string {
	if x != nil {
		return x.OriginalReviewingProcessName
	}
	return ""
}

func (x *SubmissionReviewingProcessUpdateRequest) GetResponseReviewingProcessName() string {
	if x != nil {
		return x.ResponseReviewingProcessName
	}
	return ""
}

type GetSubmissionReviewingProcessUpdateRequestListResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ShowAlert bool                                       `protobuf:"varint,1,opt,name=show_alert,json=showAlert,proto3" json:"show_alert,omitempty"`
	Data      []*SubmissionReviewingProcessUpdateRequest `protobuf:"bytes,2,rep,name=data,proto3" json:"data,omitempty"`
}

func (x *GetSubmissionReviewingProcessUpdateRequestListResponse) Reset() {
	*x = GetSubmissionReviewingProcessUpdateRequestListResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_maincon_mmm_submission_reviewing_process_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetSubmissionReviewingProcessUpdateRequestListResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetSubmissionReviewingProcessUpdateRequestListResponse) ProtoMessage() {}

func (x *GetSubmissionReviewingProcessUpdateRequestListResponse) ProtoReflect() protoreflect.Message {
	mi := &file_maincon_mmm_submission_reviewing_process_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetSubmissionReviewingProcessUpdateRequestListResponse.ProtoReflect.Descriptor instead.
func (*GetSubmissionReviewingProcessUpdateRequestListResponse) Descriptor() ([]byte, []int) {
	return file_maincon_mmm_submission_reviewing_process_proto_rawDescGZIP(), []int{1}
}

func (x *GetSubmissionReviewingProcessUpdateRequestListResponse) GetShowAlert() bool {
	if x != nil {
		return x.ShowAlert
	}
	return false
}

func (x *GetSubmissionReviewingProcessUpdateRequestListResponse) GetData() []*SubmissionReviewingProcessUpdateRequest {
	if x != nil {
		return x.Data
	}
	return nil
}

type SelectNewReviewingProcessForUpdateRequestRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Lang                               commonpb.Language `protobuf:"varint,1,opt,name=lang,proto3,enum=common.Language" json:"lang,omitempty"`
	SmmReviewingProcessUpdateRequestId int64             `protobuf:"varint,2,opt,name=smm_reviewing_process_update_request_id,json=smmReviewingProcessUpdateRequestId,proto3" json:"smm_reviewing_process_update_request_id,omitempty"`
	ResponseReviewingProcessId         int64             `protobuf:"varint,3,opt,name=response_reviewing_process_id,json=responseReviewingProcessId,proto3" json:"response_reviewing_process_id,omitempty"`
	ResponseRemark                     string            `protobuf:"bytes,4,opt,name=response_remark,json=responseRemark,proto3" json:"response_remark,omitempty"`
}

func (x *SelectNewReviewingProcessForUpdateRequestRequest) Reset() {
	*x = SelectNewReviewingProcessForUpdateRequestRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_maincon_mmm_submission_reviewing_process_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SelectNewReviewingProcessForUpdateRequestRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SelectNewReviewingProcessForUpdateRequestRequest) ProtoMessage() {}

func (x *SelectNewReviewingProcessForUpdateRequestRequest) ProtoReflect() protoreflect.Message {
	mi := &file_maincon_mmm_submission_reviewing_process_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SelectNewReviewingProcessForUpdateRequestRequest.ProtoReflect.Descriptor instead.
func (*SelectNewReviewingProcessForUpdateRequestRequest) Descriptor() ([]byte, []int) {
	return file_maincon_mmm_submission_reviewing_process_proto_rawDescGZIP(), []int{2}
}

func (x *SelectNewReviewingProcessForUpdateRequestRequest) GetLang() commonpb.Language {
	if x != nil {
		return x.Lang
	}
	return commonpb.Language_UNKNOWN_LANGUAGE
}

func (x *SelectNewReviewingProcessForUpdateRequestRequest) GetSmmReviewingProcessUpdateRequestId() int64 {
	if x != nil {
		return x.SmmReviewingProcessUpdateRequestId
	}
	return 0
}

func (x *SelectNewReviewingProcessForUpdateRequestRequest) GetResponseReviewingProcessId() int64 {
	if x != nil {
		return x.ResponseReviewingProcessId
	}
	return 0
}

func (x *SelectNewReviewingProcessForUpdateRequestRequest) GetResponseRemark() string {
	if x != nil {
		return x.ResponseRemark
	}
	return ""
}

type CheckAndCancelReviewingProcessUpdateRequestRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Lang                 commonpb.Language `protobuf:"varint,1,opt,name=lang,proto3,enum=common.Language" json:"lang,omitempty"`
	SubmissionRevisionId int64             `protobuf:"varint,2,opt,name=submission_revision_id,json=submissionRevisionId,proto3" json:"submission_revision_id,omitempty"`
}

func (x *CheckAndCancelReviewingProcessUpdateRequestRequest) Reset() {
	*x = CheckAndCancelReviewingProcessUpdateRequestRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_maincon_mmm_submission_reviewing_process_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CheckAndCancelReviewingProcessUpdateRequestRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CheckAndCancelReviewingProcessUpdateRequestRequest) ProtoMessage() {}

func (x *CheckAndCancelReviewingProcessUpdateRequestRequest) ProtoReflect() protoreflect.Message {
	mi := &file_maincon_mmm_submission_reviewing_process_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CheckAndCancelReviewingProcessUpdateRequestRequest.ProtoReflect.Descriptor instead.
func (*CheckAndCancelReviewingProcessUpdateRequestRequest) Descriptor() ([]byte, []int) {
	return file_maincon_mmm_submission_reviewing_process_proto_rawDescGZIP(), []int{3}
}

func (x *CheckAndCancelReviewingProcessUpdateRequestRequest) GetLang() commonpb.Language {
	if x != nil {
		return x.Lang
	}
	return commonpb.Language_UNKNOWN_LANGUAGE
}

func (x *CheckAndCancelReviewingProcessUpdateRequestRequest) GetSubmissionRevisionId() int64 {
	if x != nil {
		return x.SubmissionRevisionId
	}
	return 0
}

type CheckAndCancelReviewingProcessUpdateRequestResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	SubmissionReviewingProcessUpdateRequestId []int64 `protobuf:"varint,1,rep,packed,name=submission_reviewing_process_update_request_id,json=submissionReviewingProcessUpdateRequestId,proto3" json:"submission_reviewing_process_update_request_id,omitempty"`
}

func (x *CheckAndCancelReviewingProcessUpdateRequestResponse) Reset() {
	*x = CheckAndCancelReviewingProcessUpdateRequestResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_maincon_mmm_submission_reviewing_process_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CheckAndCancelReviewingProcessUpdateRequestResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CheckAndCancelReviewingProcessUpdateRequestResponse) ProtoMessage() {}

func (x *CheckAndCancelReviewingProcessUpdateRequestResponse) ProtoReflect() protoreflect.Message {
	mi := &file_maincon_mmm_submission_reviewing_process_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CheckAndCancelReviewingProcessUpdateRequestResponse.ProtoReflect.Descriptor instead.
func (*CheckAndCancelReviewingProcessUpdateRequestResponse) Descriptor() ([]byte, []int) {
	return file_maincon_mmm_submission_reviewing_process_proto_rawDescGZIP(), []int{4}
}

func (x *CheckAndCancelReviewingProcessUpdateRequestResponse) GetSubmissionReviewingProcessUpdateRequestId() []int64 {
	if x != nil {
		return x.SubmissionReviewingProcessUpdateRequestId
	}
	return nil
}

var File_maincon_mmm_submission_reviewing_process_proto protoreflect.FileDescriptor

var file_maincon_mmm_submission_reviewing_process_proto_rawDesc = []byte{
	0x0a, 0x2e, 0x6d, 0x61, 0x69, 0x6e, 0x63, 0x6f, 0x6e, 0x2f, 0x6d, 0x6d, 0x6d, 0x2f, 0x73, 0x75,
	0x62, 0x6d, 0x69, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x2f, 0x72, 0x65, 0x76, 0x69, 0x65, 0x77, 0x69,
	0x6e, 0x67, 0x2d, 0x70, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x12, 0x16, 0x6d, 0x61, 0x69, 0x6e, 0x63, 0x6f, 0x6e, 0x2e, 0x6d, 0x6d, 0x6d, 0x2e, 0x73, 0x75,
	0x62, 0x6d, 0x69, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x1a, 0x1f, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65,
	0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x74, 0x69, 0x6d, 0x65, 0x73, 0x74,
	0x61, 0x6d, 0x70, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x13, 0x63, 0x6f, 0x6d, 0x6d, 0x6f,
	0x6e, 0x2f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x23,
	0x6d, 0x61, 0x69, 0x6e, 0x63, 0x6f, 0x6e, 0x2f, 0x6d, 0x6d, 0x6d, 0x2f, 0x73, 0x75, 0x62, 0x6d,
	0x69, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x2f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x22, 0xdf, 0x07, 0x0a, 0x27, 0x53, 0x75, 0x62, 0x6d, 0x69, 0x73, 0x73, 0x69,
	0x6f, 0x6e, 0x52, 0x65, 0x76, 0x69, 0x65, 0x77, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x63, 0x65,
	0x73, 0x73, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12,
	0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x02, 0x69, 0x64, 0x12,
	0x6a, 0x0a, 0x33, 0x72, 0x65, 0x76, 0x69, 0x65, 0x77, 0x65, 0x72, 0x5f, 0x70, 0x6f, 0x72, 0x74,
	0x61, 0x6c, 0x5f, 0x72, 0x65, 0x76, 0x69, 0x65, 0x77, 0x69, 0x6e, 0x67, 0x5f, 0x70, 0x72, 0x6f,
	0x63, 0x65, 0x73, 0x73, 0x5f, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x5f, 0x72, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x2d, 0x72, 0x65,
	0x76, 0x69, 0x65, 0x77, 0x65, 0x72, 0x50, 0x6f, 0x72, 0x74, 0x61, 0x6c, 0x52, 0x65, 0x76, 0x69,
	0x65, 0x77, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x55, 0x70, 0x64, 0x61,
	0x74, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x49, 0x64, 0x12, 0x19, 0x0a, 0x08, 0x6d,
	0x6f, 0x64, 0x65, 0x6c, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x03, 0x52, 0x07, 0x6d,
	0x6f, 0x64, 0x65, 0x6c, 0x49, 0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x5f,
	0x74, 0x79, 0x70, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x6d, 0x6f, 0x64, 0x65,
	0x6c, 0x54, 0x79, 0x70, 0x65, 0x12, 0x1a, 0x0a, 0x08, 0x72, 0x65, 0x76, 0x69, 0x73, 0x69, 0x6f,
	0x6e, 0x18, 0x14, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x72, 0x65, 0x76, 0x69, 0x73, 0x69, 0x6f,
	0x6e, 0x12, 0x34, 0x0a, 0x16, 0x73, 0x75, 0x62, 0x6d, 0x69, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x5f,
	0x72, 0x65, 0x76, 0x69, 0x73, 0x69, 0x6f, 0x6e, 0x5f, 0x69, 0x64, 0x18, 0x05, 0x20, 0x01, 0x28,
	0x03, 0x52, 0x14, 0x73, 0x75, 0x62, 0x6d, 0x69, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x76,
	0x69, 0x73, 0x69, 0x6f, 0x6e, 0x49, 0x64, 0x12, 0x25, 0x0a, 0x0e, 0x72, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x65, 0x72, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x0d, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x65, 0x72, 0x4e, 0x61, 0x6d, 0x65, 0x12, 0x27,
	0x0a, 0x0f, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x65, 0x72, 0x5f, 0x65, 0x6d, 0x61, 0x69,
	0x6c, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0e, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x65, 0x72, 0x45, 0x6d, 0x61, 0x69, 0x6c, 0x12, 0x2b, 0x0a, 0x11, 0x72, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x65, 0x72, 0x5f, 0x63, 0x6f, 0x6e, 0x74, 0x61, 0x63, 0x74, 0x18, 0x08, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x10, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x65, 0x72, 0x43, 0x6f, 0x6e,
	0x74, 0x61, 0x63, 0x74, 0x12, 0x21, 0x0a, 0x0c, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x65,
	0x64, 0x5f, 0x62, 0x79, 0x18, 0x09, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0b, 0x72, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x65, 0x64, 0x42, 0x79, 0x12, 0x3d, 0x0a, 0x0c, 0x72, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a, 0x2e,
	0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e,
	0x54, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61, 0x6d, 0x70, 0x52, 0x0b, 0x72, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x41, 0x0a, 0x1d, 0x6f, 0x72, 0x69, 0x67, 0x69, 0x6e,
	0x61, 0x6c, 0x5f, 0x72, 0x65, 0x76, 0x69, 0x65, 0x77, 0x69, 0x6e, 0x67, 0x5f, 0x70, 0x72, 0x6f,
	0x63, 0x65, 0x73, 0x73, 0x5f, 0x69, 0x64, 0x18, 0x0b, 0x20, 0x01, 0x28, 0x03, 0x52, 0x1a, 0x6f,
	0x72, 0x69, 0x67, 0x69, 0x6e, 0x61, 0x6c, 0x52, 0x65, 0x76, 0x69, 0x65, 0x77, 0x69, 0x6e, 0x67,
	0x50, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x49, 0x64, 0x12, 0x16, 0x0a, 0x06, 0x72, 0x65, 0x6d,
	0x61, 0x72, 0x6b, 0x18, 0x0c, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x72, 0x65, 0x6d, 0x61, 0x72,
	0x6b, 0x12, 0x41, 0x0a, 0x1d, 0x72, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x5f, 0x72, 0x65,
	0x76, 0x69, 0x65, 0x77, 0x69, 0x6e, 0x67, 0x5f, 0x70, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x5f,
	0x69, 0x64, 0x18, 0x0d, 0x20, 0x01, 0x28, 0x03, 0x52, 0x1a, 0x72, 0x65, 0x73, 0x70, 0x6f, 0x6e,
	0x73, 0x65, 0x52, 0x65, 0x76, 0x69, 0x65, 0x77, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x63, 0x65,
	0x73, 0x73, 0x49, 0x64, 0x12, 0x27, 0x0a, 0x0f, 0x72, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x5f, 0x72, 0x65, 0x6d, 0x61, 0x72, 0x6b, 0x18, 0x0e, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0e, 0x72,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x52, 0x65, 0x6d, 0x61, 0x72, 0x6b, 0x12, 0x21, 0x0a,
	0x0c, 0x72, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x64, 0x65, 0x64, 0x5f, 0x62, 0x79, 0x18, 0x0f, 0x20,
	0x01, 0x28, 0x03, 0x52, 0x0b, 0x72, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x64, 0x65, 0x64, 0x42, 0x79,
	0x12, 0x3d, 0x0a, 0x0c, 0x72, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x64, 0x65, 0x64, 0x5f, 0x61, 0x74,
	0x18, 0x10, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61,
	0x6d, 0x70, 0x52, 0x0b, 0x72, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x64, 0x65, 0x64, 0x41, 0x74, 0x12,
	0x16, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x11, 0x20, 0x01, 0x28, 0x05, 0x52,
	0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x45, 0x0a, 0x1f, 0x6f, 0x72, 0x69, 0x67, 0x69,
	0x6e, 0x61, 0x6c, 0x5f, 0x72, 0x65, 0x76, 0x69, 0x65, 0x77, 0x69, 0x6e, 0x67, 0x5f, 0x70, 0x72,
	0x6f, 0x63, 0x65, 0x73, 0x73, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x12, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x1c, 0x6f, 0x72, 0x69, 0x67, 0x69, 0x6e, 0x61, 0x6c, 0x52, 0x65, 0x76, 0x69, 0x65, 0x77,
	0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x4e, 0x61, 0x6d, 0x65, 0x12, 0x45,
	0x0a, 0x1f, 0x72, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x5f, 0x72, 0x65, 0x76, 0x69, 0x65,
	0x77, 0x69, 0x6e, 0x67, 0x5f, 0x70, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x5f, 0x6e, 0x61, 0x6d,
	0x65, 0x18, 0x13, 0x20, 0x01, 0x28, 0x09, 0x52, 0x1c, 0x72, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73,
	0x65, 0x52, 0x65, 0x76, 0x69, 0x65, 0x77, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x63, 0x65, 0x73,
	0x73, 0x4e, 0x61, 0x6d, 0x65, 0x22, 0xac, 0x01, 0x0a, 0x36, 0x47, 0x65, 0x74, 0x53, 0x75, 0x62,
	0x6d, 0x69, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x76, 0x69, 0x65, 0x77, 0x69, 0x6e, 0x67,
	0x50, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x12, 0x1d, 0x0a, 0x0a, 0x73, 0x68, 0x6f, 0x77, 0x5f, 0x61, 0x6c, 0x65, 0x72, 0x74, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x08, 0x52, 0x09, 0x73, 0x68, 0x6f, 0x77, 0x41, 0x6c, 0x65, 0x72, 0x74, 0x12,
	0x53, 0x0a, 0x04, 0x64, 0x61, 0x74, 0x61, 0x18, 0x02, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x3f, 0x2e,
	0x6d, 0x61, 0x69, 0x6e, 0x63, 0x6f, 0x6e, 0x2e, 0x6d, 0x6d, 0x6d, 0x2e, 0x73, 0x75, 0x62, 0x6d,
	0x69, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x2e, 0x53, 0x75, 0x62, 0x6d, 0x69, 0x73, 0x73, 0x69, 0x6f,
	0x6e, 0x52, 0x65, 0x76, 0x69, 0x65, 0x77, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x63, 0x65, 0x73,
	0x73, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x52, 0x04,
	0x64, 0x61, 0x74, 0x61, 0x22, 0x99, 0x02, 0x0a, 0x30, 0x53, 0x65, 0x6c, 0x65, 0x63, 0x74, 0x4e,
	0x65, 0x77, 0x52, 0x65, 0x76, 0x69, 0x65, 0x77, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x63, 0x65,
	0x73, 0x73, 0x46, 0x6f, 0x72, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x24, 0x0a, 0x04, 0x6c, 0x61, 0x6e,
	0x67, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x10, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e,
	0x2e, 0x4c, 0x61, 0x6e, 0x67, 0x75, 0x61, 0x67, 0x65, 0x52, 0x04, 0x6c, 0x61, 0x6e, 0x67, 0x12,
	0x53, 0x0a, 0x27, 0x73, 0x6d, 0x6d, 0x5f, 0x72, 0x65, 0x76, 0x69, 0x65, 0x77, 0x69, 0x6e, 0x67,
	0x5f, 0x70, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x5f, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x5f,
	0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03,
	0x52, 0x22, 0x73, 0x6d, 0x6d, 0x52, 0x65, 0x76, 0x69, 0x65, 0x77, 0x69, 0x6e, 0x67, 0x50, 0x72,
	0x6f, 0x63, 0x65, 0x73, 0x73, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x49, 0x64, 0x12, 0x41, 0x0a, 0x1d, 0x72, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x5f, 0x72, 0x65, 0x76, 0x69, 0x65, 0x77, 0x69, 0x6e, 0x67, 0x5f, 0x70, 0x72, 0x6f, 0x63, 0x65,
	0x73, 0x73, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x03, 0x52, 0x1a, 0x72, 0x65, 0x73,
	0x70, 0x6f, 0x6e, 0x73, 0x65, 0x52, 0x65, 0x76, 0x69, 0x65, 0x77, 0x69, 0x6e, 0x67, 0x50, 0x72,
	0x6f, 0x63, 0x65, 0x73, 0x73, 0x49, 0x64, 0x12, 0x27, 0x0a, 0x0f, 0x72, 0x65, 0x73, 0x70, 0x6f,
	0x6e, 0x73, 0x65, 0x5f, 0x72, 0x65, 0x6d, 0x61, 0x72, 0x6b, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x0e, 0x72, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x52, 0x65, 0x6d, 0x61, 0x72, 0x6b,
	0x22, 0x90, 0x01, 0x0a, 0x32, 0x43, 0x68, 0x65, 0x63, 0x6b, 0x41, 0x6e, 0x64, 0x43, 0x61, 0x6e,
	0x63, 0x65, 0x6c, 0x52, 0x65, 0x76, 0x69, 0x65, 0x77, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x63,
	0x65, 0x73, 0x73, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x24, 0x0a, 0x04, 0x6c, 0x61, 0x6e, 0x67, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x10, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x4c,
	0x61, 0x6e, 0x67, 0x75, 0x61, 0x67, 0x65, 0x52, 0x04, 0x6c, 0x61, 0x6e, 0x67, 0x12, 0x34, 0x0a,
	0x16, 0x73, 0x75, 0x62, 0x6d, 0x69, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x5f, 0x72, 0x65, 0x76, 0x69,
	0x73, 0x69, 0x6f, 0x6e, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x14, 0x73,
	0x75, 0x62, 0x6d, 0x69, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x76, 0x69, 0x73, 0x69, 0x6f,
	0x6e, 0x49, 0x64, 0x22, 0x98, 0x01, 0x0a, 0x33, 0x43, 0x68, 0x65, 0x63, 0x6b, 0x41, 0x6e, 0x64,
	0x43, 0x61, 0x6e, 0x63, 0x65, 0x6c, 0x52, 0x65, 0x76, 0x69, 0x65, 0x77, 0x69, 0x6e, 0x67, 0x50,
	0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x61, 0x0a, 0x2e, 0x73,
	0x75, 0x62, 0x6d, 0x69, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x5f, 0x72, 0x65, 0x76, 0x69, 0x65, 0x77,
	0x69, 0x6e, 0x67, 0x5f, 0x70, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x5f, 0x75, 0x70, 0x64, 0x61,
	0x74, 0x65, 0x5f, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20,
	0x03, 0x28, 0x03, 0x52, 0x29, 0x73, 0x75, 0x62, 0x6d, 0x69, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x52,
	0x65, 0x76, 0x69, 0x65, 0x77, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x55,
	0x70, 0x64, 0x61, 0x74, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x49, 0x64, 0x32, 0xba,
	0x04, 0x0a, 0x17, 0x52, 0x65, 0x76, 0x69, 0x65, 0x77, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x63,
	0x65, 0x73, 0x73, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0xb5, 0x01, 0x0a, 0x2e, 0x47,
	0x65, 0x74, 0x53, 0x75, 0x62, 0x6d, 0x69, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x76, 0x69,
	0x65, 0x77, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x55, 0x70, 0x64, 0x61,
	0x74, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x12, 0x33, 0x2e,
	0x6d, 0x61, 0x69, 0x6e, 0x63, 0x6f, 0x6e, 0x2e, 0x6d, 0x6d, 0x6d, 0x2e, 0x73, 0x75, 0x62, 0x6d,
	0x69, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x2e, 0x53, 0x75, 0x62, 0x6d, 0x69, 0x73, 0x73, 0x69, 0x6f,
	0x6e, 0x52, 0x65, 0x76, 0x69, 0x73, 0x69, 0x6f, 0x6e, 0x49, 0x64, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x1a, 0x4e, 0x2e, 0x6d, 0x61, 0x69, 0x6e, 0x63, 0x6f, 0x6e, 0x2e, 0x6d, 0x6d, 0x6d,
	0x2e, 0x73, 0x75, 0x62, 0x6d, 0x69, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x2e, 0x47, 0x65, 0x74, 0x53,
	0x75, 0x62, 0x6d, 0x69, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x76, 0x69, 0x65, 0x77, 0x69,
	0x6e, 0x67, 0x50, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e,
	0x73, 0x65, 0x12, 0x9d, 0x01, 0x0a, 0x29, 0x53, 0x65, 0x6c, 0x65, 0x63, 0x74, 0x4e, 0x65, 0x77,
	0x52, 0x65, 0x76, 0x69, 0x65, 0x77, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73,
	0x46, 0x6f, 0x72, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x12, 0x48, 0x2e, 0x6d, 0x61, 0x69, 0x6e, 0x63, 0x6f, 0x6e, 0x2e, 0x6d, 0x6d, 0x6d, 0x2e, 0x73,
	0x75, 0x62, 0x6d, 0x69, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x2e, 0x53, 0x65, 0x6c, 0x65, 0x63, 0x74,
	0x4e, 0x65, 0x77, 0x52, 0x65, 0x76, 0x69, 0x65, 0x77, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x63,
	0x65, 0x73, 0x73, 0x46, 0x6f, 0x72, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x26, 0x2e, 0x6d, 0x61, 0x69,
	0x6e, 0x63, 0x6f, 0x6e, 0x2e, 0x6d, 0x6d, 0x6d, 0x2e, 0x73, 0x75, 0x62, 0x6d, 0x69, 0x73, 0x73,
	0x69, 0x6f, 0x6e, 0x2e, 0x53, 0x69, 0x6d, 0x70, 0x6c, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e,
	0x73, 0x65, 0x12, 0xc6, 0x01, 0x0a, 0x2b, 0x43, 0x68, 0x65, 0x63, 0x6b, 0x41, 0x6e, 0x64, 0x43,
	0x61, 0x6e, 0x63, 0x65, 0x6c, 0x52, 0x65, 0x76, 0x69, 0x65, 0x77, 0x69, 0x6e, 0x67, 0x50, 0x72,
	0x6f, 0x63, 0x65, 0x73, 0x73, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x12, 0x4a, 0x2e, 0x6d, 0x61, 0x69, 0x6e, 0x63, 0x6f, 0x6e, 0x2e, 0x6d, 0x6d, 0x6d,
	0x2e, 0x73, 0x75, 0x62, 0x6d, 0x69, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x2e, 0x43, 0x68, 0x65, 0x63,
	0x6b, 0x41, 0x6e, 0x64, 0x43, 0x61, 0x6e, 0x63, 0x65, 0x6c, 0x52, 0x65, 0x76, 0x69, 0x65, 0x77,
	0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x4b,
	0x2e, 0x6d, 0x61, 0x69, 0x6e, 0x63, 0x6f, 0x6e, 0x2e, 0x6d, 0x6d, 0x6d, 0x2e, 0x73, 0x75, 0x62,
	0x6d, 0x69, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x2e, 0x43, 0x68, 0x65, 0x63, 0x6b, 0x41, 0x6e, 0x64,
	0x43, 0x61, 0x6e, 0x63, 0x65, 0x6c, 0x52, 0x65, 0x76, 0x69, 0x65, 0x77, 0x69, 0x6e, 0x67, 0x50,
	0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x42, 0x1a, 0x5a, 0x18, 0x6d,
	0x61, 0x69, 0x6e, 0x63, 0x6f, 0x6e, 0x2f, 0x6d, 0x6d, 0x6d, 0x2f, 0x73, 0x75, 0x62, 0x6d, 0x69,
	0x73, 0x73, 0x69, 0x6f, 0x6e, 0x70, 0x62, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_maincon_mmm_submission_reviewing_process_proto_rawDescOnce sync.Once
	file_maincon_mmm_submission_reviewing_process_proto_rawDescData = file_maincon_mmm_submission_reviewing_process_proto_rawDesc
)

func file_maincon_mmm_submission_reviewing_process_proto_rawDescGZIP() []byte {
	file_maincon_mmm_submission_reviewing_process_proto_rawDescOnce.Do(func() {
		file_maincon_mmm_submission_reviewing_process_proto_rawDescData = protoimpl.X.CompressGZIP(file_maincon_mmm_submission_reviewing_process_proto_rawDescData)
	})
	return file_maincon_mmm_submission_reviewing_process_proto_rawDescData
}

var file_maincon_mmm_submission_reviewing_process_proto_msgTypes = make([]protoimpl.MessageInfo, 5)
var file_maincon_mmm_submission_reviewing_process_proto_goTypes = []interface{}{
	(*SubmissionReviewingProcessUpdateRequest)(nil),                // 0: maincon.mmm.submission.SubmissionReviewingProcessUpdateRequest
	(*GetSubmissionReviewingProcessUpdateRequestListResponse)(nil), // 1: maincon.mmm.submission.GetSubmissionReviewingProcessUpdateRequestListResponse
	(*SelectNewReviewingProcessForUpdateRequestRequest)(nil),       // 2: maincon.mmm.submission.SelectNewReviewingProcessForUpdateRequestRequest
	(*CheckAndCancelReviewingProcessUpdateRequestRequest)(nil),     // 3: maincon.mmm.submission.CheckAndCancelReviewingProcessUpdateRequestRequest
	(*CheckAndCancelReviewingProcessUpdateRequestResponse)(nil),    // 4: maincon.mmm.submission.CheckAndCancelReviewingProcessUpdateRequestResponse
	(*timestamppb.Timestamp)(nil),                                  // 5: google.protobuf.Timestamp
	(commonpb.Language)(0),                                         // 6: common.Language
	(*SubmissionRevisionIdRequest)(nil),                            // 7: maincon.mmm.submission.SubmissionRevisionIdRequest
	(*SimpleResponse)(nil),                                         // 8: maincon.mmm.submission.SimpleResponse
}
var file_maincon_mmm_submission_reviewing_process_proto_depIdxs = []int32{
	5, // 0: maincon.mmm.submission.SubmissionReviewingProcessUpdateRequest.requested_at:type_name -> google.protobuf.Timestamp
	5, // 1: maincon.mmm.submission.SubmissionReviewingProcessUpdateRequest.responded_at:type_name -> google.protobuf.Timestamp
	0, // 2: maincon.mmm.submission.GetSubmissionReviewingProcessUpdateRequestListResponse.data:type_name -> maincon.mmm.submission.SubmissionReviewingProcessUpdateRequest
	6, // 3: maincon.mmm.submission.SelectNewReviewingProcessForUpdateRequestRequest.lang:type_name -> common.Language
	6, // 4: maincon.mmm.submission.CheckAndCancelReviewingProcessUpdateRequestRequest.lang:type_name -> common.Language
	7, // 5: maincon.mmm.submission.ReviewingProcessService.GetSubmissionReviewingProcessUpdateRequestList:input_type -> maincon.mmm.submission.SubmissionRevisionIdRequest
	2, // 6: maincon.mmm.submission.ReviewingProcessService.SelectNewReviewingProcessForUpdateRequest:input_type -> maincon.mmm.submission.SelectNewReviewingProcessForUpdateRequestRequest
	3, // 7: maincon.mmm.submission.ReviewingProcessService.CheckAndCancelReviewingProcessUpdateRequest:input_type -> maincon.mmm.submission.CheckAndCancelReviewingProcessUpdateRequestRequest
	1, // 8: maincon.mmm.submission.ReviewingProcessService.GetSubmissionReviewingProcessUpdateRequestList:output_type -> maincon.mmm.submission.GetSubmissionReviewingProcessUpdateRequestListResponse
	8, // 9: maincon.mmm.submission.ReviewingProcessService.SelectNewReviewingProcessForUpdateRequest:output_type -> maincon.mmm.submission.SimpleResponse
	4, // 10: maincon.mmm.submission.ReviewingProcessService.CheckAndCancelReviewingProcessUpdateRequest:output_type -> maincon.mmm.submission.CheckAndCancelReviewingProcessUpdateRequestResponse
	8, // [8:11] is the sub-list for method output_type
	5, // [5:8] is the sub-list for method input_type
	5, // [5:5] is the sub-list for extension type_name
	5, // [5:5] is the sub-list for extension extendee
	0, // [0:5] is the sub-list for field type_name
}

func init() { file_maincon_mmm_submission_reviewing_process_proto_init() }
func file_maincon_mmm_submission_reviewing_process_proto_init() {
	if File_maincon_mmm_submission_reviewing_process_proto != nil {
		return
	}
	file_maincon_mmm_submission_common_proto_init()
	if !protoimpl.UnsafeEnabled {
		file_maincon_mmm_submission_reviewing_process_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SubmissionReviewingProcessUpdateRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_maincon_mmm_submission_reviewing_process_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetSubmissionReviewingProcessUpdateRequestListResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_maincon_mmm_submission_reviewing_process_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SelectNewReviewingProcessForUpdateRequestRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_maincon_mmm_submission_reviewing_process_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CheckAndCancelReviewingProcessUpdateRequestRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_maincon_mmm_submission_reviewing_process_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CheckAndCancelReviewingProcessUpdateRequestResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_maincon_mmm_submission_reviewing_process_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   5,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_maincon_mmm_submission_reviewing_process_proto_goTypes,
		DependencyIndexes: file_maincon_mmm_submission_reviewing_process_proto_depIdxs,
		MessageInfos:      file_maincon_mmm_submission_reviewing_process_proto_msgTypes,
	}.Build()
	File_maincon_mmm_submission_reviewing_process_proto = out.File
	file_maincon_mmm_submission_reviewing_process_proto_rawDesc = nil
	file_maincon_mmm_submission_reviewing_process_proto_goTypes = nil
	file_maincon_mmm_submission_reviewing_process_proto_depIdxs = nil
}
