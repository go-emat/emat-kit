#!/bin/bash -e

#protoc-gen-go-grpc v1.0.1
#protoc-gen-go v1.25.0
#protoc v3.14.0

#PATH=$PATH:$GOPATH/bin

protodir=../protobuf

#ematkit=$GOPATH/src/emat-kit/
#Emat Common
protoc -I $protodir $protodir/common/*.proto --go_out=../ --go-grpc_out=../

#Emat Logstash
protoc -I $protodir $protodir/emat/logstash/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/

#Emat Rating
protoc -I $protodir $protodir/emat/rating/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/

#Emat Setting
protoc --experimental_allow_proto3_optional -I $protodir $protodir/emat/setting/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/

#Main Contractor TMM Service
protoc -I $protodir $protodir/maincon/tmm/document/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/
#
protoc -I $protodir $protodir/maincon/tmm/middleware/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/
#
protoc -I $protodir $protodir/maincon/tmm/mmm/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/

#Main Contractor MMM Service
protoc --experimental_allow_proto3_optional -I $protodir $protodir/maincon/mmm/general/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/
#
protoc --experimental_allow_proto3_optional -I $protodir $protodir/maincon/mmm/material/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/
#
protoc --experimental_allow_proto3_optional -I $protodir $protodir/maincon/mmm/submission/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/
#
protoc --experimental_allow_proto3_optional -I $protodir $protodir/maincon/mmm/mmmSetting/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/

#Main Contractor Setting Service
protoc -I $protodir $protodir/maincon/setting/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/

#Main Contractor Micro Service
protoc -I $protodir $protodir/maincon/micro/docassignee/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/

protoc -I $protodir $protodir/maincon/micro/itemstatus/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/

protoc -I $protodir $protodir/maincon/micro/etcd/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/

protoc -I $protodir $protodir/maincon/micro/relation/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/


#SubContrator Service
protoc -I $protodir $protodir/subcon/general/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/

protoc -I $protodir $protodir/subcon/setting/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/

protoc -I $protodir $protodir/subcon/material/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/

protoc -I $protodir $protodir/subcon/document/*.proto  \
--go_opt=Msubcon/setting/payment_term.proto=gitlab.com/go-emat/emat-kit/api/subcon/settingpb \
--go_opt=Msubcon/setting/country.proto=gitlab.com/go-emat/emat-kit/api/subcon/settingpb \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go-grpc_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/

#Reviewer Service
protoc --experimental_allow_proto3_optional -I $protodir $protodir/reviewer/edms/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/

protoc --experimental_allow_proto3_optional -I $protodir $protodir/reviewer/general/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_opt=Mreviewer/setting/user.proto=gitlab.com/go-emat/emat-kit/api/reviewer/settingpb \
--go_out=../api/ --go-grpc_out=../api/

protoc --experimental_allow_proto3_optional -I $protodir $protodir/reviewer/setting/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/

protoc --experimental_allow_proto3_optional -I $protodir $protodir/reviewer/submission/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_opt=Mreviewer/setting/submission_purpose.proto=gitlab.com/go-emat/emat-kit/api/reviewer/settingpb \
--go_opt=Mreviewer/setting/reviewer.proto=gitlab.com/go-emat/emat-kit/api/reviewer/settingpb \
--go_opt=Mreviewer/setting/common.proto=gitlab.com/go-emat/emat-kit/api/reviewer/settingpb \
--go-grpc_opt=Mreviewer/setting/submission_purpose.proto=gitlab.com/go-emat/emat-kit/api/reviewer/settingpb \
--go_out=../api/ --go-grpc_out=../api/

#Supplier Portal Service
protoc -I $protodir $protodir/supplier/general/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/

protoc -I $protodir $protodir/supplier/setting/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/

protoc -I $protodir $protodir/supplier/product/*.proto  \
--go_opt=Msupplier/setting/country.proto=gitlab.com/go-emat/emat-kit/api/supplier/settingpb \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/

protoc -I $protodir $protodir/supplier/document/*.proto  \
--go_opt=Msupplier/setting/payment_term.proto=gitlab.com/go-emat/emat-kit/api/supplier/settingpb \
--go_opt=Msupplier/setting/country.proto=gitlab.com/go-emat/emat-kit/api/supplier/settingpb \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go-grpc_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/

protoc -I $protodir $protodir/supplier/mdm/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_opt=Msupplier/product/category.proto=gitlab.com/go-emat/emat-kit/api/supplier/productpb \
--go-grpc_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/

#Mattex TMS Service
protoc -I $protodir $protodir/tms/document/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/

#Mattex VSM Service
protoc -I $protodir $protodir/vsm/document/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/

protoc -I $protodir $protodir/vsm/setting/*.proto  \
--go_opt=Mcommon/common.proto=gitlab.com/go-emat/emat-kit/api/commonpb \
--go_out=../api/ --go-grpc_out=../api/