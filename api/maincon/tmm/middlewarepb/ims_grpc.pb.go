// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package middlewarepb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// ImsServiceClient is the client API for ImsService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type ImsServiceClient interface {
	PostDailyRecordToImsCall(ctx context.Context, in *PostDailyRecordToImsCallRequest, opts ...grpc.CallOption) (*PostDailyRecordToImsCallResponse, error)
	ProcessImsGrActionCall(ctx context.Context, in *ProcessImsGrActionCallRequest, opts ...grpc.CallOption) (*ProcessImsGrActionCallResponse, error)
	VerifyNeedPush2ImsCall(ctx context.Context, in *VerifyNeedPush2ImsCallRequest, opts ...grpc.CallOption) (*VerifyNeedPush2ImsCallResponse, error)
	GetImsProductCategoryCall(ctx context.Context, in *GetImsProductCategoryCallRequest, opts ...grpc.CallOption) (*GetImsProductCategoryCallResponse, error)
}

type imsServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewImsServiceClient(cc grpc.ClientConnInterface) ImsServiceClient {
	return &imsServiceClient{cc}
}

func (c *imsServiceClient) PostDailyRecordToImsCall(ctx context.Context, in *PostDailyRecordToImsCallRequest, opts ...grpc.CallOption) (*PostDailyRecordToImsCallResponse, error) {
	out := new(PostDailyRecordToImsCallResponse)
	err := c.cc.Invoke(ctx, "/maincon.tmm.imsService/PostDailyRecordToImsCall", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *imsServiceClient) ProcessImsGrActionCall(ctx context.Context, in *ProcessImsGrActionCallRequest, opts ...grpc.CallOption) (*ProcessImsGrActionCallResponse, error) {
	out := new(ProcessImsGrActionCallResponse)
	err := c.cc.Invoke(ctx, "/maincon.tmm.imsService/ProcessImsGrActionCall", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *imsServiceClient) VerifyNeedPush2ImsCall(ctx context.Context, in *VerifyNeedPush2ImsCallRequest, opts ...grpc.CallOption) (*VerifyNeedPush2ImsCallResponse, error) {
	out := new(VerifyNeedPush2ImsCallResponse)
	err := c.cc.Invoke(ctx, "/maincon.tmm.imsService/VerifyNeedPush2ImsCall", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *imsServiceClient) GetImsProductCategoryCall(ctx context.Context, in *GetImsProductCategoryCallRequest, opts ...grpc.CallOption) (*GetImsProductCategoryCallResponse, error) {
	out := new(GetImsProductCategoryCallResponse)
	err := c.cc.Invoke(ctx, "/maincon.tmm.imsService/GetImsProductCategoryCall", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ImsServiceServer is the server API for ImsService service.
// All implementations must embed UnimplementedImsServiceServer
// for forward compatibility
type ImsServiceServer interface {
	PostDailyRecordToImsCall(context.Context, *PostDailyRecordToImsCallRequest) (*PostDailyRecordToImsCallResponse, error)
	ProcessImsGrActionCall(context.Context, *ProcessImsGrActionCallRequest) (*ProcessImsGrActionCallResponse, error)
	VerifyNeedPush2ImsCall(context.Context, *VerifyNeedPush2ImsCallRequest) (*VerifyNeedPush2ImsCallResponse, error)
	GetImsProductCategoryCall(context.Context, *GetImsProductCategoryCallRequest) (*GetImsProductCategoryCallResponse, error)
	mustEmbedUnimplementedImsServiceServer()
}

// UnimplementedImsServiceServer must be embedded to have forward compatible implementations.
type UnimplementedImsServiceServer struct {
}

func (UnimplementedImsServiceServer) PostDailyRecordToImsCall(context.Context, *PostDailyRecordToImsCallRequest) (*PostDailyRecordToImsCallResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method PostDailyRecordToImsCall not implemented")
}
func (UnimplementedImsServiceServer) ProcessImsGrActionCall(context.Context, *ProcessImsGrActionCallRequest) (*ProcessImsGrActionCallResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ProcessImsGrActionCall not implemented")
}
func (UnimplementedImsServiceServer) VerifyNeedPush2ImsCall(context.Context, *VerifyNeedPush2ImsCallRequest) (*VerifyNeedPush2ImsCallResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method VerifyNeedPush2ImsCall not implemented")
}
func (UnimplementedImsServiceServer) GetImsProductCategoryCall(context.Context, *GetImsProductCategoryCallRequest) (*GetImsProductCategoryCallResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetImsProductCategoryCall not implemented")
}
func (UnimplementedImsServiceServer) mustEmbedUnimplementedImsServiceServer() {}

// UnsafeImsServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to ImsServiceServer will
// result in compilation errors.
type UnsafeImsServiceServer interface {
	mustEmbedUnimplementedImsServiceServer()
}

func RegisterImsServiceServer(s grpc.ServiceRegistrar, srv ImsServiceServer) {
	s.RegisterService(&_ImsService_serviceDesc, srv)
}

func _ImsService_PostDailyRecordToImsCall_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(PostDailyRecordToImsCallRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ImsServiceServer).PostDailyRecordToImsCall(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.tmm.imsService/PostDailyRecordToImsCall",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ImsServiceServer).PostDailyRecordToImsCall(ctx, req.(*PostDailyRecordToImsCallRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ImsService_ProcessImsGrActionCall_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ProcessImsGrActionCallRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ImsServiceServer).ProcessImsGrActionCall(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.tmm.imsService/ProcessImsGrActionCall",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ImsServiceServer).ProcessImsGrActionCall(ctx, req.(*ProcessImsGrActionCallRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ImsService_VerifyNeedPush2ImsCall_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(VerifyNeedPush2ImsCallRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ImsServiceServer).VerifyNeedPush2ImsCall(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.tmm.imsService/VerifyNeedPush2ImsCall",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ImsServiceServer).VerifyNeedPush2ImsCall(ctx, req.(*VerifyNeedPush2ImsCallRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ImsService_GetImsProductCategoryCall_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetImsProductCategoryCallRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ImsServiceServer).GetImsProductCategoryCall(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.tmm.imsService/GetImsProductCategoryCall",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ImsServiceServer).GetImsProductCategoryCall(ctx, req.(*GetImsProductCategoryCallRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _ImsService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "maincon.tmm.imsService",
	HandlerType: (*ImsServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "PostDailyRecordToImsCall",
			Handler:    _ImsService_PostDailyRecordToImsCall_Handler,
		},
		{
			MethodName: "ProcessImsGrActionCall",
			Handler:    _ImsService_ProcessImsGrActionCall_Handler,
		},
		{
			MethodName: "VerifyNeedPush2ImsCall",
			Handler:    _ImsService_VerifyNeedPush2ImsCall_Handler,
		},
		{
			MethodName: "GetImsProductCategoryCall",
			Handler:    _ImsService_GetImsProductCategoryCall_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "maincon/tmm/middleware/ims.proto",
}
