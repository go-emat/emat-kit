// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.25.0
// 	protoc        v3.14.0
// source: supplier/setting/exchange_rate.proto

package settingpb

import (
	proto "github.com/golang/protobuf/proto"
	commonpb "gitlab.com/go-emat/emat-kit/api/commonpb"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type ExchangeRate struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id               int64           `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	FromCurrency     string          `protobuf:"bytes,2,opt,name=from_currency,json=fromCurrency,proto3" json:"from_currency,omitempty"`
	FromCurrencyDesc string          `protobuf:"bytes,3,opt,name=from_currency_desc,json=fromCurrencyDesc,proto3" json:"from_currency_desc,omitempty"`
	ToCurrency       string          `protobuf:"bytes,4,opt,name=to_currency,json=toCurrency,proto3" json:"to_currency,omitempty"`
	ToCurrencyDesc   string          `protobuf:"bytes,5,opt,name=to_currency_desc,json=toCurrencyDesc,proto3" json:"to_currency_desc,omitempty"`
	Rate             float64         `protobuf:"fixed64,6,opt,name=rate,proto3" json:"rate,omitempty"`
	Audit            *commonpb.Audit `protobuf:"bytes,7,opt,name=audit,proto3" json:"audit,omitempty"`
	Creator          string          `protobuf:"bytes,8,opt,name=creator,proto3" json:"creator,omitempty"`
}

func (x *ExchangeRate) Reset() {
	*x = ExchangeRate{}
	if protoimpl.UnsafeEnabled {
		mi := &file_supplier_setting_exchange_rate_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ExchangeRate) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ExchangeRate) ProtoMessage() {}

func (x *ExchangeRate) ProtoReflect() protoreflect.Message {
	mi := &file_supplier_setting_exchange_rate_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ExchangeRate.ProtoReflect.Descriptor instead.
func (*ExchangeRate) Descriptor() ([]byte, []int) {
	return file_supplier_setting_exchange_rate_proto_rawDescGZIP(), []int{0}
}

func (x *ExchangeRate) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *ExchangeRate) GetFromCurrency() string {
	if x != nil {
		return x.FromCurrency
	}
	return ""
}

func (x *ExchangeRate) GetFromCurrencyDesc() string {
	if x != nil {
		return x.FromCurrencyDesc
	}
	return ""
}

func (x *ExchangeRate) GetToCurrency() string {
	if x != nil {
		return x.ToCurrency
	}
	return ""
}

func (x *ExchangeRate) GetToCurrencyDesc() string {
	if x != nil {
		return x.ToCurrencyDesc
	}
	return ""
}

func (x *ExchangeRate) GetRate() float64 {
	if x != nil {
		return x.Rate
	}
	return 0
}

func (x *ExchangeRate) GetAudit() *commonpb.Audit {
	if x != nil {
		return x.Audit
	}
	return nil
}

func (x *ExchangeRate) GetCreator() string {
	if x != nil {
		return x.Creator
	}
	return ""
}

type CreateExchangeRateRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ExchangeRate *ExchangeRate     `protobuf:"bytes,1,opt,name=exchange_rate,json=exchangeRate,proto3" json:"exchange_rate,omitempty"`
	Lang         commonpb.Language `protobuf:"varint,2,opt,name=lang,proto3,enum=common.Language" json:"lang,omitempty"`
}

func (x *CreateExchangeRateRequest) Reset() {
	*x = CreateExchangeRateRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_supplier_setting_exchange_rate_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateExchangeRateRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateExchangeRateRequest) ProtoMessage() {}

func (x *CreateExchangeRateRequest) ProtoReflect() protoreflect.Message {
	mi := &file_supplier_setting_exchange_rate_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateExchangeRateRequest.ProtoReflect.Descriptor instead.
func (*CreateExchangeRateRequest) Descriptor() ([]byte, []int) {
	return file_supplier_setting_exchange_rate_proto_rawDescGZIP(), []int{1}
}

func (x *CreateExchangeRateRequest) GetExchangeRate() *ExchangeRate {
	if x != nil {
		return x.ExchangeRate
	}
	return nil
}

func (x *CreateExchangeRateRequest) GetLang() commonpb.Language {
	if x != nil {
		return x.Lang
	}
	return commonpb.Language_UNKNOWN_LANGUAGE
}

type CreateExchangeRateResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Data *ExchangeRate `protobuf:"bytes,1,opt,name=data,proto3" json:"data,omitempty"`
}

func (x *CreateExchangeRateResponse) Reset() {
	*x = CreateExchangeRateResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_supplier_setting_exchange_rate_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateExchangeRateResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateExchangeRateResponse) ProtoMessage() {}

func (x *CreateExchangeRateResponse) ProtoReflect() protoreflect.Message {
	mi := &file_supplier_setting_exchange_rate_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateExchangeRateResponse.ProtoReflect.Descriptor instead.
func (*CreateExchangeRateResponse) Descriptor() ([]byte, []int) {
	return file_supplier_setting_exchange_rate_proto_rawDescGZIP(), []int{2}
}

func (x *CreateExchangeRateResponse) GetData() *ExchangeRate {
	if x != nil {
		return x.Data
	}
	return nil
}

type ReadExchangeRateRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id   int64             `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Lang commonpb.Language `protobuf:"varint,2,opt,name=lang,proto3,enum=common.Language" json:"lang,omitempty"`
}

func (x *ReadExchangeRateRequest) Reset() {
	*x = ReadExchangeRateRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_supplier_setting_exchange_rate_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ReadExchangeRateRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ReadExchangeRateRequest) ProtoMessage() {}

func (x *ReadExchangeRateRequest) ProtoReflect() protoreflect.Message {
	mi := &file_supplier_setting_exchange_rate_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ReadExchangeRateRequest.ProtoReflect.Descriptor instead.
func (*ReadExchangeRateRequest) Descriptor() ([]byte, []int) {
	return file_supplier_setting_exchange_rate_proto_rawDescGZIP(), []int{3}
}

func (x *ReadExchangeRateRequest) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *ReadExchangeRateRequest) GetLang() commonpb.Language {
	if x != nil {
		return x.Lang
	}
	return commonpb.Language_UNKNOWN_LANGUAGE
}

type ReadExchangeRateResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Data *ExchangeRate `protobuf:"bytes,1,opt,name=data,proto3" json:"data,omitempty"`
}

func (x *ReadExchangeRateResponse) Reset() {
	*x = ReadExchangeRateResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_supplier_setting_exchange_rate_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ReadExchangeRateResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ReadExchangeRateResponse) ProtoMessage() {}

func (x *ReadExchangeRateResponse) ProtoReflect() protoreflect.Message {
	mi := &file_supplier_setting_exchange_rate_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ReadExchangeRateResponse.ProtoReflect.Descriptor instead.
func (*ReadExchangeRateResponse) Descriptor() ([]byte, []int) {
	return file_supplier_setting_exchange_rate_proto_rawDescGZIP(), []int{4}
}

func (x *ReadExchangeRateResponse) GetData() *ExchangeRate {
	if x != nil {
		return x.Data
	}
	return nil
}

type UpdateExchangeRateRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ExchangeRate *ExchangeRate     `protobuf:"bytes,1,opt,name=exchange_rate,json=exchangeRate,proto3" json:"exchange_rate,omitempty"`
	Lang         commonpb.Language `protobuf:"varint,2,opt,name=lang,proto3,enum=common.Language" json:"lang,omitempty"`
}

func (x *UpdateExchangeRateRequest) Reset() {
	*x = UpdateExchangeRateRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_supplier_setting_exchange_rate_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateExchangeRateRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateExchangeRateRequest) ProtoMessage() {}

func (x *UpdateExchangeRateRequest) ProtoReflect() protoreflect.Message {
	mi := &file_supplier_setting_exchange_rate_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateExchangeRateRequest.ProtoReflect.Descriptor instead.
func (*UpdateExchangeRateRequest) Descriptor() ([]byte, []int) {
	return file_supplier_setting_exchange_rate_proto_rawDescGZIP(), []int{5}
}

func (x *UpdateExchangeRateRequest) GetExchangeRate() *ExchangeRate {
	if x != nil {
		return x.ExchangeRate
	}
	return nil
}

func (x *UpdateExchangeRateRequest) GetLang() commonpb.Language {
	if x != nil {
		return x.Lang
	}
	return commonpb.Language_UNKNOWN_LANGUAGE
}

type UpdateExchangeRateResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Data *ExchangeRate `protobuf:"bytes,1,opt,name=data,proto3" json:"data,omitempty"`
}

func (x *UpdateExchangeRateResponse) Reset() {
	*x = UpdateExchangeRateResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_supplier_setting_exchange_rate_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateExchangeRateResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateExchangeRateResponse) ProtoMessage() {}

func (x *UpdateExchangeRateResponse) ProtoReflect() protoreflect.Message {
	mi := &file_supplier_setting_exchange_rate_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateExchangeRateResponse.ProtoReflect.Descriptor instead.
func (*UpdateExchangeRateResponse) Descriptor() ([]byte, []int) {
	return file_supplier_setting_exchange_rate_proto_rawDescGZIP(), []int{6}
}

func (x *UpdateExchangeRateResponse) GetData() *ExchangeRate {
	if x != nil {
		return x.Data
	}
	return nil
}

type DeleteExchangeRateRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id   int64             `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Lang commonpb.Language `protobuf:"varint,2,opt,name=lang,proto3,enum=common.Language" json:"lang,omitempty"`
}

func (x *DeleteExchangeRateRequest) Reset() {
	*x = DeleteExchangeRateRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_supplier_setting_exchange_rate_proto_msgTypes[7]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DeleteExchangeRateRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DeleteExchangeRateRequest) ProtoMessage() {}

func (x *DeleteExchangeRateRequest) ProtoReflect() protoreflect.Message {
	mi := &file_supplier_setting_exchange_rate_proto_msgTypes[7]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DeleteExchangeRateRequest.ProtoReflect.Descriptor instead.
func (*DeleteExchangeRateRequest) Descriptor() ([]byte, []int) {
	return file_supplier_setting_exchange_rate_proto_rawDescGZIP(), []int{7}
}

func (x *DeleteExchangeRateRequest) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *DeleteExchangeRateRequest) GetLang() commonpb.Language {
	if x != nil {
		return x.Lang
	}
	return commonpb.Language_UNKNOWN_LANGUAGE
}

type DeleteExchangeRateResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Success bool `protobuf:"varint,1,opt,name=success,proto3" json:"success,omitempty"`
}

func (x *DeleteExchangeRateResponse) Reset() {
	*x = DeleteExchangeRateResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_supplier_setting_exchange_rate_proto_msgTypes[8]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DeleteExchangeRateResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DeleteExchangeRateResponse) ProtoMessage() {}

func (x *DeleteExchangeRateResponse) ProtoReflect() protoreflect.Message {
	mi := &file_supplier_setting_exchange_rate_proto_msgTypes[8]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DeleteExchangeRateResponse.ProtoReflect.Descriptor instead.
func (*DeleteExchangeRateResponse) Descriptor() ([]byte, []int) {
	return file_supplier_setting_exchange_rate_proto_rawDescGZIP(), []int{8}
}

func (x *DeleteExchangeRateResponse) GetSuccess() bool {
	if x != nil {
		return x.Success
	}
	return false
}

type ListExchangeRatesRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	FromCurrency      string                      `protobuf:"bytes,1,opt,name=from_currency,json=fromCurrency,proto3" json:"from_currency,omitempty"`
	ToCurrency        string                      `protobuf:"bytes,2,opt,name=to_currency,json=toCurrency,proto3" json:"to_currency,omitempty"`
	PaginationRequest *commonpb.PaginationRequest `protobuf:"bytes,4,opt,name=pagination_request,json=paginationRequest,proto3" json:"pagination_request,omitempty"`
	Lang              commonpb.Language           `protobuf:"varint,5,opt,name=lang,proto3,enum=common.Language" json:"lang,omitempty"`
}

func (x *ListExchangeRatesRequest) Reset() {
	*x = ListExchangeRatesRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_supplier_setting_exchange_rate_proto_msgTypes[9]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ListExchangeRatesRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ListExchangeRatesRequest) ProtoMessage() {}

func (x *ListExchangeRatesRequest) ProtoReflect() protoreflect.Message {
	mi := &file_supplier_setting_exchange_rate_proto_msgTypes[9]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ListExchangeRatesRequest.ProtoReflect.Descriptor instead.
func (*ListExchangeRatesRequest) Descriptor() ([]byte, []int) {
	return file_supplier_setting_exchange_rate_proto_rawDescGZIP(), []int{9}
}

func (x *ListExchangeRatesRequest) GetFromCurrency() string {
	if x != nil {
		return x.FromCurrency
	}
	return ""
}

func (x *ListExchangeRatesRequest) GetToCurrency() string {
	if x != nil {
		return x.ToCurrency
	}
	return ""
}

func (x *ListExchangeRatesRequest) GetPaginationRequest() *commonpb.PaginationRequest {
	if x != nil {
		return x.PaginationRequest
	}
	return nil
}

func (x *ListExchangeRatesRequest) GetLang() commonpb.Language {
	if x != nil {
		return x.Lang
	}
	return commonpb.Language_UNKNOWN_LANGUAGE
}

type ListExchangeRatesResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Data []*ExchangeRate         `protobuf:"bytes,1,rep,name=data,proto3" json:"data,omitempty"`
	Meta *commonpb.MataPaginator `protobuf:"bytes,2,opt,name=meta,proto3" json:"meta,omitempty"`
}

func (x *ListExchangeRatesResponse) Reset() {
	*x = ListExchangeRatesResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_supplier_setting_exchange_rate_proto_msgTypes[10]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ListExchangeRatesResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ListExchangeRatesResponse) ProtoMessage() {}

func (x *ListExchangeRatesResponse) ProtoReflect() protoreflect.Message {
	mi := &file_supplier_setting_exchange_rate_proto_msgTypes[10]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ListExchangeRatesResponse.ProtoReflect.Descriptor instead.
func (*ListExchangeRatesResponse) Descriptor() ([]byte, []int) {
	return file_supplier_setting_exchange_rate_proto_rawDescGZIP(), []int{10}
}

func (x *ListExchangeRatesResponse) GetData() []*ExchangeRate {
	if x != nil {
		return x.Data
	}
	return nil
}

func (x *ListExchangeRatesResponse) GetMeta() *commonpb.MataPaginator {
	if x != nil {
		return x.Meta
	}
	return nil
}

var File_supplier_setting_exchange_rate_proto protoreflect.FileDescriptor

var file_supplier_setting_exchange_rate_proto_rawDesc = []byte{
	0x0a, 0x24, 0x73, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72, 0x2f, 0x73, 0x65, 0x74, 0x74, 0x69,
	0x6e, 0x67, 0x2f, 0x65, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x5f, 0x72, 0x61, 0x74, 0x65,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x08, 0x73, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72,
	0x1a, 0x13, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x8f, 0x02, 0x0a, 0x0c, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e,
	0x67, 0x65, 0x52, 0x61, 0x74, 0x65, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x03, 0x52, 0x02, 0x69, 0x64, 0x12, 0x23, 0x0a, 0x0d, 0x66, 0x72, 0x6f, 0x6d, 0x5f, 0x63,
	0x75, 0x72, 0x72, 0x65, 0x6e, 0x63, 0x79, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0c, 0x66,
	0x72, 0x6f, 0x6d, 0x43, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x63, 0x79, 0x12, 0x2c, 0x0a, 0x12, 0x66,
	0x72, 0x6f, 0x6d, 0x5f, 0x63, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x63, 0x79, 0x5f, 0x64, 0x65, 0x73,
	0x63, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x10, 0x66, 0x72, 0x6f, 0x6d, 0x43, 0x75, 0x72,
	0x72, 0x65, 0x6e, 0x63, 0x79, 0x44, 0x65, 0x73, 0x63, 0x12, 0x1f, 0x0a, 0x0b, 0x74, 0x6f, 0x5f,
	0x63, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x63, 0x79, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a,
	0x74, 0x6f, 0x43, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x63, 0x79, 0x12, 0x28, 0x0a, 0x10, 0x74, 0x6f,
	0x5f, 0x63, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x63, 0x79, 0x5f, 0x64, 0x65, 0x73, 0x63, 0x18, 0x05,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x0e, 0x74, 0x6f, 0x43, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x63, 0x79,
	0x44, 0x65, 0x73, 0x63, 0x12, 0x12, 0x0a, 0x04, 0x72, 0x61, 0x74, 0x65, 0x18, 0x06, 0x20, 0x01,
	0x28, 0x01, 0x52, 0x04, 0x72, 0x61, 0x74, 0x65, 0x12, 0x23, 0x0a, 0x05, 0x61, 0x75, 0x64, 0x69,
	0x74, 0x18, 0x07, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x0d, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e,
	0x2e, 0x41, 0x75, 0x64, 0x69, 0x74, 0x52, 0x05, 0x61, 0x75, 0x64, 0x69, 0x74, 0x12, 0x18, 0x0a,
	0x07, 0x63, 0x72, 0x65, 0x61, 0x74, 0x6f, 0x72, 0x18, 0x08, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07,
	0x63, 0x72, 0x65, 0x61, 0x74, 0x6f, 0x72, 0x22, 0x7e, 0x0a, 0x19, 0x43, 0x72, 0x65, 0x61, 0x74,
	0x65, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x61, 0x74, 0x65, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x12, 0x3b, 0x0a, 0x0d, 0x65, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65,
	0x5f, 0x72, 0x61, 0x74, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x16, 0x2e, 0x73, 0x75,
	0x70, 0x70, 0x6c, 0x69, 0x65, 0x72, 0x2e, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52,
	0x61, 0x74, 0x65, 0x52, 0x0c, 0x65, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x61, 0x74,
	0x65, 0x12, 0x24, 0x0a, 0x04, 0x6c, 0x61, 0x6e, 0x67, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0e, 0x32,
	0x10, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x4c, 0x61, 0x6e, 0x67, 0x75, 0x61, 0x67,
	0x65, 0x52, 0x04, 0x6c, 0x61, 0x6e, 0x67, 0x22, 0x48, 0x0a, 0x1a, 0x43, 0x72, 0x65, 0x61, 0x74,
	0x65, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x61, 0x74, 0x65, 0x52, 0x65, 0x73,
	0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x2a, 0x0a, 0x04, 0x64, 0x61, 0x74, 0x61, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x0b, 0x32, 0x16, 0x2e, 0x73, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72, 0x2e, 0x45,
	0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x61, 0x74, 0x65, 0x52, 0x04, 0x64, 0x61, 0x74,
	0x61, 0x22, 0x4f, 0x0a, 0x17, 0x52, 0x65, 0x61, 0x64, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67,
	0x65, 0x52, 0x61, 0x74, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x0e, 0x0a, 0x02,
	0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x02, 0x69, 0x64, 0x12, 0x24, 0x0a, 0x04,
	0x6c, 0x61, 0x6e, 0x67, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x10, 0x2e, 0x63, 0x6f, 0x6d,
	0x6d, 0x6f, 0x6e, 0x2e, 0x4c, 0x61, 0x6e, 0x67, 0x75, 0x61, 0x67, 0x65, 0x52, 0x04, 0x6c, 0x61,
	0x6e, 0x67, 0x22, 0x46, 0x0a, 0x18, 0x52, 0x65, 0x61, 0x64, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e,
	0x67, 0x65, 0x52, 0x61, 0x74, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x2a,
	0x0a, 0x04, 0x64, 0x61, 0x74, 0x61, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x16, 0x2e, 0x73,
	0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72, 0x2e, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65,
	0x52, 0x61, 0x74, 0x65, 0x52, 0x04, 0x64, 0x61, 0x74, 0x61, 0x22, 0x7e, 0x0a, 0x19, 0x55, 0x70,
	0x64, 0x61, 0x74, 0x65, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x61, 0x74, 0x65,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x3b, 0x0a, 0x0d, 0x65, 0x78, 0x63, 0x68, 0x61,
	0x6e, 0x67, 0x65, 0x5f, 0x72, 0x61, 0x74, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x16,
	0x2e, 0x73, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72, 0x2e, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e,
	0x67, 0x65, 0x52, 0x61, 0x74, 0x65, 0x52, 0x0c, 0x65, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65,
	0x52, 0x61, 0x74, 0x65, 0x12, 0x24, 0x0a, 0x04, 0x6c, 0x61, 0x6e, 0x67, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x0e, 0x32, 0x10, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x4c, 0x61, 0x6e, 0x67,
	0x75, 0x61, 0x67, 0x65, 0x52, 0x04, 0x6c, 0x61, 0x6e, 0x67, 0x22, 0x48, 0x0a, 0x1a, 0x55, 0x70,
	0x64, 0x61, 0x74, 0x65, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x61, 0x74, 0x65,
	0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x2a, 0x0a, 0x04, 0x64, 0x61, 0x74, 0x61,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x16, 0x2e, 0x73, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65,
	0x72, 0x2e, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x61, 0x74, 0x65, 0x52, 0x04,
	0x64, 0x61, 0x74, 0x61, 0x22, 0x51, 0x0a, 0x19, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x45, 0x78,
	0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x61, 0x74, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x02, 0x69,
	0x64, 0x12, 0x24, 0x0a, 0x04, 0x6c, 0x61, 0x6e, 0x67, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0e, 0x32,
	0x10, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x4c, 0x61, 0x6e, 0x67, 0x75, 0x61, 0x67,
	0x65, 0x52, 0x04, 0x6c, 0x61, 0x6e, 0x67, 0x22, 0x36, 0x0a, 0x1a, 0x44, 0x65, 0x6c, 0x65, 0x74,
	0x65, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x61, 0x74, 0x65, 0x52, 0x65, 0x73,
	0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x73, 0x75, 0x63, 0x63, 0x65, 0x73, 0x73,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x08, 0x52, 0x07, 0x73, 0x75, 0x63, 0x63, 0x65, 0x73, 0x73, 0x22,
	0xd0, 0x01, 0x0a, 0x18, 0x4c, 0x69, 0x73, 0x74, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65,
	0x52, 0x61, 0x74, 0x65, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x23, 0x0a, 0x0d,
	0x66, 0x72, 0x6f, 0x6d, 0x5f, 0x63, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x63, 0x79, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x0c, 0x66, 0x72, 0x6f, 0x6d, 0x43, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x63,
	0x79, 0x12, 0x1f, 0x0a, 0x0b, 0x74, 0x6f, 0x5f, 0x63, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x63, 0x79,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x74, 0x6f, 0x43, 0x75, 0x72, 0x72, 0x65, 0x6e,
	0x63, 0x79, 0x12, 0x48, 0x0a, 0x12, 0x70, 0x61, 0x67, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e,
	0x5f, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x18, 0x04, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x19,
	0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x50, 0x61, 0x67, 0x69, 0x6e, 0x61, 0x74, 0x69,
	0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x52, 0x11, 0x70, 0x61, 0x67, 0x69, 0x6e,
	0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x24, 0x0a, 0x04,
	0x6c, 0x61, 0x6e, 0x67, 0x18, 0x05, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x10, 0x2e, 0x63, 0x6f, 0x6d,
	0x6d, 0x6f, 0x6e, 0x2e, 0x4c, 0x61, 0x6e, 0x67, 0x75, 0x61, 0x67, 0x65, 0x52, 0x04, 0x6c, 0x61,
	0x6e, 0x67, 0x22, 0x72, 0x0a, 0x19, 0x4c, 0x69, 0x73, 0x74, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e,
	0x67, 0x65, 0x52, 0x61, 0x74, 0x65, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12,
	0x2a, 0x0a, 0x04, 0x64, 0x61, 0x74, 0x61, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x16, 0x2e,
	0x73, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72, 0x2e, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67,
	0x65, 0x52, 0x61, 0x74, 0x65, 0x52, 0x04, 0x64, 0x61, 0x74, 0x61, 0x12, 0x29, 0x0a, 0x04, 0x6d,
	0x65, 0x74, 0x61, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x15, 0x2e, 0x63, 0x6f, 0x6d, 0x6d,
	0x6f, 0x6e, 0x2e, 0x4d, 0x61, 0x74, 0x61, 0x50, 0x61, 0x67, 0x69, 0x6e, 0x61, 0x74, 0x6f, 0x72,
	0x52, 0x04, 0x6d, 0x65, 0x74, 0x61, 0x32, 0xf1, 0x03, 0x0a, 0x13, 0x45, 0x78, 0x63, 0x68, 0x61,
	0x6e, 0x67, 0x65, 0x52, 0x61, 0x74, 0x65, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x5f,
	0x0a, 0x12, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65,
	0x52, 0x61, 0x74, 0x65, 0x12, 0x23, 0x2e, 0x73, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72, 0x2e,
	0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x61,
	0x74, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x24, 0x2e, 0x73, 0x75, 0x70, 0x70,
	0x6c, 0x69, 0x65, 0x72, 0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x45, 0x78, 0x63, 0x68, 0x61,
	0x6e, 0x67, 0x65, 0x52, 0x61, 0x74, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12,
	0x59, 0x0a, 0x10, 0x52, 0x65, 0x61, 0x64, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52,
	0x61, 0x74, 0x65, 0x12, 0x21, 0x2e, 0x73, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72, 0x2e, 0x52,
	0x65, 0x61, 0x64, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x61, 0x74, 0x65, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x22, 0x2e, 0x73, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65,
	0x72, 0x2e, 0x52, 0x65, 0x61, 0x64, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x61,
	0x74, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x5f, 0x0a, 0x12, 0x55, 0x70,
	0x64, 0x61, 0x74, 0x65, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x61, 0x74, 0x65,
	0x12, 0x23, 0x2e, 0x73, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72, 0x2e, 0x55, 0x70, 0x64, 0x61,
	0x74, 0x65, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x61, 0x74, 0x65, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x24, 0x2e, 0x73, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72,
	0x2e, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52,
	0x61, 0x74, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x5f, 0x0a, 0x12, 0x44,
	0x65, 0x6c, 0x65, 0x74, 0x65, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x61, 0x74,
	0x65, 0x12, 0x23, 0x2e, 0x73, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72, 0x2e, 0x44, 0x65, 0x6c,
	0x65, 0x74, 0x65, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x61, 0x74, 0x65, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x24, 0x2e, 0x73, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65,
	0x72, 0x2e, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65,
	0x52, 0x61, 0x74, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x5c, 0x0a, 0x11,
	0x4c, 0x69, 0x73, 0x74, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x61, 0x74, 0x65,
	0x73, 0x12, 0x22, 0x2e, 0x73, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72, 0x2e, 0x4c, 0x69, 0x73,
	0x74, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x61, 0x74, 0x65, 0x73, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x23, 0x2e, 0x73, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72,
	0x2e, 0x4c, 0x69, 0x73, 0x74, 0x45, 0x78, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x61, 0x74,
	0x65, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x42, 0x14, 0x5a, 0x12, 0x73, 0x75,
	0x70, 0x70, 0x6c, 0x69, 0x65, 0x72, 0x2f, 0x73, 0x65, 0x74, 0x74, 0x69, 0x6e, 0x67, 0x70, 0x62,
	0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_supplier_setting_exchange_rate_proto_rawDescOnce sync.Once
	file_supplier_setting_exchange_rate_proto_rawDescData = file_supplier_setting_exchange_rate_proto_rawDesc
)

func file_supplier_setting_exchange_rate_proto_rawDescGZIP() []byte {
	file_supplier_setting_exchange_rate_proto_rawDescOnce.Do(func() {
		file_supplier_setting_exchange_rate_proto_rawDescData = protoimpl.X.CompressGZIP(file_supplier_setting_exchange_rate_proto_rawDescData)
	})
	return file_supplier_setting_exchange_rate_proto_rawDescData
}

var file_supplier_setting_exchange_rate_proto_msgTypes = make([]protoimpl.MessageInfo, 11)
var file_supplier_setting_exchange_rate_proto_goTypes = []interface{}{
	(*ExchangeRate)(nil),               // 0: supplier.ExchangeRate
	(*CreateExchangeRateRequest)(nil),  // 1: supplier.CreateExchangeRateRequest
	(*CreateExchangeRateResponse)(nil), // 2: supplier.CreateExchangeRateResponse
	(*ReadExchangeRateRequest)(nil),    // 3: supplier.ReadExchangeRateRequest
	(*ReadExchangeRateResponse)(nil),   // 4: supplier.ReadExchangeRateResponse
	(*UpdateExchangeRateRequest)(nil),  // 5: supplier.UpdateExchangeRateRequest
	(*UpdateExchangeRateResponse)(nil), // 6: supplier.UpdateExchangeRateResponse
	(*DeleteExchangeRateRequest)(nil),  // 7: supplier.DeleteExchangeRateRequest
	(*DeleteExchangeRateResponse)(nil), // 8: supplier.DeleteExchangeRateResponse
	(*ListExchangeRatesRequest)(nil),   // 9: supplier.ListExchangeRatesRequest
	(*ListExchangeRatesResponse)(nil),  // 10: supplier.ListExchangeRatesResponse
	(*commonpb.Audit)(nil),             // 11: common.Audit
	(commonpb.Language)(0),             // 12: common.Language
	(*commonpb.PaginationRequest)(nil), // 13: common.PaginationRequest
	(*commonpb.MataPaginator)(nil),     // 14: common.MataPaginator
}
var file_supplier_setting_exchange_rate_proto_depIdxs = []int32{
	11, // 0: supplier.ExchangeRate.audit:type_name -> common.Audit
	0,  // 1: supplier.CreateExchangeRateRequest.exchange_rate:type_name -> supplier.ExchangeRate
	12, // 2: supplier.CreateExchangeRateRequest.lang:type_name -> common.Language
	0,  // 3: supplier.CreateExchangeRateResponse.data:type_name -> supplier.ExchangeRate
	12, // 4: supplier.ReadExchangeRateRequest.lang:type_name -> common.Language
	0,  // 5: supplier.ReadExchangeRateResponse.data:type_name -> supplier.ExchangeRate
	0,  // 6: supplier.UpdateExchangeRateRequest.exchange_rate:type_name -> supplier.ExchangeRate
	12, // 7: supplier.UpdateExchangeRateRequest.lang:type_name -> common.Language
	0,  // 8: supplier.UpdateExchangeRateResponse.data:type_name -> supplier.ExchangeRate
	12, // 9: supplier.DeleteExchangeRateRequest.lang:type_name -> common.Language
	13, // 10: supplier.ListExchangeRatesRequest.pagination_request:type_name -> common.PaginationRequest
	12, // 11: supplier.ListExchangeRatesRequest.lang:type_name -> common.Language
	0,  // 12: supplier.ListExchangeRatesResponse.data:type_name -> supplier.ExchangeRate
	14, // 13: supplier.ListExchangeRatesResponse.meta:type_name -> common.MataPaginator
	1,  // 14: supplier.ExchangeRateService.CreateExchangeRate:input_type -> supplier.CreateExchangeRateRequest
	3,  // 15: supplier.ExchangeRateService.ReadExchangeRate:input_type -> supplier.ReadExchangeRateRequest
	5,  // 16: supplier.ExchangeRateService.UpdateExchangeRate:input_type -> supplier.UpdateExchangeRateRequest
	7,  // 17: supplier.ExchangeRateService.DeleteExchangeRate:input_type -> supplier.DeleteExchangeRateRequest
	9,  // 18: supplier.ExchangeRateService.ListExchangeRates:input_type -> supplier.ListExchangeRatesRequest
	2,  // 19: supplier.ExchangeRateService.CreateExchangeRate:output_type -> supplier.CreateExchangeRateResponse
	4,  // 20: supplier.ExchangeRateService.ReadExchangeRate:output_type -> supplier.ReadExchangeRateResponse
	6,  // 21: supplier.ExchangeRateService.UpdateExchangeRate:output_type -> supplier.UpdateExchangeRateResponse
	8,  // 22: supplier.ExchangeRateService.DeleteExchangeRate:output_type -> supplier.DeleteExchangeRateResponse
	10, // 23: supplier.ExchangeRateService.ListExchangeRates:output_type -> supplier.ListExchangeRatesResponse
	19, // [19:24] is the sub-list for method output_type
	14, // [14:19] is the sub-list for method input_type
	14, // [14:14] is the sub-list for extension type_name
	14, // [14:14] is the sub-list for extension extendee
	0,  // [0:14] is the sub-list for field type_name
}

func init() { file_supplier_setting_exchange_rate_proto_init() }
func file_supplier_setting_exchange_rate_proto_init() {
	if File_supplier_setting_exchange_rate_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_supplier_setting_exchange_rate_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ExchangeRate); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_supplier_setting_exchange_rate_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateExchangeRateRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_supplier_setting_exchange_rate_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateExchangeRateResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_supplier_setting_exchange_rate_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ReadExchangeRateRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_supplier_setting_exchange_rate_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ReadExchangeRateResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_supplier_setting_exchange_rate_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdateExchangeRateRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_supplier_setting_exchange_rate_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdateExchangeRateResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_supplier_setting_exchange_rate_proto_msgTypes[7].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DeleteExchangeRateRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_supplier_setting_exchange_rate_proto_msgTypes[8].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DeleteExchangeRateResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_supplier_setting_exchange_rate_proto_msgTypes[9].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ListExchangeRatesRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_supplier_setting_exchange_rate_proto_msgTypes[10].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ListExchangeRatesResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_supplier_setting_exchange_rate_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   11,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_supplier_setting_exchange_rate_proto_goTypes,
		DependencyIndexes: file_supplier_setting_exchange_rate_proto_depIdxs,
		MessageInfos:      file_supplier_setting_exchange_rate_proto_msgTypes,
	}.Build()
	File_supplier_setting_exchange_rate_proto = out.File
	file_supplier_setting_exchange_rate_proto_rawDesc = nil
	file_supplier_setting_exchange_rate_proto_goTypes = nil
	file_supplier_setting_exchange_rate_proto_depIdxs = nil
}
