package migrate

import (
	"reflect"
	"strings"
	"unicode"
	"unicode/utf8"
)

var typeRegistry = make(map[string]reflect.Type)


func RegisterType(typedNil MigrateInterface) {
	t := reflect.TypeOf(typedNil)
	typeRegistry[getRegistryTypeFileName(t.Elem().Name())] = t
}

func getRegistryTypeFileName(typeName string) (filename string) {
	splitted := Split(typeName)
	stringLen := len(splitted) - 1
	for i := 0; i < len(splitted); i++ {
		if i == stringLen {
			filename = filename + strings.ToLower(splitted[i])
		} else {
			filename = filename + strings.ToLower(splitted[i]) + "_"
		}
	}
	return filename
}

func Split(src string) (entries []string) {
	// don't split invalid utf8
	if !utf8.ValidString(src) {
		return []string{src}
	}
	entries = []string{}
	var runes [][]rune
	lastClass := 0
	class := 0
	// split into fields based on class of unicode character
	for _, r := range src {
		switch true {
		case unicode.IsLower(r):
			class = 1
		case unicode.IsUpper(r):
			class = 2
		case unicode.IsDigit(r):
			class = 3
		default:
			class = 4
		}
		if class == lastClass {
			runes[len(runes)-1] = append(runes[len(runes)-1], r)
		} else {
			runes = append(runes, []rune{r})
		}
		lastClass = class
	}
	// handle upper case -> lower case sequences, e.g.
	// "PDFL", "oader" -> "PDF", "Loader"
	for i := 0; i < len(runes)-1; i++ {
		if unicode.IsUpper(runes[i][0]) && unicode.IsLower(runes[i+1][0]) {
			runes[i+1] = append([]rune{runes[i][len(runes[i])-1]}, runes[i+1]...)
			runes[i] = runes[i][:len(runes[i])-1]
		}
	}
	// construct []string from results
	for _, s := range runes {
		if len(s) > 0 {
			entries = append(entries, string(s))
		}
	}
	return
}
