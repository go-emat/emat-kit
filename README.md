Mattex eMat Kit

[![Language](https://img.shields.io/badge/Language-Go-blue.svg)](https://golang.org/)

## Directory Structure
    ├── api                 # 服务子命令
    ├── docker              # docker-compose 依赖的image
    ├── kafka               # kafka
    ├── migrate             # Migrate
    ├── paginator           # 环境配置
    ├── protobuf            # eMat protobuf file
    ├── rabbitmq            # Rabbit MQ
    └── make                # make run command

## Generate proto file
    make
    
