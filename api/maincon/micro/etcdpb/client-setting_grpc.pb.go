// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package etcdpb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// ClientSettingServiceClient is the client API for ClientSettingService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type ClientSettingServiceClient interface {
	FetchAllClientSettings(ctx context.Context, in *FetchAllClientSettingsRequest, opts ...grpc.CallOption) (*FetchAllClientSettingsResponse, error)
}

type clientSettingServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewClientSettingServiceClient(cc grpc.ClientConnInterface) ClientSettingServiceClient {
	return &clientSettingServiceClient{cc}
}

func (c *clientSettingServiceClient) FetchAllClientSettings(ctx context.Context, in *FetchAllClientSettingsRequest, opts ...grpc.CallOption) (*FetchAllClientSettingsResponse, error) {
	out := new(FetchAllClientSettingsResponse)
	err := c.cc.Invoke(ctx, "/maincon.micro.ClientSettingService/FetchAllClientSettings", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ClientSettingServiceServer is the server API for ClientSettingService service.
// All implementations must embed UnimplementedClientSettingServiceServer
// for forward compatibility
type ClientSettingServiceServer interface {
	FetchAllClientSettings(context.Context, *FetchAllClientSettingsRequest) (*FetchAllClientSettingsResponse, error)
	mustEmbedUnimplementedClientSettingServiceServer()
}

// UnimplementedClientSettingServiceServer must be embedded to have forward compatible implementations.
type UnimplementedClientSettingServiceServer struct {
}

func (UnimplementedClientSettingServiceServer) FetchAllClientSettings(context.Context, *FetchAllClientSettingsRequest) (*FetchAllClientSettingsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method FetchAllClientSettings not implemented")
}
func (UnimplementedClientSettingServiceServer) mustEmbedUnimplementedClientSettingServiceServer() {}

// UnsafeClientSettingServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to ClientSettingServiceServer will
// result in compilation errors.
type UnsafeClientSettingServiceServer interface {
	mustEmbedUnimplementedClientSettingServiceServer()
}

func RegisterClientSettingServiceServer(s grpc.ServiceRegistrar, srv ClientSettingServiceServer) {
	s.RegisterService(&_ClientSettingService_serviceDesc, srv)
}

func _ClientSettingService_FetchAllClientSettings_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(FetchAllClientSettingsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ClientSettingServiceServer).FetchAllClientSettings(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.micro.ClientSettingService/FetchAllClientSettings",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ClientSettingServiceServer).FetchAllClientSettings(ctx, req.(*FetchAllClientSettingsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _ClientSettingService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "maincon.micro.ClientSettingService",
	HandlerType: (*ClientSettingServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "FetchAllClientSettings",
			Handler:    _ClientSettingService_FetchAllClientSettings_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "maincon/micro/etcd/client-setting.proto",
}
