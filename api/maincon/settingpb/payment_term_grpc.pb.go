// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package settingpb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// PaymentTermServiceClient is the client API for PaymentTermService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type PaymentTermServiceClient interface {
	ReadPaymentTerm(ctx context.Context, in *ReadPaymentTermRequest, opts ...grpc.CallOption) (*ReadPaymentTermResponse, error)
	ListPaymentTerms(ctx context.Context, in *ListPaymentTermsRequest, opts ...grpc.CallOption) (*ListPaymentTermsResponse, error)
	CreatePaymentTerm(ctx context.Context, in *CreatePaymentTermRequest, opts ...grpc.CallOption) (*CreatePaymentTermResponse, error)
	UpdatePaymentTerm(ctx context.Context, in *UpdatePaymentTermRequest, opts ...grpc.CallOption) (*UpdatePaymentTermResponse, error)
	DeletePaymentTerm(ctx context.Context, in *DeletePaymentTermRequest, opts ...grpc.CallOption) (*DeletePaymentTermResponse, error)
}

type paymentTermServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewPaymentTermServiceClient(cc grpc.ClientConnInterface) PaymentTermServiceClient {
	return &paymentTermServiceClient{cc}
}

func (c *paymentTermServiceClient) ReadPaymentTerm(ctx context.Context, in *ReadPaymentTermRequest, opts ...grpc.CallOption) (*ReadPaymentTermResponse, error) {
	out := new(ReadPaymentTermResponse)
	err := c.cc.Invoke(ctx, "/maincon.setting.PaymentTermService/ReadPaymentTerm", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *paymentTermServiceClient) ListPaymentTerms(ctx context.Context, in *ListPaymentTermsRequest, opts ...grpc.CallOption) (*ListPaymentTermsResponse, error) {
	out := new(ListPaymentTermsResponse)
	err := c.cc.Invoke(ctx, "/maincon.setting.PaymentTermService/ListPaymentTerms", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *paymentTermServiceClient) CreatePaymentTerm(ctx context.Context, in *CreatePaymentTermRequest, opts ...grpc.CallOption) (*CreatePaymentTermResponse, error) {
	out := new(CreatePaymentTermResponse)
	err := c.cc.Invoke(ctx, "/maincon.setting.PaymentTermService/CreatePaymentTerm", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *paymentTermServiceClient) UpdatePaymentTerm(ctx context.Context, in *UpdatePaymentTermRequest, opts ...grpc.CallOption) (*UpdatePaymentTermResponse, error) {
	out := new(UpdatePaymentTermResponse)
	err := c.cc.Invoke(ctx, "/maincon.setting.PaymentTermService/UpdatePaymentTerm", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *paymentTermServiceClient) DeletePaymentTerm(ctx context.Context, in *DeletePaymentTermRequest, opts ...grpc.CallOption) (*DeletePaymentTermResponse, error) {
	out := new(DeletePaymentTermResponse)
	err := c.cc.Invoke(ctx, "/maincon.setting.PaymentTermService/DeletePaymentTerm", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// PaymentTermServiceServer is the server API for PaymentTermService service.
// All implementations must embed UnimplementedPaymentTermServiceServer
// for forward compatibility
type PaymentTermServiceServer interface {
	ReadPaymentTerm(context.Context, *ReadPaymentTermRequest) (*ReadPaymentTermResponse, error)
	ListPaymentTerms(context.Context, *ListPaymentTermsRequest) (*ListPaymentTermsResponse, error)
	CreatePaymentTerm(context.Context, *CreatePaymentTermRequest) (*CreatePaymentTermResponse, error)
	UpdatePaymentTerm(context.Context, *UpdatePaymentTermRequest) (*UpdatePaymentTermResponse, error)
	DeletePaymentTerm(context.Context, *DeletePaymentTermRequest) (*DeletePaymentTermResponse, error)
	mustEmbedUnimplementedPaymentTermServiceServer()
}

// UnimplementedPaymentTermServiceServer must be embedded to have forward compatible implementations.
type UnimplementedPaymentTermServiceServer struct {
}

func (UnimplementedPaymentTermServiceServer) ReadPaymentTerm(context.Context, *ReadPaymentTermRequest) (*ReadPaymentTermResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ReadPaymentTerm not implemented")
}
func (UnimplementedPaymentTermServiceServer) ListPaymentTerms(context.Context, *ListPaymentTermsRequest) (*ListPaymentTermsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListPaymentTerms not implemented")
}
func (UnimplementedPaymentTermServiceServer) CreatePaymentTerm(context.Context, *CreatePaymentTermRequest) (*CreatePaymentTermResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreatePaymentTerm not implemented")
}
func (UnimplementedPaymentTermServiceServer) UpdatePaymentTerm(context.Context, *UpdatePaymentTermRequest) (*UpdatePaymentTermResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdatePaymentTerm not implemented")
}
func (UnimplementedPaymentTermServiceServer) DeletePaymentTerm(context.Context, *DeletePaymentTermRequest) (*DeletePaymentTermResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeletePaymentTerm not implemented")
}
func (UnimplementedPaymentTermServiceServer) mustEmbedUnimplementedPaymentTermServiceServer() {}

// UnsafePaymentTermServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to PaymentTermServiceServer will
// result in compilation errors.
type UnsafePaymentTermServiceServer interface {
	mustEmbedUnimplementedPaymentTermServiceServer()
}

func RegisterPaymentTermServiceServer(s grpc.ServiceRegistrar, srv PaymentTermServiceServer) {
	s.RegisterService(&_PaymentTermService_serviceDesc, srv)
}

func _PaymentTermService_ReadPaymentTerm_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ReadPaymentTermRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PaymentTermServiceServer).ReadPaymentTerm(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.setting.PaymentTermService/ReadPaymentTerm",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PaymentTermServiceServer).ReadPaymentTerm(ctx, req.(*ReadPaymentTermRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _PaymentTermService_ListPaymentTerms_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListPaymentTermsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PaymentTermServiceServer).ListPaymentTerms(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.setting.PaymentTermService/ListPaymentTerms",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PaymentTermServiceServer).ListPaymentTerms(ctx, req.(*ListPaymentTermsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _PaymentTermService_CreatePaymentTerm_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreatePaymentTermRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PaymentTermServiceServer).CreatePaymentTerm(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.setting.PaymentTermService/CreatePaymentTerm",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PaymentTermServiceServer).CreatePaymentTerm(ctx, req.(*CreatePaymentTermRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _PaymentTermService_UpdatePaymentTerm_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdatePaymentTermRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PaymentTermServiceServer).UpdatePaymentTerm(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.setting.PaymentTermService/UpdatePaymentTerm",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PaymentTermServiceServer).UpdatePaymentTerm(ctx, req.(*UpdatePaymentTermRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _PaymentTermService_DeletePaymentTerm_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeletePaymentTermRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PaymentTermServiceServer).DeletePaymentTerm(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.setting.PaymentTermService/DeletePaymentTerm",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PaymentTermServiceServer).DeletePaymentTerm(ctx, req.(*DeletePaymentTermRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _PaymentTermService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "maincon.setting.PaymentTermService",
	HandlerType: (*PaymentTermServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "ReadPaymentTerm",
			Handler:    _PaymentTermService_ReadPaymentTerm_Handler,
		},
		{
			MethodName: "ListPaymentTerms",
			Handler:    _PaymentTermService_ListPaymentTerms_Handler,
		},
		{
			MethodName: "CreatePaymentTerm",
			Handler:    _PaymentTermService_CreatePaymentTerm_Handler,
		},
		{
			MethodName: "UpdatePaymentTerm",
			Handler:    _PaymentTermService_UpdatePaymentTerm_Handler,
		},
		{
			MethodName: "DeletePaymentTerm",
			Handler:    _PaymentTermService_DeletePaymentTerm_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "maincon/setting/payment_term.proto",
}
