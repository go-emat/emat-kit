// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package submissionpb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// DocumentUserPinServiceClient is the client API for DocumentUserPinService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type DocumentUserPinServiceClient interface {
	UpdateDocumentPinning(ctx context.Context, in *UpdateDocumentPinningRequest, opts ...grpc.CallOption) (*SimpleResponse, error)
}

type documentUserPinServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewDocumentUserPinServiceClient(cc grpc.ClientConnInterface) DocumentUserPinServiceClient {
	return &documentUserPinServiceClient{cc}
}

func (c *documentUserPinServiceClient) UpdateDocumentPinning(ctx context.Context, in *UpdateDocumentPinningRequest, opts ...grpc.CallOption) (*SimpleResponse, error) {
	out := new(SimpleResponse)
	err := c.cc.Invoke(ctx, "/reviewer.DocumentUserPinService/UpdateDocumentPinning", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// DocumentUserPinServiceServer is the server API for DocumentUserPinService service.
// All implementations must embed UnimplementedDocumentUserPinServiceServer
// for forward compatibility
type DocumentUserPinServiceServer interface {
	UpdateDocumentPinning(context.Context, *UpdateDocumentPinningRequest) (*SimpleResponse, error)
	mustEmbedUnimplementedDocumentUserPinServiceServer()
}

// UnimplementedDocumentUserPinServiceServer must be embedded to have forward compatible implementations.
type UnimplementedDocumentUserPinServiceServer struct {
}

func (UnimplementedDocumentUserPinServiceServer) UpdateDocumentPinning(context.Context, *UpdateDocumentPinningRequest) (*SimpleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateDocumentPinning not implemented")
}
func (UnimplementedDocumentUserPinServiceServer) mustEmbedUnimplementedDocumentUserPinServiceServer() {
}

// UnsafeDocumentUserPinServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to DocumentUserPinServiceServer will
// result in compilation errors.
type UnsafeDocumentUserPinServiceServer interface {
	mustEmbedUnimplementedDocumentUserPinServiceServer()
}

func RegisterDocumentUserPinServiceServer(s grpc.ServiceRegistrar, srv DocumentUserPinServiceServer) {
	s.RegisterService(&_DocumentUserPinService_serviceDesc, srv)
}

func _DocumentUserPinService_UpdateDocumentPinning_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateDocumentPinningRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DocumentUserPinServiceServer).UpdateDocumentPinning(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.DocumentUserPinService/UpdateDocumentPinning",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DocumentUserPinServiceServer).UpdateDocumentPinning(ctx, req.(*UpdateDocumentPinningRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _DocumentUserPinService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "reviewer.DocumentUserPinService",
	HandlerType: (*DocumentUserPinServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "UpdateDocumentPinning",
			Handler:    _DocumentUserPinService_UpdateDocumentPinning_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "reviewer/submission/document_user_pin.proto",
}
