package storage

import (
	"io"
	"strings"

	"github.com/360EntSecGroup-Skylar/excelize/v2"
	"gitlab.com/go-emat/emat-kit/storage/common"
)

type UploadFileRequest struct {
	FileContent          []byte
	FileContentReader    io.Reader // please provide either FileContent or FileContentReader
	FilePath             string
	FileName             string
	FileExtension        string
	PutUnderDocumentType string
}

type UploadFileResponse struct {
	FilePath      string
	FileName      string
	FileExtension string
}

type MoveFileRequest struct {
	FilePath       string
	MoveToFilePath string
}

type MoveFileResponse struct {
	FilePath string
}

type DeleteFileRequest struct {
	FilePath string
}

type DeleteFileResponse struct {
	FilePath string
}

type DownloadFileRequest struct {
	FilePath        string
	FileName        string
	SignedUrlOption *common.SignedUrlOption
}

type DownloadFileResponse struct {
	File     []byte
	FileName string
	FilePath string
}

type SessionURLRequest struct {
	FileName             string
	ContentType          string
	InitialRequestLength string
	Origin               string
	XUploadContentType   string
	XUploadContentLength string
}
type fileService struct {
	StorageFactory *DocumentStorageFactory
}

func NewFileService(storageType string, bucketName string, configFile ...string) *fileService {
	svc := &fileService{StorageFactory: &DocumentStorageFactory{
		StorageBucketName: bucketName,
		StorageType:       storageType,
	}}
	if len(configFile) > 0 {
		svc.StorageFactory.ConfigFile = configFile[0]
	}
	return svc
}

func GetFileExtension(filename string) string {
	filePart := strings.Split(filename, ".")
	extension := ""
	if len(filePart) > 1 {
		extension = filePart[len(filePart)-1]
	}
	return extension
}

func (s *fileService) SetAbsLocalPath(absPrefixPath string) {
	s.StorageFactory.LocalAbsPrefixPath = absPrefixPath
}

func NewDefaultFileService() *fileService {
	svc := &fileService{StorageFactory: newDefaultDocumentStorageFactory()}
	return svc
}

func (s *fileService) UploadFile(req *UploadFileRequest) (*UploadFileResponse, error) {

	createFileResponse, err := s.StorageFactory.SaveFileToStorage(req)
	if err != nil {
		return nil, err
	}
	pbFile := UploadFileResponse{
		FilePath:      createFileResponse.FilePath,
		FileName:      req.FileName,
		FileExtension: req.FileExtension,
	}
	return &pbFile, nil
}

func (s *fileService) MoveFile(req *MoveFileRequest) (*MoveFileResponse, error) {
	if err := s.StorageFactory.MoveStorageFile(req.FilePath, req.MoveToFilePath); err != nil {
		return nil, err
	}
	pbFile := MoveFileResponse{
		FilePath: req.MoveToFilePath,
	}
	return &pbFile, nil
}

func (s *fileService) DownloadFile(req *DownloadFileRequest, localAbsPrefixPath ...string) (*DownloadFileResponse, error) {
	if len(localAbsPrefixPath) > 0 {
		s.SetAbsLocalPath(localAbsPrefixPath[0])
	}
	fileResponse, err := s.StorageFactory.DownloadStorageFile(req.FilePath)
	if err != nil {
		return nil, err
	}
	pbFile := DownloadFileResponse{
		File:     fileResponse,
		FileName: req.FileName,
		FilePath: req.FilePath,
	}
	return &pbFile, nil
}

func (s *fileService) DeleteFile(req *DeleteFileRequest) (*DeleteFileResponse, error) {
	if err := s.StorageFactory.DeleteStorageFile(req.FilePath); err != nil {
		return nil, err
	}
	pbFile := DeleteFileResponse{
		FilePath: req.FilePath,
	}
	return &pbFile, nil
}

func (s *fileService) SaveExcel(filename string, xlsx *excelize.File, extension string) (string, error) {
	return s.StorageFactory.SaveExcelToStorage(filename, xlsx, extension)
}

func (s *fileService) Zip(req *DownloadFileRequest) (*DownloadFileResponse, error) {
	fileResponse, err := s.StorageFactory.DownloadStorageFile(req.FilePath)
	if err != nil {
		return nil, err
	}
	pbFile := DownloadFileResponse{
		File: fileResponse,
	}
	return &pbFile, nil
}

func (s *fileService) ZipFiles(zipFilePath, zipFolder string, files []string) (string, error) {
	return s.StorageFactory.ZipFiles(zipFilePath, zipFolder, files)
}

func (s *fileService) ZipFilesAndDownload(zipContent *common.ZipContent) (fileBuf []byte, err error) {
	return s.StorageFactory.ZipContentToStorageDocument(zipContent)
}

func (s *fileService) GenerateSignedDownloadURL(req *DownloadFileRequest) (string, error) {
	if req.SignedUrlOption != nil {
		return s.StorageFactory.GenerateSignedDownloadURL(req.FilePath, *req.SignedUrlOption)
	}
	return s.StorageFactory.GenerateSignedDownloadURL(req.FilePath)
}

func (s *fileService) GenerateResumableSessionURL(req *SessionURLRequest) (string, error) {
	return s.StorageFactory.GenerateResumableSessionURL(req)
}

func (s *fileService) CompleteSessionUrl(filepath, uploadID string) error {
	return s.StorageFactory.CompleteSessionUrl(filepath, uploadID)
}

// FilePathExists FilePathExists
func (s *fileService) FileExists(path string) bool {
	return s.StorageFactory.FileExists(path)
}

func (s *fileService) GetReader(filePath string) (reader io.ReadCloser, err error) {
	return s.StorageFactory.GetReader(filePath)
}

func (s *fileService) GetFileMeta(filePath string) (fileMeta *common.FileMeta, err error) {
	return s.StorageFactory.GetFileMeta(filePath)
}
