package kafka

import (
	"fmt"
	"log"
	"time"

	"github.com/Shopify/sarama"
)

// ProducerMessageHandler registed by application to process message, each Async Producer has one message handler.
// Returned error means produce failed
type ProducerMessageHandler interface {
	Error(err *sarama.ProducerError) error
	Success(msg *sarama.ProducerMessage) error
}

// Producer Kafka Async Producer client
type EmatAsyncProducer struct {
	asyncProducer sarama.AsyncProducer
	pushTimeout   time.Duration
	encoder       Encoder
	msgHandler    ProducerMessageHandler
}

// NewAsyncProducer create new Producer instance
func NewEmatAsyncProducer(cfg *ProducerConfig, msgHandler ProducerMessageHandler) (*EmatAsyncProducer, error) {
	config := NewKafkaProducerConfig(cfg)
	asyncProducer, err := sarama.NewAsyncProducer(cfg.Brokers, config)
	if err != nil {
		log.Printf("Kafka init async Producer err: %v", err)
		return nil, err
	}

	// 保证push时channel容量足够
	pushTimeout := cfg.FlushFrequency + 1*time.Second
	producer := &EmatAsyncProducer{
		asyncProducer: asyncProducer,
		pushTimeout:   pushTimeout,
		encoder:       JSONEncoder(),
		msgHandler:    msgHandler,
	}

	go func() {
		for {
			select {
			case err, ok := <-producer.asyncProducer.Errors():
				if ok {
					//msg := err.Msg
					//log.Printf("Kafka Producer: fail push topic[%s]-partition[%d]-offset[%d] error=%s",
					//	msg.Topic, msg.Partition, msg.Offset, err.Error())
					producer.msgHandler.Error(err)
				}
			case msg, ok := <-producer.asyncProducer.Successes():
				if ok {
					//log.Printf("Kafka Producer: success push topic[%s]-partition[%d]-offset[%d]",
					//	msg.Topic, msg.Partition, msg.Offset)
					producer.msgHandler.Success(msg)

				}
			}
		}
	}()

	log.Println("Kafka Producer init success")
	return producer, nil
}

// Push pubsh message to kafka, can be a JSON string.
//
// Attention that if config.Partitioner is set HashPartitioner, then all message will be
// pushed to the only ONE partition.
func (p *EmatAsyncProducer) Push(topic string, msg []byte) error {
	return p.PushWithKey(topic, "", msg)
}

// PushWithKey push message to kafka with specified key, which means this message
// **should** be sent to specified partition if the config.Partitioner is HashPartitioner
func (p *EmatAsyncProducer) PushWithKey(topic, key string, msg []byte) error {
	return p.push(topic, key, msg)
}

func (p *EmatAsyncProducer) push(topic, key string, value []byte) error {
	if p.asyncProducer == nil {
		return fmt.Errorf("Kafka Producer not init")
	}
	pMsg := &sarama.ProducerMessage{
		Topic: topic,
		Key:   sarama.ByteEncoder(key),
		Value: sarama.ByteEncoder(value),
	}
	select {
	case p.asyncProducer.Input() <- pMsg:
		return nil
	case <-time.After(p.pushTimeout):
		err := fmt.Errorf("Kafka Producer: %s publish msg timeout", p.getID(topic, key))
		return err
	}
}

// Close application MUST close the producer to make sure the remaining messages
// are pushed to Kafka when application exit
func (p *EmatAsyncProducer) Close() error {
	var err error
	if p.asyncProducer == nil {
		return err
	}
	if err = p.asyncProducer.Close(); err != nil {
		log.Printf("Kafka Producer: close fail, %v", err)
	}
	return err
}

func (p *EmatAsyncProducer) getID(topic, key string) string {
	return fmt.Sprintf("topic[%s]-key[%s]", topic, key)
}

// SetEncoder set encoder for produced message
func (p *EmatAsyncProducer) SetEncoder(encoder Encoder) {
	p.encoder = encoder
}
