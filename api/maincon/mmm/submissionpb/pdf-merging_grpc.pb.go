// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package submissionpb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// PdfMergingServiceClient is the client API for PdfMergingService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type PdfMergingServiceClient interface {
	SendPdfMergingErrorRecordsEmail(ctx context.Context, in *SendPdfMergingErrorRecordsEmailRequest, opts ...grpc.CallOption) (*SendPdfMergingErrorRecordsEmailResponse, error)
}

type pdfMergingServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewPdfMergingServiceClient(cc grpc.ClientConnInterface) PdfMergingServiceClient {
	return &pdfMergingServiceClient{cc}
}

func (c *pdfMergingServiceClient) SendPdfMergingErrorRecordsEmail(ctx context.Context, in *SendPdfMergingErrorRecordsEmailRequest, opts ...grpc.CallOption) (*SendPdfMergingErrorRecordsEmailResponse, error) {
	out := new(SendPdfMergingErrorRecordsEmailResponse)
	err := c.cc.Invoke(ctx, "/maincon.mmm.submission.PdfMergingService/SendPdfMergingErrorRecordsEmail", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// PdfMergingServiceServer is the server API for PdfMergingService service.
// All implementations must embed UnimplementedPdfMergingServiceServer
// for forward compatibility
type PdfMergingServiceServer interface {
	SendPdfMergingErrorRecordsEmail(context.Context, *SendPdfMergingErrorRecordsEmailRequest) (*SendPdfMergingErrorRecordsEmailResponse, error)
	mustEmbedUnimplementedPdfMergingServiceServer()
}

// UnimplementedPdfMergingServiceServer must be embedded to have forward compatible implementations.
type UnimplementedPdfMergingServiceServer struct {
}

func (UnimplementedPdfMergingServiceServer) SendPdfMergingErrorRecordsEmail(context.Context, *SendPdfMergingErrorRecordsEmailRequest) (*SendPdfMergingErrorRecordsEmailResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SendPdfMergingErrorRecordsEmail not implemented")
}
func (UnimplementedPdfMergingServiceServer) mustEmbedUnimplementedPdfMergingServiceServer() {}

// UnsafePdfMergingServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to PdfMergingServiceServer will
// result in compilation errors.
type UnsafePdfMergingServiceServer interface {
	mustEmbedUnimplementedPdfMergingServiceServer()
}

func RegisterPdfMergingServiceServer(s grpc.ServiceRegistrar, srv PdfMergingServiceServer) {
	s.RegisterService(&_PdfMergingService_serviceDesc, srv)
}

func _PdfMergingService_SendPdfMergingErrorRecordsEmail_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SendPdfMergingErrorRecordsEmailRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PdfMergingServiceServer).SendPdfMergingErrorRecordsEmail(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.mmm.submission.PdfMergingService/SendPdfMergingErrorRecordsEmail",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PdfMergingServiceServer).SendPdfMergingErrorRecordsEmail(ctx, req.(*SendPdfMergingErrorRecordsEmailRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _PdfMergingService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "maincon.mmm.submission.PdfMergingService",
	HandlerType: (*PdfMergingServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "SendPdfMergingErrorRecordsEmail",
			Handler:    _PdfMergingService_SendPdfMergingErrorRecordsEmail_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "maincon/mmm/submission/pdf-merging.proto",
}
