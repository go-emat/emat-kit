package alicloudstorage

import (
	"fmt"
	"io"

	"github.com/aliyun/aliyun-oss-go-sdk/oss"
)

type CircularBuffer struct {
	data       []byte
	bufferSize int64
	index      int64
	dataLength int64
}

func NewCircularBuffer(size int64) *CircularBuffer {
	if size <= 0 {
		panic("Size must be positive")
	}
	return &CircularBuffer{
		data:       make([]byte, size),
		bufferSize: size,
	}
}

func (b *CircularBuffer) Len() int64 {
	return b.dataLength
}

func (b *CircularBuffer) Write(p []byte) (n int, err error) {
	pLen := int64(len(p))
	if pLen >= b.bufferSize {
		p = p[pLen-b.bufferSize:]
		pLen = b.bufferSize
	}

	remainingLen := b.bufferSize - b.index
	if pLen <= remainingLen {
		copy(b.data[b.index:], p)
	} else {
		copy(b.data[b.index:], p[:remainingLen])
		copy(b.data, p[remainingLen:])
	}

	n = int(pLen)
	b.index += pLen
	b.index %= b.bufferSize
	b.dataLength += pLen
	if b.dataLength > b.bufferSize {
		b.dataLength = b.bufferSize
	}
	return
}

func (b *CircularBuffer) GetLastBytes(fetchLengh int64) (lastBytes []byte) {
	if fetchLengh > b.dataLength {
		fetchLengh = b.dataLength
	}

	if fetchLengh <= b.index {
		lastBytes = b.data[b.index-fetchLengh : b.index]
	} else {
		lastBytes = append(b.data[b.bufferSize-(fetchLengh-b.index):], b.data[0:b.index]...)
	}
	return
}

func (b *CircularBuffer) Reset() {
	b.index = 0
	b.dataLength = 0
}

type AliCloudStorageReadSeekCloser struct {
	bucket    *oss.Bucket
	objectKey string
	reader    io.ReadCloser
	index     int64
	fileSize  int64

	cacheBuffer          *CircularBuffer
	availableCachedBytes []byte
}

func NewAliCloudStorageReadSeekCloser(bucket *oss.Bucket, objectKey string, fileSize, cacheSize int64) *AliCloudStorageReadSeekCloser {
	readSeeker := &AliCloudStorageReadSeekCloser{
		objectKey: objectKey,
		bucket:    bucket,
		index:     0,
		fileSize:  fileSize,
	}
	if cacheSize > 0 {
		readSeeker.cacheBuffer = NewCircularBuffer(cacheSize)
	}
	return readSeeker
}

func (s *AliCloudStorageReadSeekCloser) Read(p []byte) (n int, err error) {
	n1 := s.readAvailableCachedBytes(p)
	if n1 > 0 {
		s.writeToCache(p[:n1])
		n += n1
		s.index += int64(n1)
	}
	if n1 >= len(p) {
		return
	}

	if s.index >= s.fileSize {
		err = io.EOF
		s.Close()
		return
	}

	if s.reader == nil {
		s.reader, err = s.bucket.GetObject(s.objectKey, oss.Range(s.index, s.fileSize-1))
		if err != nil {
			return
		}
	}

	for n < len(p) {
		n2, err := s.reader.Read(p[n:])
		if n2 > 0 {
			s.writeToCache(p[n : n+n2])
			n += n2
			s.index += int64(n2)
		}
		if err == io.EOF {
			s.reader.Close()
		}
		if err != nil {
			return n, err
		}
	}
	return
}

func (s *AliCloudStorageReadSeekCloser) Seek(offset int64, whence int) (n int64, err error) {
	// log.Println("dbg AliCloudStorageReadSeekCloser: offset = ", offset)
	// log.Println("dbg AliCloudStorageReadSeekCloser: whence = ", whence)
	// log.Println("dbg AliCloudStorageReadSeekCloser: mIndex = ", s.index)
	// log.Println("dbg AliCloudStorageReadSeekCloser: mFileSize = ", s.fileSize)

	switch whence {
	case io.SeekStart:
		n = offset
	case io.SeekCurrent:
		n = s.index + offset
	case io.SeekEnd:
		n = s.fileSize + offset
	default:
		return 0, fmt.Errorf("invalid whence @ AliCloudStorageReadSeeker.Seek: %d", whence)
	}
	if n < 0 || n > s.fileSize {
		return 0, fmt.Errorf("position out of bound @ AliCloudStorageReadSeeker.Seek: index %d : offset: %d whence %d : fileSize %d", n, offset, whence, s.fileSize)
	}

	indexDiff := n - s.index
	s.index = n

	if indexDiff == 0 {
		return
	}

	if s.prepareAvailableCachedBytes(indexDiff) {
		return
	}

	if err = s.Close(); err != nil {
		return
	}
	// log.Println("dbg AliCloudStorageReadSeekCloser rtn err = ", err)
	// log.Println("dbg AliCloudStorageReadSeekCloser rtn n = ", n)
	return
}

func (s *AliCloudStorageReadSeekCloser) Close() error {
	if s.reader != nil {
		if err := s.reader.Close(); err != nil {
			return fmt.Errorf("failed to close reader: %w", err)
		}
		s.reader = nil
	}
	return nil
}

func (s *AliCloudStorageReadSeekCloser) readAvailableCachedBytes(p []byte) (n int) {
	aLen := len(s.availableCachedBytes)
	if aLen > 0 {
		pLen := len(p)
		if aLen <= pLen {
			copy(p[:aLen], s.availableCachedBytes)
			n = aLen
			s.availableCachedBytes = nil
		} else {
			copy(p, s.availableCachedBytes[:pLen])
			n = pLen
			s.availableCachedBytes = s.availableCachedBytes[n:]
		}
	}
	return
}

func (s *AliCloudStorageReadSeekCloser) writeToCache(p []byte) {
	if s.cacheBuffer != nil {
		s.cacheBuffer.Write(p)
	}
}

func (s *AliCloudStorageReadSeekCloser) prepareAvailableCachedBytes(indexDiff int64) (success bool) {
	s.availableCachedBytes = nil
	if s.cacheBuffer != nil {
		if indexDiff < 0 && s.cacheBuffer.Len() >= -indexDiff {
			s.availableCachedBytes = s.cacheBuffer.GetLastBytes(-indexDiff)
			success = true
		}
		s.cacheBuffer.Reset()
	}
	return
}
