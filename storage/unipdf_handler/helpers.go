package unipdf_handler

func IsTiffImage(ext string) bool {
	return  ext == "tif" || ext == "tiff"
}