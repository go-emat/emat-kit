// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.25.0
// 	protoc        v3.14.0
// source: subcon/setting/project_mapping.proto

package settingpb

import (
	proto "github.com/golang/protobuf/proto"
	commonpb "gitlab.com/go-emat/emat-kit/api/commonpb"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type ProjectMapping struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id                int64           `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	ProjectId         int64           `protobuf:"varint,2,opt,name=project_id,json=projectId,proto3" json:"project_id,omitempty"`
	ClientId          int64           `protobuf:"varint,3,opt,name=client_id,json=clientId,proto3" json:"client_id,omitempty"`
	ClientProjectId   int64           `protobuf:"varint,4,opt,name=client_project_id,json=clientProjectId,proto3" json:"client_project_id,omitempty"`
	ClientProjectName string          `protobuf:"bytes,5,opt,name=client_project_name,json=clientProjectName,proto3" json:"client_project_name,omitempty"`
	Audit             *commonpb.Audit `protobuf:"bytes,6,opt,name=audit,proto3" json:"audit,omitempty"`
}

func (x *ProjectMapping) Reset() {
	*x = ProjectMapping{}
	if protoimpl.UnsafeEnabled {
		mi := &file_subcon_setting_project_mapping_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ProjectMapping) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ProjectMapping) ProtoMessage() {}

func (x *ProjectMapping) ProtoReflect() protoreflect.Message {
	mi := &file_subcon_setting_project_mapping_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ProjectMapping.ProtoReflect.Descriptor instead.
func (*ProjectMapping) Descriptor() ([]byte, []int) {
	return file_subcon_setting_project_mapping_proto_rawDescGZIP(), []int{0}
}

func (x *ProjectMapping) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *ProjectMapping) GetProjectId() int64 {
	if x != nil {
		return x.ProjectId
	}
	return 0
}

func (x *ProjectMapping) GetClientId() int64 {
	if x != nil {
		return x.ClientId
	}
	return 0
}

func (x *ProjectMapping) GetClientProjectId() int64 {
	if x != nil {
		return x.ClientProjectId
	}
	return 0
}

func (x *ProjectMapping) GetClientProjectName() string {
	if x != nil {
		return x.ClientProjectName
	}
	return ""
}

func (x *ProjectMapping) GetAudit() *commonpb.Audit {
	if x != nil {
		return x.Audit
	}
	return nil
}

type CreateProjectMappingRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ProjectMapping *ProjectMapping   `protobuf:"bytes,1,opt,name=project_mapping,json=projectMapping,proto3" json:"project_mapping,omitempty"`
	Lang           commonpb.Language `protobuf:"varint,2,opt,name=lang,proto3,enum=common.Language" json:"lang,omitempty"`
}

func (x *CreateProjectMappingRequest) Reset() {
	*x = CreateProjectMappingRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_subcon_setting_project_mapping_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateProjectMappingRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateProjectMappingRequest) ProtoMessage() {}

func (x *CreateProjectMappingRequest) ProtoReflect() protoreflect.Message {
	mi := &file_subcon_setting_project_mapping_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateProjectMappingRequest.ProtoReflect.Descriptor instead.
func (*CreateProjectMappingRequest) Descriptor() ([]byte, []int) {
	return file_subcon_setting_project_mapping_proto_rawDescGZIP(), []int{1}
}

func (x *CreateProjectMappingRequest) GetProjectMapping() *ProjectMapping {
	if x != nil {
		return x.ProjectMapping
	}
	return nil
}

func (x *CreateProjectMappingRequest) GetLang() commonpb.Language {
	if x != nil {
		return x.Lang
	}
	return commonpb.Language_UNKNOWN_LANGUAGE
}

type CreateProjectMappingResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Data *ProjectMapping `protobuf:"bytes,1,opt,name=data,proto3" json:"data,omitempty"`
}

func (x *CreateProjectMappingResponse) Reset() {
	*x = CreateProjectMappingResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_subcon_setting_project_mapping_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateProjectMappingResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateProjectMappingResponse) ProtoMessage() {}

func (x *CreateProjectMappingResponse) ProtoReflect() protoreflect.Message {
	mi := &file_subcon_setting_project_mapping_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateProjectMappingResponse.ProtoReflect.Descriptor instead.
func (*CreateProjectMappingResponse) Descriptor() ([]byte, []int) {
	return file_subcon_setting_project_mapping_proto_rawDescGZIP(), []int{2}
}

func (x *CreateProjectMappingResponse) GetData() *ProjectMapping {
	if x != nil {
		return x.Data
	}
	return nil
}

type ReadProjectMappingRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ClientId        int64 `protobuf:"varint,1,opt,name=client_id,json=clientId,proto3" json:"client_id,omitempty"`
	ProjectId       int64 `protobuf:"varint,2,opt,name=project_id,json=projectId,proto3" json:"project_id,omitempty"`
	ClientProjectId int64 `protobuf:"varint,3,opt,name=client_project_id,json=clientProjectId,proto3" json:"client_project_id,omitempty"`
}

func (x *ReadProjectMappingRequest) Reset() {
	*x = ReadProjectMappingRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_subcon_setting_project_mapping_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ReadProjectMappingRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ReadProjectMappingRequest) ProtoMessage() {}

func (x *ReadProjectMappingRequest) ProtoReflect() protoreflect.Message {
	mi := &file_subcon_setting_project_mapping_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ReadProjectMappingRequest.ProtoReflect.Descriptor instead.
func (*ReadProjectMappingRequest) Descriptor() ([]byte, []int) {
	return file_subcon_setting_project_mapping_proto_rawDescGZIP(), []int{3}
}

func (x *ReadProjectMappingRequest) GetClientId() int64 {
	if x != nil {
		return x.ClientId
	}
	return 0
}

func (x *ReadProjectMappingRequest) GetProjectId() int64 {
	if x != nil {
		return x.ProjectId
	}
	return 0
}

func (x *ReadProjectMappingRequest) GetClientProjectId() int64 {
	if x != nil {
		return x.ClientProjectId
	}
	return 0
}

type ReadProjectMappingResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Data *ProjectMapping `protobuf:"bytes,1,opt,name=data,proto3" json:"data,omitempty"`
}

func (x *ReadProjectMappingResponse) Reset() {
	*x = ReadProjectMappingResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_subcon_setting_project_mapping_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ReadProjectMappingResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ReadProjectMappingResponse) ProtoMessage() {}

func (x *ReadProjectMappingResponse) ProtoReflect() protoreflect.Message {
	mi := &file_subcon_setting_project_mapping_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ReadProjectMappingResponse.ProtoReflect.Descriptor instead.
func (*ReadProjectMappingResponse) Descriptor() ([]byte, []int) {
	return file_subcon_setting_project_mapping_proto_rawDescGZIP(), []int{4}
}

func (x *ReadProjectMappingResponse) GetData() *ProjectMapping {
	if x != nil {
		return x.Data
	}
	return nil
}

var File_subcon_setting_project_mapping_proto protoreflect.FileDescriptor

var file_subcon_setting_project_mapping_proto_rawDesc = []byte{
	0x0a, 0x24, 0x73, 0x75, 0x62, 0x63, 0x6f, 0x6e, 0x2f, 0x73, 0x65, 0x74, 0x74, 0x69, 0x6e, 0x67,
	0x2f, 0x70, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x5f, 0x6d, 0x61, 0x70, 0x70, 0x69, 0x6e, 0x67,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x06, 0x73, 0x75, 0x62, 0x63, 0x6f, 0x6e, 0x1a, 0x13,
	0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x22, 0xdd, 0x01, 0x0a, 0x0e, 0x50, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x4d,
	0x61, 0x70, 0x70, 0x69, 0x6e, 0x67, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x03, 0x52, 0x02, 0x69, 0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x70, 0x72, 0x6f, 0x6a, 0x65, 0x63,
	0x74, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x09, 0x70, 0x72, 0x6f, 0x6a,
	0x65, 0x63, 0x74, 0x49, 0x64, 0x12, 0x1b, 0x0a, 0x09, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x5f,
	0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x03, 0x52, 0x08, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74,
	0x49, 0x64, 0x12, 0x2a, 0x0a, 0x11, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x5f, 0x70, 0x72, 0x6f,
	0x6a, 0x65, 0x63, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0f, 0x63,
	0x6c, 0x69, 0x65, 0x6e, 0x74, 0x50, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x49, 0x64, 0x12, 0x2e,
	0x0a, 0x13, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x5f, 0x70, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74,
	0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x11, 0x63, 0x6c, 0x69,
	0x65, 0x6e, 0x74, 0x50, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x4e, 0x61, 0x6d, 0x65, 0x12, 0x23,
	0x0a, 0x05, 0x61, 0x75, 0x64, 0x69, 0x74, 0x18, 0x06, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x0d, 0x2e,
	0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x41, 0x75, 0x64, 0x69, 0x74, 0x52, 0x05, 0x61, 0x75,
	0x64, 0x69, 0x74, 0x22, 0x84, 0x01, 0x0a, 0x1b, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x50, 0x72,
	0x6f, 0x6a, 0x65, 0x63, 0x74, 0x4d, 0x61, 0x70, 0x70, 0x69, 0x6e, 0x67, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x12, 0x3f, 0x0a, 0x0f, 0x70, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x5f, 0x6d,
	0x61, 0x70, 0x70, 0x69, 0x6e, 0x67, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x16, 0x2e, 0x73,
	0x75, 0x62, 0x63, 0x6f, 0x6e, 0x2e, 0x50, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x4d, 0x61, 0x70,
	0x70, 0x69, 0x6e, 0x67, 0x52, 0x0e, 0x70, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x4d, 0x61, 0x70,
	0x70, 0x69, 0x6e, 0x67, 0x12, 0x24, 0x0a, 0x04, 0x6c, 0x61, 0x6e, 0x67, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x0e, 0x32, 0x10, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x4c, 0x61, 0x6e, 0x67,
	0x75, 0x61, 0x67, 0x65, 0x52, 0x04, 0x6c, 0x61, 0x6e, 0x67, 0x22, 0x4a, 0x0a, 0x1c, 0x43, 0x72,
	0x65, 0x61, 0x74, 0x65, 0x50, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x4d, 0x61, 0x70, 0x70, 0x69,
	0x6e, 0x67, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x2a, 0x0a, 0x04, 0x64, 0x61,
	0x74, 0x61, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x16, 0x2e, 0x73, 0x75, 0x62, 0x63, 0x6f,
	0x6e, 0x2e, 0x50, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x4d, 0x61, 0x70, 0x70, 0x69, 0x6e, 0x67,
	0x52, 0x04, 0x64, 0x61, 0x74, 0x61, 0x22, 0x83, 0x01, 0x0a, 0x19, 0x52, 0x65, 0x61, 0x64, 0x50,
	0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x4d, 0x61, 0x70, 0x70, 0x69, 0x6e, 0x67, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x12, 0x1b, 0x0a, 0x09, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x5f, 0x69,
	0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x08, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x49,
	0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x70, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x5f, 0x69, 0x64, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x09, 0x70, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x49, 0x64,
	0x12, 0x2a, 0x0a, 0x11, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x5f, 0x70, 0x72, 0x6f, 0x6a, 0x65,
	0x63, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0f, 0x63, 0x6c, 0x69,
	0x65, 0x6e, 0x74, 0x50, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x49, 0x64, 0x22, 0x48, 0x0a, 0x1a,
	0x52, 0x65, 0x61, 0x64, 0x50, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x4d, 0x61, 0x70, 0x70, 0x69,
	0x6e, 0x67, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x2a, 0x0a, 0x04, 0x64, 0x61,
	0x74, 0x61, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x16, 0x2e, 0x73, 0x75, 0x62, 0x63, 0x6f,
	0x6e, 0x2e, 0x50, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x4d, 0x61, 0x70, 0x70, 0x69, 0x6e, 0x67,
	0x52, 0x04, 0x64, 0x61, 0x74, 0x61, 0x32, 0xd7, 0x01, 0x0a, 0x15, 0x50, 0x72, 0x6f, 0x6a, 0x65,
	0x63, 0x74, 0x4d, 0x61, 0x70, 0x70, 0x69, 0x6e, 0x67, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65,
	0x12, 0x61, 0x0a, 0x14, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x50, 0x72, 0x6f, 0x6a, 0x65, 0x63,
	0x74, 0x4d, 0x61, 0x70, 0x70, 0x69, 0x6e, 0x67, 0x12, 0x23, 0x2e, 0x73, 0x75, 0x62, 0x63, 0x6f,
	0x6e, 0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x50, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x4d,
	0x61, 0x70, 0x70, 0x69, 0x6e, 0x67, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x24, 0x2e,
	0x73, 0x75, 0x62, 0x63, 0x6f, 0x6e, 0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x50, 0x72, 0x6f,
	0x6a, 0x65, 0x63, 0x74, 0x4d, 0x61, 0x70, 0x70, 0x69, 0x6e, 0x67, 0x52, 0x65, 0x73, 0x70, 0x6f,
	0x6e, 0x73, 0x65, 0x12, 0x5b, 0x0a, 0x12, 0x52, 0x65, 0x61, 0x64, 0x50, 0x72, 0x6f, 0x6a, 0x65,
	0x63, 0x74, 0x4d, 0x61, 0x70, 0x70, 0x69, 0x6e, 0x67, 0x12, 0x21, 0x2e, 0x73, 0x75, 0x62, 0x63,
	0x6f, 0x6e, 0x2e, 0x52, 0x65, 0x61, 0x64, 0x50, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x4d, 0x61,
	0x70, 0x70, 0x69, 0x6e, 0x67, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x22, 0x2e, 0x73,
	0x75, 0x62, 0x63, 0x6f, 0x6e, 0x2e, 0x52, 0x65, 0x61, 0x64, 0x50, 0x72, 0x6f, 0x6a, 0x65, 0x63,
	0x74, 0x4d, 0x61, 0x70, 0x70, 0x69, 0x6e, 0x67, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x42, 0x12, 0x5a, 0x10, 0x73, 0x75, 0x62, 0x63, 0x6f, 0x6e, 0x2f, 0x73, 0x65, 0x74, 0x74, 0x69,
	0x6e, 0x67, 0x70, 0x62, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_subcon_setting_project_mapping_proto_rawDescOnce sync.Once
	file_subcon_setting_project_mapping_proto_rawDescData = file_subcon_setting_project_mapping_proto_rawDesc
)

func file_subcon_setting_project_mapping_proto_rawDescGZIP() []byte {
	file_subcon_setting_project_mapping_proto_rawDescOnce.Do(func() {
		file_subcon_setting_project_mapping_proto_rawDescData = protoimpl.X.CompressGZIP(file_subcon_setting_project_mapping_proto_rawDescData)
	})
	return file_subcon_setting_project_mapping_proto_rawDescData
}

var file_subcon_setting_project_mapping_proto_msgTypes = make([]protoimpl.MessageInfo, 5)
var file_subcon_setting_project_mapping_proto_goTypes = []interface{}{
	(*ProjectMapping)(nil),               // 0: subcon.ProjectMapping
	(*CreateProjectMappingRequest)(nil),  // 1: subcon.CreateProjectMappingRequest
	(*CreateProjectMappingResponse)(nil), // 2: subcon.CreateProjectMappingResponse
	(*ReadProjectMappingRequest)(nil),    // 3: subcon.ReadProjectMappingRequest
	(*ReadProjectMappingResponse)(nil),   // 4: subcon.ReadProjectMappingResponse
	(*commonpb.Audit)(nil),               // 5: common.Audit
	(commonpb.Language)(0),               // 6: common.Language
}
var file_subcon_setting_project_mapping_proto_depIdxs = []int32{
	5, // 0: subcon.ProjectMapping.audit:type_name -> common.Audit
	0, // 1: subcon.CreateProjectMappingRequest.project_mapping:type_name -> subcon.ProjectMapping
	6, // 2: subcon.CreateProjectMappingRequest.lang:type_name -> common.Language
	0, // 3: subcon.CreateProjectMappingResponse.data:type_name -> subcon.ProjectMapping
	0, // 4: subcon.ReadProjectMappingResponse.data:type_name -> subcon.ProjectMapping
	1, // 5: subcon.ProjectMappingService.CreateProjectMapping:input_type -> subcon.CreateProjectMappingRequest
	3, // 6: subcon.ProjectMappingService.ReadProjectMapping:input_type -> subcon.ReadProjectMappingRequest
	2, // 7: subcon.ProjectMappingService.CreateProjectMapping:output_type -> subcon.CreateProjectMappingResponse
	4, // 8: subcon.ProjectMappingService.ReadProjectMapping:output_type -> subcon.ReadProjectMappingResponse
	7, // [7:9] is the sub-list for method output_type
	5, // [5:7] is the sub-list for method input_type
	5, // [5:5] is the sub-list for extension type_name
	5, // [5:5] is the sub-list for extension extendee
	0, // [0:5] is the sub-list for field type_name
}

func init() { file_subcon_setting_project_mapping_proto_init() }
func file_subcon_setting_project_mapping_proto_init() {
	if File_subcon_setting_project_mapping_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_subcon_setting_project_mapping_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ProjectMapping); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_subcon_setting_project_mapping_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateProjectMappingRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_subcon_setting_project_mapping_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateProjectMappingResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_subcon_setting_project_mapping_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ReadProjectMappingRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_subcon_setting_project_mapping_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ReadProjectMappingResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_subcon_setting_project_mapping_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   5,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_subcon_setting_project_mapping_proto_goTypes,
		DependencyIndexes: file_subcon_setting_project_mapping_proto_depIdxs,
		MessageInfos:      file_subcon_setting_project_mapping_proto_msgTypes,
	}.Build()
	File_subcon_setting_project_mapping_proto = out.File
	file_subcon_setting_project_mapping_proto_rawDesc = nil
	file_subcon_setting_project_mapping_proto_goTypes = nil
	file_subcon_setting_project_mapping_proto_depIdxs = nil
}
