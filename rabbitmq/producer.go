package rabbitmq

import (
	"errors"
	"fmt"
	"github.com/streadway/amqp"
	"log"
	"sync"
	"time"
)

type Producer struct {
	// Producer的名字, "" is OK
	name string

	// RabbitMQ实例
	rabbitMQ *RabbitMQ

	// channel pool, 重复使用
	channelPool *sync.Pool

	// publish数据的锁
	publishMutex sync.RWMutex

	// 保护数据安全地并发读写
	mutex sync.RWMutex

	// RabbitMQ的会话channel
	mqChannel *amqp.Channel

	// RabbitMQ的exchange与其绑定的queues
	exchangeBinds []*ExchangeBinds

	// 生产者confirm开关
	enableConfirm bool
	// 监听publish confirm
	confirmChan chan amqp.Confirmation
	// confirm结果检测
	confirm *confirmHelper

	// 监听会话channel关闭
	closeChan chan *amqp.Error
	// Producer关闭控制
	stopChan chan struct{}

	// Producer 状态
	state uint8
}

func newProducer(name string, rabbitMQ *RabbitMQ) *Producer {
	return &Producer{
		name:     name,
		rabbitMQ: rabbitMQ,
		channelPool: &sync.Pool{
			New: func() interface{} { return make(chan bool, 1) },
		},
		state: StateClosed,
	}
}

func (p Producer) Name() string {
	return p.name
}

// CloseChan 仅用于测试使用,勿手动调用
func (p *Producer) CloseChan() {
	p.mutex.Lock()
	p.mqChannel.Close()
	p.mutex.Unlock()
}

// Confirm 是否开启生产者confirm功能, 默认为false, 该选项在Open()前设置.
// 说明: 目前仅实现串行化的confirm, 每次的等待confirm额外需要约50ms,建议上层并发调用Publish
func (p *Producer) Confirm(enable bool) *Producer {
	p.mutex.Lock()
	p.enableConfirm = enable
	p.mutex.Unlock()
	return p
}

func (p *Producer) SetExchangeBinds(eb []*ExchangeBinds) *Producer {
	p.mutex.Lock()
	if p.state != StateOpened {
		p.exchangeBinds = eb
	}
	p.mutex.Unlock()
	return p
}

func (p *Producer) Open() error {
	p.mutex.Lock()
	defer p.mutex.Unlock()

	// 条件检测
	if p.rabbitMQ == nil {
		return errors.New("RabbitMQ: Bad producer")
	}
	if len(p.exchangeBinds) <= 0 {
		return errors.New("RabbitMQ: No exchangeBinds found. You should SetExchangeBinds before open.")
	}
	if p.state == StateOpened {
		return errors.New("RabbitMQ: Producer had been opened")
	}

	// 创建并初始化channel
	connChannel, err := p.rabbitMQ.connChannel()
	if err != nil {
		return fmt.Errorf("RabbitMQ: Create channel failed, %v", err)
	}
	if err = applyExchangeBinds(connChannel, p.exchangeBinds); err != nil {
		connChannel.Close()
		return err
	}

	p.mqChannel = connChannel
	p.state = StateOpened

	// 初始化发送Confirm
	if p.enableConfirm {
		p.confirmChan = make(chan amqp.Confirmation, 1) // channel关闭时自动关闭
		p.mqChannel.Confirm(false)
		p.mqChannel.NotifyPublish(p.confirmChan)
		if p.confirm == nil {
			p.confirm = newConfirmHelper()
		} else {
			p.confirm.Reset()
		}

		go p.listenConfirm()
	}

	// 初始化Keepalive
	p.stopChan = make(chan struct{})
	p.closeChan = make(chan *amqp.Error, 1) // channel关闭时自动关闭
	p.mqChannel.NotifyClose(p.closeChan)

	go p.keepalive()

	return nil
}

func (p *Producer) Close() {
	p.mutex.Lock()
	defer p.mutex.Unlock()

	select {
	case <-p.stopChan:
		// had been closed
	default:
		close(p.stopChan)
	}
}

// 在同步Publish Confirm模式下, 每次Publish将额外有约50ms的等待时间.如果采用这种模式,建议上层并发publish
func (p *Producer) Publish(exchange, routeKey string, msg *PublishMsg) error {
	if msg == nil {
		return errors.New("RabbitMQ: Nil publish msg")
	}
	st := p.State()
	if st == StateReopening {
		if e := p.Open(); e != nil {
			log.Printf("[WARN] RabbitMQ: Producer(%s) recover channel failed, Err:%v\n", p.name, e)
		}
	}
	if st != StateOpened {
		return fmt.Errorf("RabbitMQ: Producer unopened, now state is %d", p.state)
	}

	pub := amqp.Publishing{
		ContentType:     msg.ContentType,
		ContentEncoding: msg.ContentEncoding,
		DeliveryMode:    msg.DeliveryMode,
		Priority:        msg.Priority,
		Timestamp:       msg.Timestamp,
		Body:            msg.Body,
		MessageId:		 msg.MessageId,
	}

	// 非confirm模式
	if p.enableConfirm == false {
		return p.mqChannel.Publish(exchange, routeKey, false, false, pub)
	}

	// confirm模式
	// 这里加锁保证消息发送顺序与接收ack的channel的编号一致
	p.publishMutex.Lock()
	if err := p.mqChannel.Publish(exchange, routeKey, false, false, pub); err != nil {
		p.publishMutex.Unlock()
		return fmt.Errorf("RabbitMQ: Producer publish failed, %v", err)
	}
	ch := p.channelPool.Get().(chan bool)
	p.confirm.Listen(ch)
	p.publishMutex.Unlock()

	ack, ok := <-ch
	p.channelPool.Put(ch)
	if !ack || !ok {
		return fmt.Errorf("RabbitMQ: Producer publish failed, confirm ack is false. ack:%t, ok:%t", ack, ok)
	}
	return nil
}

func (p *Producer) State() uint8 {
	p.mutex.RLock()
	defer p.mutex.RUnlock()
	return p.state
}

func (p *Producer) keepalive() {
	select {
	case <-p.stopChan:
		// 正常关闭
		log.Printf("[WARN] RabbitMQ: Producer(%s) shutdown normally.\n", p.name)
		p.mutex.Lock()
		p.mqChannel.Close()
		p.state = StateClosed
		p.mutex.Unlock()

	case err := <-p.closeChan:
		if err == nil {
			log.Printf("[ERROR] RabbitMQ: Producer(%s)'s channel was closed, but Error detail is nil\n", p.name)
		} else {
			log.Printf("[ERROR] RabbitMQ: Producer(%s)'s channel was closed, code:%d, reason:%s\n", p.name, err.Code, err.Reason)
		}

		// channel被异常关闭了
		p.mutex.Lock()
		p.state = StateReopening
		p.mutex.Unlock()

		maxRetry := 99999999
		for i := 0; i < maxRetry; i++ {
			time.Sleep(time.Second)
			if p.rabbitMQ.State() != StateOpened {
				log.Printf("[WARN] RabbitMQ: Producer(%s) try to recover channel for %d times, but mq's state != StateOpened\n", p.name, i+1)
				continue
			}
			if e := p.Open(); e != nil {
				log.Printf("[WARN] RabbitMQ: Producer(%s) recover channel failed for %d times, Err:%v\n", p.name, i+1, e)
				continue
			}
			log.Printf("[INFO] RabbitMQ: Producer(%s) recover channel OK. Total try %d times\n", p.name, i+1)
			return
		}
		log.Printf("[ERROR] RabbitMQ: Producer(%s) try to recover channel over maxRetry(%d), so exit\n", p.name, maxRetry)
	}
}

func (p *Producer) listenConfirm() {
	for c := range p.confirmChan {
		// TODO: 可以做个并发控制
		go p.confirm.Callback(c.DeliveryTag, c.Ack)
	}
}

func applyExchangeBinds(ch *amqp.Channel, exchangeBinds []*ExchangeBinds) (err error) {
	if ch == nil {
		return errors.New("RabbitMQ: Nil producer channel")
	}
	if len(exchangeBinds) <= 0 {
		return errors.New("RabbitMQ: Empty exchangeBinds")
	}

	for _, exchangeBind := range exchangeBinds {
		if exchangeBind.Exchange == nil {
			return errors.New("RabbitMQ: Nil exchange found.")
		}
		if len(exchangeBind.Bindings) <= 0 {
			return fmt.Errorf("RabbitMQ: No bindings queue found for exchange(%s)", exchangeBind.Exchange.Name)
		}
		// declare exchange
		ex := exchangeBind.Exchange
		if err = ch.ExchangeDeclare(ex.Name, ex.Kind, ex.Durable, ex.AutoDelete, ex.Internal, ex.NoWait, ex.Args); err != nil {
			return fmt.Errorf("RabbitMQ: Declare exchange(%s) failed, %v", ex.Name, err)
		}
		// declare and bind queues
		for _, b := range exchangeBind.Bindings {
			if b == nil {
				return fmt.Errorf("RabbitMQ: Nil binding found, exchange:%s", ex.Name)
			}
			if len(b.Queues) <= 0 {
				return fmt.Errorf("RabbitMQ: No queues found for exchange(%s)", ex.Name)
			}
			for _, q := range b.Queues {
				if q == nil {
					return fmt.Errorf("RabbitMQ: Nil queue found, exchange:%s", ex.Name)
				}
				if _, err = ch.QueueDeclare(q.Name, q.Durable, q.AutoDelete, q.Exclusive, q.NoWait, q.Args); err != nil {
					return fmt.Errorf("RabbitMQ: Declare queue(%s) failed, %v", q.Name, err)
				}
				if err = ch.QueueBind(q.Name, b.RouteKey, ex.Name, b.NoWait, b.Args); err != nil {
					return fmt.Errorf("RabbitMQ: Bind exchange(%s) <--> queue(%s) failed, %v", ex.Name, q.Name, err)
				}
			}
		}
	}
	return nil
}

type confirmHelper struct {
	mutex     sync.RWMutex
	listeners map[uint64]chan<- bool
	count     uint64
}

func newConfirmHelper() *confirmHelper {
	h := confirmHelper{}
	return h.Reset()
}

func (h *confirmHelper) Reset() *confirmHelper {
	h.mutex.Lock()
	defer h.mutex.Unlock()

	// 解除所有等待listener返回ACK的阻塞的地方
	for _, ch := range h.listeners {
		close(ch)
	}

	// Reset
	h.count = uint64(0)
	h.listeners = make(map[uint64]chan<- bool)
	return h
}

func (h *confirmHelper) Listen(ch chan<- bool) {
	h.mutex.Lock()
	h.count++
	h.listeners[h.count] = ch
	h.mutex.Unlock()
}

func (h *confirmHelper) Callback(idx uint64, ack bool) {
	h.mutex.Lock()
	ch := h.listeners[idx]
	delete(h.listeners, idx)
	h.mutex.Unlock()
	ch <- ack
}
