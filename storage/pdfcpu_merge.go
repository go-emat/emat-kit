package storage

import (
	"bytes"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"log"
	"strings"
	"time"

	"gitlab.com/go-emat/emat-kit/storage/pdf_merge"
	"gitlab.com/go-emat/emat-kit/storage/pdfcpu_helper"
	"gitlab.com/go-emat/pdfcpu-mattex/pkg/pdfcpu"
)

type PdfcpuMergeService struct {
	StorageFactory *DocumentStorageFactory
}

func NewPdfcpuMergeService(storageFactory *DocumentStorageFactory) *PdfcpuMergeService {
	return &PdfcpuMergeService{
		StorageFactory: storageFactory,
	}
}

func (s *PdfcpuMergeService) MergeFiles(req *PdfMergeRequest) (resp *PdfMergeResponse, errs *pdf_merge.Errors) {
	startTime := time.Now()
	resp = &PdfMergeResponse{}
	errs = pdf_merge.NewErrors()
	defer func() {
		resp.Duration = time.Since(startTime).Seconds()
		if len(errs.Errs) == 0 && len(errs.FatalErrs) == 0 {
			errs = nil
		}
	}()

	if len(req.InputFiles) == 0 {
		errs.AddFatal(errors.New("pdfMergeService.MergeFiles: should provide at least one input file"))
		return
	}

	files, totalUnsupportedFiles, es := s.getFilesAndUnsupportedFiles(req.InputFiles)
	if es != nil {
		errs.Append(es)
	}
	for _, f := range files {
		switch f.FileType {
		case PdfMergeFileTypeUnsupported, PdfMergeFileTypeNotExist:
			resp.UnsupportedFileNames = append(resp.UnsupportedFileNames, f.FileName)
		default:
			resp.SupportedFileNames = append(resp.SupportedFileNames, f.FileName)
		}
	}
	outputSource, err := s.StorageFactory.GetWriteCloser(req.OutputFilePath, "application/pdf")
	if err != nil {
		errs.AddFatal(err)
		return
	}

	defer func() {
		if err := outputSource.Close(); err != nil {
			errs.AddFatal(err)
		}
	}()

	initPdfCtx, es := s.createInitialPdfCtxWithAttachments(totalUnsupportedFiles)
	if es != nil {
		errs.Append(es)
		return
	}

	var watermarkTexts []string
	createAndMergePageWithWatermarks := func() {
		if req.DocumentNo != "" {
			watermarkTexts = append(watermarkTexts, fmt.Sprintf("Please find unsupported file(s) in document %v on the website", req.DocumentNo))
		} else {
			watermarkTexts = append(watermarkTexts, "Please find unsupported file(s) in attachments")
		}
		pdfCtx, err := s.createBlankPdfCtxWithWatermark(strings.Join(watermarkTexts, "\n"))
		if err != nil {
			errs.AddFatal(err)
		} else {
			if err := pdfcpu_helper.MergePdfToCtx(initPdfCtx, pdfCtx, "blank_page_with_watermarks"); err != nil {
				errs.AddFatal(err)
			}
		}
		watermarkTexts = []string{}
	}

	createAppendixLeadPage := func(file *PdfMergeFile) {
		pdfCtx, err := s.createAppendixLeadPage(file)
		if err != nil {
			errs.AddFatal(err)
		} else {
			if err := pdfcpu_helper.MergePdfToCtx(initPdfCtx, pdfCtx, "appendix_lead_page"); err != nil {
				errs.AddFatal(err)
			}
		}
	}

	for _, f := range files {
		if f.FileType == PdfMergeFileTypeUnsupported {
			watermarkTexts = append(watermarkTexts, fmt.Sprintf("Cannot merge file %s", f.FileName))
		} else if f.FileType == PdfMergeFileTypeNotExist {
			watermarkTexts = append(watermarkTexts, fmt.Sprintf("Cannot merge nonexistent file %s", f.FileName))
		} else if len(watermarkTexts) > 0 {
			createAndMergePageWithWatermarks()
		}

		if f.FileType == PdfMergeFileTypePDF {
			if f.Appendix != "" {
				createAppendixLeadPage(f)
			}
			if err := pdfcpu_helper.MergePdfToCtx(initPdfCtx, f.PdfcpuCtx, f.FileName); err != nil {
				errs.AddFatal(err)
			}
		}
		if f.FileType == PdfMergeFileTypeImage {
			if f.Appendix != "" {
				createAppendixLeadPage(f)
			}
			if err := pdfcpu_helper.ImportImageToCtx(initPdfCtx, nil, f.InputSource, f.FileName); err != nil {
				errs.AddFatal(err)
			}
		}
	}

	if len(watermarkTexts) > 0 {
		createAndMergePageWithWatermarks()
	}

	if err := pdfcpu_helper.ValidateAndWriteContext(initPdfCtx, outputSource); err != nil {
		errs.AddFatal(err)
		return
	}

	return
}

func (s *PdfcpuMergeService) GetPdfPageCount(filePath string) (int, error) {
	fileInputSource, err := s.StorageFactory.GetFileReadSeekCloser(filePath)
	if err != nil {
		return 0, errors.New(fmt.Sprintf("pdfMergeService.GetPdfPageCount: %s", err.Error()))
	}

	defer func() {
		if err := fileInputSource.Close(); err != nil {
			return
		}
	}()
	return pdfcpu_helper.PdfInfo(fileInputSource)
}

func (s *PdfcpuMergeService) ValidateMergeFileFormat(inputFile *PdfMergeFileInfo, outputDir string) (f *PdfMergeFile, err error) {
	f = &PdfMergeFile{
		PdfMergeFileInfo: inputFile,
	}
	fileInputSource, err := s.StorageFactory.GetFileReadSeekCloser(inputFile.FilePath)
	if err != nil {
		f.FileType = PdfMergeFileTypeNotExist
		return f, pdf_merge.NewFileOpenError(inputFile.FileName, err)
	}
	defer fileInputSource.Close()
	f.InputSource = fileInputSource
	f.FileType = getTypeByExt(strings.ToLower(inputFile.FileExtension))
	if f.FileType == PdfMergeFileTypePDF {
		pdfCtx, err := pdfcpu_helper.ReadAndValidateInputSource(f.FileName, f.InputSource, outputDir)
		if err != nil {
			log.Println("pdf validate err:", err)
			f.FileType = PdfMergeFileTypeUnsupported
			if f.InputSource != nil {
				if _, seekErr := f.InputSource.Seek(0, io.SeekStart); seekErr != nil {
					return f, seekErr
				}
			}
			return f, err
		} else {
			f.PdfcpuCtx = pdfCtx
		}
	}
	if f.FileType == PdfMergeFileTypeImage {
		if err := pdfcpu_helper.ValidateImageInputSource(&pdfcpu_helper.ImageFile{
			FileName:    f.FileName,
			InputSource: f.InputSource,
		}); err != nil {
			log.Println("image validate err:", err)
			f.FileType = PdfMergeFileTypeUnsupported
			if _, seekErr := f.InputSource.Seek(0, io.SeekStart); seekErr != nil {
				return f, seekErr
			}
			return f, err
		}
	}

	return f, nil
}

func (s *PdfcpuMergeService) ReadMergeFileFormat(inputFile *PdfMergeFileInfo) (f *PdfMergeFile, err error) {
	f = &PdfMergeFile{
		PdfMergeFileInfo: inputFile,
	}
	fileInputSource, err := s.StorageFactory.GetFileReadSeekCloser(inputFile.FilePath)
	if err != nil {
		f.FileType = PdfMergeFileTypeNotExist
		err = pdf_merge.NewFileOpenError(inputFile.FileName, err)
		return f, err
	}
	defer func() {
		if err := fileInputSource.Close(); err != nil {
			return
		}
	}()
	f.InputSource = fileInputSource
	f.FileType = getTypeByExt(strings.ToLower(inputFile.FileExtension))
	if f.FileType == PdfMergeFileTypePDF {
		pdfCtx, err := pdfcpu_helper.ReadInputSource(f.FileName, f.InputSource)
		if err != nil {
			f.FileType = PdfMergeFileTypeUnsupported
			if _, seekErr := f.InputSource.Seek(0, io.SeekStart); seekErr != nil {
				return f, seekErr
			}
			return f, err
		} else {
			f.PdfcpuCtx = pdfCtx
		}
	}
	if f.FileType == PdfMergeFileTypeImage {
		if err := pdfcpu_helper.ValidateImageInputSource(&pdfcpu_helper.ImageFile{
			FileName:    f.FileName,
			InputSource: f.InputSource,
		}); err != nil {
			log.Println("image validate err:", err)
			f.FileType = PdfMergeFileTypeUnsupported
			if _, seekErr := f.InputSource.Seek(0, io.SeekStart); seekErr != nil {
				return f, seekErr
			}
			return f, err
		}
	}

	return f, nil
}

func (s *PdfcpuMergeService) getFilesAndUnsupportedFiles(inputFiles []*PdfMergeFileInfo) (files, unsupportedFiles []*PdfMergeFile, errs *pdf_merge.Errors) {
	files, unsupportedFiles = []*PdfMergeFile{}, []*PdfMergeFile{}
	errs = pdf_merge.NewErrors()
	defer func() {
		if len(errs.Errs) == 0 && len(errs.FatalErrs) == 0 {
			errs = nil
		}
	}()
	for _, inputFile := range inputFiles {
		var f *PdfMergeFile
		var err error
		f, err = s.ValidateMergeFileFormat(inputFile, "")
		if err != nil {
			errs.Add(err)
		}
		files = append(files, f)
		if f.FileType == PdfMergeFileTypeUnsupported {
			unsupportedFiles = append(unsupportedFiles, f)
		}
	}
	return
}

func (s *PdfcpuMergeService) createInitialPdfCtxWithAttachments(files []*PdfMergeFile) (pdfCtx *pdfcpu.Context, errs *pdf_merge.Errors) {
	errs = pdf_merge.NewErrors()
	defer func() {
		if len(errs.Errs) == 0 && len(errs.FatalErrs) == 0 {
			errs = nil
		}
	}()

	pdfCtx, err := pdfcpu_helper.NewEmptyContext()
	if err != nil {
		errs.AddFatal(err)
		return
	}
	if len(files) > 0 {
		for _, af := range files {
			a := pdfcpu.Attachment{Reader: af.InputSource, ID: af.FileName, ModTime: af.UpdatedAt}
			if err = pdfCtx.AddAttachment(a, true); err != nil {
				errs.AddFatal(err)
				return
			}
		}
	}
	pdfCtx, err = pdfcpu_helper.WriteContextAndGetNewContext(pdfCtx)
	if err != nil {
		errs.AddFatal(err)
		return
	}
	return
}

func (s *PdfcpuMergeService) createBlankPdfCtxWithWatermark(text string) (pdfCtx *pdfcpu.Context, err error) {
	pdfCtx, err = pdfcpu_helper.NewBlankContext()
	if err != nil {
		return
	}

	wm, err := pdfcpu.ParseTextWatermarkDetails(text, "position:l, rotation:0, scalefactor:1, margins:5", true, pdfcpu.POINTS)
	if err != nil {
		return nil, err
	}
	if err := pdfcpu_helper.AddWatermarksToCtx(pdfCtx, []string{"1"}, wm); err != nil {
		return nil, err
	}
	return
}

func (s *PdfcpuMergeService) AddWatermarksToPdf(req *PdfWatermarksFileInfo) (err error) {
	if req.Watermarks == nil {
		return fmt.Errorf("Watermarks is required")
	}
	fileInputSource, err := s.StorageFactory.GetFileReadSeekCloser(req.FilePath)
	if err != nil {
		err = pdf_merge.NewFileOpenError(req.FileName, err)
		return err
	}
	defer func() {
		if err := fileInputSource.Close(); err != nil {
			return
		}
	}()

	pdfCtx, err := pdfcpu_helper.ReadAndValidateInputSource(req.FileName, fileInputSource, "")
	if err != nil {
		return err
	}
	for _, watermark := range req.Watermarks {
		wm, err := pdfcpu.ParseTextWatermarkDetails(watermark.Text, watermark.Desc, watermark.OnTop, pdfcpu.POINTS)
		if err != nil {
			return err
		}

		if watermark.Image != "" {
			wm.Mode = pdfcpu.WMImage
			buf := bytes.NewBufferString(watermark.Image)
			dec := base64.NewDecoder(base64.StdEncoding, buf)
			wm.FileName = watermark.Text
			wm.Image = dec
		}

		if err := pdfcpu_helper.AddWatermarksToCtx(pdfCtx, watermark.Pages, wm); err != nil {
			return err
		}
	}

	outputSource, err := s.StorageFactory.GetWriteCloser(req.FilePath, "application/pdf")

	if err != nil {
		return err
	}

	defer func() {
		if err := outputSource.Close(); err != nil {
			return
		}
	}()

	if err := pdfcpu_helper.ValidateAndWriteContext(pdfCtx, outputSource); err != nil {
		return err
	}

	return nil
}

func (s *PdfcpuMergeService) createAppendixLeadPage(file *PdfMergeFile) (*pdfcpu.Context, error) {
	leadPage := pdfcpu.Page{MediaBox: pdfcpu.RectForFormat("A4"), Fm: pdfcpu.FontMap{}, Buf: new(bytes.Buffer)}
	fontName := "Times-Roman"
	k := leadPage.Fm.EnsureKey(fontName)
	td := pdfcpu.TextDescriptor{
		Text:     file.Appendix,
		FontName: fontName,
		FontKey:  k,
		FontSize: 18,
		Scale:    1.0,
		ScaleAbs: true,
		X:        150,
		Y:        540,
	}
	pdfcpu.WriteMultiLine(leadPage.Buf, leadPage.MediaBox, nil, td)

	td = pdfcpu.TextDescriptor{
		Text:     strings.TrimRight(file.FileName, "."+file.FileExtension),
		FontName: fontName,
		FontKey:  k,
		FontSize: 18,
		Scale:    1.0,
		ScaleAbs: true,
		X:        150,
		Y:        500,
	}
	pdfcpu.WriteMultiLine(leadPage.Buf, leadPage.MediaBox, nil, td)

	return pdfcpu_helper.NewContextWithPage(&leadPage)
}
