package kafka

import (
	"context"
	"errors"
	"strings"
	"sync"
)

const (
	Waiting = iota
	Running
)

type EventsError struct {
	EventErrors []error
}

func (e EventsError) Error() string {
	if len(e.EventErrors) > 0 {
		var strs []string
		for _, err := range e.EventErrors {
			strs = append(strs, err.Error())
		}
		return strings.Join(strs, ";")
	}
	return ""
}

var WrongStateError = errors.New("can not take the event in the current producer agent state")

type EventResult struct {
	ProduceKafkaMessageID int64
	Partition             int32
	Offset                int64
	EventError            error
}

type EventReceiver interface {
	OnEvent(evt ProducerEvent)
}

type ProducerAgent struct {
	confirmChan chan int
	notify      chan []*EventResult
	cancel      context.CancelFunc
	ctx         context.Context
	events      []*ProducerEvent
	state       int
}

func NewProducerAgent() *ProducerAgent {
	return &ProducerAgent{
		confirmChan: make(chan int),
		notify:      make(chan []*EventResult),
		state:       Waiting,
	}
}

func (agt *ProducerAgent) EventProcessGoroutine() {
	select {
	case confirm := <-agt.confirmChan:
		if confirm > 0 {
			err := agt.startEvents()
			agt.notify <- err
		}
	case <-agt.ctx.Done():
		return
	}
}

func (agt *ProducerAgent) startEvents() []*EventResult {
	var eventResults []*EventResult
	var mutex sync.Mutex
	var wg sync.WaitGroup
	for _, event := range agt.events {
		wg.Add(1)
		go func(event *ProducerEvent, ctx context.Context) {
			defer func() {
				mutex.Unlock()
			}()
			defer wg.Done()
			eventResult := event.Process(ctx)
			mutex.Lock()
			eventResults = append(eventResults, eventResult)
		}(event, agt.ctx)
	}
	wg.Wait()
	return eventResults
}

func (agt *ProducerAgent) RegisterEvent(event *ProducerEvent) error {
	if agt.state != Running {
		return WrongStateError
	}
	agt.events = append(agt.events, event)
	return nil
}

func (agt *ProducerAgent) Start() error {
	if agt.state != Waiting {
		return WrongStateError
	}
	agt.state = Running
	agt.ctx, agt.cancel = context.WithCancel(context.Background())
	go agt.EventProcessGoroutine()
	return nil
}

func (agt *ProducerAgent) Confirm() ([]*EventResult, error) {
	if agt.state != Running {
		return nil, WrongStateError
	}
	agt.confirmChan <- 1
	return <-agt.notify, nil
}

func (agt *ProducerAgent) Stop() error {
	//close by context
	if agt.state != Running {
		return WrongStateError
	}
	agt.state = Waiting
	agt.cancel()
	return nil
	//return agt.stopEvents() // because no trigger the start event,  so will trigger timeout to return
}

func (agt *ProducerAgent) stopEvents() error {
	var err error
	var errs EventsError
	for _, event := range agt.events {
		if err = event.Stop(); err != nil {
			errs.EventErrors = append(errs.EventErrors, errors.New(event.Topic+event.Key+":"+err.Error()))
		}
	}
	return errs
}
