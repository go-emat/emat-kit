// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package documentpb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// InboundMaterialDeliveryRequestServiceClient is the client API for InboundMaterialDeliveryRequestService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type InboundMaterialDeliveryRequestServiceClient interface {
	CreateInboundMdr(ctx context.Context, in *CreateInboundMdrRequest, opts ...grpc.CallOption) (*CreateInboundMdrResponse, error)
	UpdateInboundMdr(ctx context.Context, in *UpdateInboundMdrRequest, opts ...grpc.CallOption) (*SimpleResponse, error)
	GetAvailableItems(ctx context.Context, in *GetAvailableItemsRequest, opts ...grpc.CallOption) (*GetAvailableItemsResponse, error)
	UpdateItemsOutstandingQty(ctx context.Context, in *UpdateItemsOutstandingQtyRequest, opts ...grpc.CallOption) (*SimpleResponse, error)
}

type inboundMaterialDeliveryRequestServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewInboundMaterialDeliveryRequestServiceClient(cc grpc.ClientConnInterface) InboundMaterialDeliveryRequestServiceClient {
	return &inboundMaterialDeliveryRequestServiceClient{cc}
}

func (c *inboundMaterialDeliveryRequestServiceClient) CreateInboundMdr(ctx context.Context, in *CreateInboundMdrRequest, opts ...grpc.CallOption) (*CreateInboundMdrResponse, error) {
	out := new(CreateInboundMdrResponse)
	err := c.cc.Invoke(ctx, "/tms.InboundMaterialDeliveryRequestService/CreateInboundMdr", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *inboundMaterialDeliveryRequestServiceClient) UpdateInboundMdr(ctx context.Context, in *UpdateInboundMdrRequest, opts ...grpc.CallOption) (*SimpleResponse, error) {
	out := new(SimpleResponse)
	err := c.cc.Invoke(ctx, "/tms.InboundMaterialDeliveryRequestService/UpdateInboundMdr", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *inboundMaterialDeliveryRequestServiceClient) GetAvailableItems(ctx context.Context, in *GetAvailableItemsRequest, opts ...grpc.CallOption) (*GetAvailableItemsResponse, error) {
	out := new(GetAvailableItemsResponse)
	err := c.cc.Invoke(ctx, "/tms.InboundMaterialDeliveryRequestService/GetAvailableItems", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *inboundMaterialDeliveryRequestServiceClient) UpdateItemsOutstandingQty(ctx context.Context, in *UpdateItemsOutstandingQtyRequest, opts ...grpc.CallOption) (*SimpleResponse, error) {
	out := new(SimpleResponse)
	err := c.cc.Invoke(ctx, "/tms.InboundMaterialDeliveryRequestService/UpdateItemsOutstandingQty", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// InboundMaterialDeliveryRequestServiceServer is the server API for InboundMaterialDeliveryRequestService service.
// All implementations must embed UnimplementedInboundMaterialDeliveryRequestServiceServer
// for forward compatibility
type InboundMaterialDeliveryRequestServiceServer interface {
	CreateInboundMdr(context.Context, *CreateInboundMdrRequest) (*CreateInboundMdrResponse, error)
	UpdateInboundMdr(context.Context, *UpdateInboundMdrRequest) (*SimpleResponse, error)
	GetAvailableItems(context.Context, *GetAvailableItemsRequest) (*GetAvailableItemsResponse, error)
	UpdateItemsOutstandingQty(context.Context, *UpdateItemsOutstandingQtyRequest) (*SimpleResponse, error)
	mustEmbedUnimplementedInboundMaterialDeliveryRequestServiceServer()
}

// UnimplementedInboundMaterialDeliveryRequestServiceServer must be embedded to have forward compatible implementations.
type UnimplementedInboundMaterialDeliveryRequestServiceServer struct {
}

func (UnimplementedInboundMaterialDeliveryRequestServiceServer) CreateInboundMdr(context.Context, *CreateInboundMdrRequest) (*CreateInboundMdrResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateInboundMdr not implemented")
}
func (UnimplementedInboundMaterialDeliveryRequestServiceServer) UpdateInboundMdr(context.Context, *UpdateInboundMdrRequest) (*SimpleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateInboundMdr not implemented")
}
func (UnimplementedInboundMaterialDeliveryRequestServiceServer) GetAvailableItems(context.Context, *GetAvailableItemsRequest) (*GetAvailableItemsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetAvailableItems not implemented")
}
func (UnimplementedInboundMaterialDeliveryRequestServiceServer) UpdateItemsOutstandingQty(context.Context, *UpdateItemsOutstandingQtyRequest) (*SimpleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateItemsOutstandingQty not implemented")
}
func (UnimplementedInboundMaterialDeliveryRequestServiceServer) mustEmbedUnimplementedInboundMaterialDeliveryRequestServiceServer() {
}

// UnsafeInboundMaterialDeliveryRequestServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to InboundMaterialDeliveryRequestServiceServer will
// result in compilation errors.
type UnsafeInboundMaterialDeliveryRequestServiceServer interface {
	mustEmbedUnimplementedInboundMaterialDeliveryRequestServiceServer()
}

func RegisterInboundMaterialDeliveryRequestServiceServer(s grpc.ServiceRegistrar, srv InboundMaterialDeliveryRequestServiceServer) {
	s.RegisterService(&_InboundMaterialDeliveryRequestService_serviceDesc, srv)
}

func _InboundMaterialDeliveryRequestService_CreateInboundMdr_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateInboundMdrRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(InboundMaterialDeliveryRequestServiceServer).CreateInboundMdr(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tms.InboundMaterialDeliveryRequestService/CreateInboundMdr",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(InboundMaterialDeliveryRequestServiceServer).CreateInboundMdr(ctx, req.(*CreateInboundMdrRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _InboundMaterialDeliveryRequestService_UpdateInboundMdr_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateInboundMdrRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(InboundMaterialDeliveryRequestServiceServer).UpdateInboundMdr(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tms.InboundMaterialDeliveryRequestService/UpdateInboundMdr",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(InboundMaterialDeliveryRequestServiceServer).UpdateInboundMdr(ctx, req.(*UpdateInboundMdrRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _InboundMaterialDeliveryRequestService_GetAvailableItems_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetAvailableItemsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(InboundMaterialDeliveryRequestServiceServer).GetAvailableItems(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tms.InboundMaterialDeliveryRequestService/GetAvailableItems",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(InboundMaterialDeliveryRequestServiceServer).GetAvailableItems(ctx, req.(*GetAvailableItemsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _InboundMaterialDeliveryRequestService_UpdateItemsOutstandingQty_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateItemsOutstandingQtyRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(InboundMaterialDeliveryRequestServiceServer).UpdateItemsOutstandingQty(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tms.InboundMaterialDeliveryRequestService/UpdateItemsOutstandingQty",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(InboundMaterialDeliveryRequestServiceServer).UpdateItemsOutstandingQty(ctx, req.(*UpdateItemsOutstandingQtyRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _InboundMaterialDeliveryRequestService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "tms.InboundMaterialDeliveryRequestService",
	HandlerType: (*InboundMaterialDeliveryRequestServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateInboundMdr",
			Handler:    _InboundMaterialDeliveryRequestService_CreateInboundMdr_Handler,
		},
		{
			MethodName: "UpdateInboundMdr",
			Handler:    _InboundMaterialDeliveryRequestService_UpdateInboundMdr_Handler,
		},
		{
			MethodName: "GetAvailableItems",
			Handler:    _InboundMaterialDeliveryRequestService_GetAvailableItems_Handler,
		},
		{
			MethodName: "UpdateItemsOutstandingQty",
			Handler:    _InboundMaterialDeliveryRequestService_UpdateItemsOutstandingQty_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "tms/document/inbound_material_delivery_request.proto",
}
