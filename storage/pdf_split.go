package storage

import (
	"errors"
	"gitlab.com/go-emat/emat-kit/storage/pdfcpu_helper"
	"time"
)

type PdfSplitInfo struct {
	File     []byte
	FileName string
}

type PdfSplitRequest struct {
	SplitFile *PdfSplitInfo
	OutputDir string
}

type PdfSplitResponse struct {
	Duration    float64
	OutputFiles []string
}

type PdfSplitService struct {
	StorageFactory *DocumentStorageFactory
}

func NewPdfSplitService(storageBucketName, storageType string) *PdfSplitService {
	return &PdfSplitService{
		StorageFactory: &DocumentStorageFactory{
			StorageBucketName: storageBucketName,
			StorageType:       storageType,
		},
	}
}

func NewDefaultPdfSplitService() *PdfSplitService {
	return &PdfSplitService{
		StorageFactory: newDefaultDocumentStorageFactory(),
	}
}

func (s *PdfSplitService) SplitPdf(req *PdfSplitRequest) (*PdfSplitResponse, error) {
	startTime := time.Now()
	resp := &PdfSplitResponse{}
	defer func() {
		resp.Duration = time.Since(startTime).Seconds()
	}()

	if req.SplitFile == nil {
		return nil, errors.New("SplitFile is required")
	}

	if req.OutputDir == "" {
		return nil, errors.New("OutputDir is required")
	}

	if err := pdfcpu_helper.MakeDir(req.OutputDir); err != nil {
		return nil, err
	}

	if err := pdfcpu_helper.SplitPdf(req.SplitFile.File, req.SplitFile.FileName, req.OutputDir); err != nil {
		return nil, err
	}

	splitFiles, err := pdfcpu_helper.ReadDir(req.OutputDir)
	if err != nil {
		return nil, err
	}

	resp.OutputFiles = splitFiles

	return resp, nil
}

func (s *PdfSplitService) RemoveSplitDir(req *PdfSplitRequest) error {
	return pdfcpu_helper.RemoveDir(req.OutputDir)
}
