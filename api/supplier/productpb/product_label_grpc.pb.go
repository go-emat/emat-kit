// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package productpb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// ProductLabelServiceClient is the client API for ProductLabelService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type ProductLabelServiceClient interface {
	CreateProductLabel(ctx context.Context, in *CreateProductLabelRequest, opts ...grpc.CallOption) (*CreateProductLabelResponse, error)
	ReadProductLabel(ctx context.Context, in *ReadProductLabelRequest, opts ...grpc.CallOption) (*ReadProductLabelResponse, error)
	UpdateProductLabel(ctx context.Context, in *UpdateProductLabelRequest, opts ...grpc.CallOption) (*UpdateProductLabelResponse, error)
	DisableProductLabel(ctx context.Context, in *DisableProductLabelRequest, opts ...grpc.CallOption) (*DisableProductLabelResponse, error)
	ListProductLabels(ctx context.Context, in *ListProductLabelsRequest, opts ...grpc.CallOption) (*ListProductLabelsResponse, error)
}

type productLabelServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewProductLabelServiceClient(cc grpc.ClientConnInterface) ProductLabelServiceClient {
	return &productLabelServiceClient{cc}
}

func (c *productLabelServiceClient) CreateProductLabel(ctx context.Context, in *CreateProductLabelRequest, opts ...grpc.CallOption) (*CreateProductLabelResponse, error) {
	out := new(CreateProductLabelResponse)
	err := c.cc.Invoke(ctx, "/supplier.ProductLabelService/CreateProductLabel", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *productLabelServiceClient) ReadProductLabel(ctx context.Context, in *ReadProductLabelRequest, opts ...grpc.CallOption) (*ReadProductLabelResponse, error) {
	out := new(ReadProductLabelResponse)
	err := c.cc.Invoke(ctx, "/supplier.ProductLabelService/ReadProductLabel", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *productLabelServiceClient) UpdateProductLabel(ctx context.Context, in *UpdateProductLabelRequest, opts ...grpc.CallOption) (*UpdateProductLabelResponse, error) {
	out := new(UpdateProductLabelResponse)
	err := c.cc.Invoke(ctx, "/supplier.ProductLabelService/UpdateProductLabel", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *productLabelServiceClient) DisableProductLabel(ctx context.Context, in *DisableProductLabelRequest, opts ...grpc.CallOption) (*DisableProductLabelResponse, error) {
	out := new(DisableProductLabelResponse)
	err := c.cc.Invoke(ctx, "/supplier.ProductLabelService/DisableProductLabel", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *productLabelServiceClient) ListProductLabels(ctx context.Context, in *ListProductLabelsRequest, opts ...grpc.CallOption) (*ListProductLabelsResponse, error) {
	out := new(ListProductLabelsResponse)
	err := c.cc.Invoke(ctx, "/supplier.ProductLabelService/ListProductLabels", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ProductLabelServiceServer is the server API for ProductLabelService service.
// All implementations must embed UnimplementedProductLabelServiceServer
// for forward compatibility
type ProductLabelServiceServer interface {
	CreateProductLabel(context.Context, *CreateProductLabelRequest) (*CreateProductLabelResponse, error)
	ReadProductLabel(context.Context, *ReadProductLabelRequest) (*ReadProductLabelResponse, error)
	UpdateProductLabel(context.Context, *UpdateProductLabelRequest) (*UpdateProductLabelResponse, error)
	DisableProductLabel(context.Context, *DisableProductLabelRequest) (*DisableProductLabelResponse, error)
	ListProductLabels(context.Context, *ListProductLabelsRequest) (*ListProductLabelsResponse, error)
	mustEmbedUnimplementedProductLabelServiceServer()
}

// UnimplementedProductLabelServiceServer must be embedded to have forward compatible implementations.
type UnimplementedProductLabelServiceServer struct {
}

func (UnimplementedProductLabelServiceServer) CreateProductLabel(context.Context, *CreateProductLabelRequest) (*CreateProductLabelResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateProductLabel not implemented")
}
func (UnimplementedProductLabelServiceServer) ReadProductLabel(context.Context, *ReadProductLabelRequest) (*ReadProductLabelResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ReadProductLabel not implemented")
}
func (UnimplementedProductLabelServiceServer) UpdateProductLabel(context.Context, *UpdateProductLabelRequest) (*UpdateProductLabelResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateProductLabel not implemented")
}
func (UnimplementedProductLabelServiceServer) DisableProductLabel(context.Context, *DisableProductLabelRequest) (*DisableProductLabelResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DisableProductLabel not implemented")
}
func (UnimplementedProductLabelServiceServer) ListProductLabels(context.Context, *ListProductLabelsRequest) (*ListProductLabelsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListProductLabels not implemented")
}
func (UnimplementedProductLabelServiceServer) mustEmbedUnimplementedProductLabelServiceServer() {}

// UnsafeProductLabelServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to ProductLabelServiceServer will
// result in compilation errors.
type UnsafeProductLabelServiceServer interface {
	mustEmbedUnimplementedProductLabelServiceServer()
}

func RegisterProductLabelServiceServer(s grpc.ServiceRegistrar, srv ProductLabelServiceServer) {
	s.RegisterService(&_ProductLabelService_serviceDesc, srv)
}

func _ProductLabelService_CreateProductLabel_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateProductLabelRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProductLabelServiceServer).CreateProductLabel(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/supplier.ProductLabelService/CreateProductLabel",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProductLabelServiceServer).CreateProductLabel(ctx, req.(*CreateProductLabelRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ProductLabelService_ReadProductLabel_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ReadProductLabelRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProductLabelServiceServer).ReadProductLabel(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/supplier.ProductLabelService/ReadProductLabel",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProductLabelServiceServer).ReadProductLabel(ctx, req.(*ReadProductLabelRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ProductLabelService_UpdateProductLabel_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateProductLabelRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProductLabelServiceServer).UpdateProductLabel(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/supplier.ProductLabelService/UpdateProductLabel",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProductLabelServiceServer).UpdateProductLabel(ctx, req.(*UpdateProductLabelRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ProductLabelService_DisableProductLabel_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DisableProductLabelRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProductLabelServiceServer).DisableProductLabel(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/supplier.ProductLabelService/DisableProductLabel",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProductLabelServiceServer).DisableProductLabel(ctx, req.(*DisableProductLabelRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ProductLabelService_ListProductLabels_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListProductLabelsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProductLabelServiceServer).ListProductLabels(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/supplier.ProductLabelService/ListProductLabels",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProductLabelServiceServer).ListProductLabels(ctx, req.(*ListProductLabelsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _ProductLabelService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "supplier.ProductLabelService",
	HandlerType: (*ProductLabelServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateProductLabel",
			Handler:    _ProductLabelService_CreateProductLabel_Handler,
		},
		{
			MethodName: "ReadProductLabel",
			Handler:    _ProductLabelService_ReadProductLabel_Handler,
		},
		{
			MethodName: "UpdateProductLabel",
			Handler:    _ProductLabelService_UpdateProductLabel_Handler,
		},
		{
			MethodName: "DisableProductLabel",
			Handler:    _ProductLabelService_DisableProductLabel_Handler,
		},
		{
			MethodName: "ListProductLabels",
			Handler:    _ProductLabelService_ListProductLabels_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "supplier/product/product_label.proto",
}
