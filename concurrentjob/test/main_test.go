package test

import (
	"os"
	"testing"

	"gitlab.com/go-emat/emat-kit/concurrentjob"
)

var jobsBroker = concurrentjob.NewModelJobsBroker(50)

func TestMain(m *testing.M) {
	go jobsBroker.Start()
	exitCode := m.Run()
	os.Exit(exitCode)
}
