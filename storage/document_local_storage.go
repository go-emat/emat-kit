package storage

import (
	"archive/zip"
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/go-emat/emat-kit/storage/common"

	"github.com/360EntSecGroup-Skylar/excelize/v2"
	"github.com/SebastiaanKlippert/go-wkhtmltopdf"
)

var (
	SkipGrpcStructure          = "skip"
	LocalDownloadAbsPrefixPath = "/gateway/"
	//LocalStorageBasePath storage base path
	LocalStorageBasePath = "./storage"
	//PdfLocalStorageDocumentBasePath pdf storage base path
	PdfLocalStorageDocumentBasePath = LocalStorageBasePath + "/document/"
	//PdfBackupLocalStorageDocumentBasePath pdf backup storage base path
	PdfBackupLocalStorageDocumentBasePath = LocalStorageBasePath + "/del/document/"
	//PdfGoogleStorageBasePath pdf base path on google storage
	PdfLocalStorageFileBasePath = LocalStorageBasePath + "/files/"

	ZipLocalStorageFileBasePath = LocalStorageBasePath + "/zip/"
)

type DocumentLocalStorage struct {
	LocalAbsPrefixPath string
}

func NewDocumentLocalStorage(LocalAbsPrefixPath ...string) *DocumentLocalStorage {
	if len(LocalAbsPrefixPath) > 0 {
		return &DocumentLocalStorage{
			LocalAbsPrefixPath: LocalAbsPrefixPath[0],
		}
	}
	return &DocumentLocalStorage{}
}

func (s *DocumentLocalStorage) GetWriter(newFilePath string, contentType string) (writer interface{}, err error) {
	dirPath := filepath.Dir(newFilePath)
	if err := os.MkdirAll(dirPath, 0755); err != nil {
		return nil, err
	}
	writer, err = os.Create(newFilePath)
	if err != nil {
		return nil, fmt.Errorf("cannot create file: %w", err)
	}
	return
}

func (s *DocumentLocalStorage) GetReader(filePath string) (reader io.ReadCloser, err error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	_, err = file.Seek(0, 0)
	if err != nil {
		return nil, err
	}
	return file, nil
}

func (s *DocumentLocalStorage) GetFileWriter(newFilePath string) (savedPath string, writer interface{}, err error) {
	dirPath := PdfLocalStorageFileBasePath + filepath.Dir(newFilePath)
	savedPath = PdfLocalStorageFileBasePath + newFilePath
	if err := os.MkdirAll(dirPath, 0755); err != nil {
		return "", nil, err
	}
	writer, err = os.Create(savedPath)
	if err != nil {
		return "", nil, fmt.Errorf("cannot create file: %w", err)
	}
	return
}

func (s *DocumentLocalStorage) WriteFile(writer interface{}, buffer []byte, index int64) (err error) {
	if writer == nil {
		return errors.New("No writer")
	}
	w := writer.(*os.File)
	if _, err := w.WriteAt(buffer, index); err != nil {
		return err
	}
	return nil
}

func (s *DocumentLocalStorage) CloseFileWriter(writer interface{}) error {
	if writer == nil {
		return errors.New("No writer")
	}
	w := writer.(*os.File)
	if err := w.Close(); err != nil {
		return err
	}
	return nil
}

// SaveFileToStorage save pdf to storage (local or google)
func (s *DocumentLocalStorage) SaveFileToStorage(req *UploadFileRequest) (*UploadFileResponse, error) {

	//or save local storage
	dirPath := PdfLocalStorageFileBasePath + filepath.Dir(req.FilePath)
	filePath := PdfLocalStorageFileBasePath + req.FilePath
	if len(req.PutUnderDocumentType) > 0 {
		dirPath = PdfLocalStorageDocumentBasePath + getDocumentPdfStoragePath(req.PutUnderDocumentType)
		filePath = dirPath + req.FileName
	}
	if err := os.MkdirAll(dirPath, 0755); err != nil {
		return nil, err
	}
	if len(req.FileContent) == 0 && req.FileContentReader != nil {
		var err error
		req.FileContent, err = ioutil.ReadAll(req.FileContentReader)
		if err != nil {
			return nil, err
		}
	}
	if err := ioutil.WriteFile(filePath, req.FileContent, 0755); err != nil {
		return nil, err
	}
	return &UploadFileResponse{
		FilePath: filePath,
		//StorageType:          StorageTypeLocal,
	}, nil
}

func (s *DocumentLocalStorage) SaveExcelToStorage(filename string, xlsx *excelize.File, extension string) (url string, err error) {
	filePath := PdfLocalStorageDocumentBasePath + filename

	dirPath := PdfLocalStorageDocumentBasePath + filepath.Dir(filePath)
	if err := os.MkdirAll(dirPath, 0755); err != nil {
		return "", err
	}

	if err := xlsx.SaveAs(filePath); err != nil {
		return "", err
	}

	return filePath, nil
}

// BackupStoragePdf backup pdf process
func (s *DocumentLocalStorage) BackupStoragePdf(dbPdfPath string) (bool, error) {
	var newFilePath string
	// Local
	if !s.FilePathExists(dbPdfPath) {
		//ignore source file not found
		return true, nil
	}
	newFilePath = strings.Replace(dbPdfPath, PdfLocalStorageDocumentBasePath, PdfBackupLocalStorageDocumentBasePath, -1)
	folderPathOnly := s.getPathExceptFile(newFilePath)
	if err := os.MkdirAll(folderPathOnly, 0755); err != nil {
		panic(err)
	}
	if err := os.Rename(dbPdfPath, newFilePath); err != nil {
		return false, err
	}
	return true, nil
}

// SavePdfToStorage save pdf to storage (local or google) BY WKHTML object
func (s *DocumentLocalStorage) SavePdfToStorage(pdfg *wkhtmltopdf.PDFGenerator, documentType, fileName string) (*GeneratePdfToStorageResponse, error) {

	documentStoragePath := getDocumentPdfStoragePath(documentType)
	dirPath := PdfLocalStorageDocumentBasePath + documentStoragePath
	filePath := dirPath + fileName
	//or save local storage

	if err := os.MkdirAll(dirPath, 0755); err != nil {
		return nil, err
	}
	if err := pdfg.WriteFile(filePath); err != nil {
		return nil, err
	}
	return &GeneratePdfToStorageResponse{
		FilePath:    filePath,
		StorageType: StorageTypeLocal,
	}, nil
}

func (s *DocumentLocalStorage) MoveStorageFile(srcPath, destPath string) error {
	folderPathOnly := s.getPathExceptFile(destPath)
	os.MkdirAll(folderPathOnly, 0755)
	return os.Rename(srcPath, destPath)
}

func (s *DocumentLocalStorage) DeleteStorageFile(filePath string) error {
	return os.Remove(filePath)
}

// DownloadFile
func (s *DocumentLocalStorage) DownloadFile(filePath string) ([]byte, error) {
	fileAbsPath := s.getIfGrpcPath(filePath)

	if s.FilePathExists(fileAbsPath) {
		return ioutil.ReadFile(fileAbsPath)
	} else {
		return nil, errors.New("File not exist")
	}
}

func (s *DocumentLocalStorage) getIfGrpcPath(filePath string) string {
	wd, err := os.Getwd()
	if err != nil {
		log.Printf("fail to get root path %v\n", err)
	}

	var fileAbsPath string
	// if we don't use GRPC
	if s.LocalAbsPrefixPath == SkipGrpcStructure {
		fileAbsPath = filePath
	} else {
		// if we use GRPC
		rootPath := filepath.Dir(wd)
		if s.LocalAbsPrefixPath == "" {
			s.LocalAbsPrefixPath = LocalDownloadAbsPrefixPath
		}
		fileAbsPath = rootPath + strings.Replace(filePath, "./", s.LocalAbsPrefixPath, 1)
	}
	return fileAbsPath
}

// ZipStorageDocument
func (s *DocumentLocalStorage) ZipStorageDocument(zipFilePath, zipFolder string, files []string) (string, error) {

	//or save local storage
	dirPath := ZipLocalStorageFileBasePath + filepath.Dir(zipFilePath)
	filePath := ZipLocalStorageFileBasePath + zipFilePath
	if err := os.MkdirAll(dirPath, 0755); err != nil {
		return "nil", err
	}
	// Create a buffer to write our archive to.
	// buf := new(bytes.Buffer)
	newZipFile, err := os.Create(filePath)
	if err != nil {
		return "", err
	}
	// Create a new zip archive to memory buffer
	zipWriter := zip.NewWriter(newZipFile)
	// go through each file in the prefix
	for _, fileName := range files {
		fileContent, err := ioutil.ReadFile(fileName)
		if err != nil {
			return "", err
		}
		// add filename to zip to do change file
		zipFile, err := zipWriter.Create(fileName)
		if err != nil {
			return "", err
		}
		_, err = zipFile.Write(fileContent)
		if err != nil {
			log.Fatal(err)
		}
	}
	err = zipWriter.Close()
	return dirPath, err
}

// ZipStorageDocument
func (s *DocumentLocalStorage) ZipContentToStorageDocument(zipContent *common.ZipContent) ([]byte, error) {
	// Create a buffer to write our archive to.
	buf := new(bytes.Buffer)
	// Create a new zip archive to memory buffer
	zipWriter := zip.NewWriter(buf)
	// go through each file in the prefix
	for _, file := range zipContent.File {
		// fmt.Println("file", file, file.Filename, s.getIfGrpcPath(file.FilePath))
		fileContent, err := ioutil.ReadFile(s.getIfGrpcPath(file.FilePath))
		if err != nil {
			return nil, err
		}
		// add filename to zip to do change file
		zipFile, err := zipWriter.Create(zipContent.FolderName + "/" + file.Filename)
		if err != nil {
			return nil, err
		}
		_, err = zipFile.Write(fileContent)
		if err != nil {
			log.Fatal(err)
		}
	}
	err := common.CopyToZipWriter(zipWriter, zipContent)
	if err != nil {
		return nil, err
	}
	err = zipWriter.Close()
	return buf.Bytes(), err
}

// GenerateSignedDownloadURL
func (s *DocumentLocalStorage) GenerateSignedDownloadURL(fileName string, option ...common.SignedUrlOption) (string, error) {
	log.Println("local storage no need signed url")
	return "", nil
	// return "http://localhost:8870/storage-files/download/zip", nil
}

func (s *DocumentLocalStorage) GenerateResumableSessionURL(req *SessionURLRequest) (string, error) {
	return "", errors.New("local storage no impl resumable session url")
}

func (s *DocumentLocalStorage) CompleteSessionUrl(filepath, uploadID string) error {
	return errors.New("local storage no impl part upload")
}

// GetFileReadSeeker
func (s *DocumentLocalStorage) GetFileReadSeekCloser(filePath string) (io.ReadSeekCloser, error) {
	if !strings.HasPrefix(filePath, LocalStorageBasePath) {
		filePath = LocalStorageBasePath + "/" + filePath
	}
	return os.Open(filePath)
}

// GetFileWriter
func (s *DocumentLocalStorage) GetFileWriteCloser(filePath, contentType string) (io.WriteCloser, error) {
	if !strings.HasPrefix(filePath, LocalStorageBasePath) {
		filePath = LocalStorageBasePath + "/" + filePath
	}
	fileDir := s.getPathExceptFile(filePath)
	if err := os.MkdirAll(fileDir, 0755); err != nil {
		return nil, err
	}
	return os.Create(filePath)
}

// GetPathExceptFile get directory path by full path
func (s *DocumentLocalStorage) getPathExceptFile(filePath string) string {
	index := strings.LastIndex(filePath, "/")
	path := filePath[:index+1]
	return path
}

// FilePathExists FilePathExists
func (s *DocumentLocalStorage) FilePathExists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return false
}

func (s *DocumentLocalStorage) FileExists(path string) bool {
	return s.FilePathExists(path)
}

func (s *DocumentLocalStorage) GetFileMeta(filePath string) (*common.FileMeta, error) {
	return nil, errors.New("local storage no impl GetFileMeta")
}