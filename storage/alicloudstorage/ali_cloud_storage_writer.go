package alicloudstorage

import (
	"bytes"
	"fmt"
	"unicode/utf8"

	"github.com/aliyun/aliyun-oss-go-sdk/oss"
)

type Writer struct {
	Bucket    *oss.Bucket
	ObjectKey string
	nextPos   int64
	buffer    *bytes.Buffer
	partSize  int64
	err       error
}

func NewWriter(bucket *oss.Bucket, objectKey string) *Writer {
	if !utf8.ValidString(objectKey) {
		return nil
	}

	return &Writer{
		Bucket:    bucket,
		ObjectKey: objectKey,
		buffer:    bytes.NewBuffer(nil),
		partSize:  4 * 1024 * 1024, // Default part size: ? MB
	}
}

func (w *Writer) Write(p []byte) (n int, err error) {
	n, err = w.buffer.Write(p)
	if err != nil {
		return 0, fmt.Errorf("failed to write to buffer: %w", err)
	}

	if w.nextPos == 0 {
		if exist, err := w.Bucket.IsObjectExist(w.ObjectKey); err != nil {
			return n, err
		} else {
			if exist {
				_, err = w.Bucket.CopyObject(w.ObjectKey, "del/"+w.ObjectKey)
				if err != nil {
					return n, err
				}
				err = w.Bucket.DeleteObject(w.ObjectKey)
				if err != nil {
					return n, err
				}
			}
		}
	}

	if int64(w.buffer.Len()) >= w.partSize {
		if err := w.flushBuffer(); err != nil {
			return 0, err
		}
	}

	return n, nil
}

func (w *Writer) flushBuffer() error {
	if w.buffer.Len() == 0 {
		return nil
	}
	nextPos, err := w.Bucket.AppendObject(w.ObjectKey, bytes.NewReader(w.buffer.Bytes()), w.nextPos)
	if err != nil {
		return fmt.Errorf("failed to append object: %w", err)
	}
	// log.Println("dk flushBuffer", err, ":::", w.nextPos, nextPos)
	w.nextPos = nextPos
	w.buffer.Reset()
	return nil
}

func (w *Writer) Close() error {
	if err := w.flushBuffer(); err != nil {
		return fmt.Errorf("failed to close writer: %w", err)
	}
	return w.err
}
