syntax = "proto3";

package supplier;

option go_package = "supplier/settingpb";
import "common/common.proto";
import "google/protobuf/timestamp.proto";

service FileService {
    rpc ListFiles (ListFilesRequest) returns (ListFilesResponse);
    rpc GetFileRecord (GetFileRecordRequest) returns (GetFileRecordResponse);
    rpc CreateFilesRecord (CreateFilesRecordRequest) returns (CreateFilesRecordResponse);
    rpc CreateOrUpdateFilesRecord (CreateOrUpdateFilesRecordRequest) returns (CreateOrUpdateFilesRecordResponse);
    rpc UpdateFileRecord (UpdateFileRecordRequest) returns (UpdateFileRecordResponse);
    rpc DeleteFileRecord (DeleteFileRecordRequest) returns (DeleteFileRecordResponse);
    rpc ListFileTypes (ListFileTypesRequest) returns (ListFileTypesResponse);
    rpc ListMdmFileTypes (ListMdmFileTypesRequest) returns (ListMdmFileTypesResponse);

    // GOOGLE API UPLOAD START
    rpc GenerateUploadFilesUrl (GenerateUploadFilesUrlRequest) returns (GenerateUploadFilesUrlResponse);
    rpc CompleteUploadFilesUrl (CompleteUploadFilesUrlRequest) returns (CompleteUploadFilesUrlResponse);
    rpc GenerateDownloadSignedUrl (GenerateDownloadSignedUrlRequest) returns (GenerateDownloadSignedUrlResponse);
    // GOOGLE API UPLOAD END

    // RPC FOR MAIN CON CALL START
    rpc ListFilesForMainCon (ListFilesForMainConRequest) returns (ListFilesResponse);
    // RPC FOR MAIN CON CALL END
}

message File {
    int64 id = 1;
    int64 supplier_id = 21;
    string file_path = 2;
    string file_name = 3;
    int64 file_type_id = 4;
    string model_type = 5;
    int64 model_id = 6;
    string original_file_path = 7;
    string thumbnail_file_path = 8;
    string file_url = 9;
    float height = 10;
    float width = 11;
    float size = 12;
    bool attach = 13;
    string extension = 14;
    string remark = 15;
    bool is_obsoleted = 19;
    string extra_info = 20;
    string creator = 16;
    string file_type_name = 17;
    FileButtons file_buttons = 22;
    string creator_email = 24;
    common.Audit audit = 18;
}

message FileButtons {
    bool show_edit_button = 1;
    bool show_download_button = 2;
    bool show_preview_button = 3;
    bool show_delete_button = 4;
}

message FileType {
    int64 id = 1;
    string name = 2;
    repeated int32 document_types = 3;
    int32 sort = 4;
}

message MdmFileType {
    int64 id = 1;
    string name = 2;
    repeated int32 mdm_types = 3;
    int32 sort = 4;
}

message ListFilesRequest {
    int64 id = 1;
    string file_name = 2;
    string file_path = 3;
    int64 file_type_id = 4;
    string model_type = 5;
    int64 model_id = 6;
    bool attach = 9; // ONLY support true only, false will get ALL
    
    common.PaginationRequest pagination_request = 7;
    common.Language lang = 8;
}

message ListFilesForMainConRequest {
    common.Language lang = 1;
    common.PaginationRequest pagination_request = 2;
    int64 id = 3;
    int64 supplier_id = 4;
    string file_name = 5;
    string file_path = 6;
    int64 file_type_id = 7;
    string model_type = 8;
    int64 model_id = 9;
}

message ListFilesResponse {
    repeated File data = 1;
    common.MataPaginator meta = 2;
}

message CreateFilesRecordRequest {
    repeated File file_records = 1;
}

message CreateFilesRecordResponse {
    repeated File data = 1;
    bool success = 2;
}

message CreateOrUpdateFilesRecordRequest {
    repeated File file_records = 1;
    bool require_signed_url = 2;
}

message CreateOrUpdateFilesRecordResponse {
    repeated File data = 1;
    bool success = 2;
}

message GetFileRecordRequest {
    int64 id = 1;
}

message GetFileRecordResponse {
    File data = 1;
}

message UpdateFileRecordRequest {
    int64 id = 2;
    int64 file_type_id = 3;
    bool attach = 4;
    string remark = 5;
}

message UpdateFileRecordResponse {
    bool success = 1;
}

message DeleteFileRecordRequest {
    int64 id = 1;
}

message DeleteFileRecordResponse {
    bool success = 1;
}

message ListFileTypesRequest {
    string name = 1;
    repeated int32 model_bits = 2;
    common.PaginationRequest pagination_request = 3;
    common.Language lang = 4;
}

message ListMdmFileTypesRequest {
    string name = 1;
    repeated int32 model_bits = 2;
    common.PaginationRequest pagination_request = 3;
    common.Language lang = 4;
}

message ListFileTypesResponse {
    repeated FileType data = 1;
    common.MataPaginator meta = 2;
}

message ListMdmFileTypesResponse {
    repeated MdmFileType data = 1;
    common.MataPaginator meta = 2;
}

// ========================
// GOOGLE API UPLOAD START

message GenerateUploadFilesUrlRequest {
    message UploadFile {
        int32 file_id = 1;
        string file_name = 2;
        string file_path_name = 3;
        string initial_request_length = 4;
        string origin = 5;
        string x_upload_content_type = 6;
        string x_upload_content_length = 7;
        string file_md5 = 8;
    }
    repeated UploadFile upload_files = 1;
    int64 model_id = 2;
    string model_type = 3;
    repeated string regenerates = 4;
}

message GenerateUploadFilesUrlResponse {
    message GenerateUploadUrl {
        int32 file_id = 1;
        string session_url = 2;
        string file_name = 3;
        string file_md5 = 4;
    }
    repeated GenerateUploadUrl data = 1;
}

message CompleteUploadFile {
    int64 id = 1;
    int64 file_type_id = 2;
    string model_type = 3;
    int64 model_id = 4;
    string file_name = 5;
    string file_path = 6;
    google.protobuf.Timestamp expiration_date = 7;
    string file_md5 = 8;
    string remark = 9;
    string extension = 10;
}

message CompleteUploadFilesUrlRequest {
    repeated CompleteUploadFile complete_upload_files = 1;
}

message CompleteUploadFilesUrlResponse {
    bool success = 1;
}

message OptionGenerateDownloadSignedUrlRequest{
    int32 ExpiryInMinutes = 1; 
}

message GenerateDownloadSignedUrlRequest {
   string file_path = 1;
   OptionGenerateDownloadSignedUrlRequest Option = 2;
}

message GenerateDownloadSignedUrlResponse {
   string signed_url = 1;
   int32 ExpiryInMinutes = 2;
}

// GOOGLE API UPLOAD END
// ========================