package pdfcpu_helper

import (
	"bytes"
	pdfcpuApi "gitlab.com/go-emat/pdfcpu-mattex/pkg/api"
	"gitlab.com/go-emat/pdfcpu-mattex/pkg/pdfcpu"
	"io/ioutil"
	"os"
)

func ExtractPdf(file []byte, fileName string, outPutDir string, selectedPages []string) error {
	conf := pdfcpu.NewDefaultConfiguration()
	conf.Cmd = pdfcpu.EXTRACTPAGES

	err := pdfcpuApi.ExtractPages(bytes.NewReader(file), outPutDir, fileName, selectedPages, conf)
	if err != nil {
		return err
	}
	return nil
}

func MakeDir(dirPath string) error {
	_, err := os.Stat(dirPath)
	if os.IsNotExist(err) {
		err := os.MkdirAll(dirPath, 0755)
		if err != nil {
			return err
		}
	}
	return nil
}


func ReadDir(dirPath string) ([]string, error) {
	files, err := ioutil.ReadDir(dirPath)
	if err != nil {
		return nil, err
	}
	var fileNames []string
	for _, file := range files {
		fileNames = append(fileNames, dirPath + "/" + file.Name())
	}
	return fileNames, nil
}

func RemoveDir(dirPath string) error {
	err := os.RemoveAll(dirPath)
	if err != nil {
		return err
	}
	return nil
}