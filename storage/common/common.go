package common

import (
	"archive/zip"
	"fmt"
	"mime"
	"strings"
)

type SignedUrlOption struct {
	ExpiryInMinutes          int32
	MimeContentType          string
	SkipMimeContentTypeCheck bool
}

type BufferContent struct {
	Filename string
	Buffer   []byte
}

type FileContent struct {
	Filename string
	FilePath string
}

type ZipContent struct {
	File          []*FileContent
	BufferContent []*BufferContent // Additional data in the buffer, that was not saved before
	FolderName    string
}

type FileMeta struct {
	FileSize int64
}

func CopyToZipWriter(zipWriter *zip.Writer, zipContent *ZipContent) error {
	for _, bufferC := range zipContent.BufferContent {
		zipFile, err := zipWriter.Create(bufferC.Filename)
		if err != nil {
			return err
		}
		_, err = zipFile.Write(bufferC.Buffer)
		if err != nil {
			return err
		}
	}
	return nil
}

func VerifyMimeTypeByFileName(inFilename, inMimeType string) (rtnMimeType string, err error) {
	lastDotIdx := strings.LastIndex(inFilename, ".")
	if lastDotIdx > 1 {
		fileExtWithDot := inFilename[lastDotIdx:]
		if len(fileExtWithDot) > 1 {
			mimeType, _, parseErr := mime.ParseMediaType(inMimeType)
			if parseErr != nil {
				err = parseErr
				return
			}
			fileDotExtension := mime.TypeByExtension(fileExtWithDot)
			if fileDotExtension != mimeType {
				err = fmt.Errorf("mime type not matched [ %s : %s ]", fileDotExtension, mimeType)
				return
			}
			rtnMimeType = mimeType
		}
	}
	return
}
