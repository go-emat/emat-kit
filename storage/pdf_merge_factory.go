package storage

import (
	"io"
	"log"
	"os"
	"time"

	unipdfCreator "github.com/unidoc/unipdf/v3/creator"
	"gitlab.com/go-emat/emat-kit/storage/pdf_merge"
	"gitlab.com/go-emat/pdfcpu-mattex/pkg/pdfcpu"
)

const (
	MergeModePdfcpu = "pdfcpu"
	MergeModeUniPdf = "unipdf"
)

const (
	PdfMergeFileTypePDF         = "pdf"
	PdfMergeFileTypeImage       = "image"
	PdfMergeFileTypeUnsupported = "unsupported"
	PdfMergeFileTypeNotExist    = "not_exist"
)

func getTypeByExt(extension string) string {
	switch extension {
	case "pdf":
		return PdfMergeFileTypePDF
	case "png", "bmp", "gif", "jpg", "jpeg", "tif", "tiff":
		return PdfMergeFileTypeImage
	default:
		return PdfMergeFileTypeUnsupported
	}
}

type PdfMergeFileInfo struct {
	FilePath          string
	FileName          string
	FileExtension     string
	Appendix          string
	UpdatedAt         *time.Time
	UniPdfCreator     *unipdfCreator.Creator
	WithoutValidation bool
}

type PdfMergeRequest struct {
	InputFiles        []*PdfMergeFileInfo
	OutputFilePath    string
	DocumentNo        string
	WithoutValidation bool
}

type PdfMergeResponse struct {
	Duration             float64
	SupportedFileNames   []string
	UnsupportedFileNames []string
}

type PdfWatermarks struct {
	Image string
	Text  string
	Desc  string
	OnTop bool
	Pages []string
}

type PdfWatermarksFileInfo struct {
	FilePath          string
	FileName          string
	Watermarks        []*PdfWatermarks
	WithoutValidation bool
}

type PdfWatermarksFile struct {
	*PdfWatermarksFileInfo
	InputSource io.ReadSeeker
}

type PdfMergeFile struct {
	*PdfMergeFileInfo
	InputSource io.ReadSeeker
	FileType    string

	PdfcpuCtx     *pdfcpu.Context
	UniPdfCreator *unipdfCreator.Creator
}

func GetDefaultMergeType() string {
	if os.Getenv("DEFAULT_MERGE_MODE") != "" {
		return os.Getenv("DEFAULT_MERGE_MODE")
	}

	return MergeModePdfcpu
}

type PdfMergeFactory interface {
	MergeFiles(req *PdfMergeRequest) (resp *PdfMergeResponse, errs *pdf_merge.Errors)
	GetPdfPageCount(filepath string) (pageCount int, err error)
	ValidateMergeFileFormat(inputFiles *PdfMergeFileInfo, outputDir string) (f *PdfMergeFile, err error)
	AddWatermarksToPdf(req *PdfWatermarksFileInfo) (err error)
}

type PdfMergeService struct {
	MergeType      string
	StorageFactory *DocumentStorageFactory
}

func NewPdfMergeService(mergeType string, storageFactory *DocumentStorageFactory) *PdfMergeService {
	return &PdfMergeService{
		MergeType:      mergeType,
		StorageFactory: storageFactory,
	}
}

func NewDefaultPdfMergeService() *PdfMergeService {
	mergeType := GetDefaultMergeType()
	defaultStorageFactory := newDefaultDocumentStorageFactory()
	factory := &PdfMergeService{
		MergeType:      mergeType,
		StorageFactory: defaultStorageFactory,
	}

	return factory
}

func (s *PdfMergeService) GetPdfMergeFactory() PdfMergeFactory {
	switch s.MergeType {
	case MergeModeUniPdf:
		if os.Getenv("UNIPDF_LICENSE") == "" {
			log.Println("UNIPDF_LICENSE is not set")
		}
		return NewUnipdfMergeService(s.StorageFactory)
	default:
		return NewPdfcpuMergeService(s.StorageFactory)
	}
}

func (s *PdfMergeService) MergeFiles(req *PdfMergeRequest) (resp *PdfMergeResponse, errs *pdf_merge.Errors) {
	resp1, errs1 := s.GetPdfMergeFactory().MergeFiles(req)
	if os.Getenv("SERVER_ENVIRONMENT") != "Production" {
		return resp1, errs1
	}
	// if pdfcpu merge pdfs failed, use unipdf to merge pdf
	if errs1 != nil && ((errs1.Errs != nil && len(errs1.Errs) > 0) || (errs1.FatalErrs != nil && len(errs1.FatalErrs) > 0)) && s.MergeType == MergeModePdfcpu && os.Getenv("UNIPDF_LICENSE") != "" {
		resp2, errs2 := NewUnipdfMergeService(s.StorageFactory).MergeFiles(req)
		if errs2 != nil && ((errs2.Errs != nil && len(errs2.Errs) > 0) || (errs2.FatalErrs != nil && len(errs2.FatalErrs) > 0)) {
			resp = resp1
			errs = errs2
		} else {
			resp = resp2
			errs = nil
		}
	} else {
		resp = resp1
		errs = errs1
	}

	return resp, errs
}

func (s *PdfMergeService) GetPdfPageCount(filepath string) (pageCount int, err error) {
	res, err := NewPdfcpuMergeService(s.StorageFactory).GetPdfPageCount(filepath)
	if err != nil && s.MergeType == MergeModePdfcpu && os.Getenv("UNIPDF_LICENSE") != "" {
		return NewUnipdfMergeService(s.StorageFactory).GetPdfPageCount(filepath)
	}
	return res, err
}

func (s *PdfMergeService) ValidateMergeFileFormat(inputFiles *PdfMergeFileInfo, outputDir string) (f *PdfMergeFile, err error) {
	return NewPdfcpuMergeService(s.StorageFactory).ValidateMergeFileFormat(inputFiles, outputDir)
}

func (s *PdfMergeService) AddWatermarksToPdf(req *PdfWatermarksFileInfo) (err error) {
	return NewPdfcpuMergeService(s.StorageFactory).AddWatermarksToPdf(req)
}
