// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package dashboardpb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// DashboardServiceClient is the client API for DashboardService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type DashboardServiceClient interface {
	GetSubmissionCounts(ctx context.Context, in *ProjectFilterRequest, opts ...grpc.CallOption) (*GetSubmissionCountsResponse, error)
	GetSubmissionsCountByRespondent(ctx context.Context, in *ProjectFilterRequest, opts ...grpc.CallOption) (*GetSubmissionsCountByRespondentResponse, error)
	GetRespondedSubmissions(ctx context.Context, in *ProjectFilterRequest, opts ...grpc.CallOption) (*GetRespondedSubmissionsResponse, error)
	GetOpenSubmissionsByTrade(ctx context.Context, in *ProjectFilterRequest, opts ...grpc.CallOption) (*GetOpenSubmissionsByTradeResponse, error)
	GetSubmissionsByDueDate(ctx context.Context, in *ProjectFilterRequest, opts ...grpc.CallOption) (*GetSubmissionsByDueDateResponse, error)
	GetSubmissionPerformance(ctx context.Context, in *PerformanceFilterRequest, opts ...grpc.CallOption) (*SubmissionPerformanceResponse, error)
	GetSubmissionPerformanceSummary(ctx context.Context, in *PerformanceFilterRequest, opts ...grpc.CallOption) (*SubmissionPerformanceSummaryResponse, error)
	GetSubmissionsOutstandingOverdue(ctx context.Context, in *ProjectFilterRequest, opts ...grpc.CallOption) (*GetSubmissionsOutstandingOverdueResponse, error)
	GetSubmissionPending(ctx context.Context, in *GetSubmissionPendingRequest, opts ...grpc.CallOption) (*GetSubmissionPendingResponse, error)
	GetSubmissionsBySubmissionType(ctx context.Context, in *ProjectFilterRequest, opts ...grpc.CallOption) (*SubmissionsBySubmissionTypeResponse, error)
}

type dashboardServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewDashboardServiceClient(cc grpc.ClientConnInterface) DashboardServiceClient {
	return &dashboardServiceClient{cc}
}

func (c *dashboardServiceClient) GetSubmissionCounts(ctx context.Context, in *ProjectFilterRequest, opts ...grpc.CallOption) (*GetSubmissionCountsResponse, error) {
	out := new(GetSubmissionCountsResponse)
	err := c.cc.Invoke(ctx, "/reviewer.DashboardService/GetSubmissionCounts", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dashboardServiceClient) GetSubmissionsCountByRespondent(ctx context.Context, in *ProjectFilterRequest, opts ...grpc.CallOption) (*GetSubmissionsCountByRespondentResponse, error) {
	out := new(GetSubmissionsCountByRespondentResponse)
	err := c.cc.Invoke(ctx, "/reviewer.DashboardService/GetSubmissionsCountByRespondent", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dashboardServiceClient) GetRespondedSubmissions(ctx context.Context, in *ProjectFilterRequest, opts ...grpc.CallOption) (*GetRespondedSubmissionsResponse, error) {
	out := new(GetRespondedSubmissionsResponse)
	err := c.cc.Invoke(ctx, "/reviewer.DashboardService/GetRespondedSubmissions", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dashboardServiceClient) GetOpenSubmissionsByTrade(ctx context.Context, in *ProjectFilterRequest, opts ...grpc.CallOption) (*GetOpenSubmissionsByTradeResponse, error) {
	out := new(GetOpenSubmissionsByTradeResponse)
	err := c.cc.Invoke(ctx, "/reviewer.DashboardService/GetOpenSubmissionsByTrade", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dashboardServiceClient) GetSubmissionsByDueDate(ctx context.Context, in *ProjectFilterRequest, opts ...grpc.CallOption) (*GetSubmissionsByDueDateResponse, error) {
	out := new(GetSubmissionsByDueDateResponse)
	err := c.cc.Invoke(ctx, "/reviewer.DashboardService/GetSubmissionsByDueDate", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dashboardServiceClient) GetSubmissionPerformance(ctx context.Context, in *PerformanceFilterRequest, opts ...grpc.CallOption) (*SubmissionPerformanceResponse, error) {
	out := new(SubmissionPerformanceResponse)
	err := c.cc.Invoke(ctx, "/reviewer.DashboardService/GetSubmissionPerformance", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dashboardServiceClient) GetSubmissionPerformanceSummary(ctx context.Context, in *PerformanceFilterRequest, opts ...grpc.CallOption) (*SubmissionPerformanceSummaryResponse, error) {
	out := new(SubmissionPerformanceSummaryResponse)
	err := c.cc.Invoke(ctx, "/reviewer.DashboardService/GetSubmissionPerformanceSummary", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dashboardServiceClient) GetSubmissionsOutstandingOverdue(ctx context.Context, in *ProjectFilterRequest, opts ...grpc.CallOption) (*GetSubmissionsOutstandingOverdueResponse, error) {
	out := new(GetSubmissionsOutstandingOverdueResponse)
	err := c.cc.Invoke(ctx, "/reviewer.DashboardService/GetSubmissionsOutstandingOverdue", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dashboardServiceClient) GetSubmissionPending(ctx context.Context, in *GetSubmissionPendingRequest, opts ...grpc.CallOption) (*GetSubmissionPendingResponse, error) {
	out := new(GetSubmissionPendingResponse)
	err := c.cc.Invoke(ctx, "/reviewer.DashboardService/GetSubmissionPending", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dashboardServiceClient) GetSubmissionsBySubmissionType(ctx context.Context, in *ProjectFilterRequest, opts ...grpc.CallOption) (*SubmissionsBySubmissionTypeResponse, error) {
	out := new(SubmissionsBySubmissionTypeResponse)
	err := c.cc.Invoke(ctx, "/reviewer.DashboardService/GetSubmissionsBySubmissionType", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// DashboardServiceServer is the server API for DashboardService service.
// All implementations must embed UnimplementedDashboardServiceServer
// for forward compatibility
type DashboardServiceServer interface {
	GetSubmissionCounts(context.Context, *ProjectFilterRequest) (*GetSubmissionCountsResponse, error)
	GetSubmissionsCountByRespondent(context.Context, *ProjectFilterRequest) (*GetSubmissionsCountByRespondentResponse, error)
	GetRespondedSubmissions(context.Context, *ProjectFilterRequest) (*GetRespondedSubmissionsResponse, error)
	GetOpenSubmissionsByTrade(context.Context, *ProjectFilterRequest) (*GetOpenSubmissionsByTradeResponse, error)
	GetSubmissionsByDueDate(context.Context, *ProjectFilterRequest) (*GetSubmissionsByDueDateResponse, error)
	GetSubmissionPerformance(context.Context, *PerformanceFilterRequest) (*SubmissionPerformanceResponse, error)
	GetSubmissionPerformanceSummary(context.Context, *PerformanceFilterRequest) (*SubmissionPerformanceSummaryResponse, error)
	GetSubmissionsOutstandingOverdue(context.Context, *ProjectFilterRequest) (*GetSubmissionsOutstandingOverdueResponse, error)
	GetSubmissionPending(context.Context, *GetSubmissionPendingRequest) (*GetSubmissionPendingResponse, error)
	GetSubmissionsBySubmissionType(context.Context, *ProjectFilterRequest) (*SubmissionsBySubmissionTypeResponse, error)
	mustEmbedUnimplementedDashboardServiceServer()
}

// UnimplementedDashboardServiceServer must be embedded to have forward compatible implementations.
type UnimplementedDashboardServiceServer struct {
}

func (UnimplementedDashboardServiceServer) GetSubmissionCounts(context.Context, *ProjectFilterRequest) (*GetSubmissionCountsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetSubmissionCounts not implemented")
}
func (UnimplementedDashboardServiceServer) GetSubmissionsCountByRespondent(context.Context, *ProjectFilterRequest) (*GetSubmissionsCountByRespondentResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetSubmissionsCountByRespondent not implemented")
}
func (UnimplementedDashboardServiceServer) GetRespondedSubmissions(context.Context, *ProjectFilterRequest) (*GetRespondedSubmissionsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetRespondedSubmissions not implemented")
}
func (UnimplementedDashboardServiceServer) GetOpenSubmissionsByTrade(context.Context, *ProjectFilterRequest) (*GetOpenSubmissionsByTradeResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetOpenSubmissionsByTrade not implemented")
}
func (UnimplementedDashboardServiceServer) GetSubmissionsByDueDate(context.Context, *ProjectFilterRequest) (*GetSubmissionsByDueDateResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetSubmissionsByDueDate not implemented")
}
func (UnimplementedDashboardServiceServer) GetSubmissionPerformance(context.Context, *PerformanceFilterRequest) (*SubmissionPerformanceResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetSubmissionPerformance not implemented")
}
func (UnimplementedDashboardServiceServer) GetSubmissionPerformanceSummary(context.Context, *PerformanceFilterRequest) (*SubmissionPerformanceSummaryResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetSubmissionPerformanceSummary not implemented")
}
func (UnimplementedDashboardServiceServer) GetSubmissionsOutstandingOverdue(context.Context, *ProjectFilterRequest) (*GetSubmissionsOutstandingOverdueResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetSubmissionsOutstandingOverdue not implemented")
}
func (UnimplementedDashboardServiceServer) GetSubmissionPending(context.Context, *GetSubmissionPendingRequest) (*GetSubmissionPendingResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetSubmissionPending not implemented")
}
func (UnimplementedDashboardServiceServer) GetSubmissionsBySubmissionType(context.Context, *ProjectFilterRequest) (*SubmissionsBySubmissionTypeResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetSubmissionsBySubmissionType not implemented")
}
func (UnimplementedDashboardServiceServer) mustEmbedUnimplementedDashboardServiceServer() {}

// UnsafeDashboardServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to DashboardServiceServer will
// result in compilation errors.
type UnsafeDashboardServiceServer interface {
	mustEmbedUnimplementedDashboardServiceServer()
}

func RegisterDashboardServiceServer(s grpc.ServiceRegistrar, srv DashboardServiceServer) {
	s.RegisterService(&_DashboardService_serviceDesc, srv)
}

func _DashboardService_GetSubmissionCounts_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ProjectFilterRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DashboardServiceServer).GetSubmissionCounts(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.DashboardService/GetSubmissionCounts",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DashboardServiceServer).GetSubmissionCounts(ctx, req.(*ProjectFilterRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DashboardService_GetSubmissionsCountByRespondent_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ProjectFilterRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DashboardServiceServer).GetSubmissionsCountByRespondent(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.DashboardService/GetSubmissionsCountByRespondent",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DashboardServiceServer).GetSubmissionsCountByRespondent(ctx, req.(*ProjectFilterRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DashboardService_GetRespondedSubmissions_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ProjectFilterRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DashboardServiceServer).GetRespondedSubmissions(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.DashboardService/GetRespondedSubmissions",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DashboardServiceServer).GetRespondedSubmissions(ctx, req.(*ProjectFilterRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DashboardService_GetOpenSubmissionsByTrade_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ProjectFilterRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DashboardServiceServer).GetOpenSubmissionsByTrade(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.DashboardService/GetOpenSubmissionsByTrade",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DashboardServiceServer).GetOpenSubmissionsByTrade(ctx, req.(*ProjectFilterRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DashboardService_GetSubmissionsByDueDate_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ProjectFilterRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DashboardServiceServer).GetSubmissionsByDueDate(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.DashboardService/GetSubmissionsByDueDate",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DashboardServiceServer).GetSubmissionsByDueDate(ctx, req.(*ProjectFilterRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DashboardService_GetSubmissionPerformance_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(PerformanceFilterRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DashboardServiceServer).GetSubmissionPerformance(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.DashboardService/GetSubmissionPerformance",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DashboardServiceServer).GetSubmissionPerformance(ctx, req.(*PerformanceFilterRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DashboardService_GetSubmissionPerformanceSummary_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(PerformanceFilterRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DashboardServiceServer).GetSubmissionPerformanceSummary(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.DashboardService/GetSubmissionPerformanceSummary",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DashboardServiceServer).GetSubmissionPerformanceSummary(ctx, req.(*PerformanceFilterRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DashboardService_GetSubmissionsOutstandingOverdue_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ProjectFilterRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DashboardServiceServer).GetSubmissionsOutstandingOverdue(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.DashboardService/GetSubmissionsOutstandingOverdue",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DashboardServiceServer).GetSubmissionsOutstandingOverdue(ctx, req.(*ProjectFilterRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DashboardService_GetSubmissionPending_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetSubmissionPendingRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DashboardServiceServer).GetSubmissionPending(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.DashboardService/GetSubmissionPending",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DashboardServiceServer).GetSubmissionPending(ctx, req.(*GetSubmissionPendingRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DashboardService_GetSubmissionsBySubmissionType_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ProjectFilterRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DashboardServiceServer).GetSubmissionsBySubmissionType(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.DashboardService/GetSubmissionsBySubmissionType",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DashboardServiceServer).GetSubmissionsBySubmissionType(ctx, req.(*ProjectFilterRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _DashboardService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "reviewer.DashboardService",
	HandlerType: (*DashboardServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetSubmissionCounts",
			Handler:    _DashboardService_GetSubmissionCounts_Handler,
		},
		{
			MethodName: "GetSubmissionsCountByRespondent",
			Handler:    _DashboardService_GetSubmissionsCountByRespondent_Handler,
		},
		{
			MethodName: "GetRespondedSubmissions",
			Handler:    _DashboardService_GetRespondedSubmissions_Handler,
		},
		{
			MethodName: "GetOpenSubmissionsByTrade",
			Handler:    _DashboardService_GetOpenSubmissionsByTrade_Handler,
		},
		{
			MethodName: "GetSubmissionsByDueDate",
			Handler:    _DashboardService_GetSubmissionsByDueDate_Handler,
		},
		{
			MethodName: "GetSubmissionPerformance",
			Handler:    _DashboardService_GetSubmissionPerformance_Handler,
		},
		{
			MethodName: "GetSubmissionPerformanceSummary",
			Handler:    _DashboardService_GetSubmissionPerformanceSummary_Handler,
		},
		{
			MethodName: "GetSubmissionsOutstandingOverdue",
			Handler:    _DashboardService_GetSubmissionsOutstandingOverdue_Handler,
		},
		{
			MethodName: "GetSubmissionPending",
			Handler:    _DashboardService_GetSubmissionPending_Handler,
		},
		{
			MethodName: "GetSubmissionsBySubmissionType",
			Handler:    _DashboardService_GetSubmissionsBySubmissionType_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "reviewer/submission/dashboard.proto",
}
