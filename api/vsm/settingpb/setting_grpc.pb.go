// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package settingpb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// SettingServiceClient is the client API for SettingService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type SettingServiceClient interface {
	AsyncProject(ctx context.Context, in *AsyncProjectRequest, opts ...grpc.CallOption) (*SettingResponse, error)
	AsyncUsers(ctx context.Context, in *AsyncUsersRequest, opts ...grpc.CallOption) (*SettingResponse, error)
	AsyncEmatUsers(ctx context.Context, in *AsyncEmatUsersRequest, opts ...grpc.CallOption) (*SettingResponse, error)
	AsyncTmmSupplierAndContacts(ctx context.Context, in *AsyncTmmSupplierRequest, opts ...grpc.CallOption) (*SettingResponse, error)
	DeleteTmmSupplier(ctx context.Context, in *DeleteTmmSupplierRequest, opts ...grpc.CallOption) (*SettingResponse, error)
	AsyncTermsConditions(ctx context.Context, in *AsyncTermsConditionsRequest, opts ...grpc.CallOption) (*SettingResponse, error)
	AsyncBatchSupplierAndContacts(ctx context.Context, in *AsyncBatchSupplierRequest, opts ...grpc.CallOption) (*SettingResponse, error)
	AsyncMaterials(ctx context.Context, in *AsyncMaterialsRequest, opts ...grpc.CallOption) (*SettingResponse, error)
	AsyncPaymentTerms(ctx context.Context, in *AsyncPaymentTermsRequest, opts ...grpc.CallOption) (*SettingResponse, error)
	AsyncCurrencies(ctx context.Context, in *AsyncCurrenciesRequest, opts ...grpc.CallOption) (*SettingResponse, error)
	AsyncSubsidiaries(ctx context.Context, in *AsyncSubsidiariesRequest, opts ...grpc.CallOption) (*SettingResponse, error)
	AsyncDepartments(ctx context.Context, in *AsyncDepartmentsRequest, opts ...grpc.CallOption) (*SettingResponse, error)
	AsyncUserNotificationSetting(ctx context.Context, in *AsyncUserNotificationSettingRequest, opts ...grpc.CallOption) (*SettingResponse, error)
	AsyncFunctionalGroupUsers(ctx context.Context, in *AsyncFunctionalGroupUsersRequest, opts ...grpc.CallOption) (*SettingResponse, error)
}

type settingServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewSettingServiceClient(cc grpc.ClientConnInterface) SettingServiceClient {
	return &settingServiceClient{cc}
}

func (c *settingServiceClient) AsyncProject(ctx context.Context, in *AsyncProjectRequest, opts ...grpc.CallOption) (*SettingResponse, error) {
	out := new(SettingResponse)
	err := c.cc.Invoke(ctx, "/vsm.SettingService/AsyncProject", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *settingServiceClient) AsyncUsers(ctx context.Context, in *AsyncUsersRequest, opts ...grpc.CallOption) (*SettingResponse, error) {
	out := new(SettingResponse)
	err := c.cc.Invoke(ctx, "/vsm.SettingService/AsyncUsers", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *settingServiceClient) AsyncEmatUsers(ctx context.Context, in *AsyncEmatUsersRequest, opts ...grpc.CallOption) (*SettingResponse, error) {
	out := new(SettingResponse)
	err := c.cc.Invoke(ctx, "/vsm.SettingService/AsyncEmatUsers", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *settingServiceClient) AsyncTmmSupplierAndContacts(ctx context.Context, in *AsyncTmmSupplierRequest, opts ...grpc.CallOption) (*SettingResponse, error) {
	out := new(SettingResponse)
	err := c.cc.Invoke(ctx, "/vsm.SettingService/AsyncTmmSupplierAndContacts", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *settingServiceClient) DeleteTmmSupplier(ctx context.Context, in *DeleteTmmSupplierRequest, opts ...grpc.CallOption) (*SettingResponse, error) {
	out := new(SettingResponse)
	err := c.cc.Invoke(ctx, "/vsm.SettingService/DeleteTmmSupplier", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *settingServiceClient) AsyncTermsConditions(ctx context.Context, in *AsyncTermsConditionsRequest, opts ...grpc.CallOption) (*SettingResponse, error) {
	out := new(SettingResponse)
	err := c.cc.Invoke(ctx, "/vsm.SettingService/AsyncTermsConditions", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *settingServiceClient) AsyncBatchSupplierAndContacts(ctx context.Context, in *AsyncBatchSupplierRequest, opts ...grpc.CallOption) (*SettingResponse, error) {
	out := new(SettingResponse)
	err := c.cc.Invoke(ctx, "/vsm.SettingService/AsyncBatchSupplierAndContacts", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *settingServiceClient) AsyncMaterials(ctx context.Context, in *AsyncMaterialsRequest, opts ...grpc.CallOption) (*SettingResponse, error) {
	out := new(SettingResponse)
	err := c.cc.Invoke(ctx, "/vsm.SettingService/AsyncMaterials", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *settingServiceClient) AsyncPaymentTerms(ctx context.Context, in *AsyncPaymentTermsRequest, opts ...grpc.CallOption) (*SettingResponse, error) {
	out := new(SettingResponse)
	err := c.cc.Invoke(ctx, "/vsm.SettingService/AsyncPaymentTerms", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *settingServiceClient) AsyncCurrencies(ctx context.Context, in *AsyncCurrenciesRequest, opts ...grpc.CallOption) (*SettingResponse, error) {
	out := new(SettingResponse)
	err := c.cc.Invoke(ctx, "/vsm.SettingService/AsyncCurrencies", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *settingServiceClient) AsyncSubsidiaries(ctx context.Context, in *AsyncSubsidiariesRequest, opts ...grpc.CallOption) (*SettingResponse, error) {
	out := new(SettingResponse)
	err := c.cc.Invoke(ctx, "/vsm.SettingService/AsyncSubsidiaries", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *settingServiceClient) AsyncDepartments(ctx context.Context, in *AsyncDepartmentsRequest, opts ...grpc.CallOption) (*SettingResponse, error) {
	out := new(SettingResponse)
	err := c.cc.Invoke(ctx, "/vsm.SettingService/AsyncDepartments", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *settingServiceClient) AsyncUserNotificationSetting(ctx context.Context, in *AsyncUserNotificationSettingRequest, opts ...grpc.CallOption) (*SettingResponse, error) {
	out := new(SettingResponse)
	err := c.cc.Invoke(ctx, "/vsm.SettingService/AsyncUserNotificationSetting", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *settingServiceClient) AsyncFunctionalGroupUsers(ctx context.Context, in *AsyncFunctionalGroupUsersRequest, opts ...grpc.CallOption) (*SettingResponse, error) {
	out := new(SettingResponse)
	err := c.cc.Invoke(ctx, "/vsm.SettingService/AsyncFunctionalGroupUsers", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// SettingServiceServer is the server API for SettingService service.
// All implementations must embed UnimplementedSettingServiceServer
// for forward compatibility
type SettingServiceServer interface {
	AsyncProject(context.Context, *AsyncProjectRequest) (*SettingResponse, error)
	AsyncUsers(context.Context, *AsyncUsersRequest) (*SettingResponse, error)
	AsyncEmatUsers(context.Context, *AsyncEmatUsersRequest) (*SettingResponse, error)
	AsyncTmmSupplierAndContacts(context.Context, *AsyncTmmSupplierRequest) (*SettingResponse, error)
	DeleteTmmSupplier(context.Context, *DeleteTmmSupplierRequest) (*SettingResponse, error)
	AsyncTermsConditions(context.Context, *AsyncTermsConditionsRequest) (*SettingResponse, error)
	AsyncBatchSupplierAndContacts(context.Context, *AsyncBatchSupplierRequest) (*SettingResponse, error)
	AsyncMaterials(context.Context, *AsyncMaterialsRequest) (*SettingResponse, error)
	AsyncPaymentTerms(context.Context, *AsyncPaymentTermsRequest) (*SettingResponse, error)
	AsyncCurrencies(context.Context, *AsyncCurrenciesRequest) (*SettingResponse, error)
	AsyncSubsidiaries(context.Context, *AsyncSubsidiariesRequest) (*SettingResponse, error)
	AsyncDepartments(context.Context, *AsyncDepartmentsRequest) (*SettingResponse, error)
	AsyncUserNotificationSetting(context.Context, *AsyncUserNotificationSettingRequest) (*SettingResponse, error)
	AsyncFunctionalGroupUsers(context.Context, *AsyncFunctionalGroupUsersRequest) (*SettingResponse, error)
	mustEmbedUnimplementedSettingServiceServer()
}

// UnimplementedSettingServiceServer must be embedded to have forward compatible implementations.
type UnimplementedSettingServiceServer struct {
}

func (UnimplementedSettingServiceServer) AsyncProject(context.Context, *AsyncProjectRequest) (*SettingResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AsyncProject not implemented")
}
func (UnimplementedSettingServiceServer) AsyncUsers(context.Context, *AsyncUsersRequest) (*SettingResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AsyncUsers not implemented")
}
func (UnimplementedSettingServiceServer) AsyncEmatUsers(context.Context, *AsyncEmatUsersRequest) (*SettingResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AsyncEmatUsers not implemented")
}
func (UnimplementedSettingServiceServer) AsyncTmmSupplierAndContacts(context.Context, *AsyncTmmSupplierRequest) (*SettingResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AsyncTmmSupplierAndContacts not implemented")
}
func (UnimplementedSettingServiceServer) DeleteTmmSupplier(context.Context, *DeleteTmmSupplierRequest) (*SettingResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteTmmSupplier not implemented")
}
func (UnimplementedSettingServiceServer) AsyncTermsConditions(context.Context, *AsyncTermsConditionsRequest) (*SettingResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AsyncTermsConditions not implemented")
}
func (UnimplementedSettingServiceServer) AsyncBatchSupplierAndContacts(context.Context, *AsyncBatchSupplierRequest) (*SettingResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AsyncBatchSupplierAndContacts not implemented")
}
func (UnimplementedSettingServiceServer) AsyncMaterials(context.Context, *AsyncMaterialsRequest) (*SettingResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AsyncMaterials not implemented")
}
func (UnimplementedSettingServiceServer) AsyncPaymentTerms(context.Context, *AsyncPaymentTermsRequest) (*SettingResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AsyncPaymentTerms not implemented")
}
func (UnimplementedSettingServiceServer) AsyncCurrencies(context.Context, *AsyncCurrenciesRequest) (*SettingResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AsyncCurrencies not implemented")
}
func (UnimplementedSettingServiceServer) AsyncSubsidiaries(context.Context, *AsyncSubsidiariesRequest) (*SettingResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AsyncSubsidiaries not implemented")
}
func (UnimplementedSettingServiceServer) AsyncDepartments(context.Context, *AsyncDepartmentsRequest) (*SettingResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AsyncDepartments not implemented")
}
func (UnimplementedSettingServiceServer) AsyncUserNotificationSetting(context.Context, *AsyncUserNotificationSettingRequest) (*SettingResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AsyncUserNotificationSetting not implemented")
}
func (UnimplementedSettingServiceServer) AsyncFunctionalGroupUsers(context.Context, *AsyncFunctionalGroupUsersRequest) (*SettingResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AsyncFunctionalGroupUsers not implemented")
}
func (UnimplementedSettingServiceServer) mustEmbedUnimplementedSettingServiceServer() {}

// UnsafeSettingServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to SettingServiceServer will
// result in compilation errors.
type UnsafeSettingServiceServer interface {
	mustEmbedUnimplementedSettingServiceServer()
}

func RegisterSettingServiceServer(s grpc.ServiceRegistrar, srv SettingServiceServer) {
	s.RegisterService(&_SettingService_serviceDesc, srv)
}

func _SettingService_AsyncProject_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AsyncProjectRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SettingServiceServer).AsyncProject(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vsm.SettingService/AsyncProject",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SettingServiceServer).AsyncProject(ctx, req.(*AsyncProjectRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SettingService_AsyncUsers_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AsyncUsersRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SettingServiceServer).AsyncUsers(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vsm.SettingService/AsyncUsers",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SettingServiceServer).AsyncUsers(ctx, req.(*AsyncUsersRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SettingService_AsyncEmatUsers_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AsyncEmatUsersRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SettingServiceServer).AsyncEmatUsers(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vsm.SettingService/AsyncEmatUsers",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SettingServiceServer).AsyncEmatUsers(ctx, req.(*AsyncEmatUsersRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SettingService_AsyncTmmSupplierAndContacts_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AsyncTmmSupplierRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SettingServiceServer).AsyncTmmSupplierAndContacts(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vsm.SettingService/AsyncTmmSupplierAndContacts",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SettingServiceServer).AsyncTmmSupplierAndContacts(ctx, req.(*AsyncTmmSupplierRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SettingService_DeleteTmmSupplier_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteTmmSupplierRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SettingServiceServer).DeleteTmmSupplier(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vsm.SettingService/DeleteTmmSupplier",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SettingServiceServer).DeleteTmmSupplier(ctx, req.(*DeleteTmmSupplierRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SettingService_AsyncTermsConditions_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AsyncTermsConditionsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SettingServiceServer).AsyncTermsConditions(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vsm.SettingService/AsyncTermsConditions",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SettingServiceServer).AsyncTermsConditions(ctx, req.(*AsyncTermsConditionsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SettingService_AsyncBatchSupplierAndContacts_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AsyncBatchSupplierRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SettingServiceServer).AsyncBatchSupplierAndContacts(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vsm.SettingService/AsyncBatchSupplierAndContacts",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SettingServiceServer).AsyncBatchSupplierAndContacts(ctx, req.(*AsyncBatchSupplierRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SettingService_AsyncMaterials_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AsyncMaterialsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SettingServiceServer).AsyncMaterials(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vsm.SettingService/AsyncMaterials",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SettingServiceServer).AsyncMaterials(ctx, req.(*AsyncMaterialsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SettingService_AsyncPaymentTerms_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AsyncPaymentTermsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SettingServiceServer).AsyncPaymentTerms(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vsm.SettingService/AsyncPaymentTerms",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SettingServiceServer).AsyncPaymentTerms(ctx, req.(*AsyncPaymentTermsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SettingService_AsyncCurrencies_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AsyncCurrenciesRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SettingServiceServer).AsyncCurrencies(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vsm.SettingService/AsyncCurrencies",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SettingServiceServer).AsyncCurrencies(ctx, req.(*AsyncCurrenciesRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SettingService_AsyncSubsidiaries_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AsyncSubsidiariesRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SettingServiceServer).AsyncSubsidiaries(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vsm.SettingService/AsyncSubsidiaries",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SettingServiceServer).AsyncSubsidiaries(ctx, req.(*AsyncSubsidiariesRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SettingService_AsyncDepartments_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AsyncDepartmentsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SettingServiceServer).AsyncDepartments(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vsm.SettingService/AsyncDepartments",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SettingServiceServer).AsyncDepartments(ctx, req.(*AsyncDepartmentsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SettingService_AsyncUserNotificationSetting_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AsyncUserNotificationSettingRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SettingServiceServer).AsyncUserNotificationSetting(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vsm.SettingService/AsyncUserNotificationSetting",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SettingServiceServer).AsyncUserNotificationSetting(ctx, req.(*AsyncUserNotificationSettingRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SettingService_AsyncFunctionalGroupUsers_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AsyncFunctionalGroupUsersRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SettingServiceServer).AsyncFunctionalGroupUsers(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vsm.SettingService/AsyncFunctionalGroupUsers",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SettingServiceServer).AsyncFunctionalGroupUsers(ctx, req.(*AsyncFunctionalGroupUsersRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _SettingService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "vsm.SettingService",
	HandlerType: (*SettingServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "AsyncProject",
			Handler:    _SettingService_AsyncProject_Handler,
		},
		{
			MethodName: "AsyncUsers",
			Handler:    _SettingService_AsyncUsers_Handler,
		},
		{
			MethodName: "AsyncEmatUsers",
			Handler:    _SettingService_AsyncEmatUsers_Handler,
		},
		{
			MethodName: "AsyncTmmSupplierAndContacts",
			Handler:    _SettingService_AsyncTmmSupplierAndContacts_Handler,
		},
		{
			MethodName: "DeleteTmmSupplier",
			Handler:    _SettingService_DeleteTmmSupplier_Handler,
		},
		{
			MethodName: "AsyncTermsConditions",
			Handler:    _SettingService_AsyncTermsConditions_Handler,
		},
		{
			MethodName: "AsyncBatchSupplierAndContacts",
			Handler:    _SettingService_AsyncBatchSupplierAndContacts_Handler,
		},
		{
			MethodName: "AsyncMaterials",
			Handler:    _SettingService_AsyncMaterials_Handler,
		},
		{
			MethodName: "AsyncPaymentTerms",
			Handler:    _SettingService_AsyncPaymentTerms_Handler,
		},
		{
			MethodName: "AsyncCurrencies",
			Handler:    _SettingService_AsyncCurrencies_Handler,
		},
		{
			MethodName: "AsyncSubsidiaries",
			Handler:    _SettingService_AsyncSubsidiaries_Handler,
		},
		{
			MethodName: "AsyncDepartments",
			Handler:    _SettingService_AsyncDepartments_Handler,
		},
		{
			MethodName: "AsyncUserNotificationSetting",
			Handler:    _SettingService_AsyncUserNotificationSetting_Handler,
		},
		{
			MethodName: "AsyncFunctionalGroupUsers",
			Handler:    _SettingService_AsyncFunctionalGroupUsers_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "vsm/setting/setting.proto",
}
