// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.25.0
// 	protoc        v3.14.0
// source: supplier/document/document_file.proto

package documentpb

import (
	proto "github.com/golang/protobuf/proto"
	commonpb "gitlab.com/go-emat/emat-kit/api/commonpb"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type GetDocumentFilesButtonsRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ButtonsRequests []*FileButtonsRequest `protobuf:"bytes,1,rep,name=buttons_requests,json=buttonsRequests,proto3" json:"buttons_requests,omitempty"`
	Lang            commonpb.Language     `protobuf:"varint,2,opt,name=lang,proto3,enum=common.Language" json:"lang,omitempty"`
}

func (x *GetDocumentFilesButtonsRequest) Reset() {
	*x = GetDocumentFilesButtonsRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_supplier_document_document_file_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetDocumentFilesButtonsRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetDocumentFilesButtonsRequest) ProtoMessage() {}

func (x *GetDocumentFilesButtonsRequest) ProtoReflect() protoreflect.Message {
	mi := &file_supplier_document_document_file_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetDocumentFilesButtonsRequest.ProtoReflect.Descriptor instead.
func (*GetDocumentFilesButtonsRequest) Descriptor() ([]byte, []int) {
	return file_supplier_document_document_file_proto_rawDescGZIP(), []int{0}
}

func (x *GetDocumentFilesButtonsRequest) GetButtonsRequests() []*FileButtonsRequest {
	if x != nil {
		return x.ButtonsRequests
	}
	return nil
}

func (x *GetDocumentFilesButtonsRequest) GetLang() commonpb.Language {
	if x != nil {
		return x.Lang
	}
	return commonpb.Language_UNKNOWN_LANGUAGE
}

type FileButtonsRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id        int64  `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	ModelType string `protobuf:"bytes,2,opt,name=model_type,json=modelType,proto3" json:"model_type,omitempty"`
	ModelId   int64  `protobuf:"varint,3,opt,name=model_id,json=modelId,proto3" json:"model_id,omitempty"`
}

func (x *FileButtonsRequest) Reset() {
	*x = FileButtonsRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_supplier_document_document_file_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *FileButtonsRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*FileButtonsRequest) ProtoMessage() {}

func (x *FileButtonsRequest) ProtoReflect() protoreflect.Message {
	mi := &file_supplier_document_document_file_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use FileButtonsRequest.ProtoReflect.Descriptor instead.
func (*FileButtonsRequest) Descriptor() ([]byte, []int) {
	return file_supplier_document_document_file_proto_rawDescGZIP(), []int{1}
}

func (x *FileButtonsRequest) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *FileButtonsRequest) GetModelType() string {
	if x != nil {
		return x.ModelType
	}
	return ""
}

func (x *FileButtonsRequest) GetModelId() int64 {
	if x != nil {
		return x.ModelId
	}
	return 0
}

type GetDocumentFilesButtonsResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ButtonsResponse []*FileButtonsResponse `protobuf:"bytes,1,rep,name=buttons_response,json=buttonsResponse,proto3" json:"buttons_response,omitempty"`
}

func (x *GetDocumentFilesButtonsResponse) Reset() {
	*x = GetDocumentFilesButtonsResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_supplier_document_document_file_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetDocumentFilesButtonsResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetDocumentFilesButtonsResponse) ProtoMessage() {}

func (x *GetDocumentFilesButtonsResponse) ProtoReflect() protoreflect.Message {
	mi := &file_supplier_document_document_file_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetDocumentFilesButtonsResponse.ProtoReflect.Descriptor instead.
func (*GetDocumentFilesButtonsResponse) Descriptor() ([]byte, []int) {
	return file_supplier_document_document_file_proto_rawDescGZIP(), []int{2}
}

func (x *GetDocumentFilesButtonsResponse) GetButtonsResponse() []*FileButtonsResponse {
	if x != nil {
		return x.ButtonsResponse
	}
	return nil
}

type FileButtonsResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id                 int64 `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	ShowEditButton     bool  `protobuf:"varint,2,opt,name=show_edit_button,json=showEditButton,proto3" json:"show_edit_button,omitempty"`
	ShowDownloadButton bool  `protobuf:"varint,3,opt,name=show_download_button,json=showDownloadButton,proto3" json:"show_download_button,omitempty"`
	ShowPreviewButton  bool  `protobuf:"varint,4,opt,name=show_preview_button,json=showPreviewButton,proto3" json:"show_preview_button,omitempty"`
	ShowDeleteButton   bool  `protobuf:"varint,5,opt,name=show_delete_button,json=showDeleteButton,proto3" json:"show_delete_button,omitempty"`
}

func (x *FileButtonsResponse) Reset() {
	*x = FileButtonsResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_supplier_document_document_file_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *FileButtonsResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*FileButtonsResponse) ProtoMessage() {}

func (x *FileButtonsResponse) ProtoReflect() protoreflect.Message {
	mi := &file_supplier_document_document_file_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use FileButtonsResponse.ProtoReflect.Descriptor instead.
func (*FileButtonsResponse) Descriptor() ([]byte, []int) {
	return file_supplier_document_document_file_proto_rawDescGZIP(), []int{3}
}

func (x *FileButtonsResponse) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *FileButtonsResponse) GetShowEditButton() bool {
	if x != nil {
		return x.ShowEditButton
	}
	return false
}

func (x *FileButtonsResponse) GetShowDownloadButton() bool {
	if x != nil {
		return x.ShowDownloadButton
	}
	return false
}

func (x *FileButtonsResponse) GetShowPreviewButton() bool {
	if x != nil {
		return x.ShowPreviewButton
	}
	return false
}

func (x *FileButtonsResponse) GetShowDeleteButton() bool {
	if x != nil {
		return x.ShowDeleteButton
	}
	return false
}

var File_supplier_document_document_file_proto protoreflect.FileDescriptor

var file_supplier_document_document_file_proto_rawDesc = []byte{
	0x0a, 0x25, 0x73, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72, 0x2f, 0x64, 0x6f, 0x63, 0x75, 0x6d,
	0x65, 0x6e, 0x74, 0x2f, 0x64, 0x6f, 0x63, 0x75, 0x6d, 0x65, 0x6e, 0x74, 0x5f, 0x66, 0x69, 0x6c,
	0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x08, 0x73, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65,
	0x72, 0x1a, 0x13, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x8f, 0x01, 0x0a, 0x1e, 0x47, 0x65, 0x74, 0x44, 0x6f,
	0x63, 0x75, 0x6d, 0x65, 0x6e, 0x74, 0x46, 0x69, 0x6c, 0x65, 0x73, 0x42, 0x75, 0x74, 0x74, 0x6f,
	0x6e, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x47, 0x0a, 0x10, 0x62, 0x75, 0x74,
	0x74, 0x6f, 0x6e, 0x73, 0x5f, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x73, 0x18, 0x01, 0x20,
	0x03, 0x28, 0x0b, 0x32, 0x1c, 0x2e, 0x73, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72, 0x2e, 0x46,
	0x69, 0x6c, 0x65, 0x42, 0x75, 0x74, 0x74, 0x6f, 0x6e, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x52, 0x0f, 0x62, 0x75, 0x74, 0x74, 0x6f, 0x6e, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x73, 0x12, 0x24, 0x0a, 0x04, 0x6c, 0x61, 0x6e, 0x67, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0e,
	0x32, 0x10, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x4c, 0x61, 0x6e, 0x67, 0x75, 0x61,
	0x67, 0x65, 0x52, 0x04, 0x6c, 0x61, 0x6e, 0x67, 0x22, 0x5e, 0x0a, 0x12, 0x46, 0x69, 0x6c, 0x65,
	0x42, 0x75, 0x74, 0x74, 0x6f, 0x6e, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x0e,
	0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x02, 0x69, 0x64, 0x12, 0x1d,
	0x0a, 0x0a, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x09, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x54, 0x79, 0x70, 0x65, 0x12, 0x19, 0x0a,
	0x08, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x03, 0x52,
	0x07, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x49, 0x64, 0x22, 0x6b, 0x0a, 0x1f, 0x47, 0x65, 0x74, 0x44,
	0x6f, 0x63, 0x75, 0x6d, 0x65, 0x6e, 0x74, 0x46, 0x69, 0x6c, 0x65, 0x73, 0x42, 0x75, 0x74, 0x74,
	0x6f, 0x6e, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x48, 0x0a, 0x10, 0x62,
	0x75, 0x74, 0x74, 0x6f, 0x6e, 0x73, 0x5f, 0x72, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x18,
	0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x1d, 0x2e, 0x73, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72,
	0x2e, 0x46, 0x69, 0x6c, 0x65, 0x42, 0x75, 0x74, 0x74, 0x6f, 0x6e, 0x73, 0x52, 0x65, 0x73, 0x70,
	0x6f, 0x6e, 0x73, 0x65, 0x52, 0x0f, 0x62, 0x75, 0x74, 0x74, 0x6f, 0x6e, 0x73, 0x52, 0x65, 0x73,
	0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0xdf, 0x01, 0x0a, 0x13, 0x46, 0x69, 0x6c, 0x65, 0x42, 0x75,
	0x74, 0x74, 0x6f, 0x6e, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x0e, 0x0a,
	0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x02, 0x69, 0x64, 0x12, 0x28, 0x0a,
	0x10, 0x73, 0x68, 0x6f, 0x77, 0x5f, 0x65, 0x64, 0x69, 0x74, 0x5f, 0x62, 0x75, 0x74, 0x74, 0x6f,
	0x6e, 0x18, 0x02, 0x20, 0x01, 0x28, 0x08, 0x52, 0x0e, 0x73, 0x68, 0x6f, 0x77, 0x45, 0x64, 0x69,
	0x74, 0x42, 0x75, 0x74, 0x74, 0x6f, 0x6e, 0x12, 0x30, 0x0a, 0x14, 0x73, 0x68, 0x6f, 0x77, 0x5f,
	0x64, 0x6f, 0x77, 0x6e, 0x6c, 0x6f, 0x61, 0x64, 0x5f, 0x62, 0x75, 0x74, 0x74, 0x6f, 0x6e, 0x18,
	0x03, 0x20, 0x01, 0x28, 0x08, 0x52, 0x12, 0x73, 0x68, 0x6f, 0x77, 0x44, 0x6f, 0x77, 0x6e, 0x6c,
	0x6f, 0x61, 0x64, 0x42, 0x75, 0x74, 0x74, 0x6f, 0x6e, 0x12, 0x2e, 0x0a, 0x13, 0x73, 0x68, 0x6f,
	0x77, 0x5f, 0x70, 0x72, 0x65, 0x76, 0x69, 0x65, 0x77, 0x5f, 0x62, 0x75, 0x74, 0x74, 0x6f, 0x6e,
	0x18, 0x04, 0x20, 0x01, 0x28, 0x08, 0x52, 0x11, 0x73, 0x68, 0x6f, 0x77, 0x50, 0x72, 0x65, 0x76,
	0x69, 0x65, 0x77, 0x42, 0x75, 0x74, 0x74, 0x6f, 0x6e, 0x12, 0x2c, 0x0a, 0x12, 0x73, 0x68, 0x6f,
	0x77, 0x5f, 0x64, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x5f, 0x62, 0x75, 0x74, 0x74, 0x6f, 0x6e, 0x18,
	0x05, 0x20, 0x01, 0x28, 0x08, 0x52, 0x10, 0x73, 0x68, 0x6f, 0x77, 0x44, 0x65, 0x6c, 0x65, 0x74,
	0x65, 0x42, 0x75, 0x74, 0x74, 0x6f, 0x6e, 0x32, 0x85, 0x01, 0x0a, 0x13, 0x44, 0x6f, 0x63, 0x75,
	0x6d, 0x65, 0x6e, 0x74, 0x46, 0x69, 0x6c, 0x65, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12,
	0x6e, 0x0a, 0x17, 0x47, 0x65, 0x74, 0x44, 0x6f, 0x63, 0x75, 0x6d, 0x65, 0x6e, 0x74, 0x46, 0x69,
	0x6c, 0x65, 0x73, 0x42, 0x75, 0x74, 0x74, 0x6f, 0x6e, 0x73, 0x12, 0x28, 0x2e, 0x73, 0x75, 0x70,
	0x70, 0x6c, 0x69, 0x65, 0x72, 0x2e, 0x47, 0x65, 0x74, 0x44, 0x6f, 0x63, 0x75, 0x6d, 0x65, 0x6e,
	0x74, 0x46, 0x69, 0x6c, 0x65, 0x73, 0x42, 0x75, 0x74, 0x74, 0x6f, 0x6e, 0x73, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x1a, 0x29, 0x2e, 0x73, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72, 0x2e,
	0x47, 0x65, 0x74, 0x44, 0x6f, 0x63, 0x75, 0x6d, 0x65, 0x6e, 0x74, 0x46, 0x69, 0x6c, 0x65, 0x73,
	0x42, 0x75, 0x74, 0x74, 0x6f, 0x6e, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x42,
	0x15, 0x5a, 0x13, 0x73, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72, 0x2f, 0x64, 0x6f, 0x63, 0x75,
	0x6d, 0x65, 0x6e, 0x74, 0x70, 0x62, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_supplier_document_document_file_proto_rawDescOnce sync.Once
	file_supplier_document_document_file_proto_rawDescData = file_supplier_document_document_file_proto_rawDesc
)

func file_supplier_document_document_file_proto_rawDescGZIP() []byte {
	file_supplier_document_document_file_proto_rawDescOnce.Do(func() {
		file_supplier_document_document_file_proto_rawDescData = protoimpl.X.CompressGZIP(file_supplier_document_document_file_proto_rawDescData)
	})
	return file_supplier_document_document_file_proto_rawDescData
}

var file_supplier_document_document_file_proto_msgTypes = make([]protoimpl.MessageInfo, 4)
var file_supplier_document_document_file_proto_goTypes = []interface{}{
	(*GetDocumentFilesButtonsRequest)(nil),  // 0: supplier.GetDocumentFilesButtonsRequest
	(*FileButtonsRequest)(nil),              // 1: supplier.FileButtonsRequest
	(*GetDocumentFilesButtonsResponse)(nil), // 2: supplier.GetDocumentFilesButtonsResponse
	(*FileButtonsResponse)(nil),             // 3: supplier.FileButtonsResponse
	(commonpb.Language)(0),                  // 4: common.Language
}
var file_supplier_document_document_file_proto_depIdxs = []int32{
	1, // 0: supplier.GetDocumentFilesButtonsRequest.buttons_requests:type_name -> supplier.FileButtonsRequest
	4, // 1: supplier.GetDocumentFilesButtonsRequest.lang:type_name -> common.Language
	3, // 2: supplier.GetDocumentFilesButtonsResponse.buttons_response:type_name -> supplier.FileButtonsResponse
	0, // 3: supplier.DocumentFileService.GetDocumentFilesButtons:input_type -> supplier.GetDocumentFilesButtonsRequest
	2, // 4: supplier.DocumentFileService.GetDocumentFilesButtons:output_type -> supplier.GetDocumentFilesButtonsResponse
	4, // [4:5] is the sub-list for method output_type
	3, // [3:4] is the sub-list for method input_type
	3, // [3:3] is the sub-list for extension type_name
	3, // [3:3] is the sub-list for extension extendee
	0, // [0:3] is the sub-list for field type_name
}

func init() { file_supplier_document_document_file_proto_init() }
func file_supplier_document_document_file_proto_init() {
	if File_supplier_document_document_file_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_supplier_document_document_file_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetDocumentFilesButtonsRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_supplier_document_document_file_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*FileButtonsRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_supplier_document_document_file_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetDocumentFilesButtonsResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_supplier_document_document_file_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*FileButtonsResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_supplier_document_document_file_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   4,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_supplier_document_document_file_proto_goTypes,
		DependencyIndexes: file_supplier_document_document_file_proto_depIdxs,
		MessageInfos:      file_supplier_document_document_file_proto_msgTypes,
	}.Build()
	File_supplier_document_document_file_proto = out.File
	file_supplier_document_document_file_proto_rawDesc = nil
	file_supplier_document_document_file_proto_goTypes = nil
	file_supplier_document_document_file_proto_depIdxs = nil
}
