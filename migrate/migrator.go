package migrate

import (
	"errors"
	"gorm.io/gorm/logger"
	"log"
	"os"
	"path/filepath"
	"reflect"
	"sort"
	"strings"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type MigrateInterface interface {
	Up(migrator *Migrator)
	Down(migrator *Migrator)
}

type Migrator struct {
	DB                *gorm.DB
	Migration         *Migration
	MigrationFilePath string
	AppName           string
}

type MigrateDatabase struct {
	DatabaseName      string
	DBUserName        string
	DBPassword        string
	DBHost            string
	DBPort            string
	MigrationFilePath string
	AppName           string
}

func NewMigrator(migrateDatabase *MigrateDatabase) *Migrator {
	sqlConnection := migrateDatabase.DBUserName + ":" + migrateDatabase.DBPassword + "@tcp(" + migrateDatabase.DBHost + ":" + migrateDatabase.DBPort + ")/" + migrateDatabase.DatabaseName + "?charset=utf8&parseTime=True&loc=Asia%2FHong_Kong&time_zone=%27Asia%2FHong_Kong%27"
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold:             time.Second,   // Slow SQL threshold
			LogLevel:                  logger.Silent, // Log level
			IgnoreRecordNotFoundError: true,          // Ignore ErrRecordNotFound error for logger
			//Colorful:                  false,         // Disable color
		},
	)
	db, err := gorm.Open(mysql.New(mysql.Config{
		DSN: sqlConnection, // data source name
		DefaultStringSize: 256, // default size for string fields
		DisableDatetimePrecision: true, // disable datetime precision, which not supported before MySQL 5.6
		DontSupportRenameIndex: true, // drop & create when rename index, rename index not supported before MySQL 5.7, MariaDB
		DontSupportRenameColumn: true, // `change` when rename column, rename column not supported before MySQL 8, MariaDB
		SkipInitializeWithVersion: false, // auto configure based on currently MySQL version
	}), &gorm.Config{
		Logger: newLogger,
	})
	if err != nil {
		panic(err.Error())
	}
	migrator := &Migrator{
		DB:                db,
		Migration:         &Migration{},
		MigrationFilePath: migrateDatabase.MigrationFilePath,
		AppName:           migrateDatabase.AppName,
	}
	return migrator
}

func (migrator *Migrator) Migrate() {
	migrator.Migration.PrepareDatabase(migrator)
	//"/cmd/database/migrations"
	log.Println(migrator.MigrationFilePath)
	migrator.run(migrator.MigrationFilePath)
	dbSQL, ok := migrator.DB.DB()
	if ok != nil {
		defer dbSQL.Close()
	}
}

func (migrator *Migrator) RollbackMigrations() {
	migrator.rollback()
	dbSQL, ok := migrator.DB.DB()
	if ok != nil {
		defer dbSQL.Close()
	}
}

func (migrator *Migrator) run(path string) {
	migrationFiles := getMigrationFiles(path)
	if len(migrationFiles) > 0 {
		migrator.runPending(migrationFiles)
	}
}

func (migrator *Migrator) rollback() {
	maxBatchNumber := migrator.Migration.getMaxBatchNumber(migrator)
	ranMigrationFiles := migrator.Migration.GetRollBackRan(migrator, maxBatchNumber)
	if len(ranMigrationFiles) > 0 {
		migrator.runRollback(ranMigrationFiles, maxBatchNumber)
	}
}

func (migrator *Migrator) runRollback(migrationFiles []string, maxBatchNumber int) {
	if len(migrationFiles) > 0 {
		// fmt.Println(maxBatchNumber)
		for _, migrationFile := range migrationFiles {
			err := migrator.runMigrationAction(migrationFile, "Down")
			if err == nil {
				migrator.Migration.DownMigrate(migrator, migrationFile, maxBatchNumber)
			}
		}
	}
}

func (migrator *Migrator) runPending(migrationFiles []string) {
	if len(migrationFiles) > 0 {
		nextBatchNumber := migrator.Migration.getNextBatchNumber(migrator)
		ranMigrationFiles := migrator.Migration.GetRan(migrator)
		for _, migrationFile := range migrationFiles {
			//check if migrate in db
			if !stringInSlice(migrationFile, ranMigrationFiles) {
				err := migrator.runMigrationAction(migrationFile, "Up")
				if err == nil {
					migrator.Migration.UpMigrate(migrator, migrationFile, nextBatchNumber)
				}
			}
		}
	}
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func (migrator *Migrator) runMigrationAction(migrationFile string, action string) error {
	registryKey := resolve(migrationFile)

	migrationInstance := getMigrationInstance(registryKey)
	if migrationInstance != nil {
		log.Printf("Migrate file:%v", migrationFile)
		t := reflect.ValueOf(migrationInstance)

		mv := t.MethodByName(action)
		args := []reflect.Value{reflect.ValueOf(migrator)}
		mv.Call(args)
		return nil
	}
	return errors.New(migrationFile + ": not add registry")
}

func getMigrationFiles(path string) (fileNames []string) {

	fileList, err := filepath.Glob(path + "/*_*.go")
	if err != nil {
		panic(err.Error())
	}
	for _, file := range fileList {
		fileNames = append(fileNames, getMigrationName(file))
	}
	sort.Strings(fileNames)
	return fileNames
}

func getMigrationName(path string) string {
	fileName := filepath.Base(path)
	fileName = strings.Replace(fileName, ".go", "", -1)
	return fileName
}

func resolve(migrationFile string) string {
	var registryKey string
	s := strings.Split(migrationFile, "_")
	sliceMigration := s[4:]
	for _, v := range sliceMigration {
		if len(registryKey) == 0 {
			registryKey = v
		} else {
			registryKey = registryKey + "_" + v
		}
	}
	return registryKey
}

func getMigrationInstance(registryKey string) interface{} {
	if typeRegistry[registryKey] == nil {
		log.Printf("Error: not registry the key file %v", registryKey)
		return nil
	}
	v := reflect.New(typeRegistry[registryKey]).Elem()
	return v.Interface()
}
