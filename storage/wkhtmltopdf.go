package storage

import "github.com/SebastiaanKlippert/go-wkhtmltopdf"

type GeneratePdfToStorageResponse struct {
	FilePath    string
	StorageType string
}

type GeneratePdfToStorageRequest struct {
	TemplateString string
	PdfOptions     *PdfOptions
	DocumentType   string
	FileName       string
	PdfBasePath    string
}

type PdfOptions struct {
	PageLandScape           bool
	PageFooterWithTerms     bool
	PageFooterRightTemplate string
	PageHeaderVoid          bool
	PageHeaderVoidDateText  string
	PageHeaderNotOrder      bool
	PageMarginTop           uint32
	PageMarginRight         uint32
	PageMarginBottom        uint32
	PageMarginLeft          uint32
	PageFooterHTML          string
	PageHeaderHTML          string
	PageHeaderSpacing       float64
	PageHeaderData          map[string]interface{}
	PageSize                string
	PageWidth               uint
	PageHeight              uint
	HiddenFooterRight       bool
	ShowFooterCenter        bool
	TocInclude              bool
	TocHeaderText           string
}

type BackupStoragePdfRequest struct {
	DbPdfPath string
}

type BackupStoragePdfResponse struct {
	Success bool
}

type DownloadStorageFileRequest struct {
	FilePath string
	FileName string
}

type DownloadStorageFileResponse struct {
	File []byte
}

type GenerateSignedDownloadUrlRequest struct {
	FilePath string
}

type GenerateSignedDownloadUrlResponse struct {
	SignedUrl string
}

type wkHtmlToPdfService struct {
	StorageFactory *DocumentStorageFactory
}

func NewWkHtmlToPdfService(storageType, bucketName string, configFile ...string) *wkHtmlToPdfService {
	svc := &wkHtmlToPdfService{StorageFactory: &DocumentStorageFactory{
		StorageBucketName: bucketName,
		StorageType:       storageType,
	}}
	if len(configFile) > 0 {
		svc.StorageFactory.ConfigFile = configFile[0]
	}
	return svc
}

func NewDefaultWkHtmlToPdfService() *wkHtmlToPdfService {
	svc := &wkHtmlToPdfService{StorageFactory: newDefaultDocumentStorageFactory()}
	return svc
}

func (s *wkHtmlToPdfService) GeneratePdfToStorage(req *GeneratePdfToStorageRequest) (*GeneratePdfToStorageResponse, error) {
	return s.StorageFactory.GeneratePdfFileToStorage(req)
}

func (s *wkHtmlToPdfService) NewPdfGenerator(templateString string, pdfOptions *PdfOptions) (*wkhtmltopdf.PDFGenerator, error) {
	return s.StorageFactory.NewPdfGenerator(templateString, pdfOptions)
}

func (s *wkHtmlToPdfService) BackupStoragePdf(req *BackupStoragePdfRequest) (*BackupStoragePdfResponse, error) {

	result, err := s.StorageFactory.BackupStoragePdf(req)
	return &BackupStoragePdfResponse{Success: result}, err
}

func (s *wkHtmlToPdfService) DownloadStorageFile(req *DownloadStorageFileRequest) (*DownloadStorageFileResponse, error) {
	file, err := s.StorageFactory.DownloadStorageFile(req.FilePath)
	return &DownloadStorageFileResponse{
		File: file,
	}, err
}

func (s *wkHtmlToPdfService) GenerateSignedDownloadUrl(req *GenerateSignedDownloadUrlRequest) (*GenerateSignedDownloadUrlResponse, error) {
	signedUrl, err := s.StorageFactory.GenerateSignedDownloadURL(req.FilePath)
	return &GenerateSignedDownloadUrlResponse{
		SignedUrl: signedUrl,
	}, err
}
