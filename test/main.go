package main

import (
	"fmt"
	"gitlab.com/go-emat/emat-kit/storage/cloudstorage"
	"log"
	"os"
)

func main() {
	bucketName := "local-contractor"
	fileName := "big-file-demo/demo2.zip"

	googleStorage, err := cloudstorage.NewGoogleCloudStorage(bucketName)
	if err != nil {
		log.Println(err)
	}
	os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", "/Users/jimmy/project/go_project/src/mattex-mmm-v2/local-developer.json")
	serviceAccount := os.Getenv("GOOGLE_APPLICATION_CREDENTIALS")
	uploadURL, err := googleStorage.GenerateResumableSessionURL(&cloudstorage.SessionURLRequest{
		FileName:             fileName,
		ServiceAccount:       serviceAccount,
	})
	if err != nil {
		err = fmt.Errorf("url.Parse: %v", err)
		log.Println(err)
		return
	}
	log.Printf("uploadURL:%v", uploadURL.String())
}
