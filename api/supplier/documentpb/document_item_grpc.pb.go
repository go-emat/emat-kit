// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package documentpb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// DocumentItemServiceClient is the client API for DocumentItemService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type DocumentItemServiceClient interface {
	GetDocumentItemCalculation(ctx context.Context, in *GetDocumentItemCalculationRequest, opts ...grpc.CallOption) (*GetDocumentItemCalculationResponse, error)
	CreateDocumentItemGroup(ctx context.Context, in *CreateDocumentItemGroupRequest, opts ...grpc.CallOption) (*CreateDocumentItemGroupResponse, error)
	UpdateDocumentItemGroup(ctx context.Context, in *UpdateDocumentItemGroupRequest, opts ...grpc.CallOption) (*UpdateDocumentItemGroupResponse, error)
}

type documentItemServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewDocumentItemServiceClient(cc grpc.ClientConnInterface) DocumentItemServiceClient {
	return &documentItemServiceClient{cc}
}

func (c *documentItemServiceClient) GetDocumentItemCalculation(ctx context.Context, in *GetDocumentItemCalculationRequest, opts ...grpc.CallOption) (*GetDocumentItemCalculationResponse, error) {
	out := new(GetDocumentItemCalculationResponse)
	err := c.cc.Invoke(ctx, "/supplier.DocumentItemService/GetDocumentItemCalculation", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *documentItemServiceClient) CreateDocumentItemGroup(ctx context.Context, in *CreateDocumentItemGroupRequest, opts ...grpc.CallOption) (*CreateDocumentItemGroupResponse, error) {
	out := new(CreateDocumentItemGroupResponse)
	err := c.cc.Invoke(ctx, "/supplier.DocumentItemService/CreateDocumentItemGroup", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *documentItemServiceClient) UpdateDocumentItemGroup(ctx context.Context, in *UpdateDocumentItemGroupRequest, opts ...grpc.CallOption) (*UpdateDocumentItemGroupResponse, error) {
	out := new(UpdateDocumentItemGroupResponse)
	err := c.cc.Invoke(ctx, "/supplier.DocumentItemService/UpdateDocumentItemGroup", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// DocumentItemServiceServer is the server API for DocumentItemService service.
// All implementations must embed UnimplementedDocumentItemServiceServer
// for forward compatibility
type DocumentItemServiceServer interface {
	GetDocumentItemCalculation(context.Context, *GetDocumentItemCalculationRequest) (*GetDocumentItemCalculationResponse, error)
	CreateDocumentItemGroup(context.Context, *CreateDocumentItemGroupRequest) (*CreateDocumentItemGroupResponse, error)
	UpdateDocumentItemGroup(context.Context, *UpdateDocumentItemGroupRequest) (*UpdateDocumentItemGroupResponse, error)
	mustEmbedUnimplementedDocumentItemServiceServer()
}

// UnimplementedDocumentItemServiceServer must be embedded to have forward compatible implementations.
type UnimplementedDocumentItemServiceServer struct {
}

func (UnimplementedDocumentItemServiceServer) GetDocumentItemCalculation(context.Context, *GetDocumentItemCalculationRequest) (*GetDocumentItemCalculationResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetDocumentItemCalculation not implemented")
}
func (UnimplementedDocumentItemServiceServer) CreateDocumentItemGroup(context.Context, *CreateDocumentItemGroupRequest) (*CreateDocumentItemGroupResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateDocumentItemGroup not implemented")
}
func (UnimplementedDocumentItemServiceServer) UpdateDocumentItemGroup(context.Context, *UpdateDocumentItemGroupRequest) (*UpdateDocumentItemGroupResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateDocumentItemGroup not implemented")
}
func (UnimplementedDocumentItemServiceServer) mustEmbedUnimplementedDocumentItemServiceServer() {}

// UnsafeDocumentItemServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to DocumentItemServiceServer will
// result in compilation errors.
type UnsafeDocumentItemServiceServer interface {
	mustEmbedUnimplementedDocumentItemServiceServer()
}

func RegisterDocumentItemServiceServer(s grpc.ServiceRegistrar, srv DocumentItemServiceServer) {
	s.RegisterService(&_DocumentItemService_serviceDesc, srv)
}

func _DocumentItemService_GetDocumentItemCalculation_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetDocumentItemCalculationRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DocumentItemServiceServer).GetDocumentItemCalculation(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/supplier.DocumentItemService/GetDocumentItemCalculation",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DocumentItemServiceServer).GetDocumentItemCalculation(ctx, req.(*GetDocumentItemCalculationRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DocumentItemService_CreateDocumentItemGroup_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateDocumentItemGroupRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DocumentItemServiceServer).CreateDocumentItemGroup(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/supplier.DocumentItemService/CreateDocumentItemGroup",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DocumentItemServiceServer).CreateDocumentItemGroup(ctx, req.(*CreateDocumentItemGroupRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DocumentItemService_UpdateDocumentItemGroup_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateDocumentItemGroupRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DocumentItemServiceServer).UpdateDocumentItemGroup(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/supplier.DocumentItemService/UpdateDocumentItemGroup",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DocumentItemServiceServer).UpdateDocumentItemGroup(ctx, req.(*UpdateDocumentItemGroupRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _DocumentItemService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "supplier.DocumentItemService",
	HandlerType: (*DocumentItemServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetDocumentItemCalculation",
			Handler:    _DocumentItemService_GetDocumentItemCalculation_Handler,
		},
		{
			MethodName: "CreateDocumentItemGroup",
			Handler:    _DocumentItemService_CreateDocumentItemGroup_Handler,
		},
		{
			MethodName: "UpdateDocumentItemGroup",
			Handler:    _DocumentItemService_UpdateDocumentItemGroup_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "supplier/document/document_item.proto",
}
