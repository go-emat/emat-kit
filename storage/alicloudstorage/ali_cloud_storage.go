package alicloudstorage

import (
	"archive/zip"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/url"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/360EntSecGroup-Skylar/excelize/v2"
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"github.com/google/go-querystring/query"
	"gitlab.com/go-emat/emat-kit/storage/common"
)

type AliCloudStorage struct {
	StorageBucket *oss.Bucket
	ConfigFile    string
}

func configFromJSON(jsonKey []byte) (*credentialsFile, error) {
	var f credentialsFile
	if err := json.Unmarshal(jsonKey, &f); err != nil {
		return nil, err
	}
	return &f, nil
}

type credentialsFile struct {
	EndPoint        string `json:"aliyun_end_point"`
	AccessKeyId     string `json:"aliyun_access_key_id"`
	AccessKeySecret string `json:"aliyun_access_key_secret"`
	Bucket          string `json:"aliyun_bucket"`
	PartUploadUrl   string `json:"aliyun_part_upload_url"`
}

func NewImur(objeckKey, uploadId string) oss.InitiateMultipartUploadResult {
	return oss.InitiateMultipartUploadResult{
		Key:      objeckKey,
		UploadID: uploadId,
	}
}

// NewAliCloudStorage new instance
func NewAliCloudStorage(configFile string) (*AliCloudStorage, error) {
	storageBucket, err := createAliStorageBucketHandle(configFile)
	if err != nil {
		return nil, err
	}
	return &AliCloudStorage{
		StorageBucket: storageBucket,
		ConfigFile:    configFile,
	}, nil
}

type SessionUrlParams struct {
	UploadID  string `url:"upload_id,omitempty"`
	ObjectKey string `url:"object_key,omitempty"`
}

func getSessionUrl(configFile, objectKey, uploadID string) (string, error) {
	jsonKey, err := ioutil.ReadFile(configFile)
	if err != nil {
		return "", fmt.Errorf("Read Aliyun config file: %v, %v", err, configFile)
	}
	conf, err := configFromJSON(jsonKey)
	if err != nil {
		return "", fmt.Errorf("ConfigFromJSON: %v", err)
	}

	params := SessionUrlParams{
		UploadID:  uploadID,
		ObjectKey: objectKey,
	}

	encodeParams, err := query.Values(params)
	if err != nil {
		return "", fmt.Errorf("aliyun encode url params: %v", err)
	}

	url := fmt.Sprintf("%s?%s", conf.PartUploadUrl, encodeParams.Encode())
	return url, nil
}

func createAliStorageBucketHandle(configFile string) (*oss.Bucket, error) {
	jsonKey, err := ioutil.ReadFile(configFile)
	if err != nil {
		return nil, fmt.Errorf("Read Aliyun config file: %v, %v", err, configFile)
	}
	conf, err := configFromJSON(jsonKey)
	if err != nil {
		return nil, fmt.Errorf("Aliyun ConfigFromJSON: %v", err)
	}
	client, err := oss.New(conf.EndPoint, conf.AccessKeyId, conf.AccessKeySecret)
	if err != nil {
		return nil, fmt.Errorf("Aliyun Oss client: %v", err)
	}
	bucket, err := client.Bucket(conf.Bucket)
	if err != nil {
		return nil, fmt.Errorf("Aliyun Oss Bucket: %v", err)
	}

	return bucket, nil
}

// SaveStorageDocument uploadFileFromForm uploads a file if it's present in the "image" form field.
func (s *AliCloudStorage) SaveStorageDocument(filename string, file []byte, extension string) (url string, err error) {
	if s.StorageBucket == nil {
		return "", errors.New("storage bucket is missing - check config")
	}
	r := bytes.NewReader(file)
	if r.Size() == 0 {
		return "", fmt.Errorf("please contact IT support. file size 0, filename: %v", filename)
	}
	if err := s.StorageBucket.PutObject(filename, r); err != nil {
		return "", err
	}

	return filename, nil
}

func (s *AliCloudStorage) SaveStorageExport(filename string, xlsx *excelize.File, extension string) (url string, err error) {

	if s.StorageBucket == nil {
		return "", errors.New("storage bucket is missing - check config.go")
	}
	buffer, err := xlsx.WriteToBuffer()
	if err != nil {
		return "", errors.New("the excel file content error")
	}

	if err := s.StorageBucket.PutObject(filename, bytes.NewReader(buffer.Bytes())); err != nil {
		return "", err
	}

	return filename, nil
}

func (s *AliCloudStorage) MoveStorageDocument(srcPath, destPath string) error {
	_, err := s.StorageBucket.CopyObject(srcPath, destPath)
	if err != nil {
		return err
	}

	err = s.StorageBucket.DeleteObject(srcPath)
	if err != nil {
		return err
	}

	return nil
}

// ReadStorageDocument read file
func (s *AliCloudStorage) ReadStorageDocument(filename string) ([]byte, error) {
	body, err := s.StorageBucket.GetObject(filename)
	if err != nil {
		return nil, err
	}
	data, err := ioutil.ReadAll(body)
	body.Close()
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (s *AliCloudStorage) GetWriterByFilePathAndContentType(filepath, contentType string) *Writer {
	w := NewWriter(s.StorageBucket, filepath)

	return w
}

func (s *AliCloudStorage) NewReadSeekCloser(filepath string) (*AliCloudStorageReadSeekCloser, error) {
	meta, err := s.StorageBucket.GetObjectDetailedMeta(filepath)
	if err != nil {
		return nil, err
	}

	contentLength := meta.Get("Content-Length")
	fileSize, _ := strconv.ParseInt(contentLength, 10, 64)

	log.Println("meta Content-Length", meta.Get("Content-Length"))
	// body.getObjectMetadata
	return NewAliCloudStorageReadSeekCloser(s.StorageBucket, filepath, fileSize, 8192), nil
}

func (s *AliCloudStorage) prepareSignedUrlParamBy(filePath string, option *common.SignedUrlOption) (expirySec int64, ossOption []oss.Option) {
	expirySec = int64(7 * 24 * 60 * 60)
	ossOption = []oss.Option{}

	dftOssOptions := []oss.Option{}
	if option != nil {
		if option.ExpiryInMinutes > 0 {
			expirySec = int64(60 * option.ExpiryInMinutes)
		}
		if len(option.MimeContentType) > 0 {
			if option.SkipMimeContentTypeCheck {
				ossOption = append(dftOssOptions, oss.ResponseContentType(option.MimeContentType))
			} else {
				mimeType, verifyErr := common.VerifyMimeTypeByFileName(filePath, option.MimeContentType)
				if verifyErr != nil {
					log.Printf("VerifyMimeTypeByFileName err: %+v", verifyErr)
				} else {
					ossOption = append(dftOssOptions, oss.ResponseContentType(mimeType))
				}
			}
		}
	}

	return
}

func (s *AliCloudStorage) GenerateObjectSignedURL(filepath string, option *common.SignedUrlOption) (string, error) {
	dftExpireSecond, dftOssOptions := s.prepareSignedUrlParamBy(filepath, option)

	signedURL, err := s.StorageBucket.SignURL(filepath, oss.HTTPGet, dftExpireSecond, dftOssOptions...)
	if err != nil {
		return "", err
	}
	return signedURL, nil
}

func (s *AliCloudStorage) GetMultipartUploadUrl(filepath string) (string, error) {
	uploader, err := s.StorageBucket.InitiateMultipartUpload(filepath)
	if err != nil {
		return "", err
	}
	return getSessionUrl(s.ConfigFile, filepath, uploader.UploadID)
}

type UploadPartResult struct {
	Name       string `json:"name"`
	FilePath   string `json:"file_path"`
	PartNumber int    `json:"part_number"`
	ETag       string `json:"e-tag"`
}

func (s *AliCloudStorage) UploadPart(filepath, uploadID string, fd io.Reader, partSize int64, partNumber int) (*UploadPartResult, error) {
	imur := NewImur(filepath, uploadID)
	result, err := s.StorageBucket.UploadPart(imur, fd, partSize, partNumber)
	if err != nil {
		return nil, err
	}

	return &UploadPartResult{Name: filepath, FilePath: filepath, PartNumber: result.PartNumber, ETag: result.ETag}, nil
}

// 完成分片上传。
func (s *AliCloudStorage) CompleteUploadPart(filepath, uploadID string) error {
	// http://uri?object_key=filepath&upload_id=055E754B5D4142DC9C6C85561A7ACB1E
	if strings.Contains(uploadID, "upload_id") && strings.Contains(uploadID, "http") {
		urlObj, err := url.Parse(uploadID)
		if err != nil {
			return err
		}
		params, err := url.ParseQuery(urlObj.RawQuery)
		if err != nil {
			return err
		}
		uploadID = params["upload_id"][0]
	}

	imur := NewImur(filepath, uploadID)

	lsRes, err := s.StorageBucket.ListUploadedParts(imur)
	if err != nil {
		return err
	}

	parts := make([]oss.UploadPart, 0)
	for _, uploadedPart := range lsRes.UploadedParts {
		parts = append(parts, oss.UploadPart{
			PartNumber: uploadedPart.PartNumber,
			ETag:       uploadedPart.ETag,
		})
	}
	_, err = s.StorageBucket.CompleteMultipartUpload(imur, parts)
	return err
}

// ZipStorageDocument read file
func (s *AliCloudStorage) ZipStorageDocument(zipFileURI, zipFolder string, fileNames ...string) error {
	storageWriter := s.GetWriterByFilePathAndContentType(zipFileURI, "")
	// Create a new zip archive to memory buffer
	zipWriter := zip.NewWriter(storageWriter)
	// go through each file in the prefix
	for _, fileName := range fileNames {
		if fileName != "" {
			storageReader, err := s.StorageBucket.GetObject(fileName)
			if err != nil {
				return err
			}
			_, relPath := filepath.Split(fileName)
			// add filename to zip to do change file
			zipFile, err := zipWriter.Create(zipFolder + "/" + relPath)
			if err != nil {
				storageReader.Close()
				return err
			}
			// copy from storage reader to zip writer
			if _, err := io.Copy(zipFile, storageReader); err != nil {
				storageReader.Close()
				return err
			}
			storageReader.Close()
		}
	}
	if err := zipWriter.Close(); err != nil {
		storageWriter.Close()
		return err
	}
	storageWriter.Close() // we should be uploaded by here
	return nil
}

func (s *AliCloudStorage) ZipContentToStorageDocument(zipContent *common.ZipContent) ([]byte, error) {
	// create storage file for writing
	// Create a new zip archive to memory buffer
	buf := new(bytes.Buffer)
	zipWriter := zip.NewWriter(buf)
	// go through each file in the prefix
	for _, file := range zipContent.File {
		if file.Filename != "" {
			storageReader, err := s.StorageBucket.GetObject(file.FilePath)
			if err != nil {
				return nil, err
			}
			zipFile, err := zipWriter.Create(zipContent.FolderName + "/" + file.Filename)
			if err != nil {
				storageReader.Close()
				return nil, err
			}
			// copy from storage reader to zip writer
			if _, err := io.Copy(zipFile, storageReader); err != nil {
				storageReader.Close()
				return nil, err
			}
			storageReader.Close()
		}
	}

	err := common.CopyToZipWriter(zipWriter, zipContent)
	if err != nil {
		return nil, err
	}
	err = zipWriter.Close()
	return buf.Bytes(), err
}
