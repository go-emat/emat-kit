package concurrentjob

import (
	"fmt"
	"strings"
	"sync"
)

type JobErr struct {
	Err error
	Id  JobIdentifier
}

func (e *JobErr) IsNil() bool {
	if e == nil || e.Err == nil {
		return true
	}
	if jes, ok := e.Err.(*JobErrs); ok {
		return jes.IsNil()
	}
	if je, ok := e.Err.(*JobErr); ok {
		return je.IsNil()
	}
	return false
}

func (e *JobErr) Error() string {
	return fmt.Sprintf("Job failed with identifier: (%s), err: %v", e.Id.Message(), e.Err)
}

type JobErrs struct {
	errs []*JobErr
}

func (e *JobErrs) Add(je *JobErr) {
	if !je.IsNil() {
		e.errs = append(e.errs, je)
	}
}

func (e *JobErrs) Error() string {
	errStrs := []string{}
	for _, err := range e.errs {
		if err != nil {
			errStrs = append(errStrs, err.Error())
		}
	}
	return strings.Join(errStrs, "\n")
}

func (e *JobErrs) IsNil() bool {
	return e == nil || len(e.errs) == 0
}

func MergeErrs(inChans []<-chan *JobErr) <-chan *JobErrs {
	outChan := make(chan *JobErrs, 1)
	errChan := make(chan *JobErr, len(inChans))
	var wg sync.WaitGroup
	wg.Add(len(inChans))
	for _, ec := range inChans {
		go func(ec <-chan *JobErr) {
			for err := range ec {
				errChan <- err
			}
			wg.Done()
		}(ec)
	}
	go func() {
		wg.Wait()
		close(errChan)
		jErrs := &JobErrs{}
		for je := range errChan {
			jErrs.Add(je)
		}
		if len(jErrs.errs) > 0 {
			outChan <- jErrs
		} else {
			outChan <- nil
		}
		close(outChan)
	}()
	return outChan
}
