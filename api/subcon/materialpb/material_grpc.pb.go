// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package materialpb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// MaterialContainerServiceClient is the client API for MaterialContainerService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type MaterialContainerServiceClient interface {
	ReadMaterialContainer(ctx context.Context, in *ReadMaterialContainerRequest, opts ...grpc.CallOption) (*ReadMaterialContainerResponse, error)
	ReadMaterialContainerByClient(ctx context.Context, in *ReadMaterialContainerByClientRequest, opts ...grpc.CallOption) (*ReadMaterialContainerResponse, error)
	GetMainContractorMaterialRPC(ctx context.Context, in *GetMainContractorMaterialRpcRequest, opts ...grpc.CallOption) (*GetMainContractorMaterialRpcResponse, error)
	CreateMaterialContainer(ctx context.Context, in *CreateMaterialContainerRequest, opts ...grpc.CallOption) (*ActionResponse, error)
	UpdateMaterialContainer(ctx context.Context, in *UpdateMaterialContainerRequest, opts ...grpc.CallOption) (*ActionResponse, error)
	SubmitToContractor(ctx context.Context, in *SubmitToContractorRequest, opts ...grpc.CallOption) (*ActionResponse, error)
	ReadMaterialContainerMilestone(ctx context.Context, in *ReadMaterialContainerMilestonesRequest, opts ...grpc.CallOption) (*ReadMaterialContainerMilestonesResponse, error)
	UpdateMaterialContainerMilestone(ctx context.Context, in *UpdateMaterialContainerMilestonesRequest, opts ...grpc.CallOption) (*ActionResponse, error)
	AcceptMaterialContainerMilestone(ctx context.Context, in *AcceptMilestoneRequest, opts ...grpc.CallOption) (*ActionResponse, error)
	RejectMaterialContainerMilestone(ctx context.Context, in *RejectMilestoneRequest, opts ...grpc.CallOption) (*ActionResponse, error)
	ListMaterialContainers(ctx context.Context, in *ListMaterialContainersRequest, opts ...grpc.CallOption) (*ListMaterialContainersResponse, error)
	ListMaterialContainerStatus(ctx context.Context, in *ListMaterialContainerStatusRequest, opts ...grpc.CallOption) (*ListMaterialContainerStatusResponse, error)
	ListDocumentNumber(ctx context.Context, in *ListDocumentNumberRequest, opts ...grpc.CallOption) (*ListDocumentNumberResponse, error)
}

type materialContainerServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewMaterialContainerServiceClient(cc grpc.ClientConnInterface) MaterialContainerServiceClient {
	return &materialContainerServiceClient{cc}
}

func (c *materialContainerServiceClient) ReadMaterialContainer(ctx context.Context, in *ReadMaterialContainerRequest, opts ...grpc.CallOption) (*ReadMaterialContainerResponse, error) {
	out := new(ReadMaterialContainerResponse)
	err := c.cc.Invoke(ctx, "/subcon.MaterialContainerService/ReadMaterialContainer", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *materialContainerServiceClient) ReadMaterialContainerByClient(ctx context.Context, in *ReadMaterialContainerByClientRequest, opts ...grpc.CallOption) (*ReadMaterialContainerResponse, error) {
	out := new(ReadMaterialContainerResponse)
	err := c.cc.Invoke(ctx, "/subcon.MaterialContainerService/ReadMaterialContainerByClient", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *materialContainerServiceClient) GetMainContractorMaterialRPC(ctx context.Context, in *GetMainContractorMaterialRpcRequest, opts ...grpc.CallOption) (*GetMainContractorMaterialRpcResponse, error) {
	out := new(GetMainContractorMaterialRpcResponse)
	err := c.cc.Invoke(ctx, "/subcon.MaterialContainerService/GetMainContractorMaterialRPC", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *materialContainerServiceClient) CreateMaterialContainer(ctx context.Context, in *CreateMaterialContainerRequest, opts ...grpc.CallOption) (*ActionResponse, error) {
	out := new(ActionResponse)
	err := c.cc.Invoke(ctx, "/subcon.MaterialContainerService/CreateMaterialContainer", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *materialContainerServiceClient) UpdateMaterialContainer(ctx context.Context, in *UpdateMaterialContainerRequest, opts ...grpc.CallOption) (*ActionResponse, error) {
	out := new(ActionResponse)
	err := c.cc.Invoke(ctx, "/subcon.MaterialContainerService/UpdateMaterialContainer", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *materialContainerServiceClient) SubmitToContractor(ctx context.Context, in *SubmitToContractorRequest, opts ...grpc.CallOption) (*ActionResponse, error) {
	out := new(ActionResponse)
	err := c.cc.Invoke(ctx, "/subcon.MaterialContainerService/SubmitToContractor", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *materialContainerServiceClient) ReadMaterialContainerMilestone(ctx context.Context, in *ReadMaterialContainerMilestonesRequest, opts ...grpc.CallOption) (*ReadMaterialContainerMilestonesResponse, error) {
	out := new(ReadMaterialContainerMilestonesResponse)
	err := c.cc.Invoke(ctx, "/subcon.MaterialContainerService/ReadMaterialContainerMilestone", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *materialContainerServiceClient) UpdateMaterialContainerMilestone(ctx context.Context, in *UpdateMaterialContainerMilestonesRequest, opts ...grpc.CallOption) (*ActionResponse, error) {
	out := new(ActionResponse)
	err := c.cc.Invoke(ctx, "/subcon.MaterialContainerService/UpdateMaterialContainerMilestone", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *materialContainerServiceClient) AcceptMaterialContainerMilestone(ctx context.Context, in *AcceptMilestoneRequest, opts ...grpc.CallOption) (*ActionResponse, error) {
	out := new(ActionResponse)
	err := c.cc.Invoke(ctx, "/subcon.MaterialContainerService/AcceptMaterialContainerMilestone", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *materialContainerServiceClient) RejectMaterialContainerMilestone(ctx context.Context, in *RejectMilestoneRequest, opts ...grpc.CallOption) (*ActionResponse, error) {
	out := new(ActionResponse)
	err := c.cc.Invoke(ctx, "/subcon.MaterialContainerService/RejectMaterialContainerMilestone", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *materialContainerServiceClient) ListMaterialContainers(ctx context.Context, in *ListMaterialContainersRequest, opts ...grpc.CallOption) (*ListMaterialContainersResponse, error) {
	out := new(ListMaterialContainersResponse)
	err := c.cc.Invoke(ctx, "/subcon.MaterialContainerService/ListMaterialContainers", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *materialContainerServiceClient) ListMaterialContainerStatus(ctx context.Context, in *ListMaterialContainerStatusRequest, opts ...grpc.CallOption) (*ListMaterialContainerStatusResponse, error) {
	out := new(ListMaterialContainerStatusResponse)
	err := c.cc.Invoke(ctx, "/subcon.MaterialContainerService/ListMaterialContainerStatus", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *materialContainerServiceClient) ListDocumentNumber(ctx context.Context, in *ListDocumentNumberRequest, opts ...grpc.CallOption) (*ListDocumentNumberResponse, error) {
	out := new(ListDocumentNumberResponse)
	err := c.cc.Invoke(ctx, "/subcon.MaterialContainerService/ListDocumentNumber", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// MaterialContainerServiceServer is the server API for MaterialContainerService service.
// All implementations must embed UnimplementedMaterialContainerServiceServer
// for forward compatibility
type MaterialContainerServiceServer interface {
	ReadMaterialContainer(context.Context, *ReadMaterialContainerRequest) (*ReadMaterialContainerResponse, error)
	ReadMaterialContainerByClient(context.Context, *ReadMaterialContainerByClientRequest) (*ReadMaterialContainerResponse, error)
	GetMainContractorMaterialRPC(context.Context, *GetMainContractorMaterialRpcRequest) (*GetMainContractorMaterialRpcResponse, error)
	CreateMaterialContainer(context.Context, *CreateMaterialContainerRequest) (*ActionResponse, error)
	UpdateMaterialContainer(context.Context, *UpdateMaterialContainerRequest) (*ActionResponse, error)
	SubmitToContractor(context.Context, *SubmitToContractorRequest) (*ActionResponse, error)
	ReadMaterialContainerMilestone(context.Context, *ReadMaterialContainerMilestonesRequest) (*ReadMaterialContainerMilestonesResponse, error)
	UpdateMaterialContainerMilestone(context.Context, *UpdateMaterialContainerMilestonesRequest) (*ActionResponse, error)
	AcceptMaterialContainerMilestone(context.Context, *AcceptMilestoneRequest) (*ActionResponse, error)
	RejectMaterialContainerMilestone(context.Context, *RejectMilestoneRequest) (*ActionResponse, error)
	ListMaterialContainers(context.Context, *ListMaterialContainersRequest) (*ListMaterialContainersResponse, error)
	ListMaterialContainerStatus(context.Context, *ListMaterialContainerStatusRequest) (*ListMaterialContainerStatusResponse, error)
	ListDocumentNumber(context.Context, *ListDocumentNumberRequest) (*ListDocumentNumberResponse, error)
	mustEmbedUnimplementedMaterialContainerServiceServer()
}

// UnimplementedMaterialContainerServiceServer must be embedded to have forward compatible implementations.
type UnimplementedMaterialContainerServiceServer struct {
}

func (UnimplementedMaterialContainerServiceServer) ReadMaterialContainer(context.Context, *ReadMaterialContainerRequest) (*ReadMaterialContainerResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ReadMaterialContainer not implemented")
}
func (UnimplementedMaterialContainerServiceServer) ReadMaterialContainerByClient(context.Context, *ReadMaterialContainerByClientRequest) (*ReadMaterialContainerResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ReadMaterialContainerByClient not implemented")
}
func (UnimplementedMaterialContainerServiceServer) GetMainContractorMaterialRPC(context.Context, *GetMainContractorMaterialRpcRequest) (*GetMainContractorMaterialRpcResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetMainContractorMaterialRPC not implemented")
}
func (UnimplementedMaterialContainerServiceServer) CreateMaterialContainer(context.Context, *CreateMaterialContainerRequest) (*ActionResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateMaterialContainer not implemented")
}
func (UnimplementedMaterialContainerServiceServer) UpdateMaterialContainer(context.Context, *UpdateMaterialContainerRequest) (*ActionResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateMaterialContainer not implemented")
}
func (UnimplementedMaterialContainerServiceServer) SubmitToContractor(context.Context, *SubmitToContractorRequest) (*ActionResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SubmitToContractor not implemented")
}
func (UnimplementedMaterialContainerServiceServer) ReadMaterialContainerMilestone(context.Context, *ReadMaterialContainerMilestonesRequest) (*ReadMaterialContainerMilestonesResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ReadMaterialContainerMilestone not implemented")
}
func (UnimplementedMaterialContainerServiceServer) UpdateMaterialContainerMilestone(context.Context, *UpdateMaterialContainerMilestonesRequest) (*ActionResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateMaterialContainerMilestone not implemented")
}
func (UnimplementedMaterialContainerServiceServer) AcceptMaterialContainerMilestone(context.Context, *AcceptMilestoneRequest) (*ActionResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AcceptMaterialContainerMilestone not implemented")
}
func (UnimplementedMaterialContainerServiceServer) RejectMaterialContainerMilestone(context.Context, *RejectMilestoneRequest) (*ActionResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method RejectMaterialContainerMilestone not implemented")
}
func (UnimplementedMaterialContainerServiceServer) ListMaterialContainers(context.Context, *ListMaterialContainersRequest) (*ListMaterialContainersResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListMaterialContainers not implemented")
}
func (UnimplementedMaterialContainerServiceServer) ListMaterialContainerStatus(context.Context, *ListMaterialContainerStatusRequest) (*ListMaterialContainerStatusResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListMaterialContainerStatus not implemented")
}
func (UnimplementedMaterialContainerServiceServer) ListDocumentNumber(context.Context, *ListDocumentNumberRequest) (*ListDocumentNumberResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListDocumentNumber not implemented")
}
func (UnimplementedMaterialContainerServiceServer) mustEmbedUnimplementedMaterialContainerServiceServer() {
}

// UnsafeMaterialContainerServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to MaterialContainerServiceServer will
// result in compilation errors.
type UnsafeMaterialContainerServiceServer interface {
	mustEmbedUnimplementedMaterialContainerServiceServer()
}

func RegisterMaterialContainerServiceServer(s grpc.ServiceRegistrar, srv MaterialContainerServiceServer) {
	s.RegisterService(&_MaterialContainerService_serviceDesc, srv)
}

func _MaterialContainerService_ReadMaterialContainer_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ReadMaterialContainerRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MaterialContainerServiceServer).ReadMaterialContainer(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/subcon.MaterialContainerService/ReadMaterialContainer",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MaterialContainerServiceServer).ReadMaterialContainer(ctx, req.(*ReadMaterialContainerRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MaterialContainerService_ReadMaterialContainerByClient_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ReadMaterialContainerByClientRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MaterialContainerServiceServer).ReadMaterialContainerByClient(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/subcon.MaterialContainerService/ReadMaterialContainerByClient",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MaterialContainerServiceServer).ReadMaterialContainerByClient(ctx, req.(*ReadMaterialContainerByClientRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MaterialContainerService_GetMainContractorMaterialRPC_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetMainContractorMaterialRpcRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MaterialContainerServiceServer).GetMainContractorMaterialRPC(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/subcon.MaterialContainerService/GetMainContractorMaterialRPC",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MaterialContainerServiceServer).GetMainContractorMaterialRPC(ctx, req.(*GetMainContractorMaterialRpcRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MaterialContainerService_CreateMaterialContainer_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateMaterialContainerRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MaterialContainerServiceServer).CreateMaterialContainer(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/subcon.MaterialContainerService/CreateMaterialContainer",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MaterialContainerServiceServer).CreateMaterialContainer(ctx, req.(*CreateMaterialContainerRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MaterialContainerService_UpdateMaterialContainer_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateMaterialContainerRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MaterialContainerServiceServer).UpdateMaterialContainer(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/subcon.MaterialContainerService/UpdateMaterialContainer",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MaterialContainerServiceServer).UpdateMaterialContainer(ctx, req.(*UpdateMaterialContainerRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MaterialContainerService_SubmitToContractor_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SubmitToContractorRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MaterialContainerServiceServer).SubmitToContractor(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/subcon.MaterialContainerService/SubmitToContractor",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MaterialContainerServiceServer).SubmitToContractor(ctx, req.(*SubmitToContractorRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MaterialContainerService_ReadMaterialContainerMilestone_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ReadMaterialContainerMilestonesRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MaterialContainerServiceServer).ReadMaterialContainerMilestone(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/subcon.MaterialContainerService/ReadMaterialContainerMilestone",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MaterialContainerServiceServer).ReadMaterialContainerMilestone(ctx, req.(*ReadMaterialContainerMilestonesRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MaterialContainerService_UpdateMaterialContainerMilestone_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateMaterialContainerMilestonesRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MaterialContainerServiceServer).UpdateMaterialContainerMilestone(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/subcon.MaterialContainerService/UpdateMaterialContainerMilestone",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MaterialContainerServiceServer).UpdateMaterialContainerMilestone(ctx, req.(*UpdateMaterialContainerMilestonesRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MaterialContainerService_AcceptMaterialContainerMilestone_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AcceptMilestoneRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MaterialContainerServiceServer).AcceptMaterialContainerMilestone(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/subcon.MaterialContainerService/AcceptMaterialContainerMilestone",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MaterialContainerServiceServer).AcceptMaterialContainerMilestone(ctx, req.(*AcceptMilestoneRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MaterialContainerService_RejectMaterialContainerMilestone_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RejectMilestoneRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MaterialContainerServiceServer).RejectMaterialContainerMilestone(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/subcon.MaterialContainerService/RejectMaterialContainerMilestone",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MaterialContainerServiceServer).RejectMaterialContainerMilestone(ctx, req.(*RejectMilestoneRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MaterialContainerService_ListMaterialContainers_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListMaterialContainersRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MaterialContainerServiceServer).ListMaterialContainers(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/subcon.MaterialContainerService/ListMaterialContainers",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MaterialContainerServiceServer).ListMaterialContainers(ctx, req.(*ListMaterialContainersRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MaterialContainerService_ListMaterialContainerStatus_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListMaterialContainerStatusRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MaterialContainerServiceServer).ListMaterialContainerStatus(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/subcon.MaterialContainerService/ListMaterialContainerStatus",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MaterialContainerServiceServer).ListMaterialContainerStatus(ctx, req.(*ListMaterialContainerStatusRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MaterialContainerService_ListDocumentNumber_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListDocumentNumberRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MaterialContainerServiceServer).ListDocumentNumber(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/subcon.MaterialContainerService/ListDocumentNumber",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MaterialContainerServiceServer).ListDocumentNumber(ctx, req.(*ListDocumentNumberRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _MaterialContainerService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "subcon.MaterialContainerService",
	HandlerType: (*MaterialContainerServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "ReadMaterialContainer",
			Handler:    _MaterialContainerService_ReadMaterialContainer_Handler,
		},
		{
			MethodName: "ReadMaterialContainerByClient",
			Handler:    _MaterialContainerService_ReadMaterialContainerByClient_Handler,
		},
		{
			MethodName: "GetMainContractorMaterialRPC",
			Handler:    _MaterialContainerService_GetMainContractorMaterialRPC_Handler,
		},
		{
			MethodName: "CreateMaterialContainer",
			Handler:    _MaterialContainerService_CreateMaterialContainer_Handler,
		},
		{
			MethodName: "UpdateMaterialContainer",
			Handler:    _MaterialContainerService_UpdateMaterialContainer_Handler,
		},
		{
			MethodName: "SubmitToContractor",
			Handler:    _MaterialContainerService_SubmitToContractor_Handler,
		},
		{
			MethodName: "ReadMaterialContainerMilestone",
			Handler:    _MaterialContainerService_ReadMaterialContainerMilestone_Handler,
		},
		{
			MethodName: "UpdateMaterialContainerMilestone",
			Handler:    _MaterialContainerService_UpdateMaterialContainerMilestone_Handler,
		},
		{
			MethodName: "AcceptMaterialContainerMilestone",
			Handler:    _MaterialContainerService_AcceptMaterialContainerMilestone_Handler,
		},
		{
			MethodName: "RejectMaterialContainerMilestone",
			Handler:    _MaterialContainerService_RejectMaterialContainerMilestone_Handler,
		},
		{
			MethodName: "ListMaterialContainers",
			Handler:    _MaterialContainerService_ListMaterialContainers_Handler,
		},
		{
			MethodName: "ListMaterialContainerStatus",
			Handler:    _MaterialContainerService_ListMaterialContainerStatus_Handler,
		},
		{
			MethodName: "ListDocumentNumber",
			Handler:    _MaterialContainerService_ListDocumentNumber_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "subcon/material/material.proto",
}
