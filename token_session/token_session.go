package token_session

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"regexp"
	"strings"
)

// EnvOrEndPoint => production | local | http://api.local-emat.mattex.com.hk/api/token/verify
// Origin => http header origin, no use now
// Token => jwt token
type VerifyParam struct {
	EnvOrEndPoint string
	Origin        string
	Token         string
}

func getVerifyDestination(env string) string {
	domain := ""
	if strings.EqualFold(env, "production") {
		domain = "https://api.emat.mattex.com.hk"
	} else if strings.EqualFold(env, "uat") {
		domain = "https://api.uat-emat.mattex.com.hk"
	} else if strings.EqualFold(env, "dev") {
		domain = "https://api.dev-emat.mattex.com.hk"
	} else if strings.EqualFold(env, "local") {
		domain = "http://api.local-emat.mattex.com.hk"
	}
	if len(domain) > 0 {
		return domain + "/api/token/verify" // http://api.local-emat.mattex.com.hk/api/token/verify
	}
	return ""
}

// help call to mattex-auth to verify token session by envOrEndPoint and token
func VerifyTokenSession(param VerifyParam) (err error) {
	endpoint := ""
	pattern := `^https?:\/\/([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+(:[0-9]{1,5})?(\/[^\s]*)?$`
	re := regexp.MustCompile(pattern)
	if re.MatchString(param.EnvOrEndPoint) {
		endpoint = param.EnvOrEndPoint
	} else {
		endpoint = getVerifyDestination(param.EnvOrEndPoint)
	}
	if len(endpoint) <= 0 {
		return errors.New("invalid endpoint")
	}

	httpRequest, _ := http.NewRequest(http.MethodPost, endpoint, nil)
	if len(param.Origin) > 0 {
		httpRequest.Header.Set("Origin", param.Origin)
	}
	httpRequest.Header.Set("Authorization", strings.Join([]string{"Bearer", param.Token}, " "))
	client := &http.Client{}
	resp, doErr := client.Do(httpRequest)
	if doErr != nil {
		return doErr
	}
	defer resp.Body.Close()

	if body, readErr := io.ReadAll(resp.Body); readErr != nil {
		return readErr
	} else {
		if resp.StatusCode != http.StatusOK {
			rtnMsg := "token session unknow error"
			bodyStruct := struct {
				Errors []struct {
					Message string `json:"message"`
				} `json:"errors"`
				Message string `json:"message"` // this is for safe, above errors should be enough
			}{}
			if unmarErr := json.Unmarshal(body, &bodyStruct); unmarErr != nil {
				return unmarErr
			}
			if len(bodyStruct.Message) > 0 {
				rtnMsg = bodyStruct.Message
			} else {
				if len(bodyStruct.Errors) > 0 && len(bodyStruct.Errors[0].Message) > 0 {
					rtnMsg = bodyStruct.Errors[0].Message
				}
			}
			return errors.New(rtnMsg)
		}
	}
	return
}
