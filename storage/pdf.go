package storage

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"time"

	"gitlab.com/go-emat/emat-kit/storage/pdfcpu_helper"
)

type PdfService struct {
	StorageFactory *DocumentStorageFactory
}

func NewPdfService(storageBucketName, storageType string) *PdfService {
	return &PdfService{
		StorageFactory: &DocumentStorageFactory{
			StorageBucketName: storageBucketName,
			StorageType:       storageType,
		},
	}
}

func NewDefaultPdfService() *PdfService {
	return &PdfService{
		StorageFactory: newDefaultDocumentStorageFactory(),
	}
}

type PdfExtractRequest struct {
	File          []byte
	FileName      string
	OutputDir     string
	SelectedPages []string
}

type PdfExtractResponse struct {
	Duration    float64
	OutputFiles []string
}

func (s *PdfService) PagesTotal(file []byte) (int, error) {
	return pdfcpu_helper.PagesTotal(file)
}

func (s *PdfService) ExtractPdf(req *PdfExtractRequest) (*PdfExtractResponse, error) {
	startTime := time.Now()
	resp := &PdfExtractResponse{}
	defer func() {
		resp.Duration = time.Since(startTime).Seconds()
	}()

	if req.File == nil || req.FileName == "" {
		return nil, errors.New("No file or file name is provided")
	}

	if req.OutputDir == "" {
		return nil, errors.New("No output directory is provided")
	}

	if req.SelectedPages == nil || len(req.SelectedPages) == 0 {
		return nil, errors.New("No selected pages")
	}

	if err := pdfcpu_helper.MakeDir(req.OutputDir); err != nil {
		return nil, err
	}

	if err := pdfcpu_helper.ExtractPdf(req.File, req.FileName, req.OutputDir, req.SelectedPages); err != nil {
		return nil, err
	}

	extracFiles, err := pdfcpu_helper.ReadDir(req.OutputDir)
	if err != nil {
		return nil, err
	}

	resp.OutputFiles = extracFiles

	return resp, nil
}

type PdfTrimRequest struct {
	File           []byte
	OutputFilePath string
	SelectedPages  []string
}

type PdfTrimResponse struct {
	Duration float64
}

func (s *PdfService) TrimPdf(req *PdfTrimRequest) (*PdfTrimResponse, error) {
	startTime := time.Now()
	resp := &PdfTrimResponse{}
	defer func() {
		resp.Duration = time.Since(startTime).Seconds()
	}()

	if req.File == nil {
		return nil, errors.New("No file is provided")
	}

	if req.OutputFilePath == "" {
		return nil, errors.New("No output file")
	}

	if req.SelectedPages == nil || len(req.SelectedPages) == 0 {
		return nil, errors.New("No selected pages")
	}

	outputSource, err := os.Create(req.OutputFilePath)
	if err != nil {
		fmt.Println("create file error:", err)
		return nil, err
	}
	defer outputSource.Close()

	if err := pdfcpu_helper.TrimPdf(req.File, outputSource, req.SelectedPages); err != nil {
		return nil, err
	}

	return resp, nil
}

type PdfStandardConvertRequest struct {
	InputFilePath  string
	OutputFilePath string
	Args           []string
}

type PdfStandardConvertResponse struct {
	Duration float64
}

func (s *PdfService) StandardConvert(req *PdfStandardConvertRequest) (*PdfStandardConvertResponse, error) {
	startTime := time.Now()
	resp := &PdfStandardConvertResponse{}
	defer func() {
		resp.Duration = time.Since(startTime).Seconds()
	}()

	if req.InputFilePath == "" || req.OutputFilePath == "" {
		return resp, fmt.Errorf("InputFilePath and OutputFilePath cannot be empty")
	}

	command := fmt.Sprintf(`gs -q -dPDFSETTINGS=/screen -dNOPAUSE -dBATCH -dDOPDFMARKS -dNOSAFER -dNOGC -sDEVICE=pdfwrite -dCompatibilityLevel=1.7 -dPDFACompatibilityPolicy=1 -sOutputFile="%s" "%s" -c quit`, req.OutputFilePath, req.InputFilePath)
	cmd := exec.Command("sh", "-c", command)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		return resp, err
	}
	if cmd.ProcessState.Success() {
		return resp, nil
	} else {
		return resp, fmt.Errorf(cmd.ProcessState.String())
	}
}

func (s *PdfService) RemoveDir(dirpath string) error {
	return pdfcpu_helper.RemoveDir(dirpath)
}

func (s *PdfService) MakeDir(dirpath string) error {
	return pdfcpu_helper.MakeDir(dirpath)
}
