syntax = "proto3";

package reviewer;

option go_package = "reviewer/submissionpb";

import "common/common.proto";
import "reviewer/submission/document_common.proto";
import "google/protobuf/timestamp.proto";

service DocumentService {
  rpc StartReviewingProcess(StartReviewingProcessRequest) returns (SimpleResponse);
  rpc ChangeReviewingProcess(ChangeReviewingProcessRequest) returns (SimpleResponse);
  rpc PassReviewingProcess(PassReviewingProcessRequest) returns (SimpleResponse);
  rpc ReverseReviewingProcess(ReverseReviewingProcessRequest) returns (SimpleResponse);
  rpc ListReferenceDocument(ListReferenceDocumentRequest) returns (ListReferenceDocumentResponse);
  rpc GetDocumentApprovalFlowValidation (FlowValidationRequest) returns (FlowValidationResponse);
  rpc ApplyToCurrentDocuments (FlowValidationRequest) returns (SimpleResponse);

  rpc GetApprovalOptions (GetApprovalOptionsRequest) returns (GetApprovalOptionsResponse);
  rpc AsyncDocumentFlow (AsyncDocumentFlowRequest) returns (SimpleResponse);
  rpc RemoveDocumentFlow (RemoveDocumentFlowRequest) returns (SimpleResponse);
  rpc ReassignmentNotify (ReassignmentNotifyRequest) returns (SimpleResponse);

  rpc GetAwaitingApprovalDocuments (GetAwaitingApprovalDocumentsRequest) returns (GetAwaitingApprovalDocumentsResponse);
  rpc ActionAfterDocumentUpdateApprovalFlow (ActionAfterDocumentUpdateApprovalFlowRequest) returns (SimpleResponse);
}

message ReverseReviewingProcessRequest {
  common.Language lang = 1;
  int64 model_id = 2;
  string model_type = 3;
  int32 step = 4;
  string remark = 5;
}

message PassReviewingProcessRequest {
  common.Language lang = 1;
  int64 model_id = 2;
  string model_type = 3;
  repeated int64 next_step_user_ids = 4;
  string comment = 5;
}

message StartReviewingProcessRequest {
  common.Language lang = 1;
  int64 model_id = 2;
  string model_type = 3;
  int64 reviewing_process_id = 4;
}

message ChangeReviewingProcessRequest {
  common.Language lang = 1;
  int64 model_id = 2;
  string model_type = 3;
  int64 reviewing_process_id = 4;
  string note = 5;
  int64 custom_created_by = 6;
}

message ListReferenceDocumentRequest {
  common.Language lang = 1;
  string model_type = 2;
  int64 reviewer_project_id = 3;
  repeated int32 type_id = 4;
}

message ListReferenceDocumentResponse {
  bool success = 1;
  repeated ReferenceDocument data = 2;
}

message ReferenceDocument {
  string value = 1;
  string label = 2;
}

message FlowValidationRequest {
  common.Language lang = 1;
  int64 flow_id = 2;
}

message FlowValidationResponse {
  repeated ValidationDocument validations = 1;
}

message ValidationDocument {
  string document_type = 1;
  string document_name = 2;
  repeated DocumentOption documents = 3;
}

message DocumentOption {
  int64 id = 1;
  string document_no = 2;
}

message GetApprovalOptionsRequest {
  common.Language lang = 1;
  int64 client_id = 2;
  repeated string contain_options = 3;
}

message GetApprovalOptionsResponse {
  ApprovalOptions data = 1;
}

message ApprovalOptions {
  repeated ProjectOption projects = 1;
  repeated UserOption users = 2;
  repeated Option submission_types = 3;
  repeated Option outbound_document_types = 4;
}

message RemoveDocumentFlowRequest {
  common.Language lang = 1;
  int64 client_id = 2;
  string module = 3;
  int64 document_id = 4;
  string document_type = 5;
  string revision = 6;
  string action = 7;
}

message AsyncDocumentFlowRequest {
  common.Language lang = 1;
  int64 client_id = 2;
  string module = 3;
  int64 document_id = 4;
  string document_type = 5;
  string revision = 6;
  string action = 7;
  int64 flow_item_id = 8;
  int32 status = 9;
  repeated EmatDocumentFlowStep steps = 10;
  bool approval_flow_updated = 11;
  int64 updated_by = 12;
  string updated_by_name = 13;
}

message EmatDocumentFlowStep {
  int64 id = 1;
  int64 document_flow_id = 2;
  int64 parent_id = 3;
  bool origin = 4;
  int32 step = 5;
  string rule = 6;
  int32 rule_number = 7;
  string role = 8;
  string hooks = 9;
  int32 signatory = 10;
  int64 invite_by = 11;
  int32 status = 12;

  repeated EmatDocumentFlowUser users = 13;
  repeated EmatDocumentFlowStep children = 14;

  google.protobuf.Timestamp created_at = 20;
  google.protobuf.Timestamp updated_at = 21;
  google.protobuf.Timestamp deleted_at = 22;
  int64 created_by = 23;
  int64 updated_by = 24;
}

message EmatDocumentFlowUser {
  int64 id = 1;
  int64 emat_document_flow_step_id = 2;
  int64 user_id = 3;
  string user_name = 4;
  string email = 5;
  string job_title = 6;
  string update_job_title = 7;
  int64 origin_user_id = 8;
  string origin_user_name = 9;
  string origin_email = 10;
  string origin_job_title = 11;
  string origin_update_job_title = 12;
  bool skip = 13;
  bool is_ignore = 14;
  string label = 15;
  int32 confirmed = 17;
  google.protobuf.Timestamp confirmed_at = 18;
  string remark = 19;
  google.protobuf.Timestamp created_at = 20;
  google.protobuf.Timestamp updated_at = 21;
  google.protobuf.Timestamp deleted_at = 22;
  int64 created_by = 23;
  int64 updated_by = 24;

  int64 flow_reassignment_item_id = 30;
  google.protobuf.Timestamp duration_start = 31;
  google.protobuf.Timestamp duration_end = 32;
  google.protobuf.Timestamp duration_valid_start = 33;
  google.protobuf.Timestamp duration_valid_end = 34;
}


message ReassignmentNotifyRequest {
  common.Language lang = 1;
  repeated ReassignmentNotifyDocument documents = 2;
}

message ReassignmentNotifyDocument {
  string document_type = 1;
  int64 document_id = 2;
  string revision = 3;
  string action = 4;
  repeated ReassignmentNotifyDocumentReassign reassign_rules = 5;
}

message ReassignmentNotifyDocumentReassign {
  string name = 2;
  string reassignment_no = 3;
  int32 type = 4; // 1:tmp 2:Permanent
  string action = 5; //reassign/remove
  int64 from_user_id = 6;
  int64 to_user_id = 7;
  repeated int64 next_step_users = 8; // when remove, return next step users
  int64 emat_document_flow_user_id = 9;
  google.protobuf.Timestamp duration_start = 10;
  google.protobuf.Timestamp duration_end = 11;
  google.protobuf.Timestamp duration_valid_start = 12;
  google.protobuf.Timestamp duration_valid_end = 13;
}

message GetAwaitingApprovalDocumentsRequest {
  common.Language lang = 1;
  repeated int64 approval_flow_ids = 2;
  string document_type = 3;
  repeated int64 submission_type_ids = 4;
  repeated int64 outbound_document_type_ids = 5;
  repeated int64 reviewer_project_ids = 6;
}

message GetAwaitingApprovalDocumentsResponse {
  repeated AwaitingApprovalDocument data = 1;
}

message AwaitingApprovalDocument {
  int64 approval_flow_id = 1;
  int64 document_id = 2;
  string document_no = 3;
  string document_type = 4;
  string revision = 5;
  string action = 6;
}

message ActionAfterDocumentUpdateApprovalFlowRequest {
  common.Language lang = 1;
  int64 document_id = 2;
  string document_type = 3;
  bool updated = 4;
  bool auto_approval = 5;
}
