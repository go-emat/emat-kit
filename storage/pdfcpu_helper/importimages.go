package pdfcpu_helper

import (
	"image"
	"io"

	"gitlab.com/go-emat/emat-kit/storage/pdf_merge"
	pdfcpuApi "gitlab.com/go-emat/pdfcpu-mattex/pkg/api"
	"gitlab.com/go-emat/pdfcpu-mattex/pkg/pdfcpu"
)

var defaultImgImp *pdfcpu.Import

func init() {
	if imp, err := pdfcpuApi.Import("formsize:A4, position:c, scalefactor:1", pdfcpu.POINTS); err == nil {
		defaultImgImp = imp
	}
}

func ValidateImageInputSource(imgFile *ImageFile) error {
	_, _, err := image.Decode(imgFile.InputSource)
	imgFile.InputSource.Seek(0, io.SeekStart)
	if err != nil {
		return pdf_merge.NewImageDecodeError(imgFile.FileName, err)
	}
	return nil
}

func ImportImageToCtx(ctx *pdfcpu.Context, imp *pdfcpu.Import, imgFileReader io.Reader, imgFileName string) (err error) {
	defer func() {
		if err != nil {
			err = pdf_merge.NewImageFileMergeError(imgFileName, err)
		}
	}()

	if imp == nil {
		imp = defaultImgImp
	}

	pagesIndRef, err := ctx.Pages()
	if err != nil {
		return
	}

	// This is the page tree root.
	pagesDict, err := ctx.DereferenceDict(*pagesIndRef)
	if err != nil {
		return
	}

	indRef, err := pdfcpu.NewPageForImage(ctx.XRefTable, imgFileReader, pagesIndRef, imp)
	if err != nil {
		return
	}

	if err = pdfcpu.AppendPageTree(indRef, 1, pagesDict); err != nil {
		return
	}

	ctx.PageCount++

	return
}
