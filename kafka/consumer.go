package kafka

import (
	"context"
	"errors"
	"fmt"
	"github.com/Shopify/sarama"
	"log"
	"os"
	"regexp"
	"strconv"
	"sync"
	"time"
)

var (
	StateClosed    = uint8(0)
	StateOpened    = uint8(1)
	StateReopening = uint8(2)
)

//GetEnvClientID Get Client ID from env params
func GetEnvClientID() int64 {
	id, err := strconv.Atoi(os.Getenv("CLIENT_ID"))
	if err != nil {
		return 0
	}
	return int64(id)
}

// ConsumerMessage struct of passed to message handler
type ConsumerMessage struct {
	*sarama.ConsumerMessage
}

// ConsumerMessageHandler registed by application to process message, each consumer has one message handler.
// Application should handle different message struct per different topic if he consumes multiple topics.
// Returned error means consume failed, then message will be retied ConsumerConfig.MaxRetry times.
type ConsumerMessageHandler interface {
	Consume(msg *ConsumerMessage) error
}

// Consumer represents a Sarama consumer group consumer
type Subscriber struct {
	clientName        string
	GroupID           string
	client            sarama.Client
	consumerGroup     sarama.ConsumerGroup
	cfg               *ConsumerConfig
	msgHandler        ConsumerMessageHandler
	partitionConsumer *PartitionConsumer

	// 保护数据并发安全
	mutex sync.RWMutex
	//// 监听会话channel关闭
	//closeChan chan error

	ctx       context.Context
	// Consumer关闭控制
	cancelCtx context.CancelFunc
	// Consumer状态
	state uint8
}

func NewSubscriber(cfg *ConsumerConfig, handler ConsumerMessageHandler) (*Subscriber, error) {
	clientName := generateKafkaClientName(cfg.GroupID)
	agent := &Subscriber{
		clientName: clientName,
		cfg:        cfg,
		msgHandler: handler,
	}
	agent.partitionConsumer = agent.newPartitionConsumer(handler)

	return agent, nil
}

func (c *Subscriber) Open() error {
	// Open期间不允许对channel做任何操作
	c.mutex.Lock()
	defer c.mutex.Unlock()
	// 状态检测
	if c.state == StateOpened {
		return errors.New("Kafka: Consumer had been opened")
	}
	if c.client == nil {
		if err := c.newClient(); err !=nil {
			return err
		}
	}
	// 初始化consumerGroup
	consumerGroup, err := sarama.NewConsumerGroupFromClient(c.cfg.GroupID, c.client)
	if err != nil {
		log.Printf("Kafka Consumer: [%s] init fail, %v", c.clientName, err)
		return err
	}
	c.consumerGroup = consumerGroup
	c.state = StateOpened
	c.ctx, c.cancelCtx = context.WithCancel(context.Background())


	c.Process()
	//c.closeChan = make(chan error, 1)
	// 健康检测
	log.Printf("Kafka Consumer: [%s] init success", c.clientName)

	go c.reportErrorAndKeepLive()

	return nil
}

func (c *Subscriber) GetAvailableTopics() ([]string, error) {
	if c.client == nil {
		return nil, errors.New("please init client first")
	}
	return c.client.Topics()
}

func (c *Subscriber) newClient() error {
	config := NewKafkaConsumerConfig(c.cfg)
	config.ClientID = "emat-kafka"
	version, err := sarama.ParseKafkaVersion(c.cfg.Version)
	if err != nil {
		log.Panicf("Error parsing Kafka version: %v", err)
	}
	config.Version = version
	client, err := sarama.NewClient(c.cfg.Brokers, config)
	if err != nil {
		return err
	}
	c.client = client
	return nil
}

func (c *Subscriber) SetTopicByRegexp(expr string) error {
	// Fetch all available topics
	if err := c.newClient(); err !=nil {
		return err
	}
	availableTopics, err := c.client.Topics()
	if err != nil {
		return err
	}
	log.Printf("Kafka Consumer availableTopics: [%v]", availableTopics)
	// Filter topics we care about
	var subTopics []string
	reg, err := regexp.Compile(expr)
	if err != nil {
		return err
	}
	for _, t := range availableTopics {
		if reg.MatchString(t) {
			subTopics = append(subTopics, t)
		}
	}
	if len(subTopics) == 0 {
		log.Panicf("Error parsing Kafka version: %v", err)
	}
	log.Printf("Kafka Consumer match topics: [%v]", subTopics)
	c.cfg.Topics = subTopics
	return nil
}

func (c *Subscriber) newPartitionConsumer(handler ConsumerMessageHandler) *PartitionConsumer {
	return &PartitionConsumer{
		handler: handler,
	}
}

// Close consumerGroup
func (c *Subscriber) Close() {

	log.Printf("[WARN] Kafka Consumer(%s) shutdown normally\n", c.clientName)
	c.mutex.Lock()
	c.cancelCtx()
	c.consumerGroup.Close()
	c.consumerGroup = nil
	c.state = StateClosed
	c.mutex.Unlock()
}

// Cleanup is run at the end of a session, once all ConsumeClaim goroutines have exited
func (c *Subscriber) Process() {

	log.Printf("Kafka Consumer Client Name: [%s] start consume", c.clientName)
	for {
		select {
		case <-c.ctx.Done():
			return
		default:
			err := c.consumerGroup.Consume(c.ctx, c.cfg.Topics, c.partitionConsumer)
			switch err {
			case nil:
			case sarama.ErrBrokerNotAvailable, sarama.ErrConsumerCoordinatorNotAvailable, sarama.ErrNotCoordinatorForConsumer:
				if err = c.client.RefreshCoordinator(c.cfg.GroupID); err != nil {
					log.Println("Kafka Consumer: RefreshCoordinator failed", err)
				}
				log.Printf("kafka error process: %v", err)
			case sarama.ErrTopicAuthorizationFailed:
				log.Fatalf("Kafka Consumer topics: %v, error: %v",  c.cfg.Topics, err)
			default:
				log.Println("Kafka Consumer: consume err", err)
			}
		}
	}
}

func (c *Subscriber) reportErrorAndKeepLive() {
	for err := range c.consumerGroup.Errors() {
		if consumerError, ok := err.(*sarama.ConsumerError); ok {
			log.Println("Kafka Consumer Group: consume failed", consumerError.Error())
		} else {
			log.Println("Kafka Consumer Group: receive unknown err", err)
		}
	}
}

func (c *Subscriber) keepalive() {
	// 被异常关闭了
	c.mutex.Lock()
	c.state = StateReopening
	c.mutex.Unlock()

	maxRetry := 100
	for i := 0; i < maxRetry; i++ {
		time.Sleep(time.Second)
		if c.client.Closed() {
			log.Printf("[WARN] Kafka: Consumer(%s) try to recover channel for %d times, but mq's state != StateOpened\n", c.clientName, i+1)
			continue
		}
		if e := c.Open(); e != nil {
			log.Printf("[WARN] Kafka: Consumer(%s) recover channel failed for %d times, Err:%v\n", c.clientName, i+1, e)
			continue
		}
		log.Printf("[INFO] Kafka: Consumer(%s) recover channel OK. Total try %d times\n", c.clientName, i+1)
		return
	}
	log.Printf("[ERROR] Kafka: Consumer(%s) try to recover channel over maxRetry(%d), so exit\n", c.clientName, maxRetry)
}

// generate client ID for debug, default use groupID-clientId,
// but if get client ID failed, then use the unix nano time
func generateKafkaClientName(groupID string) string {
	clientId := GetEnvClientID()
	if clientId == 0 {
		now := time.Now().UnixNano()
		clientIdString := strconv.FormatInt(now, 10)
		return fmt.Sprintf("%s-%s", groupID, clientIdString)
	}
	return fmt.Sprintf("%s-%v", groupID, clientId)
}

// Consumer represents a Sarama consumer group consumer
type PartitionConsumer struct {
	handler ConsumerMessageHandler
}

// Setup is run at the beginning of a new session, before ConsumeClaim
func (consumer *PartitionConsumer) Setup(sarama.ConsumerGroupSession) error {
	return nil
}

// Cleanup is run at the end of a session, once all ConsumeClaim goroutines have exited
func (consumer *PartitionConsumer) Cleanup(sarama.ConsumerGroupSession) error {
	return nil
}

// ConsumeClaim must start a consumer loop of ConsumerGroupClaim's Messages().
func (consumer *PartitionConsumer) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {

	// NOTE:
	// Do not move the code below to a goroutine.
	// The `ConsumeClaim` itself is called within a goroutine, see:
	// https://github.com/Shopify/sarama/blob/master/consumer_group.go#L27-L29
	for message := range claim.Messages() {
		err := consumer.handler.Consume(&ConsumerMessage{message})
		if err != nil {
			log.Printf("Kafka Consumer handler err: %v", err)
			return err
		}
		session.MarkMessage(message, "")
		log.Printf("kafka Commit Partition %v, Offset %v", message.Partition, message.Offset)
		session.Commit()
	}
	return nil
}
