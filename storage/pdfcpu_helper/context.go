package pdfcpu_helper

import (
	"bytes"

	pdfcpuApi "gitlab.com/go-emat/pdfcpu-mattex/pkg/api"
	"gitlab.com/go-emat/pdfcpu-mattex/pkg/pdfcpu"
)

func NewEmptyContext() (*pdfcpu.Context, error) {
	ctx, err := pdfcpu.CreateContextWithXRefTable(nil, pdfcpu.PaperSize["A4"])
	if err != nil {
		return nil, err
	}
	return WriteContextAndGetNewContext(ctx)
}

func NewBlankContext() (*pdfcpu.Context, error) {
	mediaBox := pdfcpu.RectForFormat("A4")
	p := pdfcpu.Page{MediaBox: mediaBox, Fm: pdfcpu.FontMap{}, Buf: new(bytes.Buffer)}
	xRefTable, err := pdfcpu.CreateDemoXRef(p)
	if err != nil {
		return nil, err
	}
	ctx := pdfcpu.CreateContext(xRefTable, nil)
	return WriteContextAndGetNewContext(ctx)
}

func WriteContextAndGetNewContext(ctx *pdfcpu.Context) (*pdfcpu.Context, error) {
	buf := &bytes.Buffer{}
	if err := pdfcpuApi.WriteContext(ctx, buf); err != nil {
		return nil, err
	}

	newCtx, err := pdfcpu.Read(bytes.NewReader(buf.Bytes()), nil)
	if err != nil {
		return nil, err
	}
	if err = newCtx.XRefTable.EnsurePageCount(); err != nil {
		return nil, err
	}

	return newCtx, nil
}

func NewContextWithPage(p *pdfcpu.Page) (*pdfcpu.Context, error) {
	xRefTable, err := pdfcpu.CreateDemoXRef(*p)
	if err != nil {
		return nil, err
	}
	ctx := pdfcpu.CreateContext(xRefTable, nil)

	return WriteContextAndGetNewContext(ctx)
}
