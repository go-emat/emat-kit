package pdfcpu_helper

import (
	"bytes"
	pdfcpuApi "gitlab.com/go-emat/pdfcpu-mattex/pkg/api"
	"gitlab.com/go-emat/pdfcpu-mattex/pkg/pdfcpu"
	"io"
)

func TrimPdf(file []byte, outputFile io.Writer, selectedPages []string) error  {
	conf := pdfcpu.NewDefaultConfiguration()
	conf.Cmd = pdfcpu.TRIM

	err := pdfcpuApi.Trim(bytes.NewReader(file), outputFile, selectedPages, conf)
	if err != nil {
		return err
	}
	return nil
}