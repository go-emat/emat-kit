package slack

func CreateSlackPayload(channel, text string) map[string]interface{} {
	payload := map[string]interface{}{
		"channel":  channel,
		"username": "Notification",
		"text":     text,
	}

	return payload

}
