// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package settingpb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// ProjectServiceClient is the client API for ProjectService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type ProjectServiceClient interface {
	ListProjectUsers(ctx context.Context, in *ListProjectUsersRequest, opts ...grpc.CallOption) (*ListProjectUsersResponse, error)
	ListProjectReviewers(ctx context.Context, in *ListProjectReviewersRequest, opts ...grpc.CallOption) (*ListProjectReviewersResponse, error)
	ListProjectReviewersAndUsers(ctx context.Context, in *ListProjectReviewersAndUsersRequest, opts ...grpc.CallOption) (*ListProjectReviewersAndUsersResponse, error)
	ListProjectTradesDefaultReviewers(ctx context.Context, in *ListProjectTradesDefaultReviewersRequest, opts ...grpc.CallOption) (*ListProjectTradesDefaultReviewersResponse, error)
}

type projectServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewProjectServiceClient(cc grpc.ClientConnInterface) ProjectServiceClient {
	return &projectServiceClient{cc}
}

func (c *projectServiceClient) ListProjectUsers(ctx context.Context, in *ListProjectUsersRequest, opts ...grpc.CallOption) (*ListProjectUsersResponse, error) {
	out := new(ListProjectUsersResponse)
	err := c.cc.Invoke(ctx, "/reviewer.ProjectService/ListProjectUsers", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *projectServiceClient) ListProjectReviewers(ctx context.Context, in *ListProjectReviewersRequest, opts ...grpc.CallOption) (*ListProjectReviewersResponse, error) {
	out := new(ListProjectReviewersResponse)
	err := c.cc.Invoke(ctx, "/reviewer.ProjectService/ListProjectReviewers", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *projectServiceClient) ListProjectReviewersAndUsers(ctx context.Context, in *ListProjectReviewersAndUsersRequest, opts ...grpc.CallOption) (*ListProjectReviewersAndUsersResponse, error) {
	out := new(ListProjectReviewersAndUsersResponse)
	err := c.cc.Invoke(ctx, "/reviewer.ProjectService/ListProjectReviewersAndUsers", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *projectServiceClient) ListProjectTradesDefaultReviewers(ctx context.Context, in *ListProjectTradesDefaultReviewersRequest, opts ...grpc.CallOption) (*ListProjectTradesDefaultReviewersResponse, error) {
	out := new(ListProjectTradesDefaultReviewersResponse)
	err := c.cc.Invoke(ctx, "/reviewer.ProjectService/ListProjectTradesDefaultReviewers", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ProjectServiceServer is the server API for ProjectService service.
// All implementations must embed UnimplementedProjectServiceServer
// for forward compatibility
type ProjectServiceServer interface {
	ListProjectUsers(context.Context, *ListProjectUsersRequest) (*ListProjectUsersResponse, error)
	ListProjectReviewers(context.Context, *ListProjectReviewersRequest) (*ListProjectReviewersResponse, error)
	ListProjectReviewersAndUsers(context.Context, *ListProjectReviewersAndUsersRequest) (*ListProjectReviewersAndUsersResponse, error)
	ListProjectTradesDefaultReviewers(context.Context, *ListProjectTradesDefaultReviewersRequest) (*ListProjectTradesDefaultReviewersResponse, error)
	mustEmbedUnimplementedProjectServiceServer()
}

// UnimplementedProjectServiceServer must be embedded to have forward compatible implementations.
type UnimplementedProjectServiceServer struct {
}

func (UnimplementedProjectServiceServer) ListProjectUsers(context.Context, *ListProjectUsersRequest) (*ListProjectUsersResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListProjectUsers not implemented")
}
func (UnimplementedProjectServiceServer) ListProjectReviewers(context.Context, *ListProjectReviewersRequest) (*ListProjectReviewersResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListProjectReviewers not implemented")
}
func (UnimplementedProjectServiceServer) ListProjectReviewersAndUsers(context.Context, *ListProjectReviewersAndUsersRequest) (*ListProjectReviewersAndUsersResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListProjectReviewersAndUsers not implemented")
}
func (UnimplementedProjectServiceServer) ListProjectTradesDefaultReviewers(context.Context, *ListProjectTradesDefaultReviewersRequest) (*ListProjectTradesDefaultReviewersResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListProjectTradesDefaultReviewers not implemented")
}
func (UnimplementedProjectServiceServer) mustEmbedUnimplementedProjectServiceServer() {}

// UnsafeProjectServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to ProjectServiceServer will
// result in compilation errors.
type UnsafeProjectServiceServer interface {
	mustEmbedUnimplementedProjectServiceServer()
}

func RegisterProjectServiceServer(s grpc.ServiceRegistrar, srv ProjectServiceServer) {
	s.RegisterService(&_ProjectService_serviceDesc, srv)
}

func _ProjectService_ListProjectUsers_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListProjectUsersRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProjectServiceServer).ListProjectUsers(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.ProjectService/ListProjectUsers",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProjectServiceServer).ListProjectUsers(ctx, req.(*ListProjectUsersRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ProjectService_ListProjectReviewers_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListProjectReviewersRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProjectServiceServer).ListProjectReviewers(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.ProjectService/ListProjectReviewers",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProjectServiceServer).ListProjectReviewers(ctx, req.(*ListProjectReviewersRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ProjectService_ListProjectReviewersAndUsers_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListProjectReviewersAndUsersRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProjectServiceServer).ListProjectReviewersAndUsers(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.ProjectService/ListProjectReviewersAndUsers",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProjectServiceServer).ListProjectReviewersAndUsers(ctx, req.(*ListProjectReviewersAndUsersRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ProjectService_ListProjectTradesDefaultReviewers_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListProjectTradesDefaultReviewersRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProjectServiceServer).ListProjectTradesDefaultReviewers(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.ProjectService/ListProjectTradesDefaultReviewers",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProjectServiceServer).ListProjectTradesDefaultReviewers(ctx, req.(*ListProjectTradesDefaultReviewersRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _ProjectService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "reviewer.ProjectService",
	HandlerType: (*ProjectServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "ListProjectUsers",
			Handler:    _ProjectService_ListProjectUsers_Handler,
		},
		{
			MethodName: "ListProjectReviewers",
			Handler:    _ProjectService_ListProjectReviewers_Handler,
		},
		{
			MethodName: "ListProjectReviewersAndUsers",
			Handler:    _ProjectService_ListProjectReviewersAndUsers_Handler,
		},
		{
			MethodName: "ListProjectTradesDefaultReviewers",
			Handler:    _ProjectService_ListProjectTradesDefaultReviewers_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "reviewer/setting/project.proto",
}
