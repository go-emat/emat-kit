package concurrentjob

import (
	"log"
	"reflect"
	"sync"
)

type ModelJobsBroker struct {
	sync.Mutex
	concurrentJobsChan chan struct{}
	jobsMap            map[string]ConcurrentJob
	pendingJobs        []ConcurrentJob
	stopChan           chan struct{}
}

func NewModelJobsBroker(maxConcurrentJobs int) *ModelJobsBroker {
	return &ModelJobsBroker{
		concurrentJobsChan: make(chan struct{}, maxConcurrentJobs),
		jobsMap:            map[string]ConcurrentJob{},
		stopChan:           make(chan struct{}, 1),
	}
}

func (b *ModelJobsBroker) Start() {
	for {
		select {
		case <-b.stopChan:
			return
		default:
		}

		var job ConcurrentJob
		b.Lock()
		if len(b.pendingJobs) > 0 {
			job = b.pendingJobs[0]
		}
		b.Unlock()
		if job == nil {
			continue
		}

		b.concurrentJobsChan <- struct{}{}

		go b.runJob(job)

		b.Lock()
		b.pendingJobs = b.pendingJobs[1:]
		b.Unlock()
	}
}

func (b *ModelJobsBroker) Stop() {
	b.stopChan <- struct{}{}
}

func (b *ModelJobsBroker) SubscribeDocumentJob(job ConcurrentJob) <-chan *JobErr {
	b.Lock()
	defer b.Unlock()

	b.arrangeJobOnSubscribe(job)

	promise := make(chan *JobErr, 1)
	b.jobsMap[job.Identifier()].AddSubscriber(promise)
	return promise
}

func (b *ModelJobsBroker) runJob(job ConcurrentJob) {
	err := job.Run()
	if err != nil {
		if j, ok := job.(ConcurrentJobWithErrFormatter); ok {
			log.Println(j.GetFinishedErrLogStr(err))
		} else {
			log.Println(err)
		}
	}
	<-b.concurrentJobsChan

	if b.rearrangeJobOnEnd(job) {
		return
	}

	if err == nil {
		if nextJob := job.GetNextJob(); nextJob != nil {
			err = <-b.SubscribeDocumentJob(nextJob)
		}
	}

	// if no need to retry, send messages to all subscribers
	for _, subscriber := range job.GetSubscribers() {
		subscriber <- err
		close(subscriber)
	}
}

func (b *ModelJobsBroker) arrangeJobOnSubscribe(job ConcurrentJob) {
	jobId := job.Identifier()

	if existingJob, ok := b.jobsMap[jobId]; ok {
		if isPending, _ := inArray(existingJob, b.pendingJobs); !isPending {
			retriableJob, retriable := existingJob.(RetriableConcurrentJob)
			if retriable && retriableJob.NeedRetryByNewJob(job) {
				for _, subscriber := range retriableJob.GetSubscribers() {
					job.AddSubscriber(subscriber)
				}
				if nextJob := retriableJob.GetNextJob(); nextJob != nil {
					job.SetNextJob(retriableJob)
				}
				b.jobsMap[jobId] = job
			}
		}
	} else {
		b.jobsMap[jobId] = job
		b.pendingJobs = append(b.pendingJobs, job)
	}
}

func (b *ModelJobsBroker) rearrangeJobOnEnd(job ConcurrentJob) (markedAsNeedRetry bool) {
	b.Lock()
	defer b.Unlock()

	jobId := job.Identifier()
	jobInMap, _ := b.jobsMap[jobId]
	markedAsNeedRetry = jobInMap != job
	if markedAsNeedRetry {
		b.pendingJobs = append(b.pendingJobs, jobInMap)
	} else {
		delete(b.jobsMap, jobId)
	}
	return
}

func inArray(val interface{}, array interface{}) (exists bool, index int) {
	exists = false
	index = -1

	switch reflect.TypeOf(array).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(array)

		for i := 0; i < s.Len(); i++ {
			if reflect.DeepEqual(val, s.Index(i).Interface()) {
				index = i
				exists = true
				return
			}
		}
	}
	return
}
