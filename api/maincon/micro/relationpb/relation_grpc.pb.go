// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package relationpb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// RelationServiceClient is the client API for RelationService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type RelationServiceClient interface {
	GetRelations(ctx context.Context, in *GetRelationsRequest, opts ...grpc.CallOption) (*GetRelationsResponse, error)
	GetMasterRelations(ctx context.Context, in *GetRelationsRequest, opts ...grpc.CallOption) (*GetMasterRelationsResponse, error)
	RunInit(ctx context.Context, in *RunInitRequest, opts ...grpc.CallOption) (*RunInitResponse, error)
}

type relationServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewRelationServiceClient(cc grpc.ClientConnInterface) RelationServiceClient {
	return &relationServiceClient{cc}
}

func (c *relationServiceClient) GetRelations(ctx context.Context, in *GetRelationsRequest, opts ...grpc.CallOption) (*GetRelationsResponse, error) {
	out := new(GetRelationsResponse)
	err := c.cc.Invoke(ctx, "/maincon.micro.RelationService/GetRelations", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *relationServiceClient) GetMasterRelations(ctx context.Context, in *GetRelationsRequest, opts ...grpc.CallOption) (*GetMasterRelationsResponse, error) {
	out := new(GetMasterRelationsResponse)
	err := c.cc.Invoke(ctx, "/maincon.micro.RelationService/GetMasterRelations", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *relationServiceClient) RunInit(ctx context.Context, in *RunInitRequest, opts ...grpc.CallOption) (*RunInitResponse, error) {
	out := new(RunInitResponse)
	err := c.cc.Invoke(ctx, "/maincon.micro.RelationService/RunInit", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// RelationServiceServer is the server API for RelationService service.
// All implementations must embed UnimplementedRelationServiceServer
// for forward compatibility
type RelationServiceServer interface {
	GetRelations(context.Context, *GetRelationsRequest) (*GetRelationsResponse, error)
	GetMasterRelations(context.Context, *GetRelationsRequest) (*GetMasterRelationsResponse, error)
	RunInit(context.Context, *RunInitRequest) (*RunInitResponse, error)
	mustEmbedUnimplementedRelationServiceServer()
}

// UnimplementedRelationServiceServer must be embedded to have forward compatible implementations.
type UnimplementedRelationServiceServer struct {
}

func (UnimplementedRelationServiceServer) GetRelations(context.Context, *GetRelationsRequest) (*GetRelationsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetRelations not implemented")
}
func (UnimplementedRelationServiceServer) GetMasterRelations(context.Context, *GetRelationsRequest) (*GetMasterRelationsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetMasterRelations not implemented")
}
func (UnimplementedRelationServiceServer) RunInit(context.Context, *RunInitRequest) (*RunInitResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method RunInit not implemented")
}
func (UnimplementedRelationServiceServer) mustEmbedUnimplementedRelationServiceServer() {}

// UnsafeRelationServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to RelationServiceServer will
// result in compilation errors.
type UnsafeRelationServiceServer interface {
	mustEmbedUnimplementedRelationServiceServer()
}

func RegisterRelationServiceServer(s grpc.ServiceRegistrar, srv RelationServiceServer) {
	s.RegisterService(&_RelationService_serviceDesc, srv)
}

func _RelationService_GetRelations_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetRelationsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RelationServiceServer).GetRelations(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.micro.RelationService/GetRelations",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RelationServiceServer).GetRelations(ctx, req.(*GetRelationsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RelationService_GetMasterRelations_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetRelationsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RelationServiceServer).GetMasterRelations(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.micro.RelationService/GetMasterRelations",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RelationServiceServer).GetMasterRelations(ctx, req.(*GetRelationsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RelationService_RunInit_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RunInitRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RelationServiceServer).RunInit(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.micro.RelationService/RunInit",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RelationServiceServer).RunInit(ctx, req.(*RunInitRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _RelationService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "maincon.micro.RelationService",
	HandlerType: (*RelationServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetRelations",
			Handler:    _RelationService_GetRelations_Handler,
		},
		{
			MethodName: "GetMasterRelations",
			Handler:    _RelationService_GetMasterRelations_Handler,
		},
		{
			MethodName: "RunInit",
			Handler:    _RelationService_RunInit_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "maincon/micro/relation/relation.proto",
}
