syntax = "proto3";

package supplier;

option go_package = "supplier/documentpb";

import "common/common.proto";
import "google/protobuf/timestamp.proto";
import "supplier/document/document_common.proto";

service DeliveryNoteService {
  rpc ReadDeliveryNote (ReadDeliveryNoteRequest) returns (ReadDeliveryNoteResponse);
  rpc CreateDeliveryNote (CreateDeliveryNoteRequest) returns (CreateDeliveryNoteResponse);
  rpc UpdateDeliveryNote (UpdateDeliveryNoteRequest) returns (SimpleResponse);
  rpc CancelDeliveryNote(CancelDeliveryNoteRequest) returns (SimpleResponse);
  rpc UpdateDeliveryNoteItem (UpdateDeliveryNoteItemRequest) returns (SimpleResponse);
  rpc ListDeliveryNote (ListDeliveryNoteRequest) returns (ListDeliveryNoteResponse);
  rpc ListDeliveryNoteItem (ListDeliveryNoteItemRequest) returns (ListDeliveryNoteItemResponse);
  rpc GetDeliveryNotePDFData (GetDeliveryNotePDFDataRequest) returns (GetDeliveryNotePDFDataResponse);
  rpc GetDeliveryNotePDFFileNameAndPath(GetDeliveryNotePDFDataRequest) returns (GetDeliveryNotePDFFileNameAndPathResponse);
  rpc GetSupplierLogoForDN (GetSupplierLogoForDNRequest) returns (GetSupplierLogoForDNResponse);

  rpc NotifySupplierDNConfirmReceive(NotifySupplierDNConfirmReceiveRequest) returns (NotifySupplierDNConfirmReceiveResponse);
  rpc CreateDeliveryNoteByTms (CreateDeliveryNoteByTmsRequest) returns (CreateDeliveryNoteByTmsResponse);
  rpc UpdateDeliveryNoteByTms (UpdateDeliveryNoteByTmsRequest) returns (UpdateDeliveryNoteByTmsResponse);
  rpc SubmitDeliveryNoteToTms (SubmitDeliveryNoteToTmsRequest) returns (SimpleResponse);
  rpc GetDocumentPdfByTms(GetDocumentPdfByTmsRequest) returns (GetDocumentPdfByTmsResponse);
  rpc CancelDeliveryNoteByTmsImdr(CancelDeliveryNoteByTmsImdrRequest) returns (SimpleResponse);
  rpc ReceiveDNReceiptFromTms(ReceiveDNReceiptFromTmsRequest) returns (SimpleResponse);
  rpc LockDeliveryNote (LockDeliveryNoteRequest) returns (SimpleResponse);
  rpc UnlockDeliveryNote (UnlockDeliveryNoteRequest) returns (SimpleResponse);
  rpc UpdateDeliveryNoteInfoByTms(UpdateDeliveryNoteInfoByTmsRequest) returns (SimpleResponse);
}

message DeliveryNote {
  int64 client_id = 30;
  int64 contractor_id = 31;
  int64 id = 1;
  string document_no = 2;
  int64 tms_so_id = 3;
  string tms_so_doc_no = 4;
  int64 tms_po_id = 5;
  string tms_po_doc_no = 6;
  int64 project_id = 7;
  string project_name = 8;
  google.protobuf.Timestamp delivery_date = 9;
  string delivery_address = 10;
  int64 supplier_id = 11;
  int64 receiver_id = 12;
  string receiver_name = 13;
  string receiver_contact_name = 14;
  string receiver_contact_tel = 15;
  repeated DeliveryNoteItem delivery_note_items = 16;
  int32 status = 17;
  string status_label = 18;
  string remark = 19;
  common.Audit audit = 20;
  DocumentButtons action_buttons = 21;
  string model_type = 22;
  int32 bit = 23;
  int64 parent_delivery_note_id = 24;
  string parent_delivery_note_no = 25;
  string pdf = 26;
  string receipt_pdf = 27;
  int64 tms_imdr_id = 28;
  string tms_imdr_doc_no = 29;
  int32 note_count = 32;
  string remark_collection = 33;
  bool locked = 34;
  DocumentRemarkResource display_remark_collection = 35;
  string buyer_pr_number = 36;
  int64 tms_dn_id = 37;
  string tms_dn_doc_no = 38;
}

message DeliveryNoteItem {
  int64 id = 1;
  int64 delivery_note_id = 2;
  int64 tms_imdr_item_id = 3;
  int64 tms_po_item_id = 4;
  bool is_extra_item = 5;
  int32 line_no = 6;
  int32 parent_line_no = 7;
  bool is_product_group = 8;
  string item_code = 9;
  string description = 10;
  double qty = 11;
  double outstanding_qty = 12;
  double actual_received_qty = 13;
  string unit = 14;
  int64 unit_id = 15;
  int32 sort = 16;
  DeliveryNoteItemButtons item_buttons = 17;
  common.Audit audit = 18;
  string buyer_pr_number = 19;

  repeated DeliveryNoteItem children = 20;
  string remark = 21;
}

message DeliveryNoteItemButtons {
  bool edit_item_button = 1;
}


message CreateDeliveryNoteRequest{
  DeliveryNote delivery_note = 1;
  common.Language lang = 2;
}

message CreateDeliveryNoteResponse {
  DeliveryNote delivery_note = 1;
}

message ListDeliveryNoteRequest {
  string receiver_name = 1;
  string document_no = 2;
  int32 status = 3;
  int64 tms_imdr_id = 4;
  int64 tms_po_id = 5;
  int64 tms_so_id = 6;
  int64 project_id = 7;
  string parent_document_no = 8;
  string tms_so_doc_no = 9;
  string tms_po_doc_no = 10;
  common.PaginationRequest pagination_request = 11;
  common.Language lang = 12;
}

message ListDeliveryNoteResponse {
  repeated DeliveryNote data = 1;
  common.MataPaginator meta = 2;
}

message CreateDeliveryNoteByTmsRequest {
  repeated DeliveryNote delivery_notes = 1;
  common.Language lang = 2;
  string mdr_doc_no = 5;
}

message CreateDeliveryNoteByTmsResponse {
  repeated DeliveryNote delivery_notes = 1;
}

message UpdateDeliveryNoteByTmsRequest {
  DeliveryNote delivery_note = 1;

  common.Language lang = 2;
}

message UpdateDeliveryNoteByTmsResponse {
  DeliveryNote delivery_note = 1;
}

message ReadDeliveryNoteRequest {
  int64 id = 1;
  common.Language lang = 2;
}

message ReadDeliveryNoteResponse {
  DeliveryNote data = 1;
}

message ListDeliveryNoteItemRequest {
  int64 delivery_note_id = 1;
  common.PaginationRequest pagination_request = 2;
  common.Language lang = 3;
}

message ListDeliveryNoteItemResponse {
  repeated DeliveryNoteItem data = 1;
  common.MataPaginator meta = 2;
}

message UpdateDeliveryNoteRequest {
  int64 id = 1;
  string delivery_address = 2;
  google.protobuf.Timestamp delivery_date = 3;
  string receiver_name = 4;
  string receiver_contact_name = 5;
  string receiver_contact_tel = 6;
}

message CancelDeliveryNoteRequest {
  int64 id = 1;
  string note = 2;
  string pdf_encoded_file = 3;
  string pdf_url = 4;

  common.Language lang = 5;
}


message UpdateDeliveryNoteItemRequest {
  int64 id = 1;
  double qty = 2;
}

message GetDeliveryNotePDFDataRequest {
  int64 id = 1;
  common.Language lang = 2;
}

message GetDeliveryNotePDFDataResponse {
  DeliveryNote delivery_note = 1;
  DocumentPDFCommonData document_data = 2;
}

message GetDeliveryNotePDFFileNameAndPathResponse {
  string file_name = 1;
  string file_path = 2;
}

message SubmitDeliveryNoteToTmsRequest {
  int64 id = 1;
  common.Language lang = 2;
}


message GetDocumentPdfByTmsRequest {
  int64 dnId = 1;
  int64 tms_model_id = 2;
  string tms_model_type = 3;
  common.Language lang = 4;
}

message GetDocumentPdfByTmsResponse {
  string signed_url = 1;
}

message NotifySupplierDNConfirmReceiveRequest {
  int64 dn_id = 1;
  SyncDocumentProcessType document_process_type = 2;
  int64 updated_by = 3;
  repeated ConfirmReceivedItem items = 4;
  common.Language lang = 5;
}

message NotifySupplierDNConfirmReceiveResponse {
  bool success = 1;
  SyncDocumentProcessType document_process_type = 2;
}

message ConfirmReceivedItem {
  int64 id = 1;
  double actual_received_qty = 2;
  repeated ConfirmReceivedItem children = 3;
}

message ReceiveDNReceiptFromTmsRequest {
  int64 id = 1;
  string pdf_url = 2;
  string pdf_encoded_file = 3;
}

message GetSupplierLogoForDNRequest {
  int64 dn_id = 1;
  common.Language lang = 2;
}

message GetSupplierLogoForDNResponse {
  string logo = 1;
  string logo_type = 2;
}

message CancelDeliveryNoteByTmsImdrRequest {
  int64 tms_imdr_id = 1;
  string note = 2;
  common.Language lang = 3;
}


message LockDeliveryNoteRequest {
  int64 id = 1;
  common.Language lang = 2;
}

message UnlockDeliveryNoteRequest {
  int64 id = 1;

  common.Language lang = 2;
}

message UpdateDeliveryNoteInfoByTmsRequest {
    int64 supplier_dn_id = 1;
    int64 tms_dn_id = 3;
    string tms_dn_doc_no = 4;
    common.Language lang = 5;
}