// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package submissionpb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// DocumentServiceClient is the client API for DocumentService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type DocumentServiceClient interface {
	// approval flow related function
	GetDocumentApprovalFlowValidation(ctx context.Context, in *FlowValidationRequest, opts ...grpc.CallOption) (*FlowValidationResponse, error)
	ApplyToCurrentDocuments(ctx context.Context, in *FlowValidationRequest, opts ...grpc.CallOption) (*SimpleResponse, error)
	// document type related function
	GetDocumentTypes(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*GetDocumentTypesResponse, error)
	GetApprovalOptions(ctx context.Context, in *GetApprovalOptionsRequest, opts ...grpc.CallOption) (*GetApprovalOptionsResponse, error)
	AsyncDocumentFlow(ctx context.Context, in *AsyncDocumentFlowRequest, opts ...grpc.CallOption) (*SimpleResponse, error)
	RemoveDocumentFlow(ctx context.Context, in *RemoveDocumentFlowRequest, opts ...grpc.CallOption) (*SimpleResponse, error)
	ReassignmentNotify(ctx context.Context, in *ReassignmentNotifyRequest, opts ...grpc.CallOption) (*SimpleResponse, error)
	GetAwaitingApprovalDocuments(ctx context.Context, in *GetAwaitingApprovalDocumentsRequest, opts ...grpc.CallOption) (*GetAwaitingApprovalDocumentsResponse, error)
	ActionAfterDocumentUpdateApprovalFlow(ctx context.Context, in *ActionAfterDocumentUpdateApprovalFlowRequest, opts ...grpc.CallOption) (*SimpleResponse, error)
	SendNoteNotificationEmail(ctx context.Context, in *SendNoteNotificationEmailRequest, opts ...grpc.CallOption) (*SimpleResponse, error)
}

type documentServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewDocumentServiceClient(cc grpc.ClientConnInterface) DocumentServiceClient {
	return &documentServiceClient{cc}
}

func (c *documentServiceClient) GetDocumentApprovalFlowValidation(ctx context.Context, in *FlowValidationRequest, opts ...grpc.CallOption) (*FlowValidationResponse, error) {
	out := new(FlowValidationResponse)
	err := c.cc.Invoke(ctx, "/maincon.mmm.submission.DocumentService/GetDocumentApprovalFlowValidation", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *documentServiceClient) ApplyToCurrentDocuments(ctx context.Context, in *FlowValidationRequest, opts ...grpc.CallOption) (*SimpleResponse, error) {
	out := new(SimpleResponse)
	err := c.cc.Invoke(ctx, "/maincon.mmm.submission.DocumentService/ApplyToCurrentDocuments", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *documentServiceClient) GetDocumentTypes(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*GetDocumentTypesResponse, error) {
	out := new(GetDocumentTypesResponse)
	err := c.cc.Invoke(ctx, "/maincon.mmm.submission.DocumentService/GetDocumentTypes", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *documentServiceClient) GetApprovalOptions(ctx context.Context, in *GetApprovalOptionsRequest, opts ...grpc.CallOption) (*GetApprovalOptionsResponse, error) {
	out := new(GetApprovalOptionsResponse)
	err := c.cc.Invoke(ctx, "/maincon.mmm.submission.DocumentService/GetApprovalOptions", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *documentServiceClient) AsyncDocumentFlow(ctx context.Context, in *AsyncDocumentFlowRequest, opts ...grpc.CallOption) (*SimpleResponse, error) {
	out := new(SimpleResponse)
	err := c.cc.Invoke(ctx, "/maincon.mmm.submission.DocumentService/AsyncDocumentFlow", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *documentServiceClient) RemoveDocumentFlow(ctx context.Context, in *RemoveDocumentFlowRequest, opts ...grpc.CallOption) (*SimpleResponse, error) {
	out := new(SimpleResponse)
	err := c.cc.Invoke(ctx, "/maincon.mmm.submission.DocumentService/RemoveDocumentFlow", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *documentServiceClient) ReassignmentNotify(ctx context.Context, in *ReassignmentNotifyRequest, opts ...grpc.CallOption) (*SimpleResponse, error) {
	out := new(SimpleResponse)
	err := c.cc.Invoke(ctx, "/maincon.mmm.submission.DocumentService/ReassignmentNotify", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *documentServiceClient) GetAwaitingApprovalDocuments(ctx context.Context, in *GetAwaitingApprovalDocumentsRequest, opts ...grpc.CallOption) (*GetAwaitingApprovalDocumentsResponse, error) {
	out := new(GetAwaitingApprovalDocumentsResponse)
	err := c.cc.Invoke(ctx, "/maincon.mmm.submission.DocumentService/GetAwaitingApprovalDocuments", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *documentServiceClient) ActionAfterDocumentUpdateApprovalFlow(ctx context.Context, in *ActionAfterDocumentUpdateApprovalFlowRequest, opts ...grpc.CallOption) (*SimpleResponse, error) {
	out := new(SimpleResponse)
	err := c.cc.Invoke(ctx, "/maincon.mmm.submission.DocumentService/ActionAfterDocumentUpdateApprovalFlow", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *documentServiceClient) SendNoteNotificationEmail(ctx context.Context, in *SendNoteNotificationEmailRequest, opts ...grpc.CallOption) (*SimpleResponse, error) {
	out := new(SimpleResponse)
	err := c.cc.Invoke(ctx, "/maincon.mmm.submission.DocumentService/SendNoteNotificationEmail", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// DocumentServiceServer is the server API for DocumentService service.
// All implementations must embed UnimplementedDocumentServiceServer
// for forward compatibility
type DocumentServiceServer interface {
	// approval flow related function
	GetDocumentApprovalFlowValidation(context.Context, *FlowValidationRequest) (*FlowValidationResponse, error)
	ApplyToCurrentDocuments(context.Context, *FlowValidationRequest) (*SimpleResponse, error)
	// document type related function
	GetDocumentTypes(context.Context, *emptypb.Empty) (*GetDocumentTypesResponse, error)
	GetApprovalOptions(context.Context, *GetApprovalOptionsRequest) (*GetApprovalOptionsResponse, error)
	AsyncDocumentFlow(context.Context, *AsyncDocumentFlowRequest) (*SimpleResponse, error)
	RemoveDocumentFlow(context.Context, *RemoveDocumentFlowRequest) (*SimpleResponse, error)
	ReassignmentNotify(context.Context, *ReassignmentNotifyRequest) (*SimpleResponse, error)
	GetAwaitingApprovalDocuments(context.Context, *GetAwaitingApprovalDocumentsRequest) (*GetAwaitingApprovalDocumentsResponse, error)
	ActionAfterDocumentUpdateApprovalFlow(context.Context, *ActionAfterDocumentUpdateApprovalFlowRequest) (*SimpleResponse, error)
	SendNoteNotificationEmail(context.Context, *SendNoteNotificationEmailRequest) (*SimpleResponse, error)
	mustEmbedUnimplementedDocumentServiceServer()
}

// UnimplementedDocumentServiceServer must be embedded to have forward compatible implementations.
type UnimplementedDocumentServiceServer struct {
}

func (UnimplementedDocumentServiceServer) GetDocumentApprovalFlowValidation(context.Context, *FlowValidationRequest) (*FlowValidationResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetDocumentApprovalFlowValidation not implemented")
}
func (UnimplementedDocumentServiceServer) ApplyToCurrentDocuments(context.Context, *FlowValidationRequest) (*SimpleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ApplyToCurrentDocuments not implemented")
}
func (UnimplementedDocumentServiceServer) GetDocumentTypes(context.Context, *emptypb.Empty) (*GetDocumentTypesResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetDocumentTypes not implemented")
}
func (UnimplementedDocumentServiceServer) GetApprovalOptions(context.Context, *GetApprovalOptionsRequest) (*GetApprovalOptionsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetApprovalOptions not implemented")
}
func (UnimplementedDocumentServiceServer) AsyncDocumentFlow(context.Context, *AsyncDocumentFlowRequest) (*SimpleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AsyncDocumentFlow not implemented")
}
func (UnimplementedDocumentServiceServer) RemoveDocumentFlow(context.Context, *RemoveDocumentFlowRequest) (*SimpleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method RemoveDocumentFlow not implemented")
}
func (UnimplementedDocumentServiceServer) ReassignmentNotify(context.Context, *ReassignmentNotifyRequest) (*SimpleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ReassignmentNotify not implemented")
}
func (UnimplementedDocumentServiceServer) GetAwaitingApprovalDocuments(context.Context, *GetAwaitingApprovalDocumentsRequest) (*GetAwaitingApprovalDocumentsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetAwaitingApprovalDocuments not implemented")
}
func (UnimplementedDocumentServiceServer) ActionAfterDocumentUpdateApprovalFlow(context.Context, *ActionAfterDocumentUpdateApprovalFlowRequest) (*SimpleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ActionAfterDocumentUpdateApprovalFlow not implemented")
}
func (UnimplementedDocumentServiceServer) SendNoteNotificationEmail(context.Context, *SendNoteNotificationEmailRequest) (*SimpleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SendNoteNotificationEmail not implemented")
}
func (UnimplementedDocumentServiceServer) mustEmbedUnimplementedDocumentServiceServer() {}

// UnsafeDocumentServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to DocumentServiceServer will
// result in compilation errors.
type UnsafeDocumentServiceServer interface {
	mustEmbedUnimplementedDocumentServiceServer()
}

func RegisterDocumentServiceServer(s grpc.ServiceRegistrar, srv DocumentServiceServer) {
	s.RegisterService(&_DocumentService_serviceDesc, srv)
}

func _DocumentService_GetDocumentApprovalFlowValidation_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(FlowValidationRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DocumentServiceServer).GetDocumentApprovalFlowValidation(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.mmm.submission.DocumentService/GetDocumentApprovalFlowValidation",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DocumentServiceServer).GetDocumentApprovalFlowValidation(ctx, req.(*FlowValidationRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DocumentService_ApplyToCurrentDocuments_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(FlowValidationRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DocumentServiceServer).ApplyToCurrentDocuments(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.mmm.submission.DocumentService/ApplyToCurrentDocuments",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DocumentServiceServer).ApplyToCurrentDocuments(ctx, req.(*FlowValidationRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DocumentService_GetDocumentTypes_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(emptypb.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DocumentServiceServer).GetDocumentTypes(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.mmm.submission.DocumentService/GetDocumentTypes",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DocumentServiceServer).GetDocumentTypes(ctx, req.(*emptypb.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _DocumentService_GetApprovalOptions_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetApprovalOptionsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DocumentServiceServer).GetApprovalOptions(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.mmm.submission.DocumentService/GetApprovalOptions",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DocumentServiceServer).GetApprovalOptions(ctx, req.(*GetApprovalOptionsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DocumentService_AsyncDocumentFlow_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AsyncDocumentFlowRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DocumentServiceServer).AsyncDocumentFlow(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.mmm.submission.DocumentService/AsyncDocumentFlow",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DocumentServiceServer).AsyncDocumentFlow(ctx, req.(*AsyncDocumentFlowRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DocumentService_RemoveDocumentFlow_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RemoveDocumentFlowRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DocumentServiceServer).RemoveDocumentFlow(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.mmm.submission.DocumentService/RemoveDocumentFlow",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DocumentServiceServer).RemoveDocumentFlow(ctx, req.(*RemoveDocumentFlowRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DocumentService_ReassignmentNotify_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ReassignmentNotifyRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DocumentServiceServer).ReassignmentNotify(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.mmm.submission.DocumentService/ReassignmentNotify",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DocumentServiceServer).ReassignmentNotify(ctx, req.(*ReassignmentNotifyRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DocumentService_GetAwaitingApprovalDocuments_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetAwaitingApprovalDocumentsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DocumentServiceServer).GetAwaitingApprovalDocuments(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.mmm.submission.DocumentService/GetAwaitingApprovalDocuments",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DocumentServiceServer).GetAwaitingApprovalDocuments(ctx, req.(*GetAwaitingApprovalDocumentsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DocumentService_ActionAfterDocumentUpdateApprovalFlow_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ActionAfterDocumentUpdateApprovalFlowRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DocumentServiceServer).ActionAfterDocumentUpdateApprovalFlow(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.mmm.submission.DocumentService/ActionAfterDocumentUpdateApprovalFlow",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DocumentServiceServer).ActionAfterDocumentUpdateApprovalFlow(ctx, req.(*ActionAfterDocumentUpdateApprovalFlowRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DocumentService_SendNoteNotificationEmail_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SendNoteNotificationEmailRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DocumentServiceServer).SendNoteNotificationEmail(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.mmm.submission.DocumentService/SendNoteNotificationEmail",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DocumentServiceServer).SendNoteNotificationEmail(ctx, req.(*SendNoteNotificationEmailRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _DocumentService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "maincon.mmm.submission.DocumentService",
	HandlerType: (*DocumentServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetDocumentApprovalFlowValidation",
			Handler:    _DocumentService_GetDocumentApprovalFlowValidation_Handler,
		},
		{
			MethodName: "ApplyToCurrentDocuments",
			Handler:    _DocumentService_ApplyToCurrentDocuments_Handler,
		},
		{
			MethodName: "GetDocumentTypes",
			Handler:    _DocumentService_GetDocumentTypes_Handler,
		},
		{
			MethodName: "GetApprovalOptions",
			Handler:    _DocumentService_GetApprovalOptions_Handler,
		},
		{
			MethodName: "AsyncDocumentFlow",
			Handler:    _DocumentService_AsyncDocumentFlow_Handler,
		},
		{
			MethodName: "RemoveDocumentFlow",
			Handler:    _DocumentService_RemoveDocumentFlow_Handler,
		},
		{
			MethodName: "ReassignmentNotify",
			Handler:    _DocumentService_ReassignmentNotify_Handler,
		},
		{
			MethodName: "GetAwaitingApprovalDocuments",
			Handler:    _DocumentService_GetAwaitingApprovalDocuments_Handler,
		},
		{
			MethodName: "ActionAfterDocumentUpdateApprovalFlow",
			Handler:    _DocumentService_ActionAfterDocumentUpdateApprovalFlow_Handler,
		},
		{
			MethodName: "SendNoteNotificationEmail",
			Handler:    _DocumentService_SendNoteNotificationEmail_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "maincon/mmm/submission/document.proto",
}
