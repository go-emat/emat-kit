// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package settingpb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// FileServiceClient is the client API for FileService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type FileServiceClient interface {
	ListFile(ctx context.Context, in *ListFileRequest, opts ...grpc.CallOption) (*ListFileResponse, error)
	ReadFile(ctx context.Context, in *ReadFileRequest, opts ...grpc.CallOption) (*ReadFileResponse, error)
	SortFile(ctx context.Context, in *SortFileRequest, opts ...grpc.CallOption) (*UpdateFileResponse, error)
	UpdateFile(ctx context.Context, in *UpdateFileRequest, opts ...grpc.CallOption) (*UpdateFileResponse, error)
	DeleteFile(ctx context.Context, in *DeleteFileRequest, opts ...grpc.CallOption) (*DeleteFileResponse, error)
	GenerateUploadFilesUrl(ctx context.Context, in *GenerateUploadFilesUrlRequest, opts ...grpc.CallOption) (*GenerateUploadFilesUrlResponse, error)
	CompleteUploadFilesUrl(ctx context.Context, in *CompleteUploadFilesUrlRequest, opts ...grpc.CallOption) (*CompleteUploadFilesUrlResponse, error)
	// UploadFile is just for demo purpose, please use EmatUploadFile
	UploadFile(ctx context.Context, opts ...grpc.CallOption) (FileService_UploadFileClient, error)
	EmatUploadFile(ctx context.Context, opts ...grpc.CallOption) (FileService_EmatUploadFileClient, error)
	UpdateFileInfo(ctx context.Context, in *UpdateFileInfoRequest, opts ...grpc.CallOption) (*UpdateFileInfoResponse, error)
	GenerateCloudStorageFileLink(ctx context.Context, in *GenerateCloudStorageFileLinkRequest, opts ...grpc.CallOption) (*GenerateCloudStorageFileLinkResponse, error)
}

type fileServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewFileServiceClient(cc grpc.ClientConnInterface) FileServiceClient {
	return &fileServiceClient{cc}
}

func (c *fileServiceClient) ListFile(ctx context.Context, in *ListFileRequest, opts ...grpc.CallOption) (*ListFileResponse, error) {
	out := new(ListFileResponse)
	err := c.cc.Invoke(ctx, "/reviewer.FileService/ListFile", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fileServiceClient) ReadFile(ctx context.Context, in *ReadFileRequest, opts ...grpc.CallOption) (*ReadFileResponse, error) {
	out := new(ReadFileResponse)
	err := c.cc.Invoke(ctx, "/reviewer.FileService/ReadFile", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fileServiceClient) SortFile(ctx context.Context, in *SortFileRequest, opts ...grpc.CallOption) (*UpdateFileResponse, error) {
	out := new(UpdateFileResponse)
	err := c.cc.Invoke(ctx, "/reviewer.FileService/SortFile", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fileServiceClient) UpdateFile(ctx context.Context, in *UpdateFileRequest, opts ...grpc.CallOption) (*UpdateFileResponse, error) {
	out := new(UpdateFileResponse)
	err := c.cc.Invoke(ctx, "/reviewer.FileService/UpdateFile", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fileServiceClient) DeleteFile(ctx context.Context, in *DeleteFileRequest, opts ...grpc.CallOption) (*DeleteFileResponse, error) {
	out := new(DeleteFileResponse)
	err := c.cc.Invoke(ctx, "/reviewer.FileService/DeleteFile", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fileServiceClient) GenerateUploadFilesUrl(ctx context.Context, in *GenerateUploadFilesUrlRequest, opts ...grpc.CallOption) (*GenerateUploadFilesUrlResponse, error) {
	out := new(GenerateUploadFilesUrlResponse)
	err := c.cc.Invoke(ctx, "/reviewer.FileService/GenerateUploadFilesUrl", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fileServiceClient) CompleteUploadFilesUrl(ctx context.Context, in *CompleteUploadFilesUrlRequest, opts ...grpc.CallOption) (*CompleteUploadFilesUrlResponse, error) {
	out := new(CompleteUploadFilesUrlResponse)
	err := c.cc.Invoke(ctx, "/reviewer.FileService/CompleteUploadFilesUrl", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fileServiceClient) UploadFile(ctx context.Context, opts ...grpc.CallOption) (FileService_UploadFileClient, error) {
	stream, err := c.cc.NewStream(ctx, &_FileService_serviceDesc.Streams[0], "/reviewer.FileService/UploadFile", opts...)
	if err != nil {
		return nil, err
	}
	x := &fileServiceUploadFileClient{stream}
	return x, nil
}

type FileService_UploadFileClient interface {
	Send(*UploadStreamRequest) error
	CloseAndRecv() (*UploadStreamResponse, error)
	grpc.ClientStream
}

type fileServiceUploadFileClient struct {
	grpc.ClientStream
}

func (x *fileServiceUploadFileClient) Send(m *UploadStreamRequest) error {
	return x.ClientStream.SendMsg(m)
}

func (x *fileServiceUploadFileClient) CloseAndRecv() (*UploadStreamResponse, error) {
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	m := new(UploadStreamResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *fileServiceClient) EmatUploadFile(ctx context.Context, opts ...grpc.CallOption) (FileService_EmatUploadFileClient, error) {
	stream, err := c.cc.NewStream(ctx, &_FileService_serviceDesc.Streams[1], "/reviewer.FileService/EmatUploadFile", opts...)
	if err != nil {
		return nil, err
	}
	x := &fileServiceEmatUploadFileClient{stream}
	return x, nil
}

type FileService_EmatUploadFileClient interface {
	Send(*UploadStreamRequest) error
	CloseAndRecv() (*UploadStreamResponse, error)
	grpc.ClientStream
}

type fileServiceEmatUploadFileClient struct {
	grpc.ClientStream
}

func (x *fileServiceEmatUploadFileClient) Send(m *UploadStreamRequest) error {
	return x.ClientStream.SendMsg(m)
}

func (x *fileServiceEmatUploadFileClient) CloseAndRecv() (*UploadStreamResponse, error) {
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	m := new(UploadStreamResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *fileServiceClient) UpdateFileInfo(ctx context.Context, in *UpdateFileInfoRequest, opts ...grpc.CallOption) (*UpdateFileInfoResponse, error) {
	out := new(UpdateFileInfoResponse)
	err := c.cc.Invoke(ctx, "/reviewer.FileService/UpdateFileInfo", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fileServiceClient) GenerateCloudStorageFileLink(ctx context.Context, in *GenerateCloudStorageFileLinkRequest, opts ...grpc.CallOption) (*GenerateCloudStorageFileLinkResponse, error) {
	out := new(GenerateCloudStorageFileLinkResponse)
	err := c.cc.Invoke(ctx, "/reviewer.FileService/GenerateCloudStorageFileLink", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// FileServiceServer is the server API for FileService service.
// All implementations must embed UnimplementedFileServiceServer
// for forward compatibility
type FileServiceServer interface {
	ListFile(context.Context, *ListFileRequest) (*ListFileResponse, error)
	ReadFile(context.Context, *ReadFileRequest) (*ReadFileResponse, error)
	SortFile(context.Context, *SortFileRequest) (*UpdateFileResponse, error)
	UpdateFile(context.Context, *UpdateFileRequest) (*UpdateFileResponse, error)
	DeleteFile(context.Context, *DeleteFileRequest) (*DeleteFileResponse, error)
	GenerateUploadFilesUrl(context.Context, *GenerateUploadFilesUrlRequest) (*GenerateUploadFilesUrlResponse, error)
	CompleteUploadFilesUrl(context.Context, *CompleteUploadFilesUrlRequest) (*CompleteUploadFilesUrlResponse, error)
	// UploadFile is just for demo purpose, please use EmatUploadFile
	UploadFile(FileService_UploadFileServer) error
	EmatUploadFile(FileService_EmatUploadFileServer) error
	UpdateFileInfo(context.Context, *UpdateFileInfoRequest) (*UpdateFileInfoResponse, error)
	GenerateCloudStorageFileLink(context.Context, *GenerateCloudStorageFileLinkRequest) (*GenerateCloudStorageFileLinkResponse, error)
	mustEmbedUnimplementedFileServiceServer()
}

// UnimplementedFileServiceServer must be embedded to have forward compatible implementations.
type UnimplementedFileServiceServer struct {
}

func (UnimplementedFileServiceServer) ListFile(context.Context, *ListFileRequest) (*ListFileResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListFile not implemented")
}
func (UnimplementedFileServiceServer) ReadFile(context.Context, *ReadFileRequest) (*ReadFileResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ReadFile not implemented")
}
func (UnimplementedFileServiceServer) SortFile(context.Context, *SortFileRequest) (*UpdateFileResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SortFile not implemented")
}
func (UnimplementedFileServiceServer) UpdateFile(context.Context, *UpdateFileRequest) (*UpdateFileResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateFile not implemented")
}
func (UnimplementedFileServiceServer) DeleteFile(context.Context, *DeleteFileRequest) (*DeleteFileResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteFile not implemented")
}
func (UnimplementedFileServiceServer) GenerateUploadFilesUrl(context.Context, *GenerateUploadFilesUrlRequest) (*GenerateUploadFilesUrlResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GenerateUploadFilesUrl not implemented")
}
func (UnimplementedFileServiceServer) CompleteUploadFilesUrl(context.Context, *CompleteUploadFilesUrlRequest) (*CompleteUploadFilesUrlResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CompleteUploadFilesUrl not implemented")
}
func (UnimplementedFileServiceServer) UploadFile(FileService_UploadFileServer) error {
	return status.Errorf(codes.Unimplemented, "method UploadFile not implemented")
}
func (UnimplementedFileServiceServer) EmatUploadFile(FileService_EmatUploadFileServer) error {
	return status.Errorf(codes.Unimplemented, "method EmatUploadFile not implemented")
}
func (UnimplementedFileServiceServer) UpdateFileInfo(context.Context, *UpdateFileInfoRequest) (*UpdateFileInfoResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateFileInfo not implemented")
}
func (UnimplementedFileServiceServer) GenerateCloudStorageFileLink(context.Context, *GenerateCloudStorageFileLinkRequest) (*GenerateCloudStorageFileLinkResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GenerateCloudStorageFileLink not implemented")
}
func (UnimplementedFileServiceServer) mustEmbedUnimplementedFileServiceServer() {}

// UnsafeFileServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to FileServiceServer will
// result in compilation errors.
type UnsafeFileServiceServer interface {
	mustEmbedUnimplementedFileServiceServer()
}

func RegisterFileServiceServer(s grpc.ServiceRegistrar, srv FileServiceServer) {
	s.RegisterService(&_FileService_serviceDesc, srv)
}

func _FileService_ListFile_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListFileRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FileServiceServer).ListFile(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.FileService/ListFile",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FileServiceServer).ListFile(ctx, req.(*ListFileRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _FileService_ReadFile_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ReadFileRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FileServiceServer).ReadFile(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.FileService/ReadFile",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FileServiceServer).ReadFile(ctx, req.(*ReadFileRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _FileService_SortFile_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SortFileRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FileServiceServer).SortFile(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.FileService/SortFile",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FileServiceServer).SortFile(ctx, req.(*SortFileRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _FileService_UpdateFile_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateFileRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FileServiceServer).UpdateFile(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.FileService/UpdateFile",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FileServiceServer).UpdateFile(ctx, req.(*UpdateFileRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _FileService_DeleteFile_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteFileRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FileServiceServer).DeleteFile(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.FileService/DeleteFile",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FileServiceServer).DeleteFile(ctx, req.(*DeleteFileRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _FileService_GenerateUploadFilesUrl_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GenerateUploadFilesUrlRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FileServiceServer).GenerateUploadFilesUrl(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.FileService/GenerateUploadFilesUrl",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FileServiceServer).GenerateUploadFilesUrl(ctx, req.(*GenerateUploadFilesUrlRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _FileService_CompleteUploadFilesUrl_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CompleteUploadFilesUrlRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FileServiceServer).CompleteUploadFilesUrl(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.FileService/CompleteUploadFilesUrl",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FileServiceServer).CompleteUploadFilesUrl(ctx, req.(*CompleteUploadFilesUrlRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _FileService_UploadFile_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(FileServiceServer).UploadFile(&fileServiceUploadFileServer{stream})
}

type FileService_UploadFileServer interface {
	SendAndClose(*UploadStreamResponse) error
	Recv() (*UploadStreamRequest, error)
	grpc.ServerStream
}

type fileServiceUploadFileServer struct {
	grpc.ServerStream
}

func (x *fileServiceUploadFileServer) SendAndClose(m *UploadStreamResponse) error {
	return x.ServerStream.SendMsg(m)
}

func (x *fileServiceUploadFileServer) Recv() (*UploadStreamRequest, error) {
	m := new(UploadStreamRequest)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func _FileService_EmatUploadFile_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(FileServiceServer).EmatUploadFile(&fileServiceEmatUploadFileServer{stream})
}

type FileService_EmatUploadFileServer interface {
	SendAndClose(*UploadStreamResponse) error
	Recv() (*UploadStreamRequest, error)
	grpc.ServerStream
}

type fileServiceEmatUploadFileServer struct {
	grpc.ServerStream
}

func (x *fileServiceEmatUploadFileServer) SendAndClose(m *UploadStreamResponse) error {
	return x.ServerStream.SendMsg(m)
}

func (x *fileServiceEmatUploadFileServer) Recv() (*UploadStreamRequest, error) {
	m := new(UploadStreamRequest)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func _FileService_UpdateFileInfo_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateFileInfoRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FileServiceServer).UpdateFileInfo(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.FileService/UpdateFileInfo",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FileServiceServer).UpdateFileInfo(ctx, req.(*UpdateFileInfoRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _FileService_GenerateCloudStorageFileLink_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GenerateCloudStorageFileLinkRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FileServiceServer).GenerateCloudStorageFileLink(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.FileService/GenerateCloudStorageFileLink",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FileServiceServer).GenerateCloudStorageFileLink(ctx, req.(*GenerateCloudStorageFileLinkRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _FileService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "reviewer.FileService",
	HandlerType: (*FileServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "ListFile",
			Handler:    _FileService_ListFile_Handler,
		},
		{
			MethodName: "ReadFile",
			Handler:    _FileService_ReadFile_Handler,
		},
		{
			MethodName: "SortFile",
			Handler:    _FileService_SortFile_Handler,
		},
		{
			MethodName: "UpdateFile",
			Handler:    _FileService_UpdateFile_Handler,
		},
		{
			MethodName: "DeleteFile",
			Handler:    _FileService_DeleteFile_Handler,
		},
		{
			MethodName: "GenerateUploadFilesUrl",
			Handler:    _FileService_GenerateUploadFilesUrl_Handler,
		},
		{
			MethodName: "CompleteUploadFilesUrl",
			Handler:    _FileService_CompleteUploadFilesUrl_Handler,
		},
		{
			MethodName: "UpdateFileInfo",
			Handler:    _FileService_UpdateFileInfo_Handler,
		},
		{
			MethodName: "GenerateCloudStorageFileLink",
			Handler:    _FileService_GenerateCloudStorageFileLink_Handler,
		},
	},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "UploadFile",
			Handler:       _FileService_UploadFile_Handler,
			ClientStreams: true,
		},
		{
			StreamName:    "EmatUploadFile",
			Handler:       _FileService_EmatUploadFile_Handler,
			ClientStreams: true,
		},
	},
	Metadata: "reviewer/setting/file.proto",
}
