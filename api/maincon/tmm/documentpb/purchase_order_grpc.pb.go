// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package documentpb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// PurchaseOrderServiceClient is the client API for PurchaseOrderService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type PurchaseOrderServiceClient interface {
	AcceptPurchaseOrder(ctx context.Context, in *AcceptPurchaseOrderRequest, opts ...grpc.CallOption) (*SimpleResponse, error)
	AcceptPurchaseContract(ctx context.Context, in *AcceptPurchaseContractRequest, opts ...grpc.CallOption) (*SimpleResponse, error)
	RejectPurchaseOrder(ctx context.Context, in *RejectPurchaseOrderRequest, opts ...grpc.CallOption) (*SimpleResponse, error)
	LockPurchaseOrderItems(ctx context.Context, in *LockPurchaseOrderItemRequest, opts ...grpc.CallOption) (*SimpleResponse, error)
}

type purchaseOrderServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewPurchaseOrderServiceClient(cc grpc.ClientConnInterface) PurchaseOrderServiceClient {
	return &purchaseOrderServiceClient{cc}
}

func (c *purchaseOrderServiceClient) AcceptPurchaseOrder(ctx context.Context, in *AcceptPurchaseOrderRequest, opts ...grpc.CallOption) (*SimpleResponse, error) {
	out := new(SimpleResponse)
	err := c.cc.Invoke(ctx, "/maincon.tmm.PurchaseOrderService/AcceptPurchaseOrder", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *purchaseOrderServiceClient) AcceptPurchaseContract(ctx context.Context, in *AcceptPurchaseContractRequest, opts ...grpc.CallOption) (*SimpleResponse, error) {
	out := new(SimpleResponse)
	err := c.cc.Invoke(ctx, "/maincon.tmm.PurchaseOrderService/AcceptPurchaseContract", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *purchaseOrderServiceClient) RejectPurchaseOrder(ctx context.Context, in *RejectPurchaseOrderRequest, opts ...grpc.CallOption) (*SimpleResponse, error) {
	out := new(SimpleResponse)
	err := c.cc.Invoke(ctx, "/maincon.tmm.PurchaseOrderService/RejectPurchaseOrder", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *purchaseOrderServiceClient) LockPurchaseOrderItems(ctx context.Context, in *LockPurchaseOrderItemRequest, opts ...grpc.CallOption) (*SimpleResponse, error) {
	out := new(SimpleResponse)
	err := c.cc.Invoke(ctx, "/maincon.tmm.PurchaseOrderService/LockPurchaseOrderItems", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// PurchaseOrderServiceServer is the server API for PurchaseOrderService service.
// All implementations must embed UnimplementedPurchaseOrderServiceServer
// for forward compatibility
type PurchaseOrderServiceServer interface {
	AcceptPurchaseOrder(context.Context, *AcceptPurchaseOrderRequest) (*SimpleResponse, error)
	AcceptPurchaseContract(context.Context, *AcceptPurchaseContractRequest) (*SimpleResponse, error)
	RejectPurchaseOrder(context.Context, *RejectPurchaseOrderRequest) (*SimpleResponse, error)
	LockPurchaseOrderItems(context.Context, *LockPurchaseOrderItemRequest) (*SimpleResponse, error)
	mustEmbedUnimplementedPurchaseOrderServiceServer()
}

// UnimplementedPurchaseOrderServiceServer must be embedded to have forward compatible implementations.
type UnimplementedPurchaseOrderServiceServer struct {
}

func (UnimplementedPurchaseOrderServiceServer) AcceptPurchaseOrder(context.Context, *AcceptPurchaseOrderRequest) (*SimpleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AcceptPurchaseOrder not implemented")
}
func (UnimplementedPurchaseOrderServiceServer) AcceptPurchaseContract(context.Context, *AcceptPurchaseContractRequest) (*SimpleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AcceptPurchaseContract not implemented")
}
func (UnimplementedPurchaseOrderServiceServer) RejectPurchaseOrder(context.Context, *RejectPurchaseOrderRequest) (*SimpleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method RejectPurchaseOrder not implemented")
}
func (UnimplementedPurchaseOrderServiceServer) LockPurchaseOrderItems(context.Context, *LockPurchaseOrderItemRequest) (*SimpleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method LockPurchaseOrderItems not implemented")
}
func (UnimplementedPurchaseOrderServiceServer) mustEmbedUnimplementedPurchaseOrderServiceServer() {}

// UnsafePurchaseOrderServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to PurchaseOrderServiceServer will
// result in compilation errors.
type UnsafePurchaseOrderServiceServer interface {
	mustEmbedUnimplementedPurchaseOrderServiceServer()
}

func RegisterPurchaseOrderServiceServer(s grpc.ServiceRegistrar, srv PurchaseOrderServiceServer) {
	s.RegisterService(&_PurchaseOrderService_serviceDesc, srv)
}

func _PurchaseOrderService_AcceptPurchaseOrder_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AcceptPurchaseOrderRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PurchaseOrderServiceServer).AcceptPurchaseOrder(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.tmm.PurchaseOrderService/AcceptPurchaseOrder",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PurchaseOrderServiceServer).AcceptPurchaseOrder(ctx, req.(*AcceptPurchaseOrderRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _PurchaseOrderService_AcceptPurchaseContract_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AcceptPurchaseContractRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PurchaseOrderServiceServer).AcceptPurchaseContract(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.tmm.PurchaseOrderService/AcceptPurchaseContract",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PurchaseOrderServiceServer).AcceptPurchaseContract(ctx, req.(*AcceptPurchaseContractRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _PurchaseOrderService_RejectPurchaseOrder_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RejectPurchaseOrderRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PurchaseOrderServiceServer).RejectPurchaseOrder(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.tmm.PurchaseOrderService/RejectPurchaseOrder",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PurchaseOrderServiceServer).RejectPurchaseOrder(ctx, req.(*RejectPurchaseOrderRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _PurchaseOrderService_LockPurchaseOrderItems_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(LockPurchaseOrderItemRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PurchaseOrderServiceServer).LockPurchaseOrderItems(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.tmm.PurchaseOrderService/LockPurchaseOrderItems",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PurchaseOrderServiceServer).LockPurchaseOrderItems(ctx, req.(*LockPurchaseOrderItemRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _PurchaseOrderService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "maincon.tmm.PurchaseOrderService",
	HandlerType: (*PurchaseOrderServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "AcceptPurchaseOrder",
			Handler:    _PurchaseOrderService_AcceptPurchaseOrder_Handler,
		},
		{
			MethodName: "AcceptPurchaseContract",
			Handler:    _PurchaseOrderService_AcceptPurchaseContract_Handler,
		},
		{
			MethodName: "RejectPurchaseOrder",
			Handler:    _PurchaseOrderService_RejectPurchaseOrder_Handler,
		},
		{
			MethodName: "LockPurchaseOrderItems",
			Handler:    _PurchaseOrderService_LockPurchaseOrderItems_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "maincon/tmm/document/purchase_order.proto",
}
