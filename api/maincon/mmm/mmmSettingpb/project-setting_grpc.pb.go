// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package mmmSettingpb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// ProjectSettingServiceClient is the client API for ProjectSettingService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type ProjectSettingServiceClient interface {
	ReadProjectSubmissionType(ctx context.Context, in *ReadProjectSubmissionTypeRequest, opts ...grpc.CallOption) (*ReadProjectSubmissionTypeResponse, error)
	UpdateProjectSubmissionType(ctx context.Context, in *UpdateProjectSubmissionTypeRequest, opts ...grpc.CallOption) (*UpdateProjectSubmissionTypeResponse, error)
	ReadProjectFileType(ctx context.Context, in *ReadProjectFileTypeRequest, opts ...grpc.CallOption) (*ReadProjectFileTypeResponse, error)
	UpdateProjectFileType(ctx context.Context, in *UpdateProjectFileTypeRequest, opts ...grpc.CallOption) (*UpdateProjectFileTypeResponse, error)
}

type projectSettingServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewProjectSettingServiceClient(cc grpc.ClientConnInterface) ProjectSettingServiceClient {
	return &projectSettingServiceClient{cc}
}

func (c *projectSettingServiceClient) ReadProjectSubmissionType(ctx context.Context, in *ReadProjectSubmissionTypeRequest, opts ...grpc.CallOption) (*ReadProjectSubmissionTypeResponse, error) {
	out := new(ReadProjectSubmissionTypeResponse)
	err := c.cc.Invoke(ctx, "/maincon.mmm.setting.ProjectSettingService/ReadProjectSubmissionType", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *projectSettingServiceClient) UpdateProjectSubmissionType(ctx context.Context, in *UpdateProjectSubmissionTypeRequest, opts ...grpc.CallOption) (*UpdateProjectSubmissionTypeResponse, error) {
	out := new(UpdateProjectSubmissionTypeResponse)
	err := c.cc.Invoke(ctx, "/maincon.mmm.setting.ProjectSettingService/UpdateProjectSubmissionType", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *projectSettingServiceClient) ReadProjectFileType(ctx context.Context, in *ReadProjectFileTypeRequest, opts ...grpc.CallOption) (*ReadProjectFileTypeResponse, error) {
	out := new(ReadProjectFileTypeResponse)
	err := c.cc.Invoke(ctx, "/maincon.mmm.setting.ProjectSettingService/ReadProjectFileType", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *projectSettingServiceClient) UpdateProjectFileType(ctx context.Context, in *UpdateProjectFileTypeRequest, opts ...grpc.CallOption) (*UpdateProjectFileTypeResponse, error) {
	out := new(UpdateProjectFileTypeResponse)
	err := c.cc.Invoke(ctx, "/maincon.mmm.setting.ProjectSettingService/UpdateProjectFileType", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ProjectSettingServiceServer is the server API for ProjectSettingService service.
// All implementations must embed UnimplementedProjectSettingServiceServer
// for forward compatibility
type ProjectSettingServiceServer interface {
	ReadProjectSubmissionType(context.Context, *ReadProjectSubmissionTypeRequest) (*ReadProjectSubmissionTypeResponse, error)
	UpdateProjectSubmissionType(context.Context, *UpdateProjectSubmissionTypeRequest) (*UpdateProjectSubmissionTypeResponse, error)
	ReadProjectFileType(context.Context, *ReadProjectFileTypeRequest) (*ReadProjectFileTypeResponse, error)
	UpdateProjectFileType(context.Context, *UpdateProjectFileTypeRequest) (*UpdateProjectFileTypeResponse, error)
	mustEmbedUnimplementedProjectSettingServiceServer()
}

// UnimplementedProjectSettingServiceServer must be embedded to have forward compatible implementations.
type UnimplementedProjectSettingServiceServer struct {
}

func (UnimplementedProjectSettingServiceServer) ReadProjectSubmissionType(context.Context, *ReadProjectSubmissionTypeRequest) (*ReadProjectSubmissionTypeResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ReadProjectSubmissionType not implemented")
}
func (UnimplementedProjectSettingServiceServer) UpdateProjectSubmissionType(context.Context, *UpdateProjectSubmissionTypeRequest) (*UpdateProjectSubmissionTypeResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateProjectSubmissionType not implemented")
}
func (UnimplementedProjectSettingServiceServer) ReadProjectFileType(context.Context, *ReadProjectFileTypeRequest) (*ReadProjectFileTypeResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ReadProjectFileType not implemented")
}
func (UnimplementedProjectSettingServiceServer) UpdateProjectFileType(context.Context, *UpdateProjectFileTypeRequest) (*UpdateProjectFileTypeResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateProjectFileType not implemented")
}
func (UnimplementedProjectSettingServiceServer) mustEmbedUnimplementedProjectSettingServiceServer() {}

// UnsafeProjectSettingServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to ProjectSettingServiceServer will
// result in compilation errors.
type UnsafeProjectSettingServiceServer interface {
	mustEmbedUnimplementedProjectSettingServiceServer()
}

func RegisterProjectSettingServiceServer(s grpc.ServiceRegistrar, srv ProjectSettingServiceServer) {
	s.RegisterService(&_ProjectSettingService_serviceDesc, srv)
}

func _ProjectSettingService_ReadProjectSubmissionType_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ReadProjectSubmissionTypeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProjectSettingServiceServer).ReadProjectSubmissionType(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.mmm.setting.ProjectSettingService/ReadProjectSubmissionType",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProjectSettingServiceServer).ReadProjectSubmissionType(ctx, req.(*ReadProjectSubmissionTypeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ProjectSettingService_UpdateProjectSubmissionType_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateProjectSubmissionTypeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProjectSettingServiceServer).UpdateProjectSubmissionType(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.mmm.setting.ProjectSettingService/UpdateProjectSubmissionType",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProjectSettingServiceServer).UpdateProjectSubmissionType(ctx, req.(*UpdateProjectSubmissionTypeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ProjectSettingService_ReadProjectFileType_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ReadProjectFileTypeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProjectSettingServiceServer).ReadProjectFileType(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.mmm.setting.ProjectSettingService/ReadProjectFileType",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProjectSettingServiceServer).ReadProjectFileType(ctx, req.(*ReadProjectFileTypeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ProjectSettingService_UpdateProjectFileType_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateProjectFileTypeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProjectSettingServiceServer).UpdateProjectFileType(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.mmm.setting.ProjectSettingService/UpdateProjectFileType",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProjectSettingServiceServer).UpdateProjectFileType(ctx, req.(*UpdateProjectFileTypeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _ProjectSettingService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "maincon.mmm.setting.ProjectSettingService",
	HandlerType: (*ProjectSettingServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "ReadProjectSubmissionType",
			Handler:    _ProjectSettingService_ReadProjectSubmissionType_Handler,
		},
		{
			MethodName: "UpdateProjectSubmissionType",
			Handler:    _ProjectSettingService_UpdateProjectSubmissionType_Handler,
		},
		{
			MethodName: "ReadProjectFileType",
			Handler:    _ProjectSettingService_ReadProjectFileType_Handler,
		},
		{
			MethodName: "UpdateProjectFileType",
			Handler:    _ProjectSettingService_UpdateProjectFileType_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "maincon/mmm/mmmSetting/project-setting.proto",
}
