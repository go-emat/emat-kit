package kafka

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"testing"
	"time"

	"github.com/Shopify/sarama"
)

const TestUser = "test-emat-user"
const TestUserPassword = "test-emat-secret"
const TestTopic = "test-document"

type tmpProducerMsgHandler struct {
	log *testing.T
}

func (t *tmpProducerMsgHandler) Success(msg *sarama.ProducerMessage) error {
	log.Printf("Kafka Producer: success push topic[%s]-partition[%d]-offset[%d]",
		msg.Topic, msg.Partition, msg.Offset)

	return nil
}

func (t *tmpProducerMsgHandler) Error(err *sarama.ProducerError) error {

	msg := err.Msg
	log.Printf("Kafka Producer: fail push topic[%s]-partition[%d]-offset[%d] error=%s",
		msg.Topic, msg.Partition, msg.Offset, err.Error())

	// control consume speed
	time.Sleep(time.Second)

	return nil
}

func initEmatAsyncProducer(t *testing.T) (*EmatAsyncProducer, error) {
	cfg := &ProducerConfig{
		Brokers:        []string{"localhost:9092", "localhost:9093", "localhost:9094"},
		MaxRetry:       5,
		Partitioner:    PartitionerHash,
		FlushFrequency: 1 * time.Second,
		User:           TestUser,
		Password:       TestUserPassword,
		SASLEnable512:  true,
	}
	handler := &tmpProducerMsgHandler{log: t}
	return NewEmatAsyncProducer(cfg, handler)
}

func TestEmatAsyncProducer(t *testing.T) {
	log.SetFlags(log.Ldate | log.Lshortfile)
	sarama.Logger = log.New(os.Stdout, "[sarama] ", log.LstdFlags)
	producer, err := initEmatAsyncProducer(t)
	if err != nil {
		t.Error(err)
	}
	defer producer.Close()

	// Create signal channel
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM)

	for i := 1; i <= 10; i++ {
		key := "jimmy"
		t := time.Now()
		value := t.Format("20060102150405")
		// err := producer.Push(topic, value)
		data, _ := json.Marshal(&Message{
			Type:   "id",
			Entity: []byte(fmt.Sprintf("{\"value\": %s", value)),
		})
		err := producer.PushWithKey(TestTopic, key, data)
		if err != nil {
			log.Printf(fmt.Sprintf("Topic[%s] push msg err=%v", TestTopic, err))
		}
		log.Printf("Pushed value: key[%s]-value[%s]", key, value)
		// control produce speed
		time.Sleep(10 * time.Millisecond)
	}
}

func initEmatProducer() (*EmatProducer, error) {
	cfg := &ProducerConfig{
		Brokers:        []string{"localhost:9092", "localhost:9093", "localhost:9094"},
		MaxRetry:       5,
		Partitioner:    PartitionerHash,
		FlushFrequency: 1 * time.Second,
		User:           TestUser,
		Password:       TestUserPassword,
		SASLEnable512:  true,
	}
	return NewEmatProducer(cfg)
}

type MessageCreateRFQDto struct {
	Event
	ID                   int64
	BuyerDocumentNo      string
	BuyerCompany         string
	BuyerProjectName     string
	SupplierId           int64
	BuyerPurchaser       string
	BuyerDeliveryAddress string
	BuyerContact         string
	ExpiryDate           string
	Pdf                  string
}

func TestEmatProduceCreateRFQ(t *testing.T) {
	messageCreateRFQDto := &MessageCreateRFQDto{
		ID:                   21,
		Event:                Event{Type: "CreateRequestForQuote"},
		BuyerDocumentNo:      "RFQ-21000031-01",
		BuyerCompany:         "Arnhold & Co Ltd.",
		BuyerProjectName:     "99999 - Demo Project",
		SupplierId:           141,
		BuyerPurchaser:       "Dickson LOK",
		BuyerDeliveryAddress: "Kowloon, Hong Kong",
		BuyerContact:         "",
		ExpiryDate:           "",
		Pdf:                  "",
	}
	message, err := json.Marshal(messageCreateRFQDto)
	if err != nil {
		log.Printf("%v", err)
	}
	agent := NewProducerAgent()
	if err := agent.Start(); err != nil {
		log.Printf("%v", err)
	}
	event := NewCreateRFQEvent(message)
	event.ProduceKafkaMessageID = 1
	if err := agent.RegisterEvent(event); err != nil {
		log.Printf("%v", err)
	}
	result, err := agent.Confirm()
	if err != nil {
		log.Printf("%v", err)
	}
	log.Printf("result: %v", result)
}

func NewCreateRFQEvent(message []byte) *ProducerEvent {
	producer, err := initEmatProducer()
	if err != nil {
		log.Printf("%v", err)
	}
	return  &ProducerEvent{
		Event:    Event{Type: "CreateRequestForQuote"},
		Topic:    TestTopic,
		Key:      "rfq123",
		Producer: producer,
		Message:  message,
	}
}
