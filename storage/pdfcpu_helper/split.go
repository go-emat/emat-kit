package pdfcpu_helper

import (
	"bytes"
	pdfcpuApi "gitlab.com/go-emat/pdfcpu-mattex/pkg/api"
	"gitlab.com/go-emat/pdfcpu-mattex/pkg/pdfcpu"
)

func SplitPdf(file []byte, fileName string, outPutDir string) error {
	conf := pdfcpu.NewDefaultConfiguration()
	conf.Cmd = pdfcpu.SPLIT

	err := pdfcpuApi.Split(bytes.NewReader(file), outPutDir, fileName, 1, conf)
	if err != nil {
		return err
	}
	return nil
}

func PagesTotal(file []byte) (int, error) {
	conf := pdfcpu.NewDefaultConfiguration()

	count, err := pdfcpuApi.PageCount(bytes.NewReader(file), conf)
	if err != nil {
		return 0, err
	}
	return count, nil
}
