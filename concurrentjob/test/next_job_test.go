package test

import (
	"fmt"
	"log"
	"testing"
	"time"

	"gitlab.com/go-emat/emat-kit/concurrentjob"
)

type testingJob1Id struct {
	id int64
}

func (i *testingJob1Id) Identifier() string {
	return fmt.Sprintf("testingJob1Id//id:%d", i.id)
}

func (i *testingJob1Id) Message() string {
	return fmt.Sprintf("id: %d", i.id)
}

type testingJob2Id struct {
	id int64
}

func (i *testingJob2Id) Identifier() string {
	return fmt.Sprintf("testingJob2Id//id:%d", i.id)
}

func (i *testingJob2Id) Message() string {
	return fmt.Sprintf("id: %d", i.id)
}

type testingJob1 struct {
	*testingJob1Id
	concurrentjob.BaseConcurrentJob
	job2Id int64
}

func newTestingJob1(id, job2Id int64) *testingJob1 {
	return &testingJob1{
		testingJob1Id: &testingJob1Id{
			id: id,
		},
		job2Id: job2Id,
	}
}

func (j *testingJob1) Run() (jobErr *concurrentjob.JobErr) {
	time.Sleep(time.Second)
	if j.job2Id > 0 {
		j.SetNextJob(newTestingJob2(j.job2Id))
	}
	log.Printf("job1 %s ended", j.Message())
	return
}

type testingJob2 struct {
	*testingJob2Id
	concurrentjob.BaseConcurrentJob
}

func newTestingJob2(id int64) *testingJob2 {
	return &testingJob2{
		testingJob2Id: &testingJob2Id{
			id: id,
		},
	}
}

func (j *testingJob2) Run() (jobErr *concurrentjob.JobErr) {
	time.Sleep(time.Second * 3 / 2)
	log.Printf("job2 %s ended", j.Message())
	return
}

func TestNextJob(t *testing.T) {
	job1List := []*testingJob1{
		newTestingJob1(1, 1),
		newTestingJob1(2, 1),
		newTestingJob1(3, 1),
		newTestingJob1(4, 2),
		newTestingJob1(5, 1),
		newTestingJob1(6, 3),
		newTestingJob1(7, 2),
		newTestingJob1(4, 2),
	}
	errChans := []<-chan *concurrentjob.JobErr{}
	for _, job1 := range job1List {
		errChan := jobsBroker.SubscribeDocumentJob(job1)
		errChans = append(errChans, errChan)
	}
	jobErrsChan := concurrentjob.MergeErrs(errChans)
	if jobErrs := <-jobErrsChan; jobErrs != nil {
		t.Errorf(jobErrs.Error())
	}
}
