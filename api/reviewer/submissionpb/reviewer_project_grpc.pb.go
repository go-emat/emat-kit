// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package submissionpb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// ReviewerProjectServiceClient is the client API for ReviewerProjectService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type ReviewerProjectServiceClient interface {
	ReadReviewerProjectByUser(ctx context.Context, in *ReadReviewerProjectRequest, opts ...grpc.CallOption) (*ReadReviewerProjectResponse, error)
	GetReviewerProjectWithClientUsers(ctx context.Context, in *ReviewerProjectWithClientUserRequest, opts ...grpc.CallOption) (*ReviewerProjectWithClientUserResponse, error)
	SyncReviewerProjectConfidential(ctx context.Context, in *ReviewerProjectConfidentialRequest, opts ...grpc.CallOption) (*SimpleResponse, error)
	SyncReviewerProjectEnableAllowSkippingJuniorReviewer(ctx context.Context, in *EnableAllowSkippingJuniorReviewerRequest, opts ...grpc.CallOption) (*SimpleResponse, error)
}

type reviewerProjectServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewReviewerProjectServiceClient(cc grpc.ClientConnInterface) ReviewerProjectServiceClient {
	return &reviewerProjectServiceClient{cc}
}

func (c *reviewerProjectServiceClient) ReadReviewerProjectByUser(ctx context.Context, in *ReadReviewerProjectRequest, opts ...grpc.CallOption) (*ReadReviewerProjectResponse, error) {
	out := new(ReadReviewerProjectResponse)
	err := c.cc.Invoke(ctx, "/reviewer.ReviewerProjectService/ReadReviewerProjectByUser", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *reviewerProjectServiceClient) GetReviewerProjectWithClientUsers(ctx context.Context, in *ReviewerProjectWithClientUserRequest, opts ...grpc.CallOption) (*ReviewerProjectWithClientUserResponse, error) {
	out := new(ReviewerProjectWithClientUserResponse)
	err := c.cc.Invoke(ctx, "/reviewer.ReviewerProjectService/GetReviewerProjectWithClientUsers", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *reviewerProjectServiceClient) SyncReviewerProjectConfidential(ctx context.Context, in *ReviewerProjectConfidentialRequest, opts ...grpc.CallOption) (*SimpleResponse, error) {
	out := new(SimpleResponse)
	err := c.cc.Invoke(ctx, "/reviewer.ReviewerProjectService/SyncReviewerProjectConfidential", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *reviewerProjectServiceClient) SyncReviewerProjectEnableAllowSkippingJuniorReviewer(ctx context.Context, in *EnableAllowSkippingJuniorReviewerRequest, opts ...grpc.CallOption) (*SimpleResponse, error) {
	out := new(SimpleResponse)
	err := c.cc.Invoke(ctx, "/reviewer.ReviewerProjectService/SyncReviewerProjectEnableAllowSkippingJuniorReviewer", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ReviewerProjectServiceServer is the server API for ReviewerProjectService service.
// All implementations must embed UnimplementedReviewerProjectServiceServer
// for forward compatibility
type ReviewerProjectServiceServer interface {
	ReadReviewerProjectByUser(context.Context, *ReadReviewerProjectRequest) (*ReadReviewerProjectResponse, error)
	GetReviewerProjectWithClientUsers(context.Context, *ReviewerProjectWithClientUserRequest) (*ReviewerProjectWithClientUserResponse, error)
	SyncReviewerProjectConfidential(context.Context, *ReviewerProjectConfidentialRequest) (*SimpleResponse, error)
	SyncReviewerProjectEnableAllowSkippingJuniorReviewer(context.Context, *EnableAllowSkippingJuniorReviewerRequest) (*SimpleResponse, error)
	mustEmbedUnimplementedReviewerProjectServiceServer()
}

// UnimplementedReviewerProjectServiceServer must be embedded to have forward compatible implementations.
type UnimplementedReviewerProjectServiceServer struct {
}

func (UnimplementedReviewerProjectServiceServer) ReadReviewerProjectByUser(context.Context, *ReadReviewerProjectRequest) (*ReadReviewerProjectResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ReadReviewerProjectByUser not implemented")
}
func (UnimplementedReviewerProjectServiceServer) GetReviewerProjectWithClientUsers(context.Context, *ReviewerProjectWithClientUserRequest) (*ReviewerProjectWithClientUserResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetReviewerProjectWithClientUsers not implemented")
}
func (UnimplementedReviewerProjectServiceServer) SyncReviewerProjectConfidential(context.Context, *ReviewerProjectConfidentialRequest) (*SimpleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SyncReviewerProjectConfidential not implemented")
}
func (UnimplementedReviewerProjectServiceServer) SyncReviewerProjectEnableAllowSkippingJuniorReviewer(context.Context, *EnableAllowSkippingJuniorReviewerRequest) (*SimpleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SyncReviewerProjectEnableAllowSkippingJuniorReviewer not implemented")
}
func (UnimplementedReviewerProjectServiceServer) mustEmbedUnimplementedReviewerProjectServiceServer() {
}

// UnsafeReviewerProjectServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to ReviewerProjectServiceServer will
// result in compilation errors.
type UnsafeReviewerProjectServiceServer interface {
	mustEmbedUnimplementedReviewerProjectServiceServer()
}

func RegisterReviewerProjectServiceServer(s grpc.ServiceRegistrar, srv ReviewerProjectServiceServer) {
	s.RegisterService(&_ReviewerProjectService_serviceDesc, srv)
}

func _ReviewerProjectService_ReadReviewerProjectByUser_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ReadReviewerProjectRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ReviewerProjectServiceServer).ReadReviewerProjectByUser(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.ReviewerProjectService/ReadReviewerProjectByUser",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ReviewerProjectServiceServer).ReadReviewerProjectByUser(ctx, req.(*ReadReviewerProjectRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ReviewerProjectService_GetReviewerProjectWithClientUsers_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ReviewerProjectWithClientUserRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ReviewerProjectServiceServer).GetReviewerProjectWithClientUsers(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.ReviewerProjectService/GetReviewerProjectWithClientUsers",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ReviewerProjectServiceServer).GetReviewerProjectWithClientUsers(ctx, req.(*ReviewerProjectWithClientUserRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ReviewerProjectService_SyncReviewerProjectConfidential_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ReviewerProjectConfidentialRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ReviewerProjectServiceServer).SyncReviewerProjectConfidential(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.ReviewerProjectService/SyncReviewerProjectConfidential",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ReviewerProjectServiceServer).SyncReviewerProjectConfidential(ctx, req.(*ReviewerProjectConfidentialRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ReviewerProjectService_SyncReviewerProjectEnableAllowSkippingJuniorReviewer_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(EnableAllowSkippingJuniorReviewerRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ReviewerProjectServiceServer).SyncReviewerProjectEnableAllowSkippingJuniorReviewer(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/reviewer.ReviewerProjectService/SyncReviewerProjectEnableAllowSkippingJuniorReviewer",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ReviewerProjectServiceServer).SyncReviewerProjectEnableAllowSkippingJuniorReviewer(ctx, req.(*EnableAllowSkippingJuniorReviewerRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _ReviewerProjectService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "reviewer.ReviewerProjectService",
	HandlerType: (*ReviewerProjectServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "ReadReviewerProjectByUser",
			Handler:    _ReviewerProjectService_ReadReviewerProjectByUser_Handler,
		},
		{
			MethodName: "GetReviewerProjectWithClientUsers",
			Handler:    _ReviewerProjectService_GetReviewerProjectWithClientUsers_Handler,
		},
		{
			MethodName: "SyncReviewerProjectConfidential",
			Handler:    _ReviewerProjectService_SyncReviewerProjectConfidential_Handler,
		},
		{
			MethodName: "SyncReviewerProjectEnableAllowSkippingJuniorReviewer",
			Handler:    _ReviewerProjectService_SyncReviewerProjectEnableAllowSkippingJuniorReviewer_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "reviewer/submission/reviewer_project.proto",
}
