package storage

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"net/textproto"
	"strings"
)

type PSPDFKitService struct {
	PspdfKitHost string
	Operations string
	Token string
}

type ProcessPDFResponse struct {
	Data Data `json:"data"`
}

type Data struct {
	CreatedAt         string        `json:"createdAt"`
	DocumentID        string        `json:"document_id"`
	Errors            []interface{} `json:"errors"`
	PasswordProtected bool          `json:"password_protected"`
	SourcePDFSha256   string        `json:"sourcePdfSha256"`
	Title             string        `json:"title"`
}

func NewPSPDFKit(pspdfKitHost string, operations string, token string) *PSPDFKitService {
	return &PSPDFKitService{
		PspdfKitHost: pspdfKitHost,
		Operations: operations,
		Token: token,
	}
}

func (s *PSPDFKitService) ProcessPDFAnnotations(fileURL string, instantJSON string) ([]byte, error) {
	operations := s.Operations
	// step 1: prepare form/multipart body
	// step 1: upload the file
	uploadRes, err := s.uploadPDFToPSPDFKit(fileURL, instantJSON, operations)
	if err != nil {
		return nil, err
	}
	// step 2: download the processed PDF
	documentID := uploadRes.Data.DocumentID
	pdfFile, err := s.downloadFromPSPDFKit(documentID)
	if err != nil {
		log.Printf("error downloading PDF: %+v", err)
		return nil, err
	}

	// step 3: delete the processed PDF
	if err := s.deleteFromPSPDFKit(documentID); err != nil {
		log.Printf("error deleting PDF: %+v", err)
		return nil, err
	}

	return pdfFile, nil
}

func  (s *PSPDFKitService) uploadPDFToPSPDFKit(fileURL string, instantJSON string, operations string) (uploadRes *ProcessPDFResponse, err error) {
	endpoint := s.PspdfKitHost + "/api/documents"

	// step 1: prepare form/multipart body
	payload := &bytes.Buffer{}
	writer := multipart.NewWriter(payload)
	if err := writer.WriteField("url", fileURL); err != nil {
		return nil, err
	}

	instantJSONReader := strings.NewReader(instantJSON)
	h := make(textproto.MIMEHeader)
	h.Set("Content-Disposition", fmt.Sprintf(`form-data; name="%s"; filename="%s"`, "instant_json", "annotation.json"))
	h.Set("Content-Type", "application/json") // !!!this is very important to set content-type as pspdfkit expects it
	part2, err := writer.CreatePart(h)
	if err != nil {
		return nil, err
	}
	if _, err := io.Copy(part2, instantJSONReader); err != nil {
		log.Printf("error writing instant_json: %+v", err)
		return nil, err
	}

	if err := writer.WriteField("operations", operations); err != nil {
		log.Printf("error writing operations: %+v", err)
		return nil, err
	}
	if err := writer.Close(); err != nil {
		log.Printf("error closing writer: %+v", err)
		return nil, err
	}

	// step 2: call pspdfkit endpoint
	client := &http.Client{}
	req, err := http.NewRequest("POST", endpoint, payload)
	if err != nil {
		log.Printf("error creating request: %+v", err)
		return nil, err
	}
	req.Header.Set("Authorization", "Token token="+s.Token)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	req.Header.Set("Accept", "application/json")
	res, err := client.Do(req)
	if err != nil {
		log.Printf("error calling pspdfkit endpoint: %+v", err)
		return nil, err
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		log.Printf("PSPDFKIT Server cannot process the request: %d", res.StatusCode)
		return nil, fmt.Errorf("PSPDFKIT Server cannot process the request: %d", res.StatusCode)
	}

	// step 3: parse response
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Printf("error reading response body: %+v", err)
		return nil, err
	}

	resBody := &ProcessPDFResponse{}
	if err := json.Unmarshal(body, resBody); err != nil {
		log.Printf("error unmarshalling response: %+v", err)
		return nil, err
	}

	return resBody, nil
}

func  (s *PSPDFKitService) downloadFromPSPDFKit(documentId string) ([]byte, error) {
	endpoint := s.PspdfKitHost + "/api/documents/" + documentId + "/pdf"
	client := &http.Client{}
	req, err := http.NewRequest("GET", endpoint, nil)
	if err != nil {
		log.Printf("error creating request: %+v", err)
		return nil, err
	}
	req.Header.Set("Authorization", "Token token="+s.Token)
	req.Header.Set("Accept", "application/pdf")
	res, err := client.Do(req)
	if err != nil {
		log.Printf("error calling pspdfkit endpoint: %+v", err)
		return nil, err
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		log.Printf("PSPDFKIT Server cannot process the download request: %d", res.StatusCode)
		return nil, fmt.Errorf("PSPDFKIT Server cannot process the download request: %d", res.StatusCode)
	}

	body, err := ioutil.ReadAll(res.Body)
	return body, nil
}

func  (s *PSPDFKitService) deleteFromPSPDFKit(documentId string) error {
	endpoint := s.PspdfKitHost + "/api/documents/" + documentId
	client := &http.Client{}
	req, err := http.NewRequest("DELETE", endpoint, nil)
	if err != nil {
		log.Printf("error creating delete request: %+v", err)
		return err
	}
	req.Header.Set("Authorization", "Token token="+ s.Token)
	req.Header.Set("Accept", "*/*")
	res, err := client.Do(req)
	if err != nil {
		log.Printf("error calling pspdfkit endpoint: %+v", err)
		return err
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		log.Printf("PSPDFKIT Server cannot process the delete request: %d", res.StatusCode)
		return fmt.Errorf("PSPDFKIT Server cannot process the delete request: %d", res.StatusCode)
	}

	return nil
}
