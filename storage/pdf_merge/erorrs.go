package pdf_merge

import (
	"fmt"
	"strings"
)

type Errors struct {
	Errs      []error
	FatalErrs []error
}

func NewErrors() *Errors {
	return &Errors{}
}

func (e *Errors) Error() (res string) {
	fatalErrStrs := []string{}
	for _, e := range e.FatalErrs {
		fatalErrStrs = append(fatalErrStrs, e.Error())
	}
	if len(fatalErrStrs) > 0 {
		res = "fatal errors:\n" + strings.Join(fatalErrStrs, "\n")
	}

	errStrs := []string{}
	for _, e := range e.Errs {
		errStrs = append(errStrs, e.Error())
	}
	if len(errStrs) > 0 {
		res += "errors:\n" + strings.Join(errStrs, "\n")
	}
	return
}

func (e *Errors) Append(es *Errors) {
	e.Errs = append(e.Errs, es.Errs...)
	e.FatalErrs = append(e.FatalErrs, es.FatalErrs...)
}

func (e *Errors) Add(err error) {
	if err != nil {
		e.Errs = append(e.Errs, err)
	}
}

func (e *Errors) AddFatal(err error) {
	if err != nil {
		e.FatalErrs = append(e.FatalErrs, err)
	}
}

func (e *Errors) ExtractErrors() (res *Errors) {
	res = &Errors{
		Errs:      nil,
		FatalErrs: nil,
	}
	if len(e.Errs) > 0 {
		res = &Errors{
			Errs: e.Errs,
		}
	}
	if len(e.FatalErrs) > 0 {
		res.FatalErrs = e.FatalErrs
	}
	return res
}

func (e *Errors) ExtractFatalErrors() (res *Errors) {
	res = &Errors{
		Errs:      nil,
		FatalErrs: nil,
	}
	if len(e.Errs) > 0 {
		res = &Errors{
			Errs: e.Errs,
		}
	}
	if len(e.FatalErrs) > 0 {
		res.FatalErrs = e.FatalErrs
	}
	return res
}

type FileOpenError struct {
	FileName string
	Err      error
}

func NewFileOpenError(filename string, err error) *FileOpenError {
	return &FileOpenError{
		FileName: filename,
		Err:      err,
	}
}

func (e *FileOpenError) Error() string {
	return fmt.Sprintf("Cannot open or create file '%s': %s", e.FileName, e.Err.Error())
}

type PdfFileReadError struct {
	FileName string
	Err      error
}

func NewPdfFileReadError(filename string, err error) *PdfFileReadError {
	return &PdfFileReadError{
		FileName: filename,
		Err:      err,
	}
}

func (e *PdfFileReadError) Error() string {
	return fmt.Sprintf("Cannot read PDF file '%s': %s", e.FileName, e.Err.Error())
}

type PdfFileValidationError struct {
	FileName string
	Err      error
}

func NewPdfFileValidationError(filename string, err error) *PdfFileValidationError {
	return &PdfFileValidationError{
		FileName: filename,
		Err:      err,
	}
}

func (e *PdfFileValidationError) Error() string {
	return fmt.Sprintf("Cannot validate PDF file '%s': %s", e.FileName, e.Err.Error())
}

type PdfFileMergeError struct {
	FileName string
	Err      error
}

func NewPdfFileMergeError(filename string, err error) *PdfFileMergeError {
	return &PdfFileMergeError{
		FileName: filename,
		Err:      err,
	}
}

func (e *PdfFileMergeError) Error() string {
	return fmt.Sprintf("Cannot merge PDF file '%s': %s", e.FileName, e.Err.Error())
}

type ImageDecodeError struct {
	FileName string
	Err      error
}

func NewImageDecodeError(filename string, err error) *ImageDecodeError {
	return &ImageDecodeError{
		FileName: filename,
		Err:      err,
	}
}

func (e *ImageDecodeError) Error() string {
	return fmt.Sprintf("Cannot decode image file '%s': %s", e.FileName, e.Err.Error())
}

type ImageFileMergeError struct {
	FileName string
	Err      error
}

func NewImageFileMergeError(filename string, err error) *ImageFileMergeError {
	return &ImageFileMergeError{
		FileName: filename,
		Err:      err,
	}
}

func (e *ImageFileMergeError) Error() string {
	return fmt.Sprintf("Cannot merge image file '%s': %s", e.FileName, e.Err.Error())
}
