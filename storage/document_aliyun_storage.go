package storage

import (
	"errors"
	"fmt"
	"io"
	"log"
	"strconv"
	"strings"

	"github.com/360EntSecGroup-Skylar/excelize/v2"
	"github.com/SebastiaanKlippert/go-wkhtmltopdf"
	"gitlab.com/go-emat/emat-kit/storage/alicloudstorage"
	"gitlab.com/go-emat/emat-kit/storage/common"
)

var (
	PdfAliyunStorageDocumentBasePath = "document/"
	PdfBackupAliyunStorageBasePath   = "del/"
	PdfAliyunStorageFilesBasePath    = "files/"
	ZipAliyunStorageFilesBasePath    = "zip/"
)

type DocumentAliyunStorage struct {
	BucketName string
	ConfigFile string
}

func NewDocumentAliyunStorage(bucketName string, configFile string) *DocumentAliyunStorage {
	return &DocumentAliyunStorage{
		BucketName: bucketName,
		ConfigFile: configFile,
	}
}

// SaveFileToStorage save file to storage (local or google)
func (s *DocumentAliyunStorage) SaveFileToStorage(req *UploadFileRequest) (*UploadFileResponse, error) {
	//save aliyun storage
	aliyun, err := alicloudstorage.NewAliCloudStorage(s.ConfigFile)
	if err != nil {
		return nil, err
	}
	filePath := PdfAliyunStorageFilesBasePath + req.FilePath
	if len(req.PutUnderDocumentType) > 0 {
		filePath = PdfAliyunStorageDocumentBasePath + getDocumentPdfStoragePath(req.PutUnderDocumentType) + req.FileName
	}
	// old method
	// _, err = aliyun.SaveStorageDocument(filePath, req.FileContent, req.FileExtension)

	// new method
	if len(req.FileContent) == 0 && req.FileContentReader != nil {
		writer := aliyun.GetWriterByFilePathAndContentType(filePath, "application/"+req.FileExtension)
		defer writer.Close()
		if seeker, ok := req.FileContentReader.(io.Seeker); ok {
			seeker.Seek(io.SeekStart, 0)
		}
		_, err = io.Copy(writer, req.FileContentReader)
	} else {
		_, err = aliyun.SaveStorageDocument(filePath, req.FileContent, req.FileExtension)
	}

	return &UploadFileResponse{FilePath: filePath}, err
}

func (s *DocumentAliyunStorage) SaveExcelToStorage(filename string, xlsx *excelize.File, extension string) (url string, err error) {
	aliyun, err := alicloudstorage.NewAliCloudStorage(s.ConfigFile)
	if err != nil {
		return "", err
	}
	filePath := PdfAliyunStorageDocumentBasePath + filename
	return aliyun.SaveStorageExport(filePath, xlsx, extension)
}

func (s *DocumentAliyunStorage) FileExists(filePath string) bool {
	aliyun, err := alicloudstorage.NewAliCloudStorage(s.ConfigFile)
	if err != nil {
		return false
	}
	exist, err := aliyun.StorageBucket.IsObjectExist(filePath)
	if err != nil {
		return false
	}
	return exist
}

// MoveStorageFile Move StorageFile
func (s *DocumentAliyunStorage) MoveStorageFile(srcPath, destPath string) error {
	//save aliyun storage
	aliyun, err := alicloudstorage.NewAliCloudStorage(s.ConfigFile)
	if err != nil {
		return err
	}

	if err := aliyun.MoveStorageDocument(srcPath, destPath); err != nil {
		return fmt.Errorf("file %v, err: %v", srcPath, err)
	}
	log.Println("move file:", srcPath, " to ", destPath)

	return nil
}

// DownloadFile
func (s *DocumentAliyunStorage) DownloadFile(filePath string) ([]byte, error) {
	aliyun, err := alicloudstorage.NewAliCloudStorage(s.ConfigFile)
	if err != nil {
		return nil, err
	}

	return aliyun.ReadStorageDocument(filePath)
}

func (s *DocumentAliyunStorage) DeleteStorageFile(filePath string) error {
	aliyun, err := alicloudstorage.NewAliCloudStorage(s.ConfigFile)
	if err != nil {
		return err
	}
	delPath := PdfBackupAliyunStorageBasePath + filePath

	return aliyun.MoveStorageDocument(filePath, delPath)
}

// GetFileReadSeeker
func (s *DocumentAliyunStorage) GetFileReadSeekCloser(filePath string) (io.ReadSeekCloser, error) {
	aliyun, err := alicloudstorage.NewAliCloudStorage(s.ConfigFile)
	if err != nil {
		return nil, err
	}

	w, err := aliyun.NewReadSeekCloser(filePath)
	log.Println("GetFileReadSeekCloser", err)
	return w, err
}

// GetFileWriteCloser
func (s *DocumentAliyunStorage) GetFileWriteCloser(filePath, contentType string) (io.WriteCloser, error) {
	aliyun, err := alicloudstorage.NewAliCloudStorage(s.ConfigFile)
	if err != nil {
		return nil, err
	}

	w := aliyun.GetWriterByFilePathAndContentType(filePath, contentType)

	return w, nil
}

func (s *DocumentAliyunStorage) GetWriter(newFilePath string, contentType string) (writer interface{}, err error) {
	aliyun, err := alicloudstorage.NewAliCloudStorage(s.ConfigFile)
	if err != nil {
		return nil, err
	}

	writer = aliyun.GetWriterByFilePathAndContentType(newFilePath, contentType+"/"+GetFileExtension(newFilePath))

	return
}

func (s *DocumentAliyunStorage) GetReader(filePath string) (reader io.ReadCloser, err error) {
	aliyun, err := alicloudstorage.NewAliCloudStorage(s.ConfigFile)
	if err != nil {
		return nil, err
	}

	return aliyun.StorageBucket.GetObject(filePath)
}

// GenerateSignedDownloadURL
func (s *DocumentAliyunStorage) GenerateSignedDownloadURL(fileName string, option ...common.SignedUrlOption) (string, error) {
	aliyun, err := alicloudstorage.NewAliCloudStorage(s.ConfigFile)
	if err != nil {
		return "", err
	}
	var mOption *common.SignedUrlOption
	if len(option) > 0 {
		mOption = &option[0]
	}
	return aliyun.GenerateObjectSignedURL(fileName, mOption)
}

func (s *DocumentAliyunStorage) GenerateResumableSessionURL(req *SessionURLRequest) (string, error) {
	aliyun, err := alicloudstorage.NewAliCloudStorage(s.ConfigFile)
	if err != nil {
		return "", err
	}
	return aliyun.GetMultipartUploadUrl(req.FileName)
}

func (s *DocumentAliyunStorage) CompleteSessionUrl(filepath, uploadID string) error {
	aliyun, err := alicloudstorage.NewAliCloudStorage(s.ConfigFile)
	if err != nil {
		return err
	}
	return aliyun.CompleteUploadPart(filepath, uploadID)
}

func (s *DocumentAliyunStorage) GetFileWriter(newFilePath string) (savedPath string, writer interface{}, err error) {
	return "", nil, errors.New("no impl GetFileWriter")
}

func (s *DocumentAliyunStorage) WriteFile(writer interface{}, buffer []byte, index int64) (err error) {
	return errors.New("no impl write file")
}

func (s *DocumentAliyunStorage) CloseFileWriter(writer interface{}) error {
	return errors.New("no impl CloseFileWriter")
}

// SavePdfToStorage save pdf to storage (local or google) BY WKHTML object
func (s *DocumentAliyunStorage) SavePdfToStorage(pdfg *wkhtmltopdf.PDFGenerator, documentType, fileName string) (*GeneratePdfToStorageResponse, error) {
	documentStoragePath := getDocumentPdfStoragePath(documentType)
	filePath := PdfAliyunStorageDocumentBasePath + documentStoragePath + fileName

	aliyun, err := alicloudstorage.NewAliCloudStorage(s.ConfigFile)
	if err != nil {
		return nil, err
	}
	_, err = aliyun.SaveStorageDocument(filePath, pdfg.Bytes(), "pdf")
	return &GeneratePdfToStorageResponse{
		FilePath:    filePath,
		StorageType: StorageTypeGoogle,
	}, err
}

// BackupStoragePdf backup pdf process
func (s *DocumentAliyunStorage) BackupStoragePdf(dbPdfPath string) (bool, error) {
	newFilePath := PdfBackupAliyunStorageBasePath + dbPdfPath
	aliyun, err := alicloudstorage.NewAliCloudStorage(s.ConfigFile)
	if err != nil {
		return false, err
	}
	newCloudStoragePath := strings.Replace(newFilePath, "./storage/", "", -1)
	if err := aliyun.MoveStorageDocument(dbPdfPath, newCloudStoragePath); err != nil {
		return false, err
	}
	return true, nil
}

// ZipStorageDocument
func (s *DocumentAliyunStorage) ZipStorageDocument(zipFilePath, zipFolder string, files []string) (string, error) {
	aliyun, err := alicloudstorage.NewAliCloudStorage(s.ConfigFile)
	if err != nil {
		return "", err
	}
	zipObjectFilePath := ZipAliyunStorageFilesBasePath + zipFilePath

	err = aliyun.ZipStorageDocument(zipObjectFilePath, zipFolder, files...)
	return zipObjectFilePath, err
}

// ZipContentToStorageDocument
func (s *DocumentAliyunStorage) ZipContentToStorageDocument(zipContent *common.ZipContent) ([]byte, error) {
	// aliyun, err := alicloudstorage.NewAliCloudStorage(s.ConfigFile)
	// if err != nil {
	// 	return "", err
	// }
	// buf, err := aliyun.ZipContentToStorageDocument(zipContent)
	// return buf, err
	return nil, errors.New("no impl ZipContentToStorageDocument")

}

func (s *DocumentAliyunStorage) GetFileMeta(filePath string) (*common.FileMeta, error) {
	fileMeta := common.FileMeta{}
	aliyun, err := alicloudstorage.NewAliCloudStorage(s.ConfigFile)
	if err != nil {
		return nil, err
	}
	meta, err := aliyun.StorageBucket.GetObjectDetailedMeta(filePath)
	if err != nil {
		return nil, err
	}
	contentLength := meta.Get("Content-Length")
	fileSize, err := strconv.ParseInt(contentLength, 10, 64)
	if err != nil {
		return nil, err
	}
	fileMeta.FileSize = fileSize
	return &fileMeta, nil
}
