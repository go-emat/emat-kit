package cloudstorage

import (
	"archive/zip"
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/url"
	"path/filepath"
	"time"

	"gitlab.com/go-emat/emat-kit/storage/common"

	"cloud.google.com/go/storage"
	"github.com/360EntSecGroup-Skylar/excelize/v2"
	"github.com/go-resty/resty/v2"
	"github.com/kataras/iris/v12"
	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"golang.org/x/oauth2/jwt"
)

// GoogleCloudStorage struct
type GoogleCloudStorage struct {
	StorageBucket     *storage.BucketHandle
	StorageBucketName string
}

type SessionURLRequest struct {
	FileName             string
	ServiceAccount       string
	ContentType          string
	InitialRequestLength string
	Origin               string
	XUploadContentType   string
	XUploadContentLength string
}

// NewGoogleCloudStorage new instance
func NewGoogleCloudStorage(storageBucketName string) (*GoogleCloudStorage, error) {
	storageBucket, err := createGoogleStorageBucketHandle(storageBucketName)
	if err != nil {
		return nil, err
	}
	return &GoogleCloudStorage{
		StorageBucket:     storageBucket,
		StorageBucketName: storageBucketName,
	}, nil
}

func createGoogleStorageBucketHandle(bucketID string) (*storage.BucketHandle, error) {
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		return nil, err
	}
	return client.Bucket(bucketID), nil
}

// SaveStorageDocument uploadFileFromForm uploads a file if it's present in the "image" form field.
func (s *GoogleCloudStorage) SaveStorageDocument(filename string, file []byte, extension string) (url string, err error) {

	if s.StorageBucket == nil {
		return "", errors.New("storage bucket is missing - check config.go")
	}

	ctx := context.Background()
	w := s.StorageBucket.Object(filename).NewWriter(ctx)
	w.ContentType = "application/" + extension
	w.CacheControl = "private, max-age=0, no-transform"
	// w.ACL = []storage.ACLRule{{Entity: storage.ACLEntity("project-owners-focused-ion-199408"), Role: storage.RoleReader}}

	if _, err := w.Write(file); err != nil {
		return "", err
	}

	if err := w.Close(); err != nil {
		return "", err
	}
	return filename, nil
}

// SaveStorageExport uploadFileFromForm uploads a file if it's present in the "image" form field.
func (s *GoogleCloudStorage) SaveStorageExport(filename string, xlsx *excelize.File, extension string) (url string, err error) {

	if s.StorageBucket == nil {
		return "", errors.New("storage bucket is missing - check config.go")
	}

	ctx := context.Background()
	w := s.StorageBucket.Object(filename).NewWriter(ctx)
	w.ContentType = "application/" + extension
	w.CacheControl = "private, max-age=0, no-transform"
	// w.ACL = []storage.ACLRule{{Entity: storage.ACLEntity("project-owners-focused-ion-199408"), Role: storage.RoleReader}}

	if _, err := xlsx.WriteTo(w); err != nil {
		return "", err
	}

	if err := w.Close(); err != nil {
		return "", err
	}
	return filename, nil
}

// GetStorageDocumentURL GetStorageDocumentURL
func (s *GoogleCloudStorage) GetStorageDocumentURL(filename string) string {

	const publicURL = "https://storage.googleapis.com/%s/%s"
	return fmt.Sprintf(publicURL, s.StorageBucketName, filename)
}

// MoveStorageDocument Move StorageDocument
func (s *GoogleCloudStorage) MoveStorageDocument(srcPath, destPath string) error {
	ctx := context.Background()
	src := s.StorageBucket.Object(srcPath)
	dst := s.StorageBucket.Object(destPath)

	if _, err := dst.CopierFrom(src).Run(ctx); err != nil {
		return err
	}
	if err := src.Delete(ctx); err != nil {
		return err
	}
	return nil
}

// DownloadGoogleStorageDocument download file
func (s *GoogleCloudStorage) DownloadGoogleStorageDocument(ctx iris.Context, filePath, fileName string) error {
	// Create new PDF generator
	fileData, err := s.ReadStorageDocument(filePath)
	if err != nil {
		return err
	}
	ctx.ResponseWriter().Header().Set("Content-Disposition", "attachment;filename="+fileName)
	if _, err = io.Copy(ctx.ResponseWriter(), bytes.NewReader(fileData)); err != nil {
		return err
	}
	return nil
}

// ReadStorageDocument read file
func (s *GoogleCloudStorage) ReadStorageDocument(filename string) ([]byte, error) {
	ctx := context.Background()
	// [START download_file]
	rc, err := s.StorageBucket.Object(filename).NewReader(ctx)
	if err != nil {
		return nil, err
	}
	defer rc.Close()

	data, err := ioutil.ReadAll(rc)
	if err != nil {
		return nil, err
	}
	return data, nil
}

// ZipStorageDocumentWithOriginalName read file
func (s *GoogleCloudStorage) ZipStorageDocumentWithOriginalName(zipFileURI, zipFolder string, fileNames []string, filePaths ...string) error {
	ctx := context.Background()
	// create storage file for writing
	storageWriter := s.StorageBucket.Object(zipFileURI).NewWriter(ctx)
	storageWriter.ContentType = "application/zip"
	defer storageWriter.Close()
	// Create a new zip archive to memory buffer
	zipWriter := zip.NewWriter(storageWriter)
	// go through each file in the prefix
	for i, filePath := range filePaths {
		if filePath != "" {
			storageReader, err := s.StorageBucket.Object(filePath).NewReader(ctx)
			if err != nil {
				return err
			}

			// add filename to zip to do change file
			zipFile, err := zipWriter.Create(zipFolder + "/" + fileNames[i])
			if err != nil {
				storageReader.Close()
				return err
			}
			// copy from storage reader to zip writer
			if _, err := io.Copy(zipFile, storageReader); err != nil {
				storageReader.Close()
				return err
			}
			storageReader.Close()
		}
	}
	if err := zipWriter.Close(); err != nil {
		storageWriter.Close()
		return err
	}
	storageWriter.Close() // we should be uploaded by here
	return nil
}

func (s *GoogleCloudStorage) ZipContentToStorageDocument(zipContent *common.ZipContent) ([]byte, error) {
	ctx := context.Background()
	// create storage file for writing
	// Create a new zip archive to memory buffer
	buf := new(bytes.Buffer)
	zipWriter := zip.NewWriter(buf)
	// go through each file in the prefix
	for _, file := range zipContent.File {
		if file.Filename != "" {
			storageReader, err := s.StorageBucket.Object(file.FilePath).NewReader(ctx)
			if err != nil {
				log.Printf("zip file err %v", err)
				return nil, err
			}
			// zipFile, err := zipWriter.Create(file.Filename)
			zipFile, err := zipWriter.Create(zipContent.FolderName + "/" + file.Filename)
			if err != nil {
				storageReader.Close()
				log.Printf("GCS zip 1 err %v", err)
				return nil, err
			}
			// copy from storage reader to zip writer
			if _, err := io.Copy(zipFile, storageReader); err != nil {
				storageReader.Close()
				log.Printf("GCS zip 2 err %v", err)
				return nil, err
			}
			storageReader.Close()
		}
	}

	err := common.CopyToZipWriter(zipWriter, zipContent)
	if err != nil {
		return nil, err
	}
	err = zipWriter.Close()
	return buf.Bytes(), err
}

// ZipStorageDocument read file
func (s *GoogleCloudStorage) ZipStorageDocument(zipFileURI, zipFolder string, fileNames ...string) error {
	ctx := context.Background()
	// create storage file for writing
	storageWriter := s.StorageBucket.Object(zipFileURI).NewWriter(ctx)
	storageWriter.ContentType = "application/zip"
	defer storageWriter.Close()
	// Create a new zip archive to memory buffer
	zipWriter := zip.NewWriter(storageWriter)
	// go through each file in the prefix
	for _, fileName := range fileNames {
		if fileName != "" {
			storageReader, err := s.StorageBucket.Object(fileName).NewReader(ctx)
			if err != nil {
				log.Printf("zip file err %v", err)
				log.Printf("s.StorageBucket %v", s.StorageBucket)
				log.Printf("fileNames %v", fileNames)
				log.Printf("fileName %v", fileName)
				return err
			}
			_, relPath := filepath.Split(fileName)
			// add filename to zip to do change file
			zipFile, err := zipWriter.Create(zipFolder + "/" + relPath)
			if err != nil {
				storageReader.Close()
				return err
			}
			// copy from storage reader to zip writer
			if _, err := io.Copy(zipFile, storageReader); err != nil {
				storageReader.Close()
				return err
			}
			storageReader.Close()
		}
	}
	if err := zipWriter.Close(); err != nil {
		storageWriter.Close()
		return err
	}
	storageWriter.Close() // we should be uploaded by here
	return nil
}

func (s *GoogleCloudStorage) defaultSignedUrlOption(cnf *jwt.Config) *storage.SignedURLOptions {
	return &storage.SignedURLOptions{
		Scheme:         storage.SigningSchemeV4,
		Method:         "GET",
		GoogleAccessID: cnf.Email,
		PrivateKey:     cnf.PrivateKey,
		//Expires:        time.Now().Add(7 * 24 * time.Hour),
		Expires: time.Now().AddDate(0, 0, 7),
	}
}

func (s *GoogleCloudStorage) prepareSignedUrlOptionBy(jwtCnf *jwt.Config, fileName string,
	option *common.SignedUrlOption) (rtn *storage.SignedURLOptions) {

	opts := s.defaultSignedUrlOption(jwtCnf)
	if option != nil {
		if option.ExpiryInMinutes > 0 {
			opts.Expires = time.Now().Add(time.Minute * time.Duration(option.ExpiryInMinutes))
		}
		if len(option.MimeContentType) > 0 {
			urlVals := url.Values{}
			if option.SkipMimeContentTypeCheck {
				urlVals.Add("response-content-type", option.MimeContentType)
			} else {
				mimeType, verifyErr := common.VerifyMimeTypeByFileName(fileName, option.MimeContentType)
				if verifyErr != nil {
					log.Printf("VerifyMimeTypeByFileName err: %+v", verifyErr)
				} else {
					urlVals.Add("response-content-type", mimeType)
				}
			}
			if len(urlVals) > 0 {
				opts.QueryParameters = urlVals // SignedURLOptions.ContentType / SignedURLOptions.Headers SUCK
			}
		}
	}
	return opts
}

func (s *GoogleCloudStorage) GenerateV4GetObjectSignedURL(fileName, serviceAccount string, option *common.SignedUrlOption) (string, error) {
	jsonKey, err := ioutil.ReadFile(serviceAccount)
	if err != nil {
		return "", fmt.Errorf("Read google config file failed: %v, filePath:[%v]", err, serviceAccount)
	}
	conf, err := google.JWTConfigFromJSON(jsonKey)
	if err != nil {
		return "", fmt.Errorf("google.JWTConfigFromJSON: %v", err)
	}

	opts := s.prepareSignedUrlOptionBy(conf, fileName, option)

	u, err := storage.SignedURL(s.StorageBucketName, fileName, opts)
	if err != nil {
		return "", fmt.Errorf("storage.SignedURL: %v", err)
	}
	log.Printf("You can use this URL with any user agent, Generated GET signed URL: %v", u)
	return u, nil
}

func (s *GoogleCloudStorage) GenerateResumableSessionURL(sessionURLRequest *SessionURLRequest) (*url.URL, error) {

	token, err := GetGCPStorageOauth2Token(sessionURLRequest.ServiceAccount)
	if err != nil {
		return nil, err
	}
	query := make(url.Values)
	//query.Set("projection", "full")
	query.Set("uploadType", "resumable")
	query.Set("name", sessionURLRequest.FileName)
	url := &url.URL{
		Scheme:   "https",
		Host:     "storage.googleapis.com",
		Opaque:   fmt.Sprintf("//storage.googleapis.com/upload/storage/v1/b/%s/o", s.StorageBucketName),
		RawQuery: query.Encode(),
	}

	// Create a Resty Client
	client := resty.New()
	request := client.R().SetHeader("Content-Type", "application/json").
		SetAuthToken(token.AccessToken)
	if sessionURLRequest.ContentType != "" {
		request.SetHeader("Content-Type", sessionURLRequest.ContentType)
		request.SetHeader("X-Upload-Content-Type", sessionURLRequest.ContentType)
	}
	if sessionURLRequest.InitialRequestLength != "" {
		request.SetHeader("Content-Length", sessionURLRequest.InitialRequestLength)
	}
	if sessionURLRequest.Origin != "" {
		request.SetHeader("Origin", sessionURLRequest.Origin)
	}
	if sessionURLRequest.XUploadContentLength != "" {
		request.SetHeader("X-Upload-Content-Length", sessionURLRequest.XUploadContentLength)
	}
	if sessionURLRequest.XUploadContentType != "" {
		request.SetHeader("X-Upload-Content-Type", sessionURLRequest.XUploadContentType)
	}
	resp, err := request.Post(url.String())
	if err != nil {
		return nil, err
	}
	println("url", resp.RawResponse.Body)

	httpResponse := resp.RawResponse
	// Extract the Location header.
	str := httpResponse.Header.Get("Location")
	if str == "" {
		return nil, errors.New("expected a Location header")
	}
	// Parse it.
	uploadURL, err := url.Parse(str)
	if err != nil {
		return nil, fmt.Errorf("url.Parse: %v", err)
	}
	log.Printf("You can use this URL with any user agent, Initiate a resumable upload URL: %v", uploadURL)
	return uploadURL, nil
}

func GetGCPStorageOauth2Token(serviceAccount string) (*oauth2.Token, error) {
	jsonKey, err := ioutil.ReadFile(serviceAccount)
	if err != nil {
		return nil, fmt.Errorf("Read google config file failed: %v, filepath:[%v]", err, serviceAccount)
	}
	conf, err := google.JWTConfigFromJSON(jsonKey)
	if err != nil {
		return nil, fmt.Errorf("google.JWTConfigFromJSON: %v", err)
	}
	config := &jwt.Config{
		Email:      conf.Email,
		PrivateKey: []byte(conf.PrivateKey),
		Scopes: []string{
			"https://www.googleapis.com/auth/devstorage.read_write",
		},
		TokenURL: google.JWTTokenURL,
	}
	return config.TokenSource(context.Background()).Token()
}

func (s *GoogleCloudStorage) NewGcsReadSeekCloser(filepath string) (*GoogleCloudStorageReadSeekCloser, error) {
	ctx := context.Background()
	storageHandle := s.StorageBucket.Object(filepath)
	attrs, err := storageHandle.Attrs(ctx)
	if err != nil {
		return nil, err
	}
	return NewGoogleCloudStorageReadSeekCloser(storageHandle, ctx, attrs.Size, 8192), nil
}

func (s *GoogleCloudStorage) GetWriterByFilePathAndContentType(filepath, contentType string) *storage.Writer {
	ctx := context.Background()
	writer := s.StorageBucket.Object(filepath).NewWriter(ctx)
	writer.ContentType = contentType
	writer.CacheControl = "private, max-age=0, no-transform"
	return writer
}
