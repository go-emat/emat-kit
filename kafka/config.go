package kafka

import (
	"crypto/tls"
	"crypto/x509"
	"github.com/Shopify/sarama"
	"io/ioutil"
	"log"
	"time"
)

// PartitionerType define different partition category for producer,
// here are 3 choices: Random, RoundRobin and Hash
type PartitionerType int8

const (
	// PartitionerRandom 消息发送会随机发送到partition
	PartitionerRandom PartitionerType = iota
	// PartitionerRoundRobin 消息会轮询的发送到所有partition
	PartitionerRoundRobin
	// PartitionerHash 通过message key来确定发送到指定的partition，此方式可以保证顺序
	PartitionerHash
)

// ProducerConfig configuration for Producer
type ProducerConfig struct {
	Brokers        []string
	Partitioner    PartitionerType
	FlushFrequency time.Duration
	MaxRetry       int
	User           string
	Password       string
	SASLEnable512  bool
	TLSCertFile    string
}

// ConsumerConfig configuration for consumer group
type ConsumerConfig struct {
	Brokers []string
	Topics  []string
	GroupID string
	// Workers count of workers per each partition consumer.
	// If you want to process message with sequence, should just set worker count=1.
	Workers       int
	MaxRetry      int
	User          string
	Password      string
	SASLEnable512 bool
	TLSCertFile   string
	OffsetOldest  bool
	Version       string                 //2.1.1
	Assignor      sarama.BalanceStrategy //range, roundrobin, sticky,

}

// Message for producer & consumer format
type Message struct {
	// Type is used for different register handler
	Type   string `json:"type"`
	Entity []byte `json:"entity"`
}

func NewKafkaConsumerConfig(cfg *ConsumerConfig) *sarama.Config {
	config := sarama.NewConfig()
	config.Metadata.RefreshFrequency = 1 * time.Minute
	config.Consumer.Offsets.Initial = sarama.OffsetNewest
	config.Consumer.Return.Errors = true
	config.Consumer.Group.Rebalance.Strategy = cfg.Assignor
	// 尽量避免Rebalance
	config.Consumer.Offsets.AutoCommit.Enable = false
	config.Consumer.Group.Session.Timeout = time.Duration(6) * time.Second
	config.Consumer.Group.Heartbeat.Interval = time.Duration(2) * time.Second
	//config.Consumer.Group.Rebalance.Timeout =

	// Get the SystemCertPool, continue with an empty pool on error
	config.Net.TLS.Enable = false
	if cfg.TLSCertFile != "" {
		config.Net.TLS.Enable = true
		config.Net.TLS.Config = NewTLSConfigFromCaCrt(cfg.TLSCertFile)
	}
	if cfg.SASLEnable512 {
		config.Net.SASL.Enable = true
		config.Net.SASL.User = cfg.User
		config.Net.SASL.Password = cfg.Password
		config.Net.SASL.SCRAMClientGeneratorFunc = func() sarama.SCRAMClient { return &XDGSCRAMClient{HashGeneratorFcn: SHA512} }
		config.Net.SASL.Mechanism = sarama.SASLTypeSCRAMSHA512
	}
	if cfg.OffsetOldest {
		config.Consumer.Offsets.Initial = sarama.OffsetOldest
	}
	return config
}

func NewKafkaProducerConfig(cfg *ProducerConfig) *sarama.Config {
	config := sarama.NewConfig()
	config.Metadata.RefreshFrequency = 3 * time.Minute
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Compression = sarama.CompressionSnappy
	config.Producer.Partitioner = createPartitioner(cfg)
	config.Producer.Flush.Frequency = cfg.FlushFrequency
	config.Producer.Retry.Max = cfg.MaxRetry
	config.Producer.Return.Successes = true
	config.Net.TLS.Enable = false
	if cfg.TLSCertFile != "" {
		config.Net.TLS.Enable = true
		config.Net.TLS.Config = NewTLSConfigFromCaCrt(cfg.TLSCertFile)
	}
	if cfg.SASLEnable512 {
		config.Net.SASL.Enable = true
		config.Net.SASL.User = cfg.User
		config.Net.SASL.Password = cfg.Password
		config.Net.SASL.SCRAMClientGeneratorFunc = func() sarama.SCRAMClient { return &XDGSCRAMClient{HashGeneratorFcn: SHA512} }
		config.Net.SASL.Mechanism = sarama.SASLTypeSCRAMSHA512
	}
	//config.Producer.Idempotent = true //幂等生产者
	config.Net.DialTimeout = 3 * time.Second
	config.Net.ReadTimeout = 3 * time.Second
	config.Net.WriteTimeout = 3 * time.Second
	return config
}

func createPartitioner(cfg *ProducerConfig) sarama.PartitionerConstructor {
	switch cfg.Partitioner {
	case PartitionerRoundRobin:
		return sarama.NewRoundRobinPartitioner
	case PartitionerHash:
		return sarama.NewHashPartitioner
	default:
		return sarama.NewRandomPartitioner
	}
}

func NewTLSConfigFromCaCrt(localCertFile string) *tls.Config {
	// Get the SystemCertPool, continue with an empty pool on error
	rootCAs, _ := x509.SystemCertPool()
	if rootCAs == nil {
		rootCAs = x509.NewCertPool()
	}

	// Read in the cert file
	certs, err := ioutil.ReadFile(localCertFile)
	if err != nil {
		log.Fatalf("Failed to append %q to RootCAs: %v", localCertFile, err)
	}

	// Append our cert to the system pool
	if ok := rootCAs.AppendCertsFromPEM(certs); !ok {
		log.Println("No certs appended, using system certs only")
	}
	return &tls.Config{
		InsecureSkipVerify: true,
		RootCAs:            rootCAs,
	}
}

// NewTLSConfig generates a TLS configuration used to authenticate on server with
// certificates.
// Parameters are the three pem files path we need to authenticate: client cert, client key and CA cert.
func NewTLSConfig(clientCertFile, clientKeyFile, caCertFile string) (*tls.Config, error) {
	tlsConfig := tls.Config{}

	// Load client cert
	cert, err := tls.LoadX509KeyPair(clientCertFile, clientKeyFile)
	if err != nil {
		return &tlsConfig, err
	}
	tlsConfig.Certificates = []tls.Certificate{cert}

	// Load CA cert
	caCert, err := ioutil.ReadFile(caCertFile)
	if err != nil {
		return &tlsConfig, err
	}
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)
	tlsConfig.RootCAs = caCertPool

	tlsConfig.BuildNameToCertificate()
	return &tlsConfig, err
}
