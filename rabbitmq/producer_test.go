package rabbitmq

import (
	"fmt"
	"os"
	"testing"
	"time"
)

var mqUrl = "amqp://emat-admin:secrect@localhost:5672/emat-auth"

func TestProducer(t *testing.T) {

	m, err := New(mqUrl).Open()
	if err != nil {
		panic(err.Error())
	}

	producer, err := m.Producer("test-producer")
	if err != nil {
		panic(fmt.Sprintf("Create producer failed, %v", err))
	}

	exchangeBinds :=[]*ExchangeBinds{
		&ExchangeBinds{
			Exchange: NewDefaultExchange("exch.unitest", ExchangeDirect),
			Bindings: []*Binding{
				&Binding{
					RouteKey: "route.unitest1",
					Queues:   []*Queue{NewDefaultQueue("queue.unitest1")},
				},
				&Binding{
					RouteKey: "route.unitest2",
					Queues:   []*Queue{NewDefaultQueue("queue.unitest2")},
				},
			},
		},
	}

	if err := producer.SetExchangeBinds(exchangeBinds).Confirm(true).Open(); err !=nil{
		panic(fmt.Sprintf("Open failed, %v", err))
	}

	for i:=0;i<1000;i++ {
		if i >0 && i%3 ==0 {
			producer.CloseChan()
		}
		err = producer.Publish("exch.unitest", "route.unitest1", NewPublishMsg([]byte(`{"name":"Oswald Chan","email":"oswald.chan222@mattex.com.hk","initial":"1","office_tel":"1","mobile_tel":"1","base64_signature":"1","base64_signature_short":"1","created_by":"1","updated_by":"1"}`)))
		t.Logf("Produce state: %d, err:%v\n", producer.State(), err)
		time.Sleep(time.Second)
	}

	producer.Close()
	m.Close()
}

func TestProducerWithTLS(t *testing.T) {

	//os.Setenv("RABBIT_MQ_SERVER_NAME", "")
	//MTLS Conn
	os.Setenv("RABBIT_MQ_MTLS", "true")
	os.Setenv("RABBIT_MQ_CA_CERT", "./dev_client_rabbitmq_ca.crt")
	os.Setenv("RABBIT_MQ_CLIENT_CERT", "./dev_client_rabbitmq_cert.crt")
	os.Setenv("RABBIT_MQ_CLIENT_KEY", "./dev_client_rabbitmq_cert.key")

	//TLS Conn
	//os.Setenv("RABBIT_MQ_TLS", "true")
	//os.Setenv("RABBIT_MQ_SERVER_CERT", "./dev_emat_rabbitmq_cert.crt")

	m, err := New("amqps://test-admin:secrect@rabbitmq.dev-emat.mattex.com.hk:5671/test").Open()
	if err != nil {
		panic(err.Error())
	}

	producer, err := m.Producer("test-producer")
	if err != nil {
		panic(fmt.Sprintf("Create producer failed, %v", err))
	}

	exchangeBinds :=[]*ExchangeBinds{
		&ExchangeBinds{
			Exchange: NewDefaultExchange("exch.unitest", ExchangeDirect),
			Bindings: []*Binding{
				&Binding{
					RouteKey: "route.unitest1",
					Queues:   []*Queue{NewDefaultQueue("queue.unitest1")},
				},
				&Binding{
					RouteKey: "route.unitest2",
					Queues:   []*Queue{NewDefaultQueue("queue.unitest2")},
				},
			},
		},
	}

	if err := producer.SetExchangeBinds(exchangeBinds).Confirm(true).Open(); err !=nil{
		panic(fmt.Sprintf("Open failed, %v", err))
	}

	for i:=0;i<1000;i++ {
		if i >0 && i%3 ==0 {
			producer.CloseChan()
		}
		err = producer.Publish("exch.unitest", "route.unitest1", NewPublishMsg([]byte(`{"name":"Oswald Chan","email":"oswald.chan222@mattex.com.hk","initial":"1","office_tel":"1","mobile_tel":"1","base64_signature":"1","base64_signature_short":"1","created_by":"1","updated_by":"1"}`)))
		t.Logf("Produce state: %d, err:%v\n", producer.State(), err)
		time.Sleep(time.Second)
	}

	producer.Close()
	m.Close()
}