syntax = "proto3";

package maincon.mmm.submission;

option go_package = "maincon/mmm/submissionpb";

import "google/protobuf/timestamp.proto";
import "common/common.proto";

service FileService {
  rpc ListFile (ListFileRequest) returns (ListFileResponse);
  rpc ReadFile (ReadFileRequest) returns (ReadFileResponse);
  rpc SortFiles (SortFileRequest) returns (SortFileResponse);
  rpc GenerateUploadFilesUrl (GenerateUploadFilesUrlRequest) returns (GenerateUploadFilesUrlResponse);
  rpc CompleteUploadFilesUrl (CompleteUploadFilesUrlRequest) returns (CompleteUploadFilesUrlResponse);
  rpc GenerateDownloadSignedUrl (GenerateDownloadSignedUrlRequest) returns (GenerateDownloadSignedUrlResponse);
  rpc GenerateDownloadSignedUrlFromEmatSupplier (GenerateDownloadSignedUrlRequest) returns (GenerateDownloadSignedUrlFromEmatSupplierResponse);
  rpc GenerateDownloadMaterialSubmissionFileList (GenerateDownloadMaterialSubmissionFileListRequest) returns (GenerateDownloadMaterialSubmissionFileListResponse);
  rpc DownloadFromUrlAndSave (ReceiveFileRequest) returns (DownloadFromUrlAndSaveResponse);
  rpc ValidatePdfFiles (ValidatePdfFilesRequest) returns (ValidatePdfFilesResponse);
  rpc ConvertFilesToStandard (ConvertFilesToStandardRequest) returns (ConvertFilesToStandardResponse);
  rpc GetFileList (GetFileListRequest) returns (GetFileListResponse);
  rpc CreateFiles (CreateFilesRequest) returns (CreateFilesResponse);
  rpc UpdateFiles (UpdateFilesRequest) returns (UpdateFilesResponse);
  rpc DeleteFile (DeleteFileRequest) returns (DeleteFileResponse);
}

message File {
  int64 id = 1;
  int64 file_type_id = 2;
  int64 model_id = 4;
  string model_type = 3;
  string revision = 29;
  string file_name = 5;
  string file_path = 6;
  string original_file_path = 7;
  string thumbnail_file_path = 8;
  string file_url = 9;
  int64 height = 10;
  int64 width = 11;
  int64 size = 12;
  int64 attach = 13;
  bool include_in_submission = 14;
  google.protobuf.Timestamp expiration_date = 15;
  string remark = 16;
  int64 content_response_id = 17;
  string extension = 18;
  string extra_info = 19;
  int64 decision_id = 20;
  string decision_type = 21;
  int32 sort = 22;
  repeated FileAdditional file_additions = 23;
  string appendix = 24;
  common.Audit audit = 25;
  repeated int64 related_component_ids = 26;
  repeated RelatedComponent related_components = 27;
  int32 standard_pdf = 28;
}

message RelatedComponent {
  int64 id = 1;
  string name = 2;
}

message FileAdditional {
  int64 id = 1;
  int64 file_id = 2;
  int64 file_type_id = 3;
  string file_type_name = 4;
  google.protobuf.Timestamp expiration_date = 5;
  string remark = 6;
}

message ReadFileRequest {
  int64 id = 1;
  common.Language lang = 2;
}

message ReadFileResponse {
  File data = 1;
}

message ListFileRequest {
  common.Language lang = 1;
  common.PaginationRequest pagination_request = 2;
  int64 model_id = 3;
  string model_type = 4;
  string revision = 5;
}

message ListFileResponse {
  repeated File data = 1;
  common.MataPaginator meta = 2;
}

message SortFileRequest {
  common.Language lang = 1;
  int64 model_id = 2;
  string model_type = 3;
  string revision = 5;
  repeated SortFile files = 4;
}

message SortFile {
  int64 id = 1;
  int32 sort = 2;
}

message SortFileResponse {
  bool success = 1;
}

message GenerateUploadFilesUrlRequest {
  message UploadFile {
    int32 file_id = 1;
    string file_name = 2;
    string file_path_name = 3;
    string initial_request_length = 4;
    string origin = 5;
    string x_upload_content_type = 6;
    string x_upload_content_length = 7;
    string file_md5 = 8;
  }
  repeated UploadFile upload_files = 1;
  int64 model_id = 2;
  string model_type = 3;
  string revision = 5;
  repeated string regenerates = 4;
}

message GenerateUploadFilesUrlResponse {

  message GenerateUploadUrl {
    int32 file_id = 1;
    string session_url = 2;
    string file_name = 3;
    string file_md5 = 4;
  }
  repeated GenerateUploadUrl data = 1;
}

message CompleteUploadFile {
  int64 id = 1;
  string model_type = 3;
  int64 model_id = 4;
  string revision = 15;
  string file_name = 5;
  string file_path = 6;
  bool include_in_submission = 7;
  string file_md5 = 9;
  string extension = 11;
  repeated CompleteFileAddition file_additions = 12;
  string appendix = 13;
  repeated int64 related_component_ids = 14;
}

message CompleteFileAddition {
  int64 file_type_id = 1;
  google.protobuf.Timestamp expiration_date = 2;
  string remark = 3;
}

message CompleteUploadFilesUrlRequest {
  repeated CompleteUploadFile complete_upload_files = 1;
  common.Language lang = 2;
  string model_type = 3;
  int64 model_id = 4;
  string revision = 5;
}

message CompleteUploadFilesUrlResponse {
  bool success = 1;
}

message OptionGenerateDownloadSignedUrlRequest{
  int32 ExpiryInMinutes = 1;
}

message GenerateDownloadSignedUrlRequest {
  string file_path = 1;
  OptionGenerateDownloadSignedUrlRequest Option = 2;
}

message GenerateDownloadSignedUrlResponse {
  string signed_url = 2;
}

message GenerateDownloadSignedUrlFromEmatSupplierResponse {
  string signed_url = 1;
  string error = 2;
}

message GenerateDownloadMaterialSubmissionFileListRequest {
  repeated int64 file_ids = 1;
  common.Language lang = 2;
}

message GenerateDownloadMaterialSubmissionFileListResponse {
  bool success = 1;
  repeated SignedUrl files = 2;
}

message SignedUrl {
  int64 file_id = 1;
  string signed_url = 2;
}

message ReceiveFileRequest {
  string public_download_link = 1;
  string original_file_name = 2;
  int64 model_id = 3;
  string model_type = 4;
  string revision = 11;
  common.Language lang = 5;
  string save_file_name = 6;
  string extension = 7;
  string extra_info = 8;
  int64 file_type_id = 9;
  bool isIncludedInSubmission = 10;
}

message DownloadFromUrlAndSaveResponse {
  bool success = 1;
}

message ConvertFilesToStandardRequest {
  common.Language lang = 1;
  int64 model_id = 2;
  string model_type = 3;
  string revision = 6;
  repeated int64 file_ids = 4;
  bool convert_immediate = 5;
}

message ConvertFilesToStandardResponse {
  bool success = 1;
}

message ValidatePdfFilesRequest {
  common.Language lang = 1;
  int64 model_id = 2;
  string model_type = 3;
  string revision = 5;
  repeated int64 file_ids = 4;
}

message ValidatePdfFilesResponse {
  bool success = 1;
}

message GetFileListRequest {
  int64 model_id = 1;
  string model_type = 2;
  string revision = 3;
}

message GetFileListResponse {
  repeated File data = 1;
}

message GetMultiFileListRequest {
  repeated int64 model_ids = 1;
  string model_type = 2;
  string revision = 4;
  google.protobuf.Timestamp created_at = 3;
}

message GetMultiFileListResponse {
  repeated File data = 1;
}

message CreateFilesRequest {
  int64 model_id = 1;
  string model_type = 2;
  string revision = 4;
  repeated File files = 3;
}

message CreateFilesResponse {
  repeated File files = 1;
}

message UpdateFilesRequest {
  int64 model_id = 1;
  string model_type = 2;
  string revision = 5;
  repeated File files = 3;
  common.Language lang = 4;
}

message UpdateFilesResponse {
  repeated File files = 1;
}

message DeleteFileRequest {
  int64 file_id = 1;
  common.Language lang = 2;
}
message DeleteFileResponse {
}

message CreateFileRecordRequest {
  string file_path = 2;
  string file_name = 3;
  int64 file_type_id = 4;
  string model_type = 5;
  int64 model_id = 6;
  string revision = 7;
}

message CreateFileRecordResponse {
  int64 id = 1;
  string file_path = 2;
  string file_name = 3;
  int64 file_type_id = 4;
  string model_type = 5;
  int64 model_id = 6;
  string revision = 7;
}

message GetFileRecordRequest {
  int64 id = 1;
}

message GetFileRecordResponse {
  int64 id = 1;
  string file_path = 2;
  string file_name = 3;
  int64 file_type_id = 4;
  string model_type = 5;
  int64 model_id = 6;
  string revision = 7;
}