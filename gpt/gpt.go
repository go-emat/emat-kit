package gpt

import (
	"errors"

	"gopkg.in/resty.v1"
)

const (
	GptEngineAaiKms = 1
)

type GptService struct {
	// The variable type of "gptEngine" should be an abstract type (interface) and the gpt engine service should be compatible to this abstract type
	// So that GptService can be a blackbox to programmer for providing required functions to call while it can call different gpt engine service to complete the tasks
	// But now only AAI KMS gpt engine is available to eMat, so skip this program structure design for saving the development time
	gptEngine *AaiKmsService
}

func NewGptService(requestorJwt string, authDomain string, gptEngineID ...int) (*GptService, error) {
	selectedGptEngine := GptEngineAaiKms // Default engine
	if len(gptEngineID) > 1 {
		selectedGptEngine = gptEngineID[0]
	}

	var err error = nil
	gptService := &GptService{gptEngine: nil}
	switch selectedGptEngine {
	case GptEngineAaiKms:
		err = gptService.connectAaiKms(requestorJwt, authDomain)
	default:
		err = errors.New("unknown GPT engine")
	}

	return gptService, err
}

func (s *GptService) connectAaiKms(requestorJwt string, authDomain string) (err error) {
	s.gptEngine, err = NewAaiKmsService(requestorJwt, authDomain)
	return err
}

func (s *GptService) isGtpEngineReady() error {
	if s.gptEngine != nil {
		return nil
	}
	return errors.New("GPT engine is not ready")
}

func (s *GptService) GetGptEngineName() string {
	if s.gptEngine != nil {
		return s.gptEngine.GetGptEngineName()
	}
	return ""
}

func (s *GptService) EnhanceGrammar(requestID, requestorID, source string) (*resty.Response, error) {
	if err := s.isGtpEngineReady(); err != nil {
		return nil, err
	}
	return s.gptEngine.EnhanceGrammar(requestID, requestorID, source)
}

func (s *GptService) ChatByPrompt(requestID, requestorID, prompt string) (*resty.Response, error) {
	if err := s.isGtpEngineReady(); err != nil {
		return nil, err
	}
	return s.gptEngine.ChatByPrompt(requestID, requestorID, prompt)
}

func (s *GptService) DocumentQueryExtract(requestID, requestorID, wtt, section string, srchArray []string) (*resty.Response, error) {
	if err := s.isGtpEngineReady(); err != nil {
		return nil, err
	}
	return s.gptEngine.DocumentQueryExtract(requestID, requestorID, wtt, section, srchArray)
}

func (s *GptService) DocumentQueryPrompt(requestID, requestorID, wtt, section, chat string, docList []string) (*resty.Response, error) {
	if err := s.isGtpEngineReady(); err != nil {
		return nil, err
	}
	return s.gptEngine.DocumentQueryPrompt(requestID, requestorID, wtt, section, chat, docList)
}

func (s *GptService) UserPromptConversation(requestID, requestorID, wtt, section, chat string) (*resty.Response, error) {
	if err := s.isGtpEngineReady(); err != nil {
		return nil, err
	}
	return s.gptEngine.UserPromptConversation(requestID, requestorID, wtt, section, chat)
}

func (s *GptService) UserPromptConversationWithoutWorkTrade(requestID, requestorID, section, chat string) (*resty.Response, error) {
	if err := s.isGtpEngineReady(); err != nil {
		return nil, err
	}
	return s.gptEngine.UserPromptConversationWithoutWorkTrade(requestID, requestorID, section, chat)
}

func (s *GptService) RestartConversation(requestID, requestorID string) (*resty.Response, error) {
	if err := s.isGtpEngineReady(); err != nil {
		return nil, err
	}
	return s.gptEngine.RestartConversation(requestID, requestorID)
}

func (s *GptService) RetrieveKmsSharePointPathUrl(encrSubmissionKey, accountId, section string) (*resty.Response, error) {
	if err := s.isGtpEngineReady(); err != nil {
		return nil, err
	}
	return s.gptEngine.RetrieveKmsSharePointPathUrl(encrSubmissionKey, accountId, section)
}
