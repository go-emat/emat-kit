package paginator

import (
	"context"
	"fmt"
	"math"
	"strings"
	"time"

	"gorm.io/gorm"
	// gormClause "gorm.io/gorm/clause"
)

const DefaultPaginationPageSize = 30

type PaginationRequest struct {
	CurrentPage  int32
	PageSize     int32
	OrderBy      []string
	UnescOrderBy []string
}

type MataPaginator struct {
	TotalRecord int32
	TotalPage   int32
	From        int32
	To          int32
	Offset      int32
	PageSize    int32
	CurrentPage int32
	PrevPage    int32
	NextPage    int32
}

type PaginationFilter struct {
	DB                *gorm.DB
	paginationRequest *PaginationRequest
	debug             bool
}

// NewPaginationFilter return PaginationFilter
func NewPaginationFilter(database *gorm.DB, request *PaginationRequest, debug bool) *PaginationFilter {
	if debug {
		database = database.Debug()
	}
	if request == nil {
		request = &PaginationRequest{}
	}
	paginationFilter := &PaginationFilter{
		DB:                database,
		paginationRequest: request,
	}
	paginationFilter.initialPagination()
	return paginationFilter
}

func (p *PaginationFilter) Select(query interface{}, args ...interface{}) {
	p.DB = p.DB.Select(query, args...)
}

func (p *PaginationFilter) Where(query interface{}, args ...interface{}) {
	p.DB = p.DB.Where(query, args)
}

func (p *PaginationFilter) Not(query interface{}, args ...interface{}) {
	p.DB = p.DB.Not(query, args)
}

func (p *PaginationFilter) Or(query interface{}, args ...interface{}) {
	p.DB = p.DB.Or(query, args)
}

func (p *PaginationFilter) Joins(query string, args ...interface{}) {
	p.DB = p.DB.Joins(query, args)
}

func (p *PaginationFilter) Preload(column string) {
	p.DB = p.DB.Preload(column)
}

func (p *PaginationFilter) Table(name string) {
	p.DB = p.DB.Table(name)
}

// Paginate getPaginator
func (p *PaginationFilter) Paginate(rows interface{}) *MataPaginator {
	done := make(chan bool, 1)
	var count int64
	var offset int32

	currentPage := p.paginationRequest.CurrentPage
	pageSize := p.paginationRequest.PageSize
	//create runtime to get totalRecord
	ctx, _ := context.WithTimeout(context.Background(), 3*time.Second)
	ctxDB := p.DB.WithContext(ctx)
	p.countRecords(ctxDB, done, &count)

	if currentPage == 1 || pageSize == -1 {
		offset = 0
	} else {
		offset = (currentPage - 1) * pageSize
	}
	p.DB.Limit(int(pageSize)).Offset(int(offset)).Find(rows)
	<-done

	return p.getMataPaginator(count, currentPage, offset, pageSize)
}

func (p *PaginationFilter) Scan(rows interface{}) *MataPaginator {
	done := make(chan bool, 1)
	var count int64
	var offset int32

	//create runtime to get totalRecord
	ctx, _ := context.WithTimeout(context.Background(), 3*time.Second)
	ctxDB := p.DB.WithContext(ctx)
	p.countRecords(ctxDB, done, &count)

	currentPage := p.paginationRequest.CurrentPage
	pageSize := p.paginationRequest.PageSize
	if currentPage == 1 || pageSize == -1 {
		offset = 0
	} else {
		offset = (currentPage - 1) * pageSize
	}
	p.DB.Limit(int(pageSize)).Offset(int(offset)).Scan(rows)
	<-done
	return p.getMataPaginator(count, currentPage, offset, pageSize)
}

func (p *PaginationFilter) getMataPaginator(count int64, currentPage, offset, pageSize int32) *MataPaginator {
	mataPaginator := &MataPaginator{}
	mataPaginator.TotalRecord = int32(count)
	mataPaginator.CurrentPage = currentPage

	mataPaginator.Offset = offset
	mataPaginator.PageSize = pageSize
	mataPaginator.TotalPage = int32(math.Ceil(float64(count) / float64(pageSize)))
	if pageSize == -1 {
		mataPaginator.TotalPage = 1
	}
	if currentPage > 1 {
		mataPaginator.PrevPage = currentPage - 1
	} else {
		mataPaginator.PrevPage = currentPage
	}
	if currentPage == mataPaginator.TotalPage {
		mataPaginator.NextPage = currentPage
	} else {
		mataPaginator.NextPage = currentPage + 1
	}
	mataPaginator.From = mataPaginator.Offset + 1
	mataPaginator.To = mataPaginator.Offset + mataPaginator.PageSize
	if mataPaginator.To > mataPaginator.TotalRecord || pageSize == -1 {
		mataPaginator.To = mataPaginator.TotalRecord
	}
	return mataPaginator
}

// initialPagination
func (p *PaginationFilter) initialPagination() {

	if p.paginationRequest.CurrentPage < 1 {
		p.paginationRequest.CurrentPage = 1
	}
	if p.paginationRequest.PageSize == 0 {
		p.paginationRequest.PageSize = DefaultPaginationPageSize
	}
	sortRequests := p.paginationRequest.OrderBy
	if len(sortRequests) > 0 {
		for sortRequestCounter, sortRequest := range sortRequests {
			sorts := strings.Split(strings.TrimSpace(sortRequest), ",")
			for sortCounter, sort := range sorts {
				components := strings.Split(strings.TrimSpace(sort), " ")
				for componentCounter, component := range components {
					if strings.ToLower(component) != "asc" && strings.ToLower(component) != "desc" {
						components[componentCounter] = fmt.Sprint("`", strings.Replace(component, ".", "`.`", -1), "`")
					}
				}
				sorts[sortCounter] = strings.Join(components, " ")
			}
			sortRequests[sortRequestCounter] = strings.Join(sorts, ",")
		}
	}
	sortRequests = append(sortRequests, p.paginationRequest.UnescOrderBy...)
	if len(sortRequests) > 0 {
		for _, sortRequest := range sortRequests {
			p.DB = p.DB.Order(sortRequest)
		}
	} else {
		/*
			blnExistingOrderBy := false
			orderByClauseName := gormClause.OrderBy{}.Name()
			for _, clause := range p.DB.Statement.Clauses {
				if clause.Name == orderByClauseName {
					blnExistingOrderBy = true
					break
				}
			}
			if !blnExistingOrderBy {
				p.DB = p.DB.Order("created_at desc")
			}
		*/
		p.DB = p.DB.Order("created_at desc")
	}
}

// countRecords count records, method Chain Safety/Goroutine Safety
func (p *PaginationFilter) countRecords(ctxDB *gorm.DB, done chan bool, count *int64) {
	ctxDB.Count(count)
	done <- true
}
