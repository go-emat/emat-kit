// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package documentpb

import (
	context "context"
	commonpb "gitlab.com/go-emat/emat-kit/api/commonpb"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// RequestPurchaseOrderServiceClient is the client API for RequestPurchaseOrderService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type RequestPurchaseOrderServiceClient interface {
	CreateRequestPurchaseOrder(ctx context.Context, in *CreateRequestPurchaseOrderRequest, opts ...grpc.CallOption) (*CreateRequestPurchaseOrderResponse, error)
	ReadRequestPurchaseOrder(ctx context.Context, in *ReadRequestPurchaseOrderRequest, opts ...grpc.CallOption) (*ReadRequestPurchaseOrderResponse, error)
	ProcessRequestPurchaseOrder(ctx context.Context, in *ProcessRequestPurchaseOrderRequest, opts ...grpc.CallOption) (*ProcessRequestPurchaseOrderResponse, error)
	RejectRequestPurchaseOrder(ctx context.Context, in *RejectRequestPurchaseOrderRequest, opts ...grpc.CallOption) (*SimpleResponse, error)
	ListRequestPurchaseOrders(ctx context.Context, in *ListRequestPurchaseOrdersRequest, opts ...grpc.CallOption) (*ListRequestPurchaseOrdersResponse, error)
	CreateOrUpdateRequestPurchaseOrder(ctx context.Context, in *CreateOrUpdateRequestPurchaseOrderRequest, opts ...grpc.CallOption) (*CreateOrUpdateRequestPurchaseOrderResponse, error)
	WithdrawRequestPurchaseOrder(ctx context.Context, in *WithdrawRequestPurchaseOrderRequest, opts ...grpc.CallOption) (*SimpleResponse, error)
	ListPOOptions(ctx context.Context, in *commonpb.OptionsRequest, opts ...grpc.CallOption) (*ListPOOptionsResponse, error)
	BuyerReviseRequestPurchaseOrder(ctx context.Context, in *BuyerReviseRequestPurchaseOrderRequest, opts ...grpc.CallOption) (*SimpleResponse, error)
	GetSupplierInfo(ctx context.Context, in *SupplierInfoRequest, opts ...grpc.CallOption) (*SupplierInfoResponse, error)
}

type requestPurchaseOrderServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewRequestPurchaseOrderServiceClient(cc grpc.ClientConnInterface) RequestPurchaseOrderServiceClient {
	return &requestPurchaseOrderServiceClient{cc}
}

func (c *requestPurchaseOrderServiceClient) CreateRequestPurchaseOrder(ctx context.Context, in *CreateRequestPurchaseOrderRequest, opts ...grpc.CallOption) (*CreateRequestPurchaseOrderResponse, error) {
	out := new(CreateRequestPurchaseOrderResponse)
	err := c.cc.Invoke(ctx, "/supplier.RequestPurchaseOrderService/CreateRequestPurchaseOrder", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *requestPurchaseOrderServiceClient) ReadRequestPurchaseOrder(ctx context.Context, in *ReadRequestPurchaseOrderRequest, opts ...grpc.CallOption) (*ReadRequestPurchaseOrderResponse, error) {
	out := new(ReadRequestPurchaseOrderResponse)
	err := c.cc.Invoke(ctx, "/supplier.RequestPurchaseOrderService/ReadRequestPurchaseOrder", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *requestPurchaseOrderServiceClient) ProcessRequestPurchaseOrder(ctx context.Context, in *ProcessRequestPurchaseOrderRequest, opts ...grpc.CallOption) (*ProcessRequestPurchaseOrderResponse, error) {
	out := new(ProcessRequestPurchaseOrderResponse)
	err := c.cc.Invoke(ctx, "/supplier.RequestPurchaseOrderService/ProcessRequestPurchaseOrder", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *requestPurchaseOrderServiceClient) RejectRequestPurchaseOrder(ctx context.Context, in *RejectRequestPurchaseOrderRequest, opts ...grpc.CallOption) (*SimpleResponse, error) {
	out := new(SimpleResponse)
	err := c.cc.Invoke(ctx, "/supplier.RequestPurchaseOrderService/RejectRequestPurchaseOrder", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *requestPurchaseOrderServiceClient) ListRequestPurchaseOrders(ctx context.Context, in *ListRequestPurchaseOrdersRequest, opts ...grpc.CallOption) (*ListRequestPurchaseOrdersResponse, error) {
	out := new(ListRequestPurchaseOrdersResponse)
	err := c.cc.Invoke(ctx, "/supplier.RequestPurchaseOrderService/ListRequestPurchaseOrders", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *requestPurchaseOrderServiceClient) CreateOrUpdateRequestPurchaseOrder(ctx context.Context, in *CreateOrUpdateRequestPurchaseOrderRequest, opts ...grpc.CallOption) (*CreateOrUpdateRequestPurchaseOrderResponse, error) {
	out := new(CreateOrUpdateRequestPurchaseOrderResponse)
	err := c.cc.Invoke(ctx, "/supplier.RequestPurchaseOrderService/CreateOrUpdateRequestPurchaseOrder", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *requestPurchaseOrderServiceClient) WithdrawRequestPurchaseOrder(ctx context.Context, in *WithdrawRequestPurchaseOrderRequest, opts ...grpc.CallOption) (*SimpleResponse, error) {
	out := new(SimpleResponse)
	err := c.cc.Invoke(ctx, "/supplier.RequestPurchaseOrderService/WithdrawRequestPurchaseOrder", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *requestPurchaseOrderServiceClient) ListPOOptions(ctx context.Context, in *commonpb.OptionsRequest, opts ...grpc.CallOption) (*ListPOOptionsResponse, error) {
	out := new(ListPOOptionsResponse)
	err := c.cc.Invoke(ctx, "/supplier.RequestPurchaseOrderService/ListPOOptions", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *requestPurchaseOrderServiceClient) BuyerReviseRequestPurchaseOrder(ctx context.Context, in *BuyerReviseRequestPurchaseOrderRequest, opts ...grpc.CallOption) (*SimpleResponse, error) {
	out := new(SimpleResponse)
	err := c.cc.Invoke(ctx, "/supplier.RequestPurchaseOrderService/BuyerReviseRequestPurchaseOrder", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *requestPurchaseOrderServiceClient) GetSupplierInfo(ctx context.Context, in *SupplierInfoRequest, opts ...grpc.CallOption) (*SupplierInfoResponse, error) {
	out := new(SupplierInfoResponse)
	err := c.cc.Invoke(ctx, "/supplier.RequestPurchaseOrderService/GetSupplierInfo", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// RequestPurchaseOrderServiceServer is the server API for RequestPurchaseOrderService service.
// All implementations must embed UnimplementedRequestPurchaseOrderServiceServer
// for forward compatibility
type RequestPurchaseOrderServiceServer interface {
	CreateRequestPurchaseOrder(context.Context, *CreateRequestPurchaseOrderRequest) (*CreateRequestPurchaseOrderResponse, error)
	ReadRequestPurchaseOrder(context.Context, *ReadRequestPurchaseOrderRequest) (*ReadRequestPurchaseOrderResponse, error)
	ProcessRequestPurchaseOrder(context.Context, *ProcessRequestPurchaseOrderRequest) (*ProcessRequestPurchaseOrderResponse, error)
	RejectRequestPurchaseOrder(context.Context, *RejectRequestPurchaseOrderRequest) (*SimpleResponse, error)
	ListRequestPurchaseOrders(context.Context, *ListRequestPurchaseOrdersRequest) (*ListRequestPurchaseOrdersResponse, error)
	CreateOrUpdateRequestPurchaseOrder(context.Context, *CreateOrUpdateRequestPurchaseOrderRequest) (*CreateOrUpdateRequestPurchaseOrderResponse, error)
	WithdrawRequestPurchaseOrder(context.Context, *WithdrawRequestPurchaseOrderRequest) (*SimpleResponse, error)
	ListPOOptions(context.Context, *commonpb.OptionsRequest) (*ListPOOptionsResponse, error)
	BuyerReviseRequestPurchaseOrder(context.Context, *BuyerReviseRequestPurchaseOrderRequest) (*SimpleResponse, error)
	GetSupplierInfo(context.Context, *SupplierInfoRequest) (*SupplierInfoResponse, error)
	mustEmbedUnimplementedRequestPurchaseOrderServiceServer()
}

// UnimplementedRequestPurchaseOrderServiceServer must be embedded to have forward compatible implementations.
type UnimplementedRequestPurchaseOrderServiceServer struct {
}

func (UnimplementedRequestPurchaseOrderServiceServer) CreateRequestPurchaseOrder(context.Context, *CreateRequestPurchaseOrderRequest) (*CreateRequestPurchaseOrderResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateRequestPurchaseOrder not implemented")
}
func (UnimplementedRequestPurchaseOrderServiceServer) ReadRequestPurchaseOrder(context.Context, *ReadRequestPurchaseOrderRequest) (*ReadRequestPurchaseOrderResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ReadRequestPurchaseOrder not implemented")
}
func (UnimplementedRequestPurchaseOrderServiceServer) ProcessRequestPurchaseOrder(context.Context, *ProcessRequestPurchaseOrderRequest) (*ProcessRequestPurchaseOrderResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ProcessRequestPurchaseOrder not implemented")
}
func (UnimplementedRequestPurchaseOrderServiceServer) RejectRequestPurchaseOrder(context.Context, *RejectRequestPurchaseOrderRequest) (*SimpleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method RejectRequestPurchaseOrder not implemented")
}
func (UnimplementedRequestPurchaseOrderServiceServer) ListRequestPurchaseOrders(context.Context, *ListRequestPurchaseOrdersRequest) (*ListRequestPurchaseOrdersResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListRequestPurchaseOrders not implemented")
}
func (UnimplementedRequestPurchaseOrderServiceServer) CreateOrUpdateRequestPurchaseOrder(context.Context, *CreateOrUpdateRequestPurchaseOrderRequest) (*CreateOrUpdateRequestPurchaseOrderResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateOrUpdateRequestPurchaseOrder not implemented")
}
func (UnimplementedRequestPurchaseOrderServiceServer) WithdrawRequestPurchaseOrder(context.Context, *WithdrawRequestPurchaseOrderRequest) (*SimpleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method WithdrawRequestPurchaseOrder not implemented")
}
func (UnimplementedRequestPurchaseOrderServiceServer) ListPOOptions(context.Context, *commonpb.OptionsRequest) (*ListPOOptionsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListPOOptions not implemented")
}
func (UnimplementedRequestPurchaseOrderServiceServer) BuyerReviseRequestPurchaseOrder(context.Context, *BuyerReviseRequestPurchaseOrderRequest) (*SimpleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method BuyerReviseRequestPurchaseOrder not implemented")
}
func (UnimplementedRequestPurchaseOrderServiceServer) GetSupplierInfo(context.Context, *SupplierInfoRequest) (*SupplierInfoResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetSupplierInfo not implemented")
}
func (UnimplementedRequestPurchaseOrderServiceServer) mustEmbedUnimplementedRequestPurchaseOrderServiceServer() {
}

// UnsafeRequestPurchaseOrderServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to RequestPurchaseOrderServiceServer will
// result in compilation errors.
type UnsafeRequestPurchaseOrderServiceServer interface {
	mustEmbedUnimplementedRequestPurchaseOrderServiceServer()
}

func RegisterRequestPurchaseOrderServiceServer(s grpc.ServiceRegistrar, srv RequestPurchaseOrderServiceServer) {
	s.RegisterService(&_RequestPurchaseOrderService_serviceDesc, srv)
}

func _RequestPurchaseOrderService_CreateRequestPurchaseOrder_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateRequestPurchaseOrderRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RequestPurchaseOrderServiceServer).CreateRequestPurchaseOrder(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/supplier.RequestPurchaseOrderService/CreateRequestPurchaseOrder",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RequestPurchaseOrderServiceServer).CreateRequestPurchaseOrder(ctx, req.(*CreateRequestPurchaseOrderRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RequestPurchaseOrderService_ReadRequestPurchaseOrder_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ReadRequestPurchaseOrderRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RequestPurchaseOrderServiceServer).ReadRequestPurchaseOrder(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/supplier.RequestPurchaseOrderService/ReadRequestPurchaseOrder",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RequestPurchaseOrderServiceServer).ReadRequestPurchaseOrder(ctx, req.(*ReadRequestPurchaseOrderRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RequestPurchaseOrderService_ProcessRequestPurchaseOrder_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ProcessRequestPurchaseOrderRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RequestPurchaseOrderServiceServer).ProcessRequestPurchaseOrder(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/supplier.RequestPurchaseOrderService/ProcessRequestPurchaseOrder",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RequestPurchaseOrderServiceServer).ProcessRequestPurchaseOrder(ctx, req.(*ProcessRequestPurchaseOrderRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RequestPurchaseOrderService_RejectRequestPurchaseOrder_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RejectRequestPurchaseOrderRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RequestPurchaseOrderServiceServer).RejectRequestPurchaseOrder(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/supplier.RequestPurchaseOrderService/RejectRequestPurchaseOrder",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RequestPurchaseOrderServiceServer).RejectRequestPurchaseOrder(ctx, req.(*RejectRequestPurchaseOrderRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RequestPurchaseOrderService_ListRequestPurchaseOrders_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListRequestPurchaseOrdersRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RequestPurchaseOrderServiceServer).ListRequestPurchaseOrders(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/supplier.RequestPurchaseOrderService/ListRequestPurchaseOrders",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RequestPurchaseOrderServiceServer).ListRequestPurchaseOrders(ctx, req.(*ListRequestPurchaseOrdersRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RequestPurchaseOrderService_CreateOrUpdateRequestPurchaseOrder_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateOrUpdateRequestPurchaseOrderRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RequestPurchaseOrderServiceServer).CreateOrUpdateRequestPurchaseOrder(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/supplier.RequestPurchaseOrderService/CreateOrUpdateRequestPurchaseOrder",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RequestPurchaseOrderServiceServer).CreateOrUpdateRequestPurchaseOrder(ctx, req.(*CreateOrUpdateRequestPurchaseOrderRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RequestPurchaseOrderService_WithdrawRequestPurchaseOrder_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(WithdrawRequestPurchaseOrderRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RequestPurchaseOrderServiceServer).WithdrawRequestPurchaseOrder(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/supplier.RequestPurchaseOrderService/WithdrawRequestPurchaseOrder",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RequestPurchaseOrderServiceServer).WithdrawRequestPurchaseOrder(ctx, req.(*WithdrawRequestPurchaseOrderRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RequestPurchaseOrderService_ListPOOptions_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(commonpb.OptionsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RequestPurchaseOrderServiceServer).ListPOOptions(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/supplier.RequestPurchaseOrderService/ListPOOptions",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RequestPurchaseOrderServiceServer).ListPOOptions(ctx, req.(*commonpb.OptionsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RequestPurchaseOrderService_BuyerReviseRequestPurchaseOrder_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(BuyerReviseRequestPurchaseOrderRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RequestPurchaseOrderServiceServer).BuyerReviseRequestPurchaseOrder(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/supplier.RequestPurchaseOrderService/BuyerReviseRequestPurchaseOrder",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RequestPurchaseOrderServiceServer).BuyerReviseRequestPurchaseOrder(ctx, req.(*BuyerReviseRequestPurchaseOrderRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RequestPurchaseOrderService_GetSupplierInfo_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SupplierInfoRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RequestPurchaseOrderServiceServer).GetSupplierInfo(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/supplier.RequestPurchaseOrderService/GetSupplierInfo",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RequestPurchaseOrderServiceServer).GetSupplierInfo(ctx, req.(*SupplierInfoRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _RequestPurchaseOrderService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "supplier.RequestPurchaseOrderService",
	HandlerType: (*RequestPurchaseOrderServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateRequestPurchaseOrder",
			Handler:    _RequestPurchaseOrderService_CreateRequestPurchaseOrder_Handler,
		},
		{
			MethodName: "ReadRequestPurchaseOrder",
			Handler:    _RequestPurchaseOrderService_ReadRequestPurchaseOrder_Handler,
		},
		{
			MethodName: "ProcessRequestPurchaseOrder",
			Handler:    _RequestPurchaseOrderService_ProcessRequestPurchaseOrder_Handler,
		},
		{
			MethodName: "RejectRequestPurchaseOrder",
			Handler:    _RequestPurchaseOrderService_RejectRequestPurchaseOrder_Handler,
		},
		{
			MethodName: "ListRequestPurchaseOrders",
			Handler:    _RequestPurchaseOrderService_ListRequestPurchaseOrders_Handler,
		},
		{
			MethodName: "CreateOrUpdateRequestPurchaseOrder",
			Handler:    _RequestPurchaseOrderService_CreateOrUpdateRequestPurchaseOrder_Handler,
		},
		{
			MethodName: "WithdrawRequestPurchaseOrder",
			Handler:    _RequestPurchaseOrderService_WithdrawRequestPurchaseOrder_Handler,
		},
		{
			MethodName: "ListPOOptions",
			Handler:    _RequestPurchaseOrderService_ListPOOptions_Handler,
		},
		{
			MethodName: "BuyerReviseRequestPurchaseOrder",
			Handler:    _RequestPurchaseOrderService_BuyerReviseRequestPurchaseOrder_Handler,
		},
		{
			MethodName: "GetSupplierInfo",
			Handler:    _RequestPurchaseOrderService_GetSupplierInfo_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "supplier/document/request_purchase_order.proto",
}
