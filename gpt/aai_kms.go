package gpt

import (
	"errors"
	"os"
	"time"

	"gopkg.in/resty.v1"
)

const (
	AaiKmsEngineResponseStatusOK          = "OK"
	AaiKmsEngineResponseStatusError       = "ERROR"
	AaiKmsEngineRestyDefaultRedirectValue = 10
	AaiKmsEngineRestyTimeoutInSecondValue = 180
	AaiKmsEngineDebug                     = true
)

const (
	EnhanceGrammarRequestPath             = "/openai/api/smm/enhance"
	ChatByPromptRequestPath               = "/openai/api/smm/chat"
	DocumentQueryExtractRequestPath       = "/openai/api/kms/document-query/extract"
	DocumentQueryPromptRequestPath        = "/openai/api/kms/document-query/query-prompt"
	UserPromptRequestPath                 = "/openai/api/kms/user-prompt/conversation"
	UserPromptWithoutWorkTradeRequestPath = "/openai/api/kms/user-prompt/conversation/without/work-trade"
	RestartConversationRequestPath        = "/openai/api/kms/conversation-clean/restart"
	RetrieveKmsSharePointPathUrlPath      = "/openai/api/kms/sharepoint/file"
)

type AaiGraphJwtRespDataStruct struct {
	ID           int64  `json:"id"`
	Name         string `json:"name"`
	Description  string `json:"description"`
	Gateway      string `json:"gateway"`
	Token        string `json:"token"`
	RefreshToken string `json:"refresh_token"`
	ApiKey       string `json:"api_key"`
}

type AaiGraphJwtResp struct {
	Message string                    `json:"message"`
	Data    AaiGraphJwtRespDataStruct `json:"data"`
}

// For EnhanceGrammer() call
type EnhanceGrammerRequestSmmKmsStruct struct {
	ReqID     string `json:"req_id"`
	Token     string `json:"token"`
	Version   string `json:"version"`
	UserText  string `json:"user_text"`
	UserID    string `json:"user_id"`
	Timestamp string `json:"timestamp"`
}

// For EnhanceGrammer() call
type EnhanceGrammarRequest struct {
	SmmKms EnhanceGrammerRequestSmmKmsStruct `json:"SMMKMS"`
}

// For EnhanceGrammer() call
type EnhanceGrammarRespSmmKmsStruct struct {
	ReqID     string `json:"req_id"`
	RespText  string `json:"resp_text"`
	Status    string `json:"status"`
	Message   string `json:"message"`
	TimeStamp string `json:"timestamp"`
}

// For EnhanceGrammer() call
type EnhanceGrammarResp struct {
	SmmKms EnhanceGrammarRespSmmKmsStruct `json:"smmkms"`
}

// For GenerateByPrompt() call
type GenerateByPromptRequestSmmKmsStruct struct {
	ReqID     string `json:"req_id"`
	Token     string `json:"token"`
	Version   string `json:"version"`
	Prompt    string `json:"prompt"`
	UserID    string `json:"user_id"`
	Timestamp string `json:"timestamp"`
}

// For GenerateByPrompt() call
type GenerateByPromptRequest struct {
	SmmKms GenerateByPromptRequestSmmKmsStruct `json:"SMMKMS"`
}

// For GenerateByPrompt() call
type GenerateByPromptRespSmmKmsStruct struct {
	ReqID     string `json:"req_id"`
	RespText  string `json:"resp_text"`
	Status    string `json:"status"`
	Message   string `json:"message"`
	TimeStamp string `json:"timestamp"`
}

// For GenerateByPrompt() call
type GenerateByPromptResp struct {
	SmmKms GenerateByPromptRespSmmKmsStruct `json:"smmkms"`
}

type DocumentQueryExtractSmmKmsStruct struct {
	ReqID     string   `json:"req_id"`
	Token     string   `json:"token"`
	Version   string   `json:"version"`
	Wtt       string   `json:"wtt"`
	Section   string   `json:"section"`
	SrchArray []string `json:"srch_array"`
	UserID    string   `json:"user_id"`
	Timestamp string   `json:"timestamp"`
}

type DocumentQueryExtractRequest struct {
	SmmKms DocumentQueryExtractSmmKmsStruct `json:"SMMKMS"`
}

type DocumentQueryExtractResp struct {
	SmmKms DocumentQueryExtractSmmKmsRespStruct `json:"smmkms"`
}

type DocumentQueryExtractSmmKmsRespStruct struct {
	ReqID     string                                      `json:"req_id"`
	RespArray []*DocumentQueryExtractSmmKmsRespItemStruct `json:"resp_array"`
	Status    string                                      `json:"status"`
	Message   string                                      `json:"message"`
	Timestamp string                                      `json:"timestamp"`
}

type DocumentQueryExtractSmmKmsRespItemStruct struct {
	Resp            string `json:"resp"`
	ProjCode        string `json:"proj_code"`
	SubmissionTitle string `json:"submission_title"`
}

type DocumentQueryPromptSmmKmsStruct struct {
	ReqID     string   `json:"req_id"`
	Token     string   `json:"token"`
	Version   string   `json:"version"`
	Wtt       string   `json:"wtt"`
	Section   string   `json:"section"`
	Chat      string   `json:"chat"`
	DocList   []string `json:"doc_list"`
	UserID    string   `json:"user_id"`
	Timestamp string   `json:"timestamp"`
}

type DocumentQueryPromptSmmKmsRespStruct struct {
	ReqID     string `json:"req_id"`
	RespText  string `json:"resp_text"`
	Status    string `json:"status"`
	Message   string `json:"message"`
	Timestamp string `json:"timestamp"`
}

type DocumentQueryPromptRequest struct {
	SmmKms DocumentQueryPromptSmmKmsStruct `json:"SMMKMS"`
}

type DocumentQueryPromptResp struct {
	SmmKms DocumentQueryPromptSmmKmsRespStruct `json:"smmkms"`
}

type UserPromptSmmKmsStruct struct {
	ReqID      string `json:"req_id"`
	Token      string `json:"token"`
	Version    string `json:"version"`
	ReqText    string `json:"req_text"`
	Wtt        string `json:"wtt"`
	Section    string `json:"section"`
	UserID     string `json:"user_id"`
	GptVersion string `json:"gpt_version"`
	Timestamp  string `json:"timestamp"`
}

type UserPromptRequest struct {
	SmmKms UserPromptSmmKmsStruct `json:"SMMKMS"`
}

type UserPromptRespSmmKmsRespStruct struct {
	Paragraghp string `json:"paragraghp"`
	PointForm  string `json:"point_form"`
}

type UserPromptRespSmmKmsStruct struct {
	ReqId     string                         `json:"req_id"`
	RespObj   UserPromptRespSmmKmsRespStruct `json:"resp_obj"`
	Status    string                         `json:"status"`
	Message   string                         `json:"message"`
	Timestamp string                         `json:"timestamp"`
}

type UserPromptResp struct {
	SmmKms UserPromptRespSmmKmsStruct `json:"smmkms"`
}

type RestartConversationSmmKmsStruct struct {
	ReqID   string `json:"req_id"`
	Token   string `json:"token"`
	Version string `json:"version"`
	UserID  string `json:"user_id"`
}

type RestartConversationRequest struct {
	SmmKms RestartConversationSmmKmsStruct `json:"SMMKMS"`
}

type RestartConversationRespSmmKmsStruct struct {
	Id        string `json:"id"`
	Status    string `json:"status"`
	Message   string `json:"message"`
	Timestamp string `json:"timestamp"`
}

type RestartConversationResp struct {
	SmmKms RestartConversationRespSmmKmsStruct `json:"smmkms"`
}

type RetrieveKmsSharePointPathUrlReqStruct struct {
	ReqId   string `json:"req_id"`
	UserId  string `json:"user_id"`
	Section string `json:"section"`
}

type RetrieveKmsSharePointPathUrlRequest struct {
	SmmKms RetrieveKmsSharePointPathUrlReqStruct `json:"SMMKMS"`
}

type RetrieveKmsSharePointPathUrlRespStruct struct {
	ReqId   string `json:"req_id"`
	Message string `json:"message"`
}

type RetrieveKmsSharePointPathUrlResponse struct {
	SmmKms RetrieveKmsSharePointPathUrlRespStruct `json:"SMMKMS"`
}

type AaiKmsService struct {
	ModuleName   string
	RequestorJwt string
	AuthDomain   string
	AaiGraphJwt  string
	ApiGateway   string
	ApiKey       string
}

func NewAaiKmsService(requestorJwt string, authDomain string) (*AaiKmsService, error) {
	serverEnvironment := os.Getenv("SERVER_ENVIRONMENT")
	restyRequest := resty.New().
		SetDebug(AaiKmsEngineDebug).
		SetRedirectPolicy(resty.FlexibleRedirectPolicy(AaiKmsEngineRestyDefaultRedirectValue)).
		SetTimeout(AaiKmsEngineRestyTimeoutInSecondValue*time.Second).R().
		SetAuthToken(requestorJwt).
		SetHeader("Accept", "application/json").
		SetResult(AaiGraphJwtResp{})

	var resp *resty.Response
	var err error
	if serverEnvironment == "local" {
		resp, err = restyRequest.Get("http://mattex-auth:8087/token-exchange/aai/graph/jwt")
	} else {
		resp, err = restyRequest.Get(authDomain + "/token-exchange/aai/graph/jwt")
	}

	if err != nil {
		return nil, err
	}

	apiResult := resp.Result().(*AaiGraphJwtResp).Data
	if apiResult.Token == "" {
		return nil, errors.New("Cannot retrieve AAI graph JWT")
	}
	if apiResult.Gateway == "" {
		return nil, errors.New("Cannot retrieve AAI KMS API gateway")
	}

	return &AaiKmsService{
		ModuleName:   apiResult.Name,
		RequestorJwt: requestorJwt,
		AuthDomain:   authDomain,
		AaiGraphJwt:  apiResult.Token,
		ApiGateway:   apiResult.Gateway,
		ApiKey:       apiResult.ApiKey,
	}, nil
}

func (s *AaiKmsService) GetGptEngineName() string {
	return s.ModuleName
}

func (s *AaiKmsService) callApi(postBody interface{}, resultStruct interface{}, apiUrl string) (*resty.Response, error) {
	return resty.New().
		SetDebug(AaiKmsEngineDebug).
		SetRedirectPolicy(resty.FlexibleRedirectPolicy(AaiKmsEngineRestyDefaultRedirectValue)).
		SetTimeout(AaiKmsEngineRestyTimeoutInSecondValue*time.Second).R().
		SetAuthToken(s.AaiGraphJwt).
		SetHeader("Content-Type", "application/json").
		SetHeader("Accept", "application/json").
		SetHeader("apiKey", s.ApiKey).
		SetBody(postBody).
		SetResult(resultStruct).
		Post(s.ApiGateway + apiUrl)
}

func (s *AaiKmsService) EnhanceGrammar(requestID, requestorID, source string) (*resty.Response, error) {
	smmKmsData := EnhanceGrammerRequestSmmKmsStruct{
		ReqID:     requestID,
		Token:     s.RequestorJwt,
		Version:   "1.0",
		UserText:  source,
		UserID:    requestorID,
		Timestamp: time.Now().Format("2006-01-02 15:04:05"),
	}

	resp, err := s.callApi(EnhanceGrammarRequest{SmmKms: smmKmsData}, EnhanceGrammarResp{}, EnhanceGrammarRequestPath)
	return resp, err
}

func (s *AaiKmsService) ChatByPrompt(requestID, requestorID, prompt string) (*resty.Response, error) {
	smmKmsData := GenerateByPromptRequestSmmKmsStruct{
		ReqID:     requestID,
		Token:     s.RequestorJwt,
		Version:   "1.0",
		Prompt:    prompt,
		UserID:    requestorID,
		Timestamp: time.Now().Format("2006-01-02 15:04:05"),
	}

	resp, err := s.callApi(GenerateByPromptRequest{SmmKms: smmKmsData}, GenerateByPromptResp{}, ChatByPromptRequestPath)
	return resp, err
}

func (s *AaiKmsService) DocumentQueryExtract(requestID, requestorID, wtt, section string, srchArray []string) (*resty.Response, error) {
	smmKmsData := DocumentQueryExtractSmmKmsStruct{
		ReqID:     requestID,
		Token:     s.RequestorJwt,
		Version:   "1.0",
		Wtt:       wtt,
		Section:   section,
		SrchArray: srchArray,
		UserID:    requestorID,
		Timestamp: time.Now().Format("2006-01-02 15:04:05"),
	}

	resp, err := s.callApi(DocumentQueryExtractRequest{SmmKms: smmKmsData}, DocumentQueryExtractResp{}, DocumentQueryExtractRequestPath)

	return resp, err
}

func (s *AaiKmsService) DocumentQueryPrompt(requestID, requestorID, wtt, section, chat string, docList []string) (*resty.Response, error) {
	smmKmsData := DocumentQueryPromptSmmKmsStruct{
		ReqID:     requestID,
		Token:     s.RequestorJwt,
		Version:   "1.0",
		Wtt:       wtt,
		Section:   section,
		Chat:      chat,
		DocList:   docList,
		UserID:    requestorID,
		Timestamp: time.Now().Format("2006-01-02 15:04:05"),
	}

	resp, err := s.callApi(DocumentQueryPromptRequest{SmmKms: smmKmsData}, DocumentQueryPromptResp{}, DocumentQueryPromptRequestPath)
	return resp, err
}

func (s *AaiKmsService) UserPromptConversation(requestID, requestorID, wtt, section, reqText string) (*resty.Response, error) {
	smmKmsData := UserPromptSmmKmsStruct{
		ReqID:      requestID,
		Token:      s.RequestorJwt,
		Version:    "1.0",
		Wtt:        wtt,
		Section:    section,
		ReqText:    reqText,
		UserID:     requestorID,
		GptVersion: "GPT-4",
		Timestamp:  time.Now().Format("2006-01-02 15:04:05"),
	}

	resp, err := s.callApi(UserPromptRequest{SmmKms: smmKmsData}, UserPromptResp{}, UserPromptRequestPath)
	return resp, err
}

func (s *AaiKmsService) UserPromptConversationWithoutWorkTrade(requestID, requestorID, section, reqText string) (*resty.Response, error) {
	smmKmsData := UserPromptSmmKmsStruct{
		ReqID:      requestID,
		Token:      s.RequestorJwt,
		Version:    "1.0",
		Section:    section,
		ReqText:    reqText,
		UserID:     requestorID,
		GptVersion: "GPT-4",
		Timestamp:  time.Now().Format("2006-01-02 15:04:05"),
	}

	resp, err := s.callApi(UserPromptRequest{SmmKms: smmKmsData}, UserPromptResp{}, UserPromptWithoutWorkTradeRequestPath)
	return resp, err
}

func (s *AaiKmsService) RestartConversation(requestId, requestorID string) (*resty.Response, error) {
	smmKmsData := RestartConversationSmmKmsStruct{
		ReqID:   requestId,
		Token:   s.RequestorJwt,
		Version: "1.0",
		UserID:  requestorID,
	}

	resp, err := s.callApi(RestartConversationRequest{SmmKms: smmKmsData}, RestartConversationResp{}, RestartConversationRequestPath)
	return resp, err
}

func (s *AaiKmsService) RetrieveKmsSharePointPathUrl(encrSubmissionKey, accountId, section string) (*resty.Response, error) {
	smmKmsData := RetrieveKmsSharePointPathUrlReqStruct{
		ReqId:   encrSubmissionKey,
		UserId:  accountId,
		Section: section,
	}
	resp, err := s.callApi(RetrieveKmsSharePointPathUrlRequest{SmmKms: smmKmsData}, RetrieveKmsSharePointPathUrlResponse{}, RetrieveKmsSharePointPathUrlPath)
	return resp, err
}
