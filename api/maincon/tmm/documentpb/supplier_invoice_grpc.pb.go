// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package documentpb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// SupplierInvoiceServiceClient is the client API for SupplierInvoiceService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type SupplierInvoiceServiceClient interface {
	CreateSupplierInvoiceByTms(ctx context.Context, in *CreateSupplierInvoiceByTmsRequest, opts ...grpc.CallOption) (*CreateSupplierInvoiceByTmsResponse, error)
	CancelSupplierInvoiceByTms(ctx context.Context, in *CancelSupplierInvoiceByTmsRequest, opts ...grpc.CallOption) (*SimpleResponse, error)
}

type supplierInvoiceServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewSupplierInvoiceServiceClient(cc grpc.ClientConnInterface) SupplierInvoiceServiceClient {
	return &supplierInvoiceServiceClient{cc}
}

func (c *supplierInvoiceServiceClient) CreateSupplierInvoiceByTms(ctx context.Context, in *CreateSupplierInvoiceByTmsRequest, opts ...grpc.CallOption) (*CreateSupplierInvoiceByTmsResponse, error) {
	out := new(CreateSupplierInvoiceByTmsResponse)
	err := c.cc.Invoke(ctx, "/maincon.tmm.SupplierInvoiceService/CreateSupplierInvoiceByTms", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *supplierInvoiceServiceClient) CancelSupplierInvoiceByTms(ctx context.Context, in *CancelSupplierInvoiceByTmsRequest, opts ...grpc.CallOption) (*SimpleResponse, error) {
	out := new(SimpleResponse)
	err := c.cc.Invoke(ctx, "/maincon.tmm.SupplierInvoiceService/CancelSupplierInvoiceByTms", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// SupplierInvoiceServiceServer is the server API for SupplierInvoiceService service.
// All implementations must embed UnimplementedSupplierInvoiceServiceServer
// for forward compatibility
type SupplierInvoiceServiceServer interface {
	CreateSupplierInvoiceByTms(context.Context, *CreateSupplierInvoiceByTmsRequest) (*CreateSupplierInvoiceByTmsResponse, error)
	CancelSupplierInvoiceByTms(context.Context, *CancelSupplierInvoiceByTmsRequest) (*SimpleResponse, error)
	mustEmbedUnimplementedSupplierInvoiceServiceServer()
}

// UnimplementedSupplierInvoiceServiceServer must be embedded to have forward compatible implementations.
type UnimplementedSupplierInvoiceServiceServer struct {
}

func (UnimplementedSupplierInvoiceServiceServer) CreateSupplierInvoiceByTms(context.Context, *CreateSupplierInvoiceByTmsRequest) (*CreateSupplierInvoiceByTmsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateSupplierInvoiceByTms not implemented")
}
func (UnimplementedSupplierInvoiceServiceServer) CancelSupplierInvoiceByTms(context.Context, *CancelSupplierInvoiceByTmsRequest) (*SimpleResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CancelSupplierInvoiceByTms not implemented")
}
func (UnimplementedSupplierInvoiceServiceServer) mustEmbedUnimplementedSupplierInvoiceServiceServer() {
}

// UnsafeSupplierInvoiceServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to SupplierInvoiceServiceServer will
// result in compilation errors.
type UnsafeSupplierInvoiceServiceServer interface {
	mustEmbedUnimplementedSupplierInvoiceServiceServer()
}

func RegisterSupplierInvoiceServiceServer(s grpc.ServiceRegistrar, srv SupplierInvoiceServiceServer) {
	s.RegisterService(&_SupplierInvoiceService_serviceDesc, srv)
}

func _SupplierInvoiceService_CreateSupplierInvoiceByTms_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateSupplierInvoiceByTmsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SupplierInvoiceServiceServer).CreateSupplierInvoiceByTms(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.tmm.SupplierInvoiceService/CreateSupplierInvoiceByTms",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SupplierInvoiceServiceServer).CreateSupplierInvoiceByTms(ctx, req.(*CreateSupplierInvoiceByTmsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SupplierInvoiceService_CancelSupplierInvoiceByTms_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CancelSupplierInvoiceByTmsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SupplierInvoiceServiceServer).CancelSupplierInvoiceByTms(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.tmm.SupplierInvoiceService/CancelSupplierInvoiceByTms",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SupplierInvoiceServiceServer).CancelSupplierInvoiceByTms(ctx, req.(*CancelSupplierInvoiceByTmsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _SupplierInvoiceService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "maincon.tmm.SupplierInvoiceService",
	HandlerType: (*SupplierInvoiceServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateSupplierInvoiceByTms",
			Handler:    _SupplierInvoiceService_CreateSupplierInvoiceByTms_Handler,
		},
		{
			MethodName: "CancelSupplierInvoiceByTms",
			Handler:    _SupplierInvoiceService_CancelSupplierInvoiceByTms_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "maincon/tmm/document/supplier_invoice.proto",
}
