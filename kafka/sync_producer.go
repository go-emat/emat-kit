package kafka

import (
	"fmt"
	"github.com/Shopify/sarama"
	"log"
)

// Producer Kafka Async Producer client
type EmatProducer struct {
	syncProducer sarama.SyncProducer
	encoder      Encoder
	cfg          *ProducerConfig
}

// NewEmatProducer create new Producer instance
func NewEmatProducer(cfg *ProducerConfig) (*EmatProducer, error) {
	config := NewKafkaProducerConfig(cfg)
	syncProducer, err := sarama.NewSyncProducer(cfg.Brokers, config)
	if err != nil {
		log.Printf("Kafka init async Producer err: %v", err)
		return nil, err
	}
	producer := &EmatProducer{
		syncProducer: syncProducer,
		encoder:      JSONEncoder(),
		cfg:          cfg,
	}
	log.Println(cfg.Brokers)
	log.Println("Kafka Sync Producer init success")
	return producer, nil
}

// Push pubsh message to kafka, can be a JSON string.
//
// Attention that if config.Partitioner is set HashPartitioner, then all message will be
// pushed to the only ONE partition.
func (p *EmatProducer) SendMessage(topic, key string, value []byte) (partition int32, offset int64, err error) {
	if p.syncProducer == nil {
		return 0, 0, fmt.Errorf("Kafka sync Producer not init")
	}
	pMsg := &sarama.ProducerMessage{
		Topic: topic,
		Key:   sarama.ByteEncoder(key),
		Value: sarama.ByteEncoder(value),
	}
	return p.syncProducer.SendMessage(pMsg)
}

func (p *EmatProducer) Close() error {
	var err error
	if p.syncProducer == nil {
		return err
	}
	if err = p.syncProducer.Close(); err != nil {
		log.Printf("Kafka sync Producer: close fail, %v", err)
	}
	return err
}

func (p *EmatProducer) getID(topic, key string) string {
	return fmt.Sprintf("topic[%s]-key[%s]", topic, key)
}

// SetEncoder set encoder for produced message
func (p *EmatProducer) SetEncoder(encoder Encoder) {
	p.encoder = encoder
}
