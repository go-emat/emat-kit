// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package submissionpb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// KnowledgeServiceClient is the client API for KnowledgeService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type KnowledgeServiceClient interface {
	ListKnowledge(ctx context.Context, in *ListKnowledgeRequest, opts ...grpc.CallOption) (*ListKnowledgeResponse, error)
	ReadKnowledge(ctx context.Context, in *IdRequest, opts ...grpc.CallOption) (*ReadKnowledgeResponse, error)
}

type knowledgeServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewKnowledgeServiceClient(cc grpc.ClientConnInterface) KnowledgeServiceClient {
	return &knowledgeServiceClient{cc}
}

func (c *knowledgeServiceClient) ListKnowledge(ctx context.Context, in *ListKnowledgeRequest, opts ...grpc.CallOption) (*ListKnowledgeResponse, error) {
	out := new(ListKnowledgeResponse)
	err := c.cc.Invoke(ctx, "/maincon.mmm.submission.KnowledgeService/ListKnowledge", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *knowledgeServiceClient) ReadKnowledge(ctx context.Context, in *IdRequest, opts ...grpc.CallOption) (*ReadKnowledgeResponse, error) {
	out := new(ReadKnowledgeResponse)
	err := c.cc.Invoke(ctx, "/maincon.mmm.submission.KnowledgeService/ReadKnowledge", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// KnowledgeServiceServer is the server API for KnowledgeService service.
// All implementations must embed UnimplementedKnowledgeServiceServer
// for forward compatibility
type KnowledgeServiceServer interface {
	ListKnowledge(context.Context, *ListKnowledgeRequest) (*ListKnowledgeResponse, error)
	ReadKnowledge(context.Context, *IdRequest) (*ReadKnowledgeResponse, error)
	mustEmbedUnimplementedKnowledgeServiceServer()
}

// UnimplementedKnowledgeServiceServer must be embedded to have forward compatible implementations.
type UnimplementedKnowledgeServiceServer struct {
}

func (UnimplementedKnowledgeServiceServer) ListKnowledge(context.Context, *ListKnowledgeRequest) (*ListKnowledgeResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListKnowledge not implemented")
}
func (UnimplementedKnowledgeServiceServer) ReadKnowledge(context.Context, *IdRequest) (*ReadKnowledgeResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ReadKnowledge not implemented")
}
func (UnimplementedKnowledgeServiceServer) mustEmbedUnimplementedKnowledgeServiceServer() {}

// UnsafeKnowledgeServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to KnowledgeServiceServer will
// result in compilation errors.
type UnsafeKnowledgeServiceServer interface {
	mustEmbedUnimplementedKnowledgeServiceServer()
}

func RegisterKnowledgeServiceServer(s grpc.ServiceRegistrar, srv KnowledgeServiceServer) {
	s.RegisterService(&_KnowledgeService_serviceDesc, srv)
}

func _KnowledgeService_ListKnowledge_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListKnowledgeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(KnowledgeServiceServer).ListKnowledge(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.mmm.submission.KnowledgeService/ListKnowledge",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(KnowledgeServiceServer).ListKnowledge(ctx, req.(*ListKnowledgeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _KnowledgeService_ReadKnowledge_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(IdRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(KnowledgeServiceServer).ReadKnowledge(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/maincon.mmm.submission.KnowledgeService/ReadKnowledge",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(KnowledgeServiceServer).ReadKnowledge(ctx, req.(*IdRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _KnowledgeService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "maincon.mmm.submission.KnowledgeService",
	HandlerType: (*KnowledgeServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "ListKnowledge",
			Handler:    _KnowledgeService_ListKnowledge_Handler,
		},
		{
			MethodName: "ReadKnowledge",
			Handler:    _KnowledgeService_ReadKnowledge_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "maincon/mmm/submission/knowledge.proto",
}
