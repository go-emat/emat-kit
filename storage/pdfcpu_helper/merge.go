package pdfcpu_helper

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"gitlab.com/go-emat/emat-kit/storage/pdf_merge"
	pdfcpuApi "gitlab.com/go-emat/pdfcpu-mattex/pkg/api"
	"gitlab.com/go-emat/pdfcpu-mattex/pkg/pdfcpu"
)

func MergePdfToCtx(ctx *pdfcpu.Context, pdfCtx *pdfcpu.Context, pdfFileName string) error {

	ctx.EnsureVersionForWriting()

	if err := pdfcpu.MergeXRefTables(pdfCtx, ctx); err != nil {
		return pdf_merge.NewPdfFileMergeError(pdfFileName, err)
	}

	return nil
}

func getFileSize(file io.ReadSeeker) (int64, error) {
	// 获取当前读取位置
	currentPos, err := file.Seek(0, io.SeekCurrent)
	if err != nil {
		return 0, err
	}
	defer file.Seek(currentPos, io.SeekStart) // 恢复读取位置

	// 将读取位置移动到文件末尾
	_, err = file.Seek(0, io.SeekEnd)
	if err != nil {
		return 0, err
	}

	// 获取文件大小
	fileSize, err := file.Seek(0, io.SeekCurrent)
	if err != nil {
		return 0, err
	}

	return fileSize, nil
}

func convertToReadSeeker(reader *bufio.Reader) (io.ReadSeeker, error) {
	// 创建一个空的bytes.Buffer
	buffer := bytes.NewBuffer(nil)

	// 将bufio.Reader的数据写入bytes.Buffer
	written, err := io.Copy(buffer, reader)
	if err != nil {
		log.Println("error converting bufio.Reader to bytes.Buffer", err)
		return nil, err
	}

	log.Printf("Bytes written to buffer: %d", written)
	if written == 0 {
		log.Println("No data was copied; the reader might be empty or exhausted")
	}

	// 将bytes.Buffer转换为io.ReadSeeker
	readSeeker := bytes.NewReader(buffer.Bytes())

	return readSeeker, nil
}

func ReadAndValidateInputSource(fileName string, rs io.ReadSeeker, outputDir string) (pdfCtx *pdfcpu.Context, err error) {
	conf := pdfcpu.NewDefaultConfiguration()
	conf.Cmd = pdfcpu.MERGECREATE
	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("failed to validate context with panic: %s", fileName)
		}
	}()

	if outputDir == "" {
		// 获取文件大小
		fileSize, err := getFileSize(rs)
		if err != nil {
			return nil, pdf_merge.NewPdfFileValidationError(fileName, err)
		}

		// 创建限制文件范围的 io.ReadSeeker
		limitReader := &io.LimitedReader{
			R: rs,
			N: fileSize,
		}

		// 创建带有缓冲区的读取器
		reader, err := convertToReadSeeker(bufio.NewReader(limitReader))
		//log.Println("ReadAndValidateInputSource reader", reader)
		if err != nil {
			return nil, pdf_merge.NewPdfFileValidationError(fileName, err)
		}
		//读取和验证文件
		pdfCtx, err = pdfcpuApi.ReadContext(reader, conf)
		if err != nil {
			log.Println("pdfcpuApi ReadContext err", err)
			return nil, err
		}

		if conf.ValidationMode != pdfcpu.ValidationNone {
			if err = pdfcpuApi.ValidateContext(pdfCtx); err != nil {
				log.Println("pdfcpuApi ValidateContext err", err)
				err = pdf_merge.NewPdfFileValidationError(fileName, err)
				return nil, err
			}
		}

		return pdfCtx, nil
	} else {
		if err := MakeDir(outputDir); err != nil {
			return nil, err
		}
		defer RemoveDir(outputDir)
		writeFile := outputDir + "/" + fileName
		outputFile, err := os.Create(writeFile)
		if err != nil {
			// 处理错误
			fmt.Println("Error creating output file:", err)
			return nil, err
		}
		defer outputFile.Close()
		command := fmt.Sprintf(`gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -o NUL "%s"`, writeFile)
		defer RemoveDir("./NUL")
		cmd := exec.Command("sh", "-c", command)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		err = cmd.Run()
		if err != nil {
			return nil, err
		}
	}
	return pdfcpuApi.ReadContext(rs, conf)
}

func ReadInputSource(fileName string, rs io.ReadSeeker) (pdfCtx *pdfcpu.Context, err error) {
	conf := pdfcpu.NewDefaultConfiguration()
	conf.Cmd = pdfcpu.MERGECREATE

	if pdfCtx, err = pdfcpuApi.ReadContext(rs, conf); err != nil {
		log.Println("pdfcpuApi ReadContext err", err)
		err = pdf_merge.NewPdfFileReadError(fileName, err)
		return
	}

	return
}

func PdfInfo(rs io.ReadSeeker) (int, error) {
	conf := pdfcpu.NewDefaultConfiguration()
	info, err := pdfcpuApi.Info(rs, nil, conf)
	if err != nil {
		return 0, err
	}
	if len(info) > 0 {
		count, err := strconv.Atoi(strings.TrimSpace(strings.Split(info[1], ":")[1]))
		if err != nil {
			return 0, err
		}
		return count, nil
	}
	return 0, nil
}

//func ValidateAndWriteContext(ctxDest *pdfcpu.Context, w io.Writer) (err error) {
//	conf := pdfcpu.NewDefaultConfiguration()
//	conf.Cmd = pdfcpu.MERGECREATE
//
//	var wg sync.WaitGroup
//	errChan := make(chan error, 2)
//
//	// 并发优化和验证
//	wg.Add(2)
//	go func() {
//		defer wg.Done()
//
//		defer func() {
//			if r := recover(); r != nil {
//				errChan <- fmt.Errorf("failed to optimize context")
//			}
//		}()
//
//		// 优化上下文
//		if err := pdfcpuApi.OptimizeContext(ctxDest); err != nil {
//			errChan <- fmt.Errorf("failed to optimize context: %w", err)
//		}
//	}()
//
//	go func() {
//		defer wg.Done()
//
//		defer func() {
//			if r := recover(); r != nil {
//				errChan <- fmt.Errorf("failed to validate context")
//			}
//		}()
//
//		// 验证上下文
//		if conf.ValidationMode != pdfcpu.ValidationNone {
//			if validateErr := pdfcpuApi.ValidateContext(ctxDest); validateErr != nil {
//				errChan <- fmt.Errorf("failed to validate context: %w", validateErr)
//			}
//		}
//	}()
//
//	// 等待并发任务完成或出现错误
//	go func() {
//		wg.Wait()
//		close(errChan)
//	}()
//
//	// 处理并发任务的错误
//	for err := range errChan {
//		if err != nil {
//			return err
//		}
//	}
//
//	// 写入上下文
//	if err := pdfcpuApi.WriteContext(ctxDest, w); err != nil {
//		return fmt.Errorf("failed to write context: %w", err)
//	}
//
//	return nil
//}

func ValidateAndWriteContext(ctxDest *pdfcpu.Context, w io.Writer) (err error) {
	conf := pdfcpu.NewDefaultConfiguration()
	conf.Cmd = pdfcpu.MERGECREATE

	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("failed to validate context")
		}
	}()

	if err = pdfcpuApi.OptimizeContext(ctxDest); err != nil {
		return
	}

	if conf.ValidationMode != pdfcpu.ValidationNone {
		if err = pdfcpuApi.ValidateContext(ctxDest); err != nil {
			return
		}
	}

	if err = pdfcpuApi.WriteContext(ctxDest, w); err != nil {
		return
	}

	return
}
