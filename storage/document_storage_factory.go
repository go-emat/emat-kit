package storage

import (
	"errors"
	"io"
	"log"
	"net/url"
	"os"
	"strings"
	"time"

	"gitlab.com/go-emat/emat-kit/storage/common"

	"github.com/360EntSecGroup-Skylar/excelize/v2"
	"github.com/SebastiaanKlippert/go-wkhtmltopdf"
)

const (
	Aliyun = "aliyun"
	Google = "google"
	Azure  = "azure"
	Local  = "local"
	//StorageTypeAliyun Storage Type aliyun for PDF
	StorageTypeAliyun = "AliyunStorage"
	//StorageTypeGoogle Storage Type Google for PDF
	StorageTypeGoogle = "GoogleStorage"
	//StorageTypeGoogle Storage Type Azure for PDF
	StorageTypeAzure = "AzureStorage"
	//StorageTypeLocal Storage Type Local for PDF
	StorageTypeLocal = "LocalStorage"
)

type DocumentStorageService interface {
	SavePdfToStorage(pdfg *wkhtmltopdf.PDFGenerator, documentType, fileName string) (*GeneratePdfToStorageResponse, error)
	BackupStoragePdf(dbPdfPath string) (bool, error)
	SaveFileToStorage(req *UploadFileRequest) (*UploadFileResponse, error)
	SaveExcelToStorage(filename string, xlsx *excelize.File, extension string) (string, error)
	MoveStorageFile(srcPath, destPath string) error
	DeleteStorageFile(filePath string) error
	DownloadFile(filePath string) ([]byte, error)
	FileExists(path string) bool
	GetFileMeta(filePath string) (*common.FileMeta, error)

	ZipStorageDocument(zipFilePath, zipFolder string, files []string) (string, error)
	ZipContentToStorageDocument(zipContent *common.ZipContent) ([]byte, error)
	GenerateSignedDownloadURL(fileName string, option ...common.SignedUrlOption) (string, error)
	GenerateResumableSessionURL(req *SessionURLRequest) (string, error)
	CompleteSessionUrl(filepath, uploadID string) error

	GetFileReadSeekCloser(filePath string) (io.ReadSeekCloser, error)
	GetFileWriteCloser(filePath, contentType string) (io.WriteCloser, error)

	// below function definition are for file uploading inside GRPC
	// GetFileWriter, WriteFile, CloseFileWriter should be used together
	// please check the usage in reviewer portal
	GetReader(filePath string) (reader io.ReadCloser, err error)
	GetWriter(filePath string, contentType string) (writer interface{}, err error)
	GetFileWriter(filePath string) (savedPath string, writer interface{}, err error)
	WriteFile(writer interface{}, buffer []byte, index int64) (err error)
	CloseFileWriter(writer interface{}) error
}

type DocumentStorageFactory struct {
	StorageBucketName  string
	StorageType        string
	LocalAbsPrefixPath string
	ConfigFile         string
}

func GetDefaultDocumentStorageFactory() *DocumentStorageFactory {
	return newDefaultDocumentStorageFactory()
}

func VerifyCloudStorage() bool {
	return os.Getenv("STORAGE") != "" && os.Getenv("STORAGE") != "local"
}

func GetStorageBucket() (storageType, bucketName, configFile string) {
	if VerifyCloudStorage() {
		if os.Getenv("STORAGE") == Aliyun {
			return StorageTypeAliyun, "", os.Getenv("STORAGE_APPLICATION_CREDENTIALS")
		}
		bucketName = os.Getenv("GOOGLE_STORAGE_BUCKET_NAME")
		if bucketName != "" {
			return StorageTypeGoogle, bucketName, os.Getenv("GOOGLE_APPLICATION_CREDENTIALS")
		}

		if os.Getenv("AZURE_STORAGE_BUCKET_NAME") != "" {
			return StorageTypeAzure, os.Getenv("AZURE_STORAGE_BUCKET_NAME"), os.Getenv("STORAGE_APPLICATION_CREDENTIALS")
		}
	}
	return StorageTypeLocal, "", ""
}

func newDefaultDocumentStorageFactory() *DocumentStorageFactory {
	storageType, bucketName, configFile := GetStorageBucket()
	storageFactory := &DocumentStorageFactory{
		StorageType:       storageType,
		StorageBucketName: bucketName,
		ConfigFile:        configFile,
	}
	log.Printf("using StorageFactory: %v", storageFactory)
	return storageFactory
}

func GetDefaultStorageService() (DocumentStorageService, error) {
	storageFactory := newDefaultDocumentStorageFactory()
	switch storageFactory.StorageType {
	case StorageTypeAliyun:
		return NewDocumentAliyunStorage(storageFactory.StorageBucketName, storageFactory.ConfigFile), nil
	case StorageTypeGoogle:
		return NewDocumentGoogleStorage(storageFactory.StorageBucketName), nil
	case StorageTypeAzure:
		return nil, errors.New("no implement azure storage")
	default:
		return NewDocumentLocalStorage(storageFactory.LocalAbsPrefixPath), nil
	}
}

func GetDefaultStorageServiceNoAbsPrefix() (DocumentStorageService, error) {
	storageFactory := newDefaultDocumentStorageFactory()
	switch storageFactory.StorageType {
	case StorageTypeGoogle:
		return NewDocumentGoogleStorage(storageFactory.StorageBucketName), nil
	case StorageTypeAzure:
		return nil, errors.New("no implement azure storage")
	default:
		return NewDocumentLocalStorage(SkipGrpcStructure), nil
	}
}

// SaveFileToStorage backup pdf process
func (s *DocumentStorageFactory) SaveFileToStorage(req *UploadFileRequest) (*UploadFileResponse, error) {
	documentStorageService, err := s.GetDocumentStorageService()
	if err != nil {
		return nil, err
	}
	return documentStorageService.SaveFileToStorage(req)
}

func (s *DocumentStorageFactory) SaveExcelToStorage(filename string, xlsx *excelize.File, extension string) (string, error) {
	documentStorageService, err := s.GetDocumentStorageService()
	if err != nil {
		return "", err
	}
	return documentStorageService.SaveExcelToStorage(filename, xlsx, extension)
}

func (s *DocumentStorageFactory) MoveStorageFile(srcPath, destPath string) error {
	documentStorageService, err := s.GetDocumentStorageService()
	if err != nil {
		return err
	}
	return documentStorageService.MoveStorageFile(srcPath, destPath)
}

func (s *DocumentStorageFactory) DeleteStorageFile(filePath string) error {
	documentStorageService, err := s.GetDocumentStorageService()
	if err != nil {
		return err
	}
	return documentStorageService.DeleteStorageFile(filePath)
}

func (s *DocumentStorageFactory) FileExists(filePath string) bool {
	documentStorageService, err := s.GetDocumentStorageService()
	if err != nil {
		return false
	}
	return documentStorageService.FileExists(filePath)
}

// SavePFDFileToStorageByWkhtml save pdf to storage (local or google) BY WKHTML object
func (s *DocumentStorageFactory) GeneratePdfFileToStorage(req *GeneratePdfToStorageRequest) (*GeneratePdfToStorageResponse, error) {
	documentStorageService, err := s.GetDocumentStorageService()
	if err != nil {
		return nil, err
	}
	if req.PdfOptions == nil {
		return nil, errors.New("please input pdf options")
	}
	pdfg, err := s.NewPdfGenerator(req.TemplateString, req.PdfOptions)
	if err != nil {
		return nil, err
	}
	return documentStorageService.SavePdfToStorage(pdfg, req.DocumentType, req.FileName)
}

func (s *DocumentStorageFactory) GeneratePdfFile(req *GeneratePdfToStorageRequest) (*GeneratePdfToStorageResponse, error) {
	documentStorageService, err := s.GetDocumentStorageService()
	if err != nil {
		return nil, err
	}
	if req.PdfOptions == nil {
		return nil, errors.New("please input pdf options")
	}
	pdfg, err := s.NewPdfGenerator(req.TemplateString, req.PdfOptions)
	if err != nil {
		return nil, err
	}
	return documentStorageService.SavePdfToStorage(pdfg, req.DocumentType, req.FileName)
}

// BackupPdfFile backup pdf process
func (s *DocumentStorageFactory) BackupStoragePdf(req *BackupStoragePdfRequest) (bool, error) {
	documentStorageService, err := s.GetDocumentStorageService()
	if err != nil {
		return false, err
	}
	return documentStorageService.BackupStoragePdf(req.DbPdfPath)
}

func (s *DocumentStorageFactory) ZipFiles(zipFilePath, zipFolder string, files []string) (string, error) {

	documentStorageService, err := s.GetDocumentStorageService()
	if err != nil {
		return "", err
	}
	return documentStorageService.ZipStorageDocument(zipFilePath, zipFolder, files)
}

func (s *DocumentStorageFactory) ZipContentToStorageDocument(zipContent *common.ZipContent) ([]byte, error) {

	documentStorageService, err := s.GetDocumentStorageService()
	if err != nil {
		return nil, err
	}
	return documentStorageService.ZipContentToStorageDocument(zipContent)
}

func (s *DocumentStorageFactory) GenerateSignedDownloadURL(fileName string, option ...common.SignedUrlOption) (string, error) {
	documentStorageService, err := s.GetDocumentStorageService()
	if err != nil {
		return "", err
	}
	return documentStorageService.GenerateSignedDownloadURL(fileName, option...)
}

func (s *DocumentStorageFactory) GenerateResumableSessionURL(req *SessionURLRequest) (string, error) {
	documentStorageService, err := s.GetDocumentStorageService()
	if err != nil {
		return "", err
	}
	return documentStorageService.GenerateResumableSessionURL(req)
}

func (s *DocumentStorageFactory) CompleteSessionUrl(filepath, uploadID string) error {
	documentStorageService, err := s.GetDocumentStorageService()
	if err != nil {
		return err
	}
	return documentStorageService.CompleteSessionUrl(filepath, uploadID)
}

func (s *DocumentStorageFactory) GenerateUploadResURL(fileName string) (string, error) {
	documentStorageService, err := s.GetDocumentStorageService()
	if err != nil {
		return "", err
	}
	return documentStorageService.GenerateSignedDownloadURL(fileName)
}

func (s *DocumentStorageFactory) DownloadStorageFile(filePath string) ([]byte, error) {
	documentStorageService, err := s.GetDocumentStorageService()
	if err != nil {
		return nil, err
	}
	return documentStorageService.DownloadFile(filePath)
}

func (s *DocumentStorageFactory) GetFileReadSeekCloser(filePath string) (io.ReadSeekCloser, error) {
	documentStorageService, err := s.GetDocumentStorageService()
	if err != nil {
		return nil, err
	}
	return documentStorageService.GetFileReadSeekCloser(filePath)
}

func (s *DocumentStorageFactory) GetWriteCloser(filePath, contentType string) (io.WriteCloser, error) {
	documentStorageService, err := s.GetDocumentStorageService()
	if err != nil {
		return nil, err
	}
	return documentStorageService.GetFileWriteCloser(filePath, contentType)
}

func (s *DocumentStorageFactory) GetReader(filePath string) (reader io.ReadCloser, err error) {
	documentStorageService, err := s.GetDocumentStorageService()
	if err != nil {
		return nil, err
	}

	return documentStorageService.GetReader(filePath)
}

// PdfDocumentGenerator pdf generator by template and footer option
func (s *DocumentStorageFactory) NewPdfGenerator(templateString string, pdfOptions *PdfOptions) (*wkhtmltopdf.PDFGenerator, error) {
	// Create new PDF generator
	pdfg, err := wkhtmltopdf.NewPDFGenerator()
	if err != nil {
		return nil, err
	}
	pdfg.Dpi.Set(300)
	pdfg.NoCollate.Set(false)
	if pdfOptions.TocInclude {
		pdfg.TOC.Include = true
		if pdfOptions.TocHeaderText != "" {
			pdfg.TOC.TocHeaderText.Set(pdfOptions.TocHeaderText)
		}
	}
	if pdfOptions.PageLandScape {
		pdfg.Orientation.Set(wkhtmltopdf.OrientationLandscape)
	}
	// pdfg.MarginBottom.Set(10)
	// pdfg.AddPage(wkhtmltopdf.NewPage(""))
	pageReader := wkhtmltopdf.NewPageReader(strings.NewReader(templateString))
	if pdfOptions.PageHeaderHTML != "" {
		pageReader.PageOptions.HeaderHTML.Set(pdfOptions.PageHeaderHTML)
	}
	if pdfOptions.PageHeaderSpacing > 0 {
		pageReader.PageOptions.HeaderSpacing.Set(pdfOptions.PageHeaderSpacing)
	}
	for name, value := range pdfOptions.PageHeaderData {
		pageReader.PageOptions.Replace.Set(name, url.QueryEscape(value.(string)))
	}
	if pdfOptions.PageHeaderVoid {
		pageReader.PageOptions.Replace.Set("querystrvoiddate", pdfOptions.PageHeaderVoidDateText)
	}
	pageReader.PageOptions.FooterHTML.Set(pdfOptions.PageFooterHTML)
	pageReader.FooterFontSize.Set(8)
	pageReader.PageOptions.EnableLocalFileAccess.Set(true)

	if !pdfOptions.HiddenFooterRight {
		pageReader.PageOptions.FooterRight.Set("Page [page] of [toPage]")
	}

	if pdfOptions.PageFooterRightTemplate != "" {
		pageReader.PageOptions.FooterRight.Set(pdfOptions.PageFooterRightTemplate)
	}

	if pdfOptions.ShowFooterCenter {
		pageReader.PageOptions.FooterCenter.Set("Page [page] of [toPage]")
	}

	// pageReader.PageOptions.FooterLeft.Set("document version")
	if pdfOptions.PageWidth > 0 && pdfOptions.PageHeight > 0 {
		pdfg.PageWidth.Set(pdfOptions.PageWidth)
		pdfg.PageHeight.Set(pdfOptions.PageHeight)
	} else {
		if pdfOptions.PageSize != "" {
			pdfg.PageSize.Set(pdfOptions.PageSize)
		} else {
			pdfg.PageSize.Set(wkhtmltopdf.PageSizeA4)
		}
	}
	pdfg.MarginTop.Set(10)
	pdfg.MarginBottom.Set(20)
	// pageReader.FooterLeft.Set()

	if pdfOptions.PageMarginTop > 0 {
		pdfg.MarginTop.Set(uint(pdfOptions.PageMarginTop))
	}
	if pdfOptions.PageMarginRight > 0 {
		pdfg.MarginRight.Set(uint(pdfOptions.PageMarginRight))
	}
	if pdfOptions.PageMarginBottom > 0 {
		pdfg.MarginBottom.Set(uint(pdfOptions.PageMarginBottom))
	}
	if pdfOptions.PageMarginLeft > 0 {
		pdfg.MarginLeft.Set(uint(pdfOptions.PageMarginLeft))
	}

	// Create PDF document in internal buffer
	pdfg.AddPage(pageReader)
	err = pdfg.Create()
	if err != nil {
		return nil, err
	}
	return pdfg, nil
}

// getDocumentPdfStoragePath get document pdf storage path by document type
func getDocumentPdfStoragePath(documentType string) string {
	return documentType + "/" + FormatHKDateTime(time.Now(), "200601") + "/"
}

func (s *DocumentStorageFactory) GetDocumentStorageService() (DocumentStorageService, error) {
	switch s.StorageType {
	case StorageTypeAliyun:
		return NewDocumentAliyunStorage(s.StorageBucketName, s.ConfigFile), nil
	case StorageTypeGoogle:
		return NewDocumentGoogleStorage(s.StorageBucketName), nil
	case StorageTypeAzure:
		return nil, errors.New("no implement azure storage")
	default:
		return NewDocumentLocalStorage(s.LocalAbsPrefixPath), nil
	}
}

// FormatHKDateTime Format HK Datetime
func FormatHKDateTime(dateTime time.Time, format string) (dateString string) {
	if dateTime.IsZero() {
		return ""
	}
	location, _ := time.LoadLocation("Asia/Hong_Kong")
	dateString = dateTime.In(location).Format(format)
	if dateString == "01/01/0001" {
		dateString = ""
	}
	return
}

func (s *DocumentStorageFactory) GetFileMeta(filePath string) (fileMeta *common.FileMeta, err error) {
	documentStorageService, err := s.GetDocumentStorageService()
	if err != nil {
		return nil, err
	}

	return documentStorageService.GetFileMeta(filePath)
}